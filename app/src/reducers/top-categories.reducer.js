import { topCategoriesAction } from "../actions/top-categories.action";

const initialState = {
  topCategories: [],
  loading: false,
  loadFailed: false
};

export const topCategoriesReducer = (state: {} = initialState, action: {}) => {
  switch (action.type) {
    case topCategoriesAction.TOP_CATEGORIES_LOADED:
      return {
        ...state,
        topCategories: action.payload.topCategories,
        loading: false,
        loadFailed: false
      };
    case topCategoriesAction.TOP_CATEGORIES_REQUESTED:
      return { ...state, loading: true, loadFailed: false };
    case topCategoriesAction.TOP_CATEGORIES_REQUEST_FAILED:
      return { ...state, loading: false, loadFailed: true };
    default:
      return state;
  }
};
