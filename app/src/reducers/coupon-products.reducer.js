import lodash from "lodash";
import { couponProductsAction } from "../actions/coupon-products.action";

const initialState = {
  couponProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function couponProductsReducer(state = initialState, action) {
  const { couponProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case couponProductsAction.COUPON_PRODUCTS_REQUESTED:
      return {
        ...state,
        page: 1,
        couponProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      };
    case couponProductsAction.COUPON_PRODUCTS_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case couponProductsAction.COUPON_PRODUCTS_FULFILLED:
      return {
        ...state,
        couponProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case couponProductsAction.COUPON_PRODUCTS_APPENDED:
      return {
        ...state,
        couponProducts: [...state.couponProducts, ...couponProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case couponProductsAction.COUPON_PRODUCTS_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case couponProductsAction.COUPON_PRODUCTS_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case couponProductsAction.COUPON_PRODUCTS_REJECTED:
      return {
        ...state,
        couponProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };
    default:
      return state;
  }
}
