import lodash from "lodash";
import { share2EarnProductsAction } from "../actions/share2earn-products.action";

const initialState = {
  share2EarnProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function share2EarnProductsReducer(state = initialState, action) {
  const { share2EarnProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case share2EarnProductsAction.SHARE2EARN_PRODUCTS_REQUESTED:
      return {
        ...state,
        page: 1,
        share2EarnProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      };
    case share2EarnProductsAction.SHARE2EARN_PRODUCTS_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case share2EarnProductsAction.SHARE2EARN_PRODUCTS_FULFILLED:
      return {
        ...state,
        share2EarnProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case share2EarnProductsAction.SHARE2EARN_PRODUCTS_APPENDED:
      return {
        ...state,
        share2EarnProducts: [...state.share2EarnProducts, ...share2EarnProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case share2EarnProductsAction.SHARE2EARN_PRODUCTS_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case share2EarnProductsAction.SHARE2EARN_PRODUCTS_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case share2EarnProductsAction.SHARE2EARN_PRODUCTS_REJECTED:
      return {
        ...state,
        share2EarnProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };
    default:
      return state;
  }
}
