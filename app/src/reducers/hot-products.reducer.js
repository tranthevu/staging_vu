import { hotProductsAction } from "../actions/hot-products.action";
import lodash from "lodash";

const initialState = {
  hotProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function hotProductsReducer(state = initialState, action) {
  const { hotProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case hotProductsAction.HOT_PRODUCTS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        hotProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false,
        refreshing: false
      });
    case hotProductsAction.HOT_PRODUCTS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case hotProductsAction.HOT_PRODUCTS_FULFILLED:
      return Object.assign({}, state, {
        hotProducts: hotProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case hotProductsAction.HOT_PRODUCTS_APPENDED:
      return Object.assign({}, state, {
        hotProducts: [...state.hotProducts, ...hotProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case hotProductsAction.HOT_PRODUCTS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case hotProductsAction.HOT_PRODUCTS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case hotProductsAction.HOT_PRODUCTS_REJECTED:
      return Object.assign({}, state, {
        hotProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    default:
      return state;
  }
}
