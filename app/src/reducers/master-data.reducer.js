import { masterDataAction } from "../actions/master-data.action";

const initialState = {
  masterData: {},
  loading: false,
  loadFailed: false
};

export const masterDataReducer = (state: {} = initialState, action: {}) => {
  switch (action.type) {
    case masterDataAction.MASTER_DATA_LOADED:
      return {
        ...state,
        masterData: action.payload.masterData,
        loading: false,
        loadFailed: false
      };
    case masterDataAction.MASTER_DATA_REQUESTED:
      return { ...state, loading: true, loadFailed: false };
    case masterDataAction.MASTER_DATA_REQUEST_FAILED:
      return { ...state, loading: false, loadFailed: true };
    default:
      return state;
  }
};
