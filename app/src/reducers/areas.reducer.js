import { areasAction } from "../actions/areas.action";
import lodash from "lodash";

const initialState = {
  areas: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function areasReducer(state = initialState, action) {
  const { areas, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case areasAction.AREAS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        areas: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      });
    case areasAction.AREAS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case areasAction.AREAS_FULFILLED:
      return Object.assign({}, state, {
        areas: areas,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case areasAction.AREAS_APPENDED:
      return Object.assign({}, state, {
        areas: [...state.areas, ...areas],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case areasAction.AREAS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case areasAction.AREAS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case areasAction.AREAS_REJECTED:
      return Object.assign({}, state, {
        areas: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    default:
      return state;
  }
}
