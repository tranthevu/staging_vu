import lodash from "lodash";
import { merchantProductsAction } from "../actions/merchant-products.action";

const initialState = {
  merchantProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function merchantProductsReducer(state = initialState, action) {
  const { merchantProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case merchantProductsAction.MERCHANT_PRODUCTS_REQUESTED:
      return {
        ...state,
        page: 1,
        merchantProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      };
    case merchantProductsAction.MERCHANT_PRODUCTS_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case merchantProductsAction.MERCHANT_PRODUCTS_FULFILLED:
      return {
        ...state,
        merchantProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case merchantProductsAction.MERCHANT_PRODUCTS_APPENDED:
      return {
        ...state,
        merchantProducts: [...state.merchantProducts, ...merchantProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case merchantProductsAction.MERCHANT_PRODUCTS_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case merchantProductsAction.MERCHANT_PRODUCTS_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case merchantProductsAction.MERCHANT_PRODUCTS_REJECTED:
      return {
        ...state,
        merchantProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };
    default:
      return state;
  }
}
