import lodash from "lodash";
import { followMerchantsAction } from "../actions/follow-merchants.action";

const initialState = {
  followMerchants: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function followMerchantsReducer(state = initialState, action) {
  const { followMerchants, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case followMerchantsAction.FOLLOW_MERCHANTS_REQUESTED:
      return {
        ...state,
        page: 1,
        followMerchants: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      };
    case followMerchantsAction.FOLLOW_MERCHANTS_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case followMerchantsAction.FOLLOW_MERCHANTS_FULFILLED:
      return {
        ...state,
        followMerchants,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case followMerchantsAction.FOLLOW_MERCHANTS_APPENDED:
      return {
        ...state,
        followMerchants: [...state.followMerchants, ...followMerchants],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case followMerchantsAction.FOLLOW_MERCHANTS_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case followMerchantsAction.FOLLOW_MERCHANTS_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case followMerchantsAction.FOLLOW_MERCHANTS_REJECTED:
      return {
        ...state,
        followMerchants: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };
    default:
      return state;
  }
}
