import { usedVouchersAction } from "../actions/used-vouchers.action";
import lodash from "lodash";
import { voucherAction } from "../actions/vouchers.action";

const initialState = {
  usedVouchers: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function usedVouchersReducer(state = initialState, action) {
  const { usedVouchers, page, voucher } = lodash.get(action, "payload", {});
  switch (action.type) {
    case usedVouchersAction.USED_VOUCHERS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        usedVouchers: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      });
    case usedVouchersAction.USED_VOUCHERS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case usedVouchersAction.USED_VOUCHERS_FULFILLED:
      return Object.assign({}, state, {
        usedVouchers: usedVouchers,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case usedVouchersAction.USED_VOUCHERS_APPENDED:
      return Object.assign({}, state, {
        usedVouchers: [...state.usedVouchers, ...usedVouchers],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case usedVouchersAction.USED_VOUCHERS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case usedVouchersAction.USED_VOUCHERS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case usedVouchersAction.USED_VOUCHERS_REJECTED:
      return Object.assign({}, state, {
        usedVouchers: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    case voucherAction.VOUCHER_REMOVED:
      return Object.assign({}, state, {
        usedVouchers: state.usedVouchers.filter(
          item => item.product.id !== voucher.product.id
        )
      });
    default:
      return state;
  }
}
