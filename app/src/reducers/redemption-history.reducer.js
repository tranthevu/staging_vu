import lodash from "lodash";
import { redemptionHistoryAction } from "../actions/redemption-history.action";

const initialState = {
  redemptionHistory: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function redemptionHistoryReducer(state = initialState, action) {
  const { redemptionHistory, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case redemptionHistoryAction.REDEMPTION_HISTORY_REQUESTED:
      return {
        ...state,
        page: 1,
        redemptionHistory: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false,
        refreshing: false
      };
    case redemptionHistoryAction.REDEMPTION_HISTORY_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case redemptionHistoryAction.REDEMPTION_HISTORY_FULFILLED:
      return {
        ...state,
        redemptionHistory,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case redemptionHistoryAction.REDEMPTION_HISTORY_APPENDED:
      return {
        ...state,
        redemptionHistory: [...state.redemptionHistory, ...redemptionHistory],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case redemptionHistoryAction.REDEMPTION_HISTORY_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case redemptionHistoryAction.REDEMPTION_HISTORY_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case redemptionHistoryAction.REDEMPTION_HISTORY_REJECTED:
      return {
        ...state,
        redemptionHistory: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };
    default:
      return state;
  }
}
