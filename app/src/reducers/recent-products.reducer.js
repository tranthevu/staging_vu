import { recentProductsAction } from "../actions/recent-products.action";
import lodash from "lodash";

const initialState = {
  recentProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function recentProductsReducer(state = initialState, action) {
  const { recentProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case recentProductsAction.RECENT_PRODUCTS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        recentProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false,
        refreshing: false
      });
    case recentProductsAction.RECENT_PRODUCTS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case recentProductsAction.RECENT_PRODUCTS_FULFILLED:
      return Object.assign({}, state, {
        recentProducts: recentProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case recentProductsAction.RECENT_PRODUCTS_APPENDED:
      return Object.assign({}, state, {
        recentProducts: [...state.recentProducts, ...recentProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case recentProductsAction.RECENT_PRODUCTS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case recentProductsAction.RECENT_PRODUCTS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case recentProductsAction.RECENT_PRODUCTS_REJECTED:
      return Object.assign({}, state, {
        recentProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    default:
      return state;
  }
}
