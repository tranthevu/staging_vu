import { homeAreasAction } from "../actions/home-areas.action";

const initialState = {
  homeAreas: [],
  loading: false,
  count: 0,
  loadFailed: false
};

export const homeAreasReducer = (state: {} = initialState, action: {}) => {
  switch (action.type) {
    case homeAreasAction.HOME_AREAS_LOADED:
      return {
        ...state,
        homeAreas: action.payload.homeAreas,
        count: action.payload.count,
        loading: false,
        loadFailed: false
      };
    case homeAreasAction.HOME_AREAS_REQUESTED:
      return { ...state, loading: true, loadFailed: false };
    case homeAreasAction.HOME_AREAS_REQUEST_FAILED:
      return { ...state, loading: false, loadFailed: true };
    default:
      return state;
  }
};
