import lodash from "lodash";
import { onlineProductsAction } from "../actions/online-products.action";

const initialState = {
  onlineProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function onlineProductsReducer(state = initialState, action) {
  const { onlineProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case onlineProductsAction.ONLINE_PRODUCTS_REQUESTED:
      return {
        ...state,
        page: 1,
        onlineProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      };
    case onlineProductsAction.ONLINE_PRODUCTS_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case onlineProductsAction.ONLINE_PRODUCTS_FULFILLED:
      return {
        ...state,
        onlineProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case onlineProductsAction.ONLINE_PRODUCTS_APPENDED:
      return {
        ...state,
        onlineProducts: [...state.onlineProducts, ...onlineProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case onlineProductsAction.ONLINE_PRODUCTS_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case onlineProductsAction.ONLINE_PRODUCTS_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case onlineProductsAction.ONLINE_PRODUCTS_REJECTED:
      return {
        ...state,
        onlineProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };
    default:
      return state;
  }
}
