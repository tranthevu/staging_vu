import { locationProductsAction } from "../actions/location-products.action";
import lodash from "lodash";

const initialState = {
  locationProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function locationProductsReducer(state = initialState, action) {
  const { locationProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case locationProductsAction.LOCATION_PRODUCTS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        locationProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        refreshing: false,
        reachedEnd: false
      });
    case locationProductsAction.LOCATION_PRODUCTS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case locationProductsAction.LOCATION_PRODUCTS_FULFILLED:
      return Object.assign({}, state, {
        locationProducts: locationProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case locationProductsAction.LOCATION_PRODUCTS_APPENDED:
      return Object.assign({}, state, {
        locationProducts: [...state.locationProducts, ...locationProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case locationProductsAction.LOCATION_PRODUCTS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case locationProductsAction.LOCATION_PRODUCTS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case locationProductsAction.LOCATION_PRODUCTS_REJECTED:
      return Object.assign({}, state, {
        locationProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    default:
      return state;
  }
}
