import lodash from "lodash";
import { partnerProductsAction } from "../actions/partner-products.action";

const initialState = {
  partnerProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function partnerProductsReducer(state = initialState, action) {
  const { partnerProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case partnerProductsAction.PARTNER_PRODUCTS_REQUESTED:
      return {
        ...state,
        page: 1,
        partnerProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      };
    case partnerProductsAction.PARTNER_PRODUCTS_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case partnerProductsAction.PARTNER_PRODUCTS_FULFILLED:
      return {
        ...state,
        partnerProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case partnerProductsAction.PARTNER_PRODUCTS_APPENDED:
      return {
        ...state,
        partnerProducts: [...state.partnerProducts, ...partnerProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case partnerProductsAction.PARTNER_PRODUCTS_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case partnerProductsAction.PARTNER_PRODUCTS_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case partnerProductsAction.PARTNER_PRODUCTS_REJECTED:
      return {
        ...state,
        partnerProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };
    default:
      return state;
  }
}
