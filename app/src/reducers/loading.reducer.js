import { DIALOG_WAITING, STATE_API } from "../actions/actionTypes";

const initialState = {
  isLoading: false,
  isApiLoading: false
};

export default function(state = initialState, action) {
  const { type } = action;
  switch (type) {
    case DIALOG_WAITING.SHOW: {
      return {
        ...state,
        isLoading: true
      };
    }
    case DIALOG_WAITING.HIDE: {
      return {
        ...state,
        isLoading: false
      };
    }
    case STATE_API.IS_LOADING: {
      return {
        ...state,
        isFetching: true
      };
    }
    case STATE_API.NON_LOADING: {
      return {
        ...state,
        isFetching: false
      };
    }
    default:
      return state;
  }
}
