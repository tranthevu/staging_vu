import { expiringProductsAction } from "../actions/expiring-products.action";
import lodash from "lodash";

const initialState = {
  expiringProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function expiringProductsReducer(state = initialState, action) {
  const { expiringProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case expiringProductsAction.EXPIRING_PRODUCTS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        expiringProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false,
        refreshing: false
      });
    case expiringProductsAction.EXPIRING_PRODUCTS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case expiringProductsAction.EXPIRING_PRODUCTS_FULFILLED:
      return Object.assign({}, state, {
        expiringProducts: expiringProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case expiringProductsAction.EXPIRING_PRODUCTS_APPENDED:
      return Object.assign({}, state, {
        expiringProducts: [...state.expiringProducts, ...expiringProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case expiringProductsAction.EXPIRING_PRODUCTS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case expiringProductsAction.EXPIRING_PRODUCTS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case expiringProductsAction.EXPIRING_PRODUCTS_REJECTED:
      return Object.assign({}, state, {
        expiringProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    default:
      return state;
  }
}
