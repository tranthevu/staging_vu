import { exclusiveProductsAction } from "../actions/exclusive-products.action";
import lodash from "lodash";

const initialState = {
  exclusiveProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function exclusiveProductsReducer(state = initialState, action) {
  const { exclusiveProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case exclusiveProductsAction.EXCLUSIVE_PRODUCTS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        exclusiveProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      });
    case exclusiveProductsAction.EXCLUSIVE_PRODUCTS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case exclusiveProductsAction.EXCLUSIVE_PRODUCTS_FULFILLED:
      return Object.assign({}, state, {
        exclusiveProducts: exclusiveProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case exclusiveProductsAction.EXCLUSIVE_PRODUCTS_APPENDED:
      return Object.assign({}, state, {
        exclusiveProducts: [...state.exclusiveProducts, ...exclusiveProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case exclusiveProductsAction.EXCLUSIVE_PRODUCTS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case exclusiveProductsAction.EXCLUSIVE_PRODUCTS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case exclusiveProductsAction.EXCLUSIVE_PRODUCTS_REJECTED:
      return Object.assign({}, state, {
        exclusiveProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    default:
      return state;
  }
}
