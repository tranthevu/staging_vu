import { GET_TOP_MERCHANT_BY_ADDRESS_ASYNC } from "../actions/actionTypes";

const initialState = {
  listMerchantByAddress: {},
  listTopMerchantByAddress: []
};

export const merchantReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TOP_MERCHANT_BY_ADDRESS_ASYNC.SUCCESS: {
      return { ...state, listTopMerchantByAddress: action.payload };
    }
    case GET_TOP_MERCHANT_BY_ADDRESS_ASYNC.CLEAR: {
      return { ...state, listTopMerchantByAddress: [] };
    }
    default:
      return state;
  }
};
