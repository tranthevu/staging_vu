import { unreadNotificationsAction } from "../actions/unread-notifications.action";
import lodash from "lodash";

const initialState = {
  unreadNotifications: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function unreadNotificationsReducer(state = initialState, action) {
  const { unreadNotifications, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case unreadNotificationsAction.UNREAD_NOTIFICATIONS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        unreadNotifications: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      });
    case unreadNotificationsAction.UNREAD_NOTIFICATIONS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case unreadNotificationsAction.UNREAD_NOTIFICATIONS_FULFILLED:
      return Object.assign({}, state, {
        unreadNotifications: unreadNotifications,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case unreadNotificationsAction.UNREAD_NOTIFICATIONS_APPENDED:
      return Object.assign({}, state, {
        unreadNotifications: [...state.unreadNotifications, ...unreadNotifications],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case unreadNotificationsAction.UNREAD_NOTIFICATIONS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case unreadNotificationsAction.UNREAD_NOTIFICATIONS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case unreadNotificationsAction.UNREAD_NOTIFICATIONS_REJECTED:
      return Object.assign({}, state, {
        unreadNotifications: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    default:
      return state;
  }
}
