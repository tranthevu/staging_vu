import lodash from "lodash";
import { joinedCampaignsAction } from "../actions/joined-campaigns.action";

const initialState = {
  joinedCampaigns: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function joinedCampaignsReducer(state = initialState, action) {
  const { joinedCampaigns, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case joinedCampaignsAction.JOINED_CAMPAIGNS_REQUESTED:
      return {
        ...state,
        page: 1,
        joinedCampaigns: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      };
    case joinedCampaignsAction.JOINED_CAMPAIGNS_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case joinedCampaignsAction.JOINED_CAMPAIGNS_FULFILLED:
      return {
        ...state,
        joinedCampaigns,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case joinedCampaignsAction.JOINED_CAMPAIGNS_APPENDED:
      return {
        ...state,
        joinedCampaigns: [...state.joinedCampaigns, ...joinedCampaigns],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case joinedCampaignsAction.JOINED_CAMPAIGNS_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case joinedCampaignsAction.JOINED_CAMPAIGNS_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case joinedCampaignsAction.JOINED_CAMPAIGNS_REJECTED:
      return {
        ...state,
        joinedCampaigns: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };
    default:
      return state;
  }
}
