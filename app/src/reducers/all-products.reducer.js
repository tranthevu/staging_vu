import { allProductsAction } from "../actions/all-products.action";
import lodash from "lodash";

const initialState = {
  allProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function allProductsReducer(state = initialState, action) {
  const { allProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case allProductsAction.ALL_PRODUCTS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        allProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false,
        refreshing: false
      });
    case allProductsAction.ALL_PRODUCTS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case allProductsAction.ALL_PRODUCTS_FULFILLED:
      return Object.assign({}, state, {
        allProducts: allProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case allProductsAction.ALL_PRODUCTS_APPENDED:
      return Object.assign({}, state, {
        allProducts: [...state.allProducts, ...allProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case allProductsAction.ALL_PRODUCTS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case allProductsAction.ALL_PRODUCTS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case allProductsAction.ALL_PRODUCTS_REJECTED:
      return Object.assign({}, state, {
        allProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    default:
      return state;
  }
}
