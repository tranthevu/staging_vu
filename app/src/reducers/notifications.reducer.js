import lodash from "lodash";
import { notificationsAction } from "../actions/notifications.action";

const initialState = {
  notifications: [],
  count: {
    unreadcount: 0
  },
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function notificationsReducer(state = initialState, action) {
  const { notifications, page, count } = lodash.get(action, "payload", {});
  switch (action.type) {
    case notificationsAction.NOTIFICATIONS_REQUESTED:
      return {
        ...state,
        page: 1,
        notifications: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      };
    case notificationsAction.NOTIFICATIONS_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case notificationsAction.NOTIFICATIONS_FULFILLED:
      return {
        ...state,
        notifications,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case notificationsAction.NOTIFICATIONS_APPENDED:
      return {
        ...state,
        notifications: [...state.notifications, ...notifications],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case notificationsAction.NOTIFICATIONS_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case notificationsAction.NOTIFICATIONS_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case notificationsAction.NOTIFICATIONS_REJECTED:
      return {
        ...state,
        notifications: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };
    case notificationsAction.NOTIFICATIONS_COUNTED:
      return { ...state, count };
    case notificationsAction.NOTIFICATIONS_READ:
      return {
        ...state,
        notifications: state.notifications.map(item => {
          if (item.id === action.payload.id) return { ...item, notify_status: "read" };
          return item;
        })
      };
    default:
      return state;
  }
}
