import { savedOffersAction } from "../actions/saved-offers.action";
import lodash from "lodash";
import { voucherAction } from "../actions/vouchers.action";

const initialState = {
  savedOffers: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function savedOffersReducer(state = initialState, action) {
  const { savedOffers, page, voucher } = lodash.get(action, "payload", {});
  switch (action.type) {
    case savedOffersAction.SAVED_OFFERS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        savedOffers: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      });
    case savedOffersAction.SAVED_OFFERS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case savedOffersAction.SAVED_OFFERS_FULFILLED:
      return Object.assign({}, state, {
        savedOffers: savedOffers,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case savedOffersAction.SAVED_OFFERS_APPENDED:
      return Object.assign({}, state, {
        savedOffers: [...state.savedOffers, ...savedOffers],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case savedOffersAction.SAVED_OFFERS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case savedOffersAction.SAVED_OFFERS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case savedOffersAction.SAVED_OFFERS_REJECTED:
      return Object.assign({}, state, {
        savedOffers: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    case voucherAction.VOUCHER_REMOVED:
      return Object.assign({}, state, {
        savedOffers: state.savedOffers.filter(item => item.id !== voucher.id)
      });
    default:
      return state;
  }
}
