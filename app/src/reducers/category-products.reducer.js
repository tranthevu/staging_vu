import { categoryProductsAction } from "../actions/category-products.action";
import lodash from "lodash";

const initialState = {
  categoryProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function categoryProductsReducer(state = initialState, action) {
  const { categoryProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case categoryProductsAction.CATEGORY_PRODUCTS_RESETTED:
      return Object.assign({}, state, initialState);
    case categoryProductsAction.CATEGORY_PRODUCTS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        categoryProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        refreshing: false,
        reachedEnd: false
      });
    case categoryProductsAction.CATEGORY_PRODUCTS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case categoryProductsAction.CATEGORY_PRODUCTS_FULFILLED:
      return Object.assign({}, state, {
        categoryProducts: categoryProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case categoryProductsAction.CATEGORY_PRODUCTS_APPENDED:
      return Object.assign({}, state, {
        categoryProducts: [...state.categoryProducts, ...categoryProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case categoryProductsAction.CATEGORY_PRODUCTS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case categoryProductsAction.CATEGORY_PRODUCTS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case categoryProductsAction.CATEGORY_PRODUCTS_REJECTED:
      return Object.assign({}, state, {
        categoryProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    default:
      return state;
  }
}
