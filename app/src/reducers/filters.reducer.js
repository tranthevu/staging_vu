import { filtersAction } from "../actions/filters.action";

const initialState = {
  filters: {},
  loading: false,
  loadFailed: false
};

export const filtersReducer = (state: {} = initialState, action: {}) => {
  switch (action.type) {
    case filtersAction.FILTERS_LOADED:
      return {
        ...state,
        filters: action.payload.filters,
        loading: false,
        loadFailed: false
      };
    case filtersAction.FILTERS_REQUESTED:
      return { ...state, loading: true, loadFailed: false };
    case filtersAction.FILTERS_REQUEST_FAILED:
      return { ...state, loading: false, loadFailed: true };
    default:
      return state;
  }
};
