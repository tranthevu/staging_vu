import { popularKeywordsAction } from "../actions/popular-keywords.action";

const initialState = {
  popularKeywords: [],
  loading: false,
  loadFailed: false
};

export const popularKeywordsReducer = (
  state: {} = initialState,
  action: {}
) => {
  switch (action.type) {
    case popularKeywordsAction.POPULAR_KEYWORDS_LOADED:
      return {
        ...state,
        popularKeywords: action.payload.popularKeywords,
        loading: false,
        loadFailed: false
      };
    case popularKeywordsAction.POPULAR_KEYWORDS_REQUESTED:
      return { ...state, loading: true, loadFailed: false };
    case popularKeywordsAction.POPULAR_KEYWORDS_REQUEST_FAILED:
      return { ...state, loading: false, loadFailed: true };
    default:
      return state;
  }
};
