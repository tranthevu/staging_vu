import { bannersAction } from "../actions/banners.action";

const initialState = {
  banners: [],
  loading: false,
  loadFailed: false,
  bannerPromos: [],
  bannerEcoupon: [],
  bannerShareToEarnPromo: []
};

export const bannersReducer = (state = initialState, action) => {
  switch (action.type) {
    case bannersAction.BANNERS_LOADED:
      return {
        ...state,
        banners: action.payload.banners,
        loading: false,
        loadFailed: false
      };
    case bannersAction.BANNERS_PROMO_LOADED:
      return {
        ...state,
        bannerPromos: action.payload.bannerPromos,
        loading: false,
        loadFailed: false
      };
    case bannersAction.BANNERS_ECOUPON_LOADED:
      return {
        ...state,
        bannerEcoupon: action.payload.bannerEcoupon,
        loading: false,
        loadFailed: false
      };
    case bannersAction.BANNERS_SHARE_TO_EARN_PROMO.SUCCESS:
      return {
        ...state,
        bannerShareToEarnPromo: action.payload,
        loading: false,
        loadFailed: false
      };
    case bannersAction.BANNERS_REQUESTED:
      return { ...state, loading: true, loadFailed: false };
    case bannersAction.BANNERS_REQUEST_FAILED:
      return { ...state, loading: false, loadFailed: true };
    default:
      return state;
  }
};
