import lodash from "lodash";
import { searchProductsAction } from "../actions/search-products.action";
import { GET_PRODUCTS_NEAR_BY } from "../actions/actionTypes";

const initialState = {
  searchProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  count: 0,
  page: 1,
  productsNearBy: {}
};

export default function searchProductsReducer(state = initialState, action) {
  const { searchProducts, page, count } = lodash.get(action, "payload", {});
  switch (action.type) {
    case searchProductsAction.SEARCH_PRODUCTS_REQUESTED:
      return {
        ...state,
        page: 1,
        searchProducts: [],
        count: 0,
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      };
    case searchProductsAction.SEARCH_PRODUCTS_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case searchProductsAction.SEARCH_PRODUCTS_FULFILLED:
      return {
        ...state,
        searchProducts,
        count,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case searchProductsAction.SEARCH_PRODUCTS_APPENDED:
      return {
        ...state,
        searchProducts: [...state.searchProducts, ...searchProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case searchProductsAction.SEARCH_PRODUCTS_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case searchProductsAction.SEARCH_PRODUCTS_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case searchProductsAction.SEARCH_PRODUCTS_REJECTED:
      return {
        ...state,
        searchProducts: [],
        refreshing: false,
        count: 0,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };

    case GET_PRODUCTS_NEAR_BY.SUCCESS:
      return {
        ...state,
        productsNearBy: action.payload
      };
    case GET_PRODUCTS_NEAR_BY.CLEAR:
      return {
        ...state,
        productsNearBy: {}
      };

    default:
      return state;
  }
}
