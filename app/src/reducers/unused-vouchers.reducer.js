import { unusedVouchersAction } from "../actions/unused-vouchers.action";
import lodash from "lodash";
import { voucherAction } from "../actions/vouchers.action";

const initialState = {
  unusedVouchers: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function unusedVouchersReducer(state = initialState, action) {
  const { unusedVouchers, page, voucher } = lodash.get(action, "payload", {});
  switch (action.type) {
    case unusedVouchersAction.UNUSED_VOUCHERS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        unusedVouchers: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      });
    case unusedVouchersAction.UNUSED_VOUCHERS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case unusedVouchersAction.UNUSED_VOUCHERS_FULFILLED:
      return Object.assign({}, state, {
        unusedVouchers: unusedVouchers,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case unusedVouchersAction.UNUSED_VOUCHERS_APPENDED:
      return Object.assign({}, state, {
        unusedVouchers: [...state.unusedVouchers, ...unusedVouchers],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case unusedVouchersAction.UNUSED_VOUCHERS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case unusedVouchersAction.UNUSED_VOUCHERS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case unusedVouchersAction.UNUSED_VOUCHERS_REJECTED:
      return Object.assign({}, state, {
        unusedVouchers: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    case voucherAction.VOUCHER_REMOVED:
      return Object.assign({}, state, {
        unusedVouchers: state.unusedVouchers.filter(
          item => item.product.id !== voucher.product.id
        )
      });
    default:
      return state;
  }
}
