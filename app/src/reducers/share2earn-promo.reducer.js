import lodash from "lodash";
import { share2EarnPromoAction } from "../actions/share2earn-promo.action";

const initialState = {
  share2EarnPromo: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function share2EarnPromoReducer(state = initialState, action) {
  const { share2EarnPromo, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case share2EarnPromoAction.SHARE2EARN_PROMO_REQUESTED:
      return {
        ...state,
        page: 1,
        share2EarnPromo: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      };
    case share2EarnPromoAction.SHARE2EARN_PROMO_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case share2EarnPromoAction.SHARE2EARN_PROMO_FULFILLED:
      return {
        ...state,
        share2EarnPromo,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case share2EarnPromoAction.SHARE2EARN_PROMO_APPENDED:
      return {
        ...state,
        share2EarnPromo: [...state.share2EarnPromo, ...share2EarnPromo],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case share2EarnPromoAction.SHARE2EARN_PROMO_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case share2EarnPromoAction.SHARE2EARN_PROMO_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case share2EarnPromoAction.SHARE2EARN_PROMO_REJECTED:
      return {
        ...state,
        share2EarnPromo: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };
    default:
      return state;
  }
}
