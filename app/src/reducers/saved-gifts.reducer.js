import { savedGiftsAction } from "../actions/saved-gifts.action";
import lodash from "lodash";
import { voucherAction } from "../actions/vouchers.action";

const initialState = {
  savedGifts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function savedGiftsReducer(state = initialState, action) {
  const { savedGifts, page, voucher } = lodash.get(action, "payload", {});
  switch (action.type) {
    case savedGiftsAction.SAVED_GIFTS_REQUESTED:
      return Object.assign({}, state, {
        page: 1,
        savedGifts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      });
    case savedGiftsAction.SAVED_GIFTS_FETCHED_MORE:
      return Object.assign({}, state, {
        loading: true,
        loaded: false,
        loadFailed: false
      });
    case savedGiftsAction.SAVED_GIFTS_FULFILLED:
      return Object.assign({}, state, {
        savedGifts: savedGifts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      });
    case savedGiftsAction.SAVED_GIFTS_APPENDED:
      return Object.assign({}, state, {
        savedGifts: [...state.savedGifts, ...savedGifts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      });
    case savedGiftsAction.SAVED_GIFTS_REACHED_END:
      return Object.assign({}, state, {
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      });
    case savedGiftsAction.SAVED_GIFTS_REFRESHED:
      return Object.assign({}, state, {
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      });
    case savedGiftsAction.SAVED_GIFTS_REJECTED:
      return Object.assign({}, state, {
        savedGifts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      });
    case voucherAction.VOUCHER_REMOVED:
      return Object.assign({}, state, {
        savedGifts: state.savedGifts.filter(item => item.id !== voucher.id)
      });
    default:
      return state;
  }
}
