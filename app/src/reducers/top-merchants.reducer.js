import { topMerchantsAction } from "../actions/top-merchants.action";

const initialState = {
  topMerchants: [],
  loading: false,
  loadFailed: false
};

export const topMerchantsReducer = (state: {} = initialState, action: {}) => {
  switch (action.type) {
    case topMerchantsAction.TOP_MERCHANTS_LOADED:
      return {
        ...state,
        topMerchants: action.payload.topMerchants,
        loading: false,
        loadFailed: false
      };
    case topMerchantsAction.TOP_MERCHANTS_REQUESTED:
      return { ...state, loading: true, loadFailed: false };
    case topMerchantsAction.TOP_MERCHANTS_REQUEST_FAILED:
      return { ...state, loading: false, loadFailed: true };
    default:
      return state;
  }
};
