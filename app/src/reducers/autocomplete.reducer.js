import { autocompleteAction } from "../actions/autocomplete.action";

const initialState = {
  autocomplete: {
    category: [],
    popular: [],
    area: [],
    tag_in_area: [],
    tag_in_category: [],
    keyword: []
  },
  loading: false,
  loadFailed: false
};

export const autocompleteReducer = (state: {} = initialState, action: {}) => {
  switch (action.type) {
    case autocompleteAction.AUTOCOMPLETE_LOADED:
      return {
        ...state,
        autocomplete: action.payload.autocomplete,
        loading: false,
        loadFailed: false
      };
    case autocompleteAction.AUTOCOMPLETE_REQUESTED:
      return {
        ...state,
        loading: true,
        autocomplete: {
          category: [],
          popular: [],
          area: [],
          tag_in_area: [],
          tag_in_category: [],
          keyword: []
        },
        loadFailed: false
      };
    case autocompleteAction.AUTOCOMPLETE_REQUEST_FAILED:
      return { ...state, loading: false, loadFailed: true };
    default:
      return state;
  }
};
