import lodash from "lodash";
import { promoProductsAction } from "../actions/promo-products.action";

const initialState = {
  promoProducts: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function promoProductsReducer(state = initialState, action) {
  const { promoProducts, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case promoProductsAction.PROMO_PRODUCTS_REQUESTED:
      return {
        ...state,
        page: 1,
        promoProducts: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false
      };
    case promoProductsAction.PROMO_PRODUCTS_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case promoProductsAction.PROMO_PRODUCTS_FULFILLED:
      return {
        ...state,
        promoProducts,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case promoProductsAction.PROMO_PRODUCTS_APPENDED:
      return {
        ...state,
        promoProducts: [...state.promoProducts, ...promoProducts],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case promoProductsAction.PROMO_PRODUCTS_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case promoProductsAction.PROMO_PRODUCTS_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case promoProductsAction.PROMO_PRODUCTS_REJECTED:
      return {
        ...state,
        promoProducts: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };
    default:
      return state;
  }
}
