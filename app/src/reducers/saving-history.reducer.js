import lodash from "lodash";
import { savingHistoryAction } from "../actions/saving-history.action";

const initialState = {
  savingHistory: [],
  loading: false,
  loadFailed: false,
  reachedEnd: false,
  refreshing: false,
  firstLoading: false,
  page: 1
};

export default function savingHistoryReducer(state = initialState, action) {
  const { savingHistory, page } = lodash.get(action, "payload", {});
  switch (action.type) {
    case savingHistoryAction.SAVING_HISTORY_REQUESTED:
      return {
        ...state,
        page: 1,
        savingHistory: [],
        firstLoading: true,
        loading: true,
        loaded: false,
        loadFailed: false,
        reachedEnd: false,
        refreshing: false
      };
    case savingHistoryAction.SAVING_HISTORY_FETCHED_MORE:
      return { ...state, loading: true, loaded: false, loadFailed: false };
    case savingHistoryAction.SAVING_HISTORY_FULFILLED:
      return {
        ...state,
        savingHistory,
        loading: false,
        loaded: true,
        loadFailed: false,
        firstLoading: false,
        refreshing: false
      };
    case savingHistoryAction.SAVING_HISTORY_APPENDED:
      return {
        ...state,
        savingHistory: [...state.savingHistory, ...savingHistory],
        loading: false,
        loaded: true,
        loadFailed: false,
        page
      };
    case savingHistoryAction.SAVING_HISTORY_REACHED_END:
      return {
        ...state,
        loading: false,
        loaded: true,
        loadFailed: false,
        reachedEnd: true
      };
    case savingHistoryAction.SAVING_HISTORY_REFRESHED:
      return {
        ...state,
        page: 1,
        refreshing: true,
        loading: true,
        reachedEnd: false,
        loadFailed: false,
        loaded: false
      };
    case savingHistoryAction.SAVING_HISTORY_REJECTED:
      return {
        ...state,
        savingHistory: [],
        refreshing: false,
        loading: false,
        loaded: false,
        loadFailed: true,
        firstLoading: false
      };
    default:
      return state;
  }
}
