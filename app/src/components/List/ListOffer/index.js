import React, { Component } from "react";
import { View, FlatList, TouchableOpacity, Image } from "react-native";
import Text from "../../common/text";
import TouchableIcon from "../../common/touchable-icon";
import CardOffer from "../../Card/CardOffer";
import { IC_CHEVRON, IC_MORE_ONLINE } from "../../../commons/images";
import ActivityIndicator from "../../ActivityIndicator";
import styles from "./styles";

const MAX_LENGTH = 7;
class ListOffer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  ListEmptyComponent = () => {
    return <Text style={styles.txtEmpty}>Không tìm thấy ưu đãi.</Text>;
  };

  render() {
    const { offers, count, title = "Ưu đãi gần bạn", isLoading } = this.props;
    return (
      <View style={[styles.container, styles.shadow]}>
        <View style={styles.bar}>
          <Text style={styles.title}>{title}</Text>
          {isLoading && <ActivityIndicator />}
          <TouchableIcon
            onPress={this.navigateToNearbyOffers}
            iconStyle={styles.arrow}
            source={IC_CHEVRON}
          />
        </View>
        {!isLoading && (
          <FlatList
            horizontal
            data={offers.length > MAX_LENGTH ? [...offers.slice(0, 7), {}] : offers}
            renderItem={this.renderNearOffer}
            keyExtractor={(item, index) => (item.id || index).toString()}
            contentContainerStyle={styles.top}
            showsHorizontalScrollIndicator={false}
            ListEmptyComponent={this.ListEmptyComponent}
          />
        )}
      </View>
    );
  }

  navigateToNearbyOffers = () => {
    const { onPressViewMore } = this.props;
    if (typeof onPressViewMore === "function") {
      onPressViewMore();
    }
  };

  renderNearOffer = ({ item, index }) => {
    if (index === MAX_LENGTH)
      return (
        <TouchableOpacity onPress={this.navigateToNearbyOffers} style={styles.more}>
          <View style={styles.moreWrap}>
            <Image style={styles.moreIcon} source={IC_MORE_ONLINE} />
            <Text style={styles.moreText}>Xem thêm</Text>
          </View>
        </TouchableOpacity>
      );
    return <CardOffer item={item} />;
  };
}

export default ListOffer;

ListOffer.defaultProps = {
  isLoading: false
};
