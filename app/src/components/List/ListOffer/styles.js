import { StyleSheet } from "react-native";
import { sizeFont, sizeWidth, moderateScale } from "../../../helpers/size.helper";
import { font, appColor } from "../../../constants/app.constant";

export default StyleSheet.create({
  shadow: {
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1
    }
  },
  title: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: sizeWidth(4),
    paddingHorizontal: sizeWidth(12)
  },
  arrow: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  top: {
    paddingHorizontal: sizeWidth(5)
  },
  container: {
    backgroundColor: "white",
    paddingVertical: sizeWidth(12),
    marginBottom: moderateScale(12)
  },
  more: {
    marginVertical: sizeWidth(3),
    marginHorizontal: sizeWidth(5)
  },
  moreWrap: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 4,
    width: sizeWidth(146),
    borderRadius: sizeWidth(6),
    flex: 1,
    borderColor: appColor.blur,
    borderStyle: "dashed",
    borderWidth: 1,
    backgroundColor: "white"
  },
  moreText: {
    color: appColor.blur,
    fontSize: sizeFont(13),
    fontFamily: font.bold
  },
  moreIcon: {
    width: moderateScale(14),
    height: moderateScale(24),
    marginBottom: moderateScale(19)
  },
  txtEmpty: {
    color: appColor.text,
    fontSize: sizeFont(13),
    fontFamily: font.bold,
    flex: 1,
    marginLeft: moderateScale(10),
    opacity: 0.5
  }
});
