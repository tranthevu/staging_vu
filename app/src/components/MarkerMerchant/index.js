/* eslint-disable no-nested-ternary */
import React, { PureComponent } from "react";
import { View, Image } from "react-native";
import { Marker } from "react-native-maps";
import styles from "./styles";
import { IMG_DEFAULT_IMAGE } from "../../commons/images";

export default class MarkerMerchant extends PureComponent {
  constructor() {
    super();
    this.state = {
      tracksViewChanges: true
    };
  }

  stopRendering = () => {
    this.setState({ tracksViewChanges: false });
  };

  onPress = () => {
    const { onPress, item } = this.props;
    if (typeof onPress === "function") {
      onPress(item);
    }
  };

  render() {
    const { children, image, item, ...otherProps } = this.props;
    const { isBranch, sourceImg } = item || {};
    const source = isBranch ? sourceImg : image || IMG_DEFAULT_IMAGE;
    return (
      <Marker
        {...otherProps}
        onPress={this.onPress}
        tracksViewChanges={this.state.tracksViewChanges}
      >
        <View style={styles.container}>
          <Image
            onLoad={this.stopRendering}
            resizeMode="cover"
            source={source}
            style={styles.avatar}
          />
        </View>
      </Marker>
    );
  }
}

MarkerMerchant.defaultProps = {
  onPress: item => {
    console.log("onPress>>>", { item });
  }
};
