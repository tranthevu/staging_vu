import React, { PureComponent } from "react";
import { View, StyleSheet, TouchableOpacity, Linking } from "react-native";
import Swiper from "react-native-swiper";
import lodash from "lodash";
import { sizeWidth } from "../../helpers/size.helper";
import { appColor } from "../../constants/app.constant";
import CacheImage from "../common/cache-image";

export default class Banner extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderItem = banner => {
    const image = lodash.get(banner, "images[0].original");
    return (
      <TouchableOpacity
        onPress={() => this.navigate(banner)}
        key={banner.id.toString()}
        style={styles.item}
      >
        <CacheImage resizeMode="stretch" uri={image} style={styles.image} />
      </TouchableOpacity>
    );
  };

  navigate = banner => {
    const url = lodash.get(banner, "url_product");
    if (url) Linking.openURL(url);
  };

  render() {
    const length = lodash.get(this, "props.data.length", 0);
    if (length === 0) return null;
    return (
      <View style={styles.container}>
        <View style={styles.back} />
        <Swiper
          paginationStyle={styles.pagination}
          dotStyle={styles.dot}
          activeDotStyle={styles.activeDot}
          style={styles.swiper}
          showsButtons={false}
          autoplay={true}
          autoplayTimeout={5}
        >
          {this.props.data.map(item => this.renderItem(item))}
        </Swiper>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: sizeWidth(158)
  },
  dot: {
    width: sizeWidth(6),
    height: sizeWidth(6),
    borderRadius: sizeWidth(3),
    borderWidth: 0.5,
    borderColor: "white",
    backgroundColor: "#DDDDDD"
  },
  activeDot: {
    width: sizeWidth(8),
    height: sizeWidth(8),
    borderWidth: 0.5,
    borderColor: "white",
    borderRadius: sizeWidth(4),
    backgroundColor: "#979797"
  },
  swiper: {},
  pagination: {
    marginBottom: -sizeWidth(16)
  },
  image: {
    width: sizeWidth(302),
    height: sizeWidth(158),
    borderRadius: sizeWidth(6)
  },
  item: {
    alignSelf: "center"
  },
  back: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    height: sizeWidth(95),
    backgroundColor: appColor.primary
  }
});
