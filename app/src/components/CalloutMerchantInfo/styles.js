import { StyleSheet } from "react-native";
import { moderateScale } from "../../../../helpers/size.helper";
import { appColor, font } from "../../../../constants/app.constant";

export default StyleSheet.create({
  container: {
    width: moderateScale(20),
    height: moderateScale(20),
    borderRadius: moderateScale(20),
    borderColor: appColor.primary,
    borderWidth: 1,
    overflow: "hidden",
    backgroundColor: appColor.bg
  },
  avatar: {
    width: moderateScale(20),
    height: moderateScale(20)
  },
  infoBox: {
    width: moderateScale(302),
    paddingVertical: moderateScale(6),
    flexDirection: "row",
    alignItems: "center",
    borderRadius: moderateScale(22)
    // overflow: "hidden",
  },
  title: {
    fontFamily: font.bold,
    fontSize: moderateScale(12),
    color: appColor.text
  },
  txtContent: {
    fontFamily: font.lightItalic,
    fontSize: moderateScale(12),
    color: appColor.text,
    marginLeft: moderateScale(6)
  },
  rightView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    flex: 1,
    paddingHorizontal: moderateScale(6)
  },
  avatarInfoBox: {
    backgroundColor: appColor.gray,
    borderRadius: moderateScale(40),
    width: moderateScale(40),
    height: moderateScale(40),
    justifyContent: "center",
    alignItems: "center"
  },
  leftContentInfo: {
    alignItems: "center",
    justifyContent: "center"
  },
  iconChevron: {
    width: moderateScale(20),
    height: moderateScale(20),
    position: "absolute"
  }
});
