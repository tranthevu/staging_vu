import React, { PureComponent } from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import styles from "./styles";
import { IC_CHEVRON } from "../../../../commons/images";

export default class CalloutMerchantInfo extends PureComponent {
  render() {
    const { children, image, item, ...otherProps } = this.props;
    const { name, content } = item || {};
    return (
      <View style={styles.infoBox}>
        <View style={styles.avatarInfoBox}>
          <Image
            source={{
              uri:
                "https://previews.123rf.com/images/triken/triken1608/triken160800029/61320775-male-avatar-profile-picture-default-user-avatar-guest-avatar-simply-human-head-vector-illustration-i.jpg"
            }}
            style={styles.avatarInfoBox}
          />
        </View>
        <View style={styles.rightView}>
          <View style={styles.leftContentInfo}>
            <Text numberOfLines={1} style={styles.title}>
              {name}
            </Text>
            <Text numberOfLines={2} style={styles.txtContent}>
              {content}
            </Text>
          </View>
          <View style={styles.iconChevron}>
            <Image source={IC_CHEVRON} resizeMode="contain" style={styles.iconChevron} />
          </View>
        </View>
      </View>
    );
  }
}
