import React, { PureComponent } from "react";
import { View, FlatList, ActivityIndicator, RefreshControl } from "react-native";
import PropTypes from "prop-types";
import { PAGE_LIMIT, appColor } from "../../constants/app.constant";
import { moderateScale, sizeFont } from "../../helpers/size.helper";
import Text from "../common/text";

const LIMIT = PAGE_LIMIT;

export default class FlatListAutoLoadMore extends PureComponent {
  static getDerivedStateFromProps(nextProps, prevState) {
    const { data, prevData, currentPage } = prevState;
    if (prevData !== nextProps.data && data !== nextProps.data) {
      return {
        prevData: nextProps.data,
        data: [...data, ...nextProps.data],
        hasData: nextProps.data.length >= LIMIT,
        currentPage: currentPage + 1
      };
    }
    return { hasData: nextProps.data.length >= LIMIT };
  }

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      hasData: false,
      currentPage: -1
    };
    this.page = 1;
  }

  ListEmptyComponent = () => (
    <View
      style={{
        padding: moderateScale(12),
        alignItems: "center",
        display: this.props.isFetching ? "none" : "flex"
      }}
    >
      <Text
        style={{
          color: "#979797",
          fontSize: sizeFont(13),
          fontStyle: "italic"
        }}
      >
        Không có dữ liệu.
      </Text>
    </View>
  );

  onFetchData = (nextPage, isRefresh) => {
    const { onFetch } = this.props;
    if (typeof onFetch === "function") {
      this.setState({ isRefresh }, () => {
        onFetch(nextPage, isRefresh);
      });
    }
  };

  ListFooterComponent = () => {
    const { isFetching, ListFooterComponent, hasButton } = this.props;
    if (ListFooterComponent) return ListFooterComponent();
    const { hasData, isRefresh } = this.state;
    return (
      <View style={{ width: "100%" }}>
        {isFetching && hasData && !isRefresh ? (
          <ActivityIndicator
            color={appColor.primary}
            style={{ padding: moderateScale(8) }}
          />
        ) : (
          hasData && hasButton && null
          // <ButtonRoundedPrimary
          //   shadow
          //   style={{
          //     marginVertical: setValue(12)
          //   }}
          //   onPress={this.onFetchData}
          //   text="Xem Thêm"
          // />
        )}
      </View>
    );
  };

  ItemSeparatorComponent = () => <View style={{ height: moderateScale(20) }} />;

  keyExtractor = (item, index) => (item.id || index).toString();

  clearData(callback = () => {}) {
    this.page = 1;
    this.setState({ currentPage: 0, data: [] }, callback);
  }

  onEndReached = () => {
    const { isFetching, hasButton } = this.props;
    if (hasButton) return;
    const { currentPage, hasData } = this.state;
    console.log("onEndReached>>>111", {
      currentPage,
      isFetching,
      page: this.page
    });
    if (hasData && !isFetching && this.page < currentPage) {
      this.page += 1;
      console.log("onEndReached>>>", {
        currentPage,
        isFetching,
        page: this.page
      });
      this.onFetchData(this.page);
    }
  };

  onRefresh = () => {
    this.clearData(() => this.onFetchData(this.page, true));
  };

  render() {
    const {
      style = {},
      contentContainerStyle = {},
      isFetching,
      ...otherProps
    } = this.props;
    const { isRefresh } = this.state;
    return (
      <FlatList
        // removeClippedSubviews
        ListEmptyComponent={this.ListEmptyComponent}
        ItemSeparatorComponent={this.ItemSeparatorComponent}
        renderSectionFooter={this.ItemSeparatorComponent}
        keyboardDismissMode="on-drag"
        onEndReachedThreshold={0.2}
        keyExtractor={this.keyExtractor}
        refreshControl={
          <RefreshControl
            refreshing={isRefresh && isFetching}
            colors={[appColor.primary]}
            onRefresh={this.onRefresh}
          />
        }
        {...otherProps}
        onEndReached={this.onEndReached}
        ListFooterComponent={this.ListFooterComponent}
        data={this.state.data}
        style={[style]}
        contentContainerStyle={[
          { paddingVertical: moderateScale(15) },
          contentContainerStyle
        ]}
      />
    );
  }
}

FlatListAutoLoadMore.propTypes = {
  data: PropTypes.any.isRequired,
  hasData: PropTypes.bool,
  hasButton: PropTypes.bool
};

FlatListAutoLoadMore.defaultProps = {
  hasData: false,
  hasButton: false
};
