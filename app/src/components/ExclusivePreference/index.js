import React, { Component } from 'react'
import { View, StyleSheet, FlatList, TouchableOpacity, Image } from 'react-native'
import { connect } from "react-redux";
import Text from "../common/text";
import TouchableIcon from "../common/touchable-icon";
import { appConfig } from "../../config/app.config";
import {
    sizeFont,
    sizeWidth,
    moderateScale
} from "../../helpers/size.helper";
import { font, appMap, appColor } from "../../constants/app.constant";
import { navigateToPage } from "../../actions/nav.action";
import PromotionItem from "../../screens/dashboard/promotion-item"

class index extends Component {

    navigateToExclusiveProducts = () => {
        appConfig.activeApp = appMap.all;
        this.props.navigateToPage("ExclusivePromo");
    };


    renderExclusive = ({ item, index }) => {
        if (index === 7)
            return (
                <TouchableOpacity onPress={this.navigateToExclusiveProducts} style={styles.more}>
                    <View style={styles.moreWrap}>
                        <Image
                            style={styles.moreIcon}
                            source={require("../../../res/icon/more-online.png")}
                        />
                        <Text style={styles.moreText}>Xem thêm</Text>
                    </View>
                </TouchableOpacity>
            );
        return <PromotionItem style={styles.promotion} key={index.toString()} item={item} />;
    };

    render() {
        return (
            <View style={[styles.online, styles.shadow]}>
                <View style={styles.bar}>
                    <Text style={styles.onlineTitle}>Top ưu đãi độc quyền</Text>
                    <TouchableIcon
                        onPress={this.navigateToExclusiveProducts}
                        iconStyle={styles.arrow}
                        source={require("../../../res/icon/chevron-right.png")}
                    />
                </View>
                <FlatList
                    horizontal
                    data={this.props.exclusiveOffersCount > 7 ? [...this.props.exclusiveOffers, {}] : this.props.exclusiveOffers}
                    renderItem={this.renderExclusive}
                    keyExtractor={(item, index) => index.toString()}
                    contentContainerStyle={styles.top}
                    showsHorizontalScrollIndicator={false}
                />
            </View>
        )
    }
}
export default connect(
    null,
    { navigateToPage }
)(index);

const styles = StyleSheet.create({
    online: {
        backgroundColor: "white",
        paddingVertical: sizeWidth(12),
        marginBottom: moderateScale(12)
    },
    shadow: {
        shadowOpacity: 1,
        shadowRadius: 0,
        elevation: 2,
        shadowColor: "rgba(0, 0, 0, 0.1)",
        shadowOffset: {
            width: 0,
            height: 1
        }
    },
    bar: {
        flexDirection: "row",
        alignItems: "center",
        marginBottom: sizeWidth(4),
        paddingHorizontal: sizeWidth(12)
    },
    onlineTitle: {
        fontSize: sizeFont(16),
        flex: 1,
        fontFamily: font.medium
    },
    arrow: {
        width: moderateScale(24),
        height: moderateScale(24)
    },
    top: {
        paddingHorizontal: sizeWidth(5)
    },
    promotion: {
        marginHorizontal: sizeWidth(5)
    },
    more: {
        marginVertical: sizeWidth(3),
        marginHorizontal: sizeWidth(5)
    },
    moreWrap: {
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 4,
        width: sizeWidth(146),
        borderRadius: sizeWidth(6),
        flex: 1,
        borderColor: appColor.blur,
        borderStyle: "dashed",
        borderWidth: 1,
        backgroundColor: "white"
    },
    moreText: {
        color: appColor.blur,
        fontSize: sizeFont(13),
        fontFamily: font.bold
    },
    moreIcon: {
        width: moderateScale(14),
        height: moderateScale(24),
        marginBottom: moderateScale(19)
    },
})