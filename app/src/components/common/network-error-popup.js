import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image } from "react-native";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";
import Text from "./text";
import Button from "./button";

export default class NetworkErrorPopup extends Component {
  render(): ReactNode {
    const { onClose } = this.props;
    return (
      <View style={styles.root}>
        <Image
          style={styles.image}
          source={require("../../../res/img/network-error.png")}
        />
        <Text style={styles.title}>Lỗi kết nối</Text>
        <Text style={styles.message}>
          Vui lòng kiểm tra kết nối mạng hoặc thử lại sau ít phút
        </Text>
        <Button
          textStyle={styles.text}
          onPress={onClose}
          style={styles.close}
          text="THỬ LẠI"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    borderRadius: sizeWidth(6),
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
    height: moderateScale(320),
    width: sizeWidth(302)
  },
  image: {
    height: moderateScale(135),
    width: moderateScale(135)
  },
  title: {
    marginBottom: moderateScale(10),
    marginTop: moderateScale(20),
    fontSize: sizeFont(18),
    fontFamily: font.medium,
    color: "#EF4136"
  },
  message: {
    paddingHorizontal: sizeWidth(43),
    fontSize: sizeFont(16),
    textAlign: "center",
    marginBottom: moderateScale(10)
  },
  close: {
    width: sizeWidth(180)
  },
  text: {
    fontFamily: font.bold
  }
});
