import React, { Component, ReactNode } from "react";
import { View, StyleSheet } from "react-native";
import { sizeWidth, sizeFont } from "../../helpers/size.helper";
import Text from "./text";
import { font } from "../../constants/app.constant";

export default class EmptyView extends Component {
  render(): ReactNode {
    const { label } = this.props;
    return (
      <View style={styles.empty}>
        <Text style={styles.noResults}>{label || "Không có dữ liệu"}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  empty: {
    alignItems: "center",
    padding: sizeWidth(12)
  },
  noResults: {
    color: "#979797",
    fontSize: sizeFont(13),
    fontStyle: "italic"
  }
});
