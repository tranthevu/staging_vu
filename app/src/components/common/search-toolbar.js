import PropTypes from "prop-types";
import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Platform,
  TouchableOpacity,
  Image,
  TextInput
} from "react-native";
import {
  sizeFont,
  sizeHeight,
  sizeWidth,
  moderateScale
} from "../../helpers/size.helper";
import Text from "./text";
import { appColor } from "../../constants/app.constant";
import { connect } from "react-redux";
import { navigateToPage } from "../../actions/nav.action";
import BackIcon from "./back-icon";
import NotificationIcon from "../../screens/dashboard/notification-icon";
import Toolbar from "./toolbar";
import BackToMainIcon from "./back-to-main-icon";

class SearchToolbar extends Component {
  navigateToSearch = () => {
    this.props.navigateToPage("Search");
  };

  focus = () => {
    if (this.ref) this.ref.focus();
  };

  render() {
    const isComeback = this.props.isComeback;
    const {
      searchable,
      value,
      onIconPress,
      onChangeText,
      placeholder
    } = this.props;
    return (
      <View style={styles.toolbar}>
        {isComeback ? ( <BackToMainIcon /> ) : ( <BackIcon /> )}
        {searchable ? (
          <TouchableOpacity onPress={this.navigateToSearch} style={styles.wrap}>
            <TextInput
              ref={ref => (this.ref = ref)}
              onChangeText={onChangeText}
              value={value}
              placeholderTextColor={appColor.blur}
              returnKeyType="search"
              onSubmitEditing={onIconPress}
              autoCapitalize="none"
              placeholder={placeholder}
              style={styles.input}
            />
            <TouchableOpacity onPress={onIconPress}>
              <Image
                style={styles.glass}
                resizeMode="stretch"
                source={require("../../../res/icon/search.png")}
              />
            </TouchableOpacity>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={this.navigateToSearch} style={styles.wrap}>
            <Text style={styles.keyword}>Tìm kiếm</Text>
            <Image
              style={styles.glass}
              resizeMode="stretch"
              source={require("../../../res/icon/search.png")}
            />
          </TouchableOpacity>
        )}
        <NotificationIcon />
      </View>
    );
  }
}

export default connect(
  null,
  { navigateToPage },
  null,
  { withRef: true }
)(SearchToolbar);

const styles = StyleSheet.create({
  glass: {
    width: moderateScale(15),
    height: moderateScale(15),
    tintColor: appColor.primary
  },
  keyword: {
    flex: 1,
    color: appColor.blur,
    fontSize: sizeFont(11),
    marginRight: sizeWidth(9)
  },
  wrap: {
    flex: 1,
    height: sizeWidth(25),
    alignItems: "center",
    marginHorizontal: moderateScale(8),
    borderRadius: sizeWidth(6),
    backgroundColor: "white",
    paddingHorizontal: sizeWidth(9),
    flexDirection: "row"
  },
  toolbar: {
    height: moderateScale(52),
    width: sizeWidth(320),
    flexDirection: "row",
    paddingHorizontal: sizeWidth(12),
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: appColor.primary
  },
  input: {
    flex: 1,
    color: appColor.text,
    fontSize: sizeFont(11),
    padding: 0,
    height: sizeWidth(25),
    marginRight: sizeWidth(9)
  }
});
