import React, { Component } from "react";
import { Image, TouchableOpacity, StyleSheet } from "react-native";
import { sizeWidth, moderateScale } from "../../helpers/size.helper";
import {navigateToPage} from "../../actions/nav.action";
import { connect } from "react-redux";

class BackToMainIcon extends Component {
    render() {
        const { style, iconStyle, source } = this.props;
        return (
            <TouchableOpacity
                style={[styles.container, style]}
                onPress={() => {
                    this.props.navigateToPage('Main');
                }}
            >
                <Image
                    style={[styles.icon, iconStyle]}
                    resizeMode="contain"
                    source={source || require("../../../res/icon/back.png")}
                />
            </TouchableOpacity>
        );
    }
}

export default connect(
  null,
  { navigateToPage }
)(BackToMainIcon);

const styles = StyleSheet.create({
    container: {
        padding: sizeWidth(4)
    },
    icon: {
        width: moderateScale(13),
        height: moderateScale(21),
        tintColor: "white"
    }
});
