import React, { Component, ReactNode } from "react";
import { View, TouchableOpacity, StyleSheet, Image, TextInput } from "react-native";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Text from "./text";
import { connectActionSheet } from "@expo/react-native-action-sheet";
import lodash from "lodash";

@connectActionSheet
export default class Dropdown extends Component {
  render(): ReactNode {
    const { style, value, label, onPress } = this.props;
    return (
      <View style={[styles.container, style]}>
        {label && <Text style={styles.label}>{label}</Text>}
        <TouchableOpacity onPress={onPress || this.showOptionPicker} style={styles.body}>
          <Text style={styles.value}>{value}</Text>
          <Image
            style={styles.expand}
            source={require("../../../res/icon/arrow-down.png")}
          />
        </TouchableOpacity>
        <View style={styles.line} />
      </View>
    );
  }

  showOptionPicker = () => {
    const { options, path, onValueChange } = this.props;
    if (!options) return;
    let values = path ? options.map(item => lodash.get(item, path)) : options;
    values = [...values, "Huỷ"];
    const cancelButtonIndex = options.length;
    const showFn = this.props.showFn || this.props.showActionSheetWithOptions;
    showFn(
      {
        options: values,
        cancelButtonIndex
      },
      buttonIndex => {
        if (onValueChange && buttonIndex != options.length)
          onValueChange(options[buttonIndex]);
      }
    );
  };
}

const styles = StyleSheet.create({
  body: {
    alignItems: "center",
    flexDirection: "row"
  },
  container: {
    alignSelf: "center",
    width: sizeWidth(302),
    paddingHorizontal: moderateScale(12),
    backgroundColor: "white",
    marginVertical: moderateScale(8)
  },
  value: {
    fontSize: sizeFont(13),
    flex: 1,
    fontFamily: font.regular
  },
  line: {
    width: "100%",
    height: 1,
    marginTop: moderateScale(6),
    backgroundColor: appColor.blur
  },
  expand: {
    width: moderateScale(10),
    marginLeft: moderateScale(12),
    height: moderateScale(6)
  },
  label: {
    fontFamily: font.medium,
    fontSize: sizeFont(13),
    marginVertical: moderateScale(12)
  }
});
