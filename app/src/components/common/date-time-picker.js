import * as React from "react";
import { View, StyleSheet, Image } from "react-native";
import Text from "./text";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import DatePicker from "react-native-datepicker";
import moment from "moment";
import { font, appColor } from "../../constants/app.constant";

export default class DateTimePicker extends React.Component {
  render = () => {
    const {
      value,
      onDateChange,
      mode,
      format,
      placeholder,
      disabled,
      maxDate,
      minDate,
      style,
      label
    } = this.props;
    const date = value
      ? moment(value, format).format(format)
      : placeholder
      ? placeholder
      : format;
    return (
      <View style={[styles.container, style]}>
        <View style={styles.body}>
          <View style={styles.info}>
            {label && <Text style={styles.label}>{label}</Text>}
            <Text
              onPress={() => this.datePicker.onPressDate()}
              style={styles.textTime}
            >
              {date}
            </Text>
          </View>
          <Image
            resizeMode="stretch"
            style={styles.icon}
            source={require("../../../res/icon/date.png")}
          />
          <DatePicker
            confirmBtnText="Chọn"
            cancelBtnText="Huỷ"
            date={value}
            ref={ref => (this.datePicker = ref)}
            style={styles.picker}
            onDateChange={onDateChange}
            maxDate={maxDate}
            minDate={minDate}
            disabled={disabled}
            mode={mode || "datetime"}
            customStyles={{
              btnTextConfirm: {
                color: "#333333"
              },
              btnTextCancel: {
                color: "#333333"
              },
              btnCancel: {
                paddingVertical: 0
              },
              btnConfirm: {
                paddingVertical: 0
              }
            }}
          />
        </View>
        <View style={styles.line} />
      </View>
    );
  };
}

DateTimePicker.defaultProps = {
  format: "DD/MM/YYYY"
};

const styles = StyleSheet.create({
  textTime: {
    fontSize: sizeFont(12)
  },
  info: {
    flex: 1
  },
  label: {
    fontFamily: font.medium,
    fontSize: sizeFont(13),
    marginVertical: moderateScale(10)
  },
  icon: {
    width: moderateScale(15),
    height: moderateScale(15)
  },
  body: {
    flexDirection: "row",
    alignItems: "flex-end"
  },
  container: {
    width: sizeWidth(302),
    marginVertical: moderateScale(10),
    alignSelf: "center",
    paddingHorizontal: sizeWidth(12)
  },
  picker: {
    width: 0,
    height: 0,
    overflow: "hidden"
  },
  line: {
    width: "100%",
    height: 1,
    marginTop: moderateScale(6),
    backgroundColor: appColor.blur
  }
});
