import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { sizeFont, moderateScale } from "../../helpers/size.helper";
import Text from "./text";
import { font, appColor } from "../../constants/app.constant";

export default class ExclusiveLabel extends Component {
  render() {
    return (
      <View style={styles.wrap}>
        <Text style={styles.text}>Độc quyền</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrap: {
    position: "absolute",
    top: moderateScale(12),
    height: moderateScale(21),
    width: moderateScale(92),
    right: moderateScale(-24),
    backgroundColor: appColor.primary,
    transform: [
      {
        rotate: "45deg"
      }
    ],
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    color: "white",
    textAlign: "center",
    fontFamily: font.bold,
    fontSize: sizeFont(11)
  }
});
