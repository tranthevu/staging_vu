import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import Text from "./text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";

export default class Checkbox extends Component {
  render(): ReactNode {
    const { label, checked, onCheck, style } = this.props;
    const icon = checked
      ? require("../../../res/icon/checked.png")
      : require("../../../res/icon/uncheck.png");
    const iconStyle = styles.icon;
    return (
      <View style={[styles.container, style]}>
        <View style={styles.body}>
          <TouchableOpacity onPress={onCheck}>
            <Image style={iconStyle} source={icon} />
          </TouchableOpacity>
          <Text style={styles.text}>{label}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginVertical: moderateScale(14),
    width: sizeWidth(302),
    alignSelf: "center"
  },
  body: {
    flexDirection: "row",
    alignItems: "center"
  },
  icon: {
    width: moderateScale(20),
    height: moderateScale(20)
  },
  text: {
    fontSize: sizeFont(13),
    marginLeft: sizeWidth(8),
    flex: 1
  }
});
