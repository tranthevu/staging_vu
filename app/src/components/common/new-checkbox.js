import React, { PureComponent, ReactNode } from "react";
import PropTypes from "prop-types";
import { Image, View, StyleSheet, TouchableOpacity } from "react-native";
import { sizeWidth } from "../../helpers/size.helper";

export default class NewCheckbox extends PureComponent {
  render(): ReactNode {
    const { style, disabled, checked, onChange } = this.props;
    return (
      <TouchableOpacity
        disabled={disabled}
        onPress={() => onChange(!checked)}
        style={style}
      >
        <View style={[styles.container, checked && styles.checkedContainer]}>
          {checked && (
            <Image
              style={[styles.checked]}
              source={require("../../../res/icon/tick-merchant.png")}
            />
          )}
        </View>
        {/* <View>
          <Image
            style={[styles.image]}
            source={require("../../../res/icon/checkbox.png")}
          />
          {
            checked && (
              <Image
                style={[styles.checked]}
                source={require("../../../res/icon/check.png")}
              />
            )
          }
        </View> */}
      </TouchableOpacity>
    );
  }
}

NewCheckbox.propTypes = {
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  checked: PropTypes.bool
};
NewCheckbox.defaultProps = {
  onChange: () => {},
  disabled: false,
  checked: false
};

const styles = StyleSheet.create({
  checked: {
    width: sizeWidth(18),
    height: sizeWidth(18)
  },
  container: {
    width: sizeWidth(18),
    height: sizeWidth(18),
    borderWidth: 2,
    borderColor: "#C4C4C4",
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center"
  },
  checkedContainer: {
    backgroundColor: "#EF4136",
    borderWidth: 0
  }
});
