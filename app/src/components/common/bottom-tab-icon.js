import React, { Component } from "react";
import { StyleSheet, Image } from "react-native";
import PropTypes from "prop-types";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { appColor } from "../../constants/app.constant";

export default class BottomTabIcon extends Component {
  render() {
    const { icon, focused, style, source } = this.props;
    const tintColorStyle = focused ? styles.active : styles.inactive;
    return (
      <Image
        resizeMode="stretch"
        source={source}
        style={[styles.icon, style, tintColorStyle]}
      />
    );
  }
}

BottomTabIcon.propTypes = {
  icon: PropTypes.any
};

const styles = StyleSheet.create({
  icon: {
    width: moderateScale(17),
    height: moderateScale(17)
  },
  active: {
    tintColor: appColor.primary
  },
  inactive: {
    tintColor: appColor.icon
  }
});
