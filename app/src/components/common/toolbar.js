import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import { sizeWidth, moderateScale } from "../../helpers/size.helper";
import { appColor } from "../../constants/app.constant";

export default class Toolbar extends Component {
  render() {
    const { left, center, right, leftStyle, rightStyle, ...otherProps } = this.props;
    return (
      <View style={styles.toolbar} {...otherProps}>
        <View style={styles.container}>
          <View style={[styles.left, leftStyle]}>{left}</View>
          <View style={styles.center}>{center}</View>
          <View style={[styles.right, rightStyle]}>{right}</View>
        </View>
        <View style={styles.statusBar} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  statusBar: {},
  toolbar: {
    height: moderateScale(52),
    width: sizeWidth(320)
  },
  container: {
    height: moderateScale(52),
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: appColor.primary,
    alignItems: "center"
  },
  left: {
    width: sizeWidth(50),
    justifyContent: "center",
    alignItems: "center"
  },
  right: {
    width: sizeWidth(50),
    justifyContent: "center",
    alignItems: "center"
  },
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
