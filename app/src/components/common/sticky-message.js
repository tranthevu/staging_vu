import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, Animated } from "react-native";
import Text from "./text";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import EventRegister from "../../helpers/event-register.helper";
import { event, text } from "../../constants/app.constant";
import validator from "validator";

export default class StickyMessage extends Component {
  constructor(props) {
    super(props);
  }

  render(): ReactNode {
    const { message } = this.props;
    if (validator.isEmpty(message)) return null;
    return (
      <Animated.View style={[styles.container]}>
        <Image
          resizeMode="stretch"
          style={styles.image}
          source={require("../../../res/icon/notif-info.png")}
        />
        <Text style={styles.text}>{message}</Text>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#FDDD62",
    height: moderateScale(45),
    overflow: "hidden",
    justifyContent: "center",
    width: sizeWidth(320)
  },
  image: {
    width: moderateScale(18),
    height: moderateScale(18),
    marginRight: sizeWidth(11)
  },
  text: {
    color: "#653A00",
    fontSize: sizeFont(13)
  }
});
