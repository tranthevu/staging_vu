import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  ActivityIndicator,
  TouchableOpacity
} from "react-native";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import Text from "./text";
import { font } from "../../constants/app.constant";

export default class TouchableRow extends Component {
  render() {
    const {
      onPress,
      style,
      icon,
      iconStyle,
      labelStyle,
      loading,
      label,
      hideArrow,
      resizeMode,
      rightIcon
    } = this.props;
    return (
      <View>
        <TouchableOpacity
          disabled={loading}
          onPress={onPress}
          style={[styles.container, style]}
        >
          {icon && (
            <Image
              resizeMode={resizeMode}
              source={icon}
              style={[styles.icon, iconStyle]}
            />
          )}
          <Text style={[styles.label, labelStyle]}>{label}</Text>
          {!hideArrow && (
            <Image
              resizeMode={resizeMode}
              style={styles.image}
              source={require("../../../res/icon/expand.png")}
            />
          )}
          {rightIcon}
          {loading && <ActivityIndicator style={styles.loading} size="small" />}
        </TouchableOpacity>
        <View style={styles.line} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: sizeWidth(12),
    height: moderateScale(44),
    backgroundColor: "white",

    flexDirection: "row",
    alignItems: "center"
  },
  label: {
    fontSize: sizeFont(12),
    flex: 1,
    fontFamily: font.regular
  },
  image: {
    width: moderateScale(7),
    height: moderateScale(11),
    marginLeft: sizeWidth(12)
  },
  line: {
    backgroundColor: "#DDDDDD",
    height: 1,
    width: "100%"
  },
  loading: {
    marginLeft: sizeWidth(12)
  },
  icon: {
    width: moderateScale(16),
    height: moderateScale(16),
    marginRight: sizeWidth(12)
  }
});
