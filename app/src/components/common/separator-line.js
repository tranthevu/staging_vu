import React, { PureComponent, ReactNode } from "react";
import { View, StyleSheet, Image } from "react-native";
import { sizeWidth } from "../../helpers/size.helper";
import { appColor } from "../../constants/app.constant";

export default class SeparatorLine extends PureComponent {
  render(): ReactNode {
    const { top } = this.props;
    if (top) {
      return (
        <>
          <Image
            style={[styles.line, { top: top + sizeWidth(9) }]}
            source={require("../../../res/icon/line.png")}
          />
          <View style={[styles.circle, styles.left, { top }]} />
          <View style={[styles.circle, styles.right, { top }]} />
        </>
      );
    }
    return (
      <View>
        <Image style={styles.separator} source={require("../../../res/icon/line.png")} />
        <View style={[styles.circle, styles.left, styles.top]} />
        <View style={[styles.circle, styles.right, styles.top]} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  circle: {
    backgroundColor: appColor.bg,
    position: "absolute",
    width: sizeWidth(18),
    height: sizeWidth(18),
    borderRadius: sizeWidth(9)
  },
  left: {
    left: sizeWidth(-9)
  },
  right: {
    right: sizeWidth(-9)
  },
  top: {
    top: sizeWidth(-9)
  },
  line: {
    position: "absolute",
    width: "100%",
    height: 1
  },
  separator: {
    width: "100%",
    height: 1
  }
});
