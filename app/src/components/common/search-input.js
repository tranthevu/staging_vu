import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, TextInput, TouchableOpacity } from "react-native";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import { appColor } from "../../constants/app.constant";
import Text from "./text";

export default class SearchInput extends Component {
  render(): ReactNode {
    const {
      style,
      placeholder,
      onPress,
      onIconPress,
      value,
      onChangeText,
      iconStyle,
      inputStyle,
    } = this.props;
    const Root = onPress ? TouchableOpacity : View;
    const autoFocus = typeof this.props.autoFocus !== "undefined" ? this.props.autoFocus : true;
    return (
      <Root onPress={onPress} style={[styles.container, style]}>
        <View style={styles.body}>
          {onPress ? (
            <View style={styles.wrap}>
              <Text style={styles.text}>{value || placeholder}</Text>
            </View>
          ) : (
            <TextInput
              placeholder={placeholder}
              value={value}
              autoFocus={autoFocus}
              autoCapitalize="none"
              returnKeyType="search"
              onChangeText={onChangeText}
              onSubmitEditing={onIconPress}
              underlineColorAndroid="transparent"
              placeholderTextColor={appColor.text}
              style={[styles.input, inputStyle]}
            />
          )}

          <TouchableOpacity disabled={!!onPress} onPress={onIconPress}>
            <Image
              resizeMode="stretch"
              source={require("../../../res/icon/search.png")}
              style={[styles.image, iconStyle]}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.line} />
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: sizeWidth(260),
    height: moderateScale(34),
    paddingHorizontal: sizeWidth(14)
  },
  body: {
    flexDirection: "row",
    alignItems: "center"
  },
  line: {
    width: "100%",
    height: 1,
    backgroundColor: appColor.blur
  },
  image: {
    width: moderateScale(15),
    height: moderateScale(15),
    tintColor: appColor.icon
  },
  input: {
    fontSize: sizeFont(14),
    flex: 1,
    textAlign: "left",
    color: "#444444",
    height: moderateScale(34),
    marginRight: sizeWidth(14)
  },
  wrap: {
    flex: 1,
    height: moderateScale(34),
    marginRight: sizeWidth(14),
    justifyContent: "center"
  },
  text: {
    fontSize: sizeFont(14),
    color: "#444444"
  }
});
