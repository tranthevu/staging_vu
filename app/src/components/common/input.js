import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, TextInput } from "react-native";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Text from "./text";

export default class Input extends Component {
  focus = () => {
    if (this.ref) this.ref.focus();
  };

  render(): ReactNode {
    const {
      style,
      value,
      onChangeText,
      placeholder,
      secureTextEntry,
      right,
      label,
      labelStyle,
      inputStyle,
      editable,
      keyboardType
    } = this.props;
    return (
      <View style={[styles.container, style]}>
        {label && <Text style={styles.label}>{label}</Text>}
        <View style={styles.body}>
          <TextInput
            placeholder={placeholder}
            value={value}
            ref={ref => (this.ref = ref)}
            editable={editable}
            keyboardType={keyboardType}
            autoCapitalize="none"
            secureTextEntry={secureTextEntry}
            onChangeText={onChangeText}
            underlineColorAndroid="transparent"
            placeholderTextColor={appColor.placeholderTextColor}
            style={[styles.input, inputStyle]}
          />
          {right}
        </View>
        <View style={styles.line} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: sizeWidth(302),
    alignSelf: "center",
    paddingHorizontal: moderateScale(12),
    backgroundColor: "white",
    overflow: "hidden",
    alignItems: "center",
    marginVertical: moderateScale(8)
  },
  body: {
    flexDirection: "row"
  },
  line: {
    width: "100%",
    height: 1,
    marginTop: moderateScale(6),
    backgroundColor: appColor.blur
  },
  input: {
    fontSize: sizeFont(13),
    flex: 1,
    textAlign: "left",
    color: "#444444",
    padding: 0,
    fontFamily: font.regular
  },
  label: {
    fontSize: sizeFont(13),
    alignSelf: "flex-start",
    marginBottom: moderateScale(12),
    color: "#444444",
    fontFamily: font.bold
  }
});
