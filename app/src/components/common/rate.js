import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image } from "react-native";
import { sizeWidth, sizeFont } from "../../helpers/size.helper";
import { appColor } from "../../constants/app.constant";

export default class Rate extends Component {
  render(): ReactNode {
    const { style, rate, maxRate } = this.props;
    const elements = [];
    for (let index = 0; index < rate; index++) {
      elements.push(
        <Image
          key={index.toString()}
          source={require("../../../res/icon/rate.png")}
          style={styles.star}
        />
      );
    }
    for (let index = rate; index < maxRate; index++) {
      elements.push(
        <Image
          key={index.toString()}
          source={require("../../../res/icon/rate.png")}
          style={styles.inactive}
        />
      );
    }
    return <View style={[styles.container, style]}>{elements}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row"
  },
  star: {
    width: sizeWidth(15),
    height: sizeWidth(15),
    marginHorizontal: sizeWidth(1)
  },
  inactive: {
    width: sizeWidth(15),
    height: sizeWidth(15),
    tintColor: appColor.icon,
    marginHorizontal: sizeWidth(1)
  }
});
