import React, { Component, ReactNode } from "react";
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  FlatList,
  ScrollView,
  ActivityIndicator
} from "react-native";
import lodash from "lodash";
import Dialog, { FadeAnimation } from "react-native-popup-dialog";
import Icon from "react-native-vector-icons/AntDesign";
import {
  sizeWidth,
  sizeFont,
  sizeHeight,
  moderateScale
} from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Text from "./text";
import LoadingIndicator from "./loading-indicator";

export default class Multichoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showOptions: false
    };
  }

  render(): ReactNode {
    const {
      style,
      value,
      label,
      merchantLoading,
      placeholder,
      path,
      onPress
    } = this.props;

    return (
      <View style={[styles.container, style]}>
        <View style={styles.top}>
          {label && <Text style={styles.label}>{label}</Text>}
          {merchantLoading && (
            <View style={styles.loading}>
              <ActivityIndicator size="small" />
            </View>
          )}
        </View>
        <TouchableOpacity
          disabled={merchantLoading}
          onPress={onPress || this.showOptionPicker}
          style={styles.body}
        >
          {value.length > 0 ? (
            <View style={styles.selectedItems}>
              <ScrollView
                horizontal
                contentContainerStyle={styles.selectedItemsScroll}
                bounces={false}
                showsHorizontalScrollIndicator={false}
                scrollEnabled={false}
              >
                {value.map((item, index) =>
                  this.renderSelectedItem(lodash.get(item, path), index)
                )}
              </ScrollView>
            </View>
          ) : (
            <Text style={styles.value}>{placeholder}</Text>
          )}
          <Image
            style={styles.expand}
            source={require("../../../res/icon/arrow-down.png")}
          />
        </TouchableOpacity>
        <View style={styles.line} />
        {this.renderChoiceDialog()}
      </View>
    );
  }

  renderSelectedItem = (text, key) => {
    return (
      <View style={styles.selectedItem} key={key}>
        <Text style={styles.selectedItemText}>{text || " - "}</Text>
      </View>
    );
  };

  renderChoiceDialog = () => {
    const { showOptions } = this.state;
    const { options, path, value, merchantLoading } = this.props;
    return (
      <Dialog
        visible={showOptions}
        rounded={false}
        useNativeDriver
        dialogStyle={styles.dialog}
        containerStyle={styles.popup}
        width={sizeWidth(320)}
        onTouchOutside={() => {
          this.setState({ showOptions: false });
        }}
        dialogAnimation={
          new FadeAnimation({
            toValue: 0,
            animationDuration: 150,
            useNativeDriver: true
          })
        }
      >
        {merchantLoading ? (
          <View style={{ height: moderateScale(100), alignItems: "center" }}>
            <LoadingIndicator />
          </View>
        ) : !options || options.length == 0 ? (
          <View
            style={{
              height: moderateScale(100),
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Text style={styles.noResults}>
              Không có nhãn hàng nào phù hợp với tiêu chí bộ lọc
            </Text>
          </View>
        ) : (
          <FlatList
            keyExtractor={(item, index) => index.toString()}
            data={options}
            removeClippedSubviews
            initialNumToRender={10}
            getItemLayout={(data, index) => ({
              length: moderateScale(45),
              offset: moderateScale(45) * index,
              index
            })}
            extraData={value}
            renderItem={({ item, index }) => {
              const selected = value.find(e => e[path] === item[path]);
              return (
                <View>
                  <TouchableOpacity
                    onPress={() => this.selectItem(item)}
                    style={styles.item}
                  >
                    <Text numberOfLines={1} style={styles.text}>
                      {item[path]}
                    </Text>
                    {!!selected && (
                      <Icon name="check" color={appColor.blur} size={moderateScale(10)} />
                    )}
                    {!selected && <View />}
                  </TouchableOpacity>
                  <View style={styles.separator} />
                </View>
              );
            }}
          />
        )}
      </Dialog>
    );
  };

  selectItem = item => {
    const { onValueChange, value, path } = this.props;
    const isExist = value.find(e => e[path] === item[path]);
    if (!onValueChange) return;
    if (isExist) {
      onValueChange(value.filter(e => e[path] !== item[path]));
    } else {
      onValueChange([...value, item]);
    }
  };

  showOptionPicker = () => {
    this.setState({ showOptions: true });
  };
}

const styles = StyleSheet.create({
  body: {
    alignItems: "center",
    flexDirection: "row"
  },
  container: {
    alignSelf: "center",
    width: sizeWidth(302),
    paddingHorizontal: sizeWidth(12),
    backgroundColor: "white",
    marginVertical: sizeWidth(5)
  },
  value: {
    fontSize: sizeFont(13),
    flex: 1,
    fontFamily: font.regular
  },
  line: {
    width: "100%",
    height: 1,
    marginTop: sizeWidth(6),
    backgroundColor: appColor.blur
  },
  expand: {
    width: sizeWidth(10),
    marginLeft: sizeWidth(12),
    height: sizeWidth(6)
  },
  loading: {
    marginLeft: sizeWidth(12)
  },
  label: {
    fontFamily: font.medium,
    fontSize: sizeFont(13),
    flex: 1,
    marginVertical: sizeWidth(10)
  },
  top: {
    flexDirection: "row",
    alignItems: "center"
  },
  item: {
    height: moderateScale(45),
    flexDirection: "row",
    paddingHorizontal: sizeWidth(12),
    alignItems: "center"
  },
  text: {
    flex: 1
  },
  popup: { justifyContent: "flex-end" },
  dialog: {
    maxHeight: sizeHeight(400)
  },
  separator: {
    height: 1,
    backgroundColor: appColor.blur,
    width: sizeWidth(320)
  },
  check: {
    tintColor: appColor.blur,
    width: moderateScale(10),
    height: moderateScale(10),
    marginRight: sizeWidth(12)
  },
  noResults: {
    color: "#979797",
    fontSize: sizeFont(13),
    fontStyle: "italic"
  },
  selectedItems: {
    flexDirection: "row",
    flex: 1
  },
  selectedItemsScroll: {
    // paddingHorizontal: sizeWidth(10)
  },
  selectedItem: {
    paddingHorizontal: sizeWidth(9),
    height: sizeWidth(22),
    marginRight: sizeWidth(5),
    backgroundColor: "#FFF",
    borderWidth: 1,
    borderColor: "#DDD",
    borderRadius: sizeWidth(15),
    alignItems: "center",
    justifyContent: "center"
  },
  selectedItemText: {
    fontSize: sizeFont(13)
  }
});
