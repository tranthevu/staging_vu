import React, { PureComponent, ReactNode } from "react";
import { Switch, View, StyleSheet, TouchableOpacity } from "react-native";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Text from "./text";

export default class AppSwitch extends PureComponent {
  render(): ReactNode {
    const { label, onValueChange, value } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.label}>{label}</Text>
        <Switch value={value} onValueChange={onValueChange} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    height: moderateScale(42),
    backgroundColor: "white",
    borderRadius: sizeWidth(6),
    paddingHorizontal: sizeWidth(12),
    alignSelf: "center"
  },
  label: {
    fontSize: sizeFont(12),
    flex: 1,
    fontFamily: font.regular
  }
});
