import React, { Component } from "react";
import {
  FlatList,
  View,
  StyleSheet,
  ActivityIndicator,
  Platform,
  ScrollView,
  RefreshControl
} from "react-native";
import PropTypes from "prop-types";
import { sizeWidth } from "../../helpers/size.helper";
import { appColor } from "../../constants/app.constant";

export default class PaginationList extends Component {
  constructor(props: object) {
    super(props);
  }

  componentDidMount() {}

  onEndReached = () => {
    const { loading, reachedEnd, onEndReached } = this.props;
    if (!reachedEnd && !loading) {
      onEndReached();
    }
  };

  renderFooterComponent = () => {
    const { loading, firstLoading, refreshing } = this.props;
    if (loading && !firstLoading && !refreshing) {
      const size = Platform.OS === "ios" ? "large" : "large";
      return (
        <ActivityIndicator
          style={styles.loadingIndicator}
          animating
          size={size}
          color={this.props.indicatorColor}
        />
      );
    }
  };

  scrollToTop = () => {
    if (this.list) this.list.scrollToOffset({ offset: 0, animated: true });
  };

  render(): ReactNode {
    const {
      data,
      style,
      horizontal,
      showsHorizontalScrollIndicator,
      showsVerticalScrollIndicator,
      numColumns,
      getItemLayout,
      initialNumToRender,
      stickyHeaderIndices,
      onRefresh,
      EmptyComponent,
      ListHeaderComponent,
      keyExtractor,
      renderItem,
      onEndReachedThreshold,
      renderSeparator,
      inverted,
      firstLoading,
      refreshing,
      contentContainerStyle,
      bounces,
      extraData,
      scrollEnabled,
      onScroll,
      scrollEventThrottle
    } = this.props;
    if (firstLoading) {
      const size = Platform.OS === "ios" ? "large" : "large";
      return (
        <View style={styles.firstLoadingWrap}>
          <ActivityIndicator animating size={size} color={this.props.indicatorColor} />
        </View>
      );
    }

    if (Array.isArray(data) && data.length === 0 && EmptyComponent) {
      return (
        <ScrollView
          onScroll={onScroll}
          scrollEventThrottle={scrollEventThrottle}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              tintColor={this.props.indicatorColor}
              onRefresh={onRefresh}
            />
          }
          contentContainerStyle={contentContainerStyle}
        >
          {ListHeaderComponent}
          {EmptyComponent}
        </ScrollView>
      );
    }

    return (
      <FlatList
        style={style}
        ref={ref => (this.list = ref)}
        bounces={bounces}
        horizontal={horizontal}
        showsHorizontalScrollIndicator={showsHorizontalScrollIndicator}
        showsVerticalScrollIndicator={showsVerticalScrollIndicator}
        stickyHeaderIndices={stickyHeaderIndices}
        numColumns={numColumns}
        contentContainerStyle={contentContainerStyle}
        ListHeaderComponent={ListHeaderComponent}
        data={data}
        getItemLayout={getItemLayout}
        extraData={extraData}
        initialNumToRender={initialNumToRender}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            tintColor={this.props.indicatorColor}
            onRefresh={onRefresh}
          />
        }
        scrollEnabled={scrollEnabled}
        ItemSeparatorComponent={renderSeparator}
        onEndReached={this.onEndReached}
        onEndReachedThreshold={onEndReachedThreshold || 0.2}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
        ListFooterComponent={this.renderFooterComponent()}
        inverted={inverted}
        onScroll={onScroll}
        scrollEventThrottle={scrollEventThrottle}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  firstLoadingWrap: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  loadingIndicator: {
    marginVertical: sizeWidth(4)
  }
});

PaginationList.defaultProps = {
  indicatorColor: appColor.icon
};

PaginationList.propTypes = {
  numColumns: PropTypes.number,
  indicatorColor: PropTypes.string,
  renderSeparator: PropTypes.func,
  keyExtractor: PropTypes.func,
  renderItem: PropTypes.func,
  onEndReachedThreshold: PropTypes.number,
  EmptyComponent: PropTypes.element,
  ListHeaderComponent: PropTypes.element,
  onRefresh: PropTypes.func,
  inverted: PropTypes.bool
};
