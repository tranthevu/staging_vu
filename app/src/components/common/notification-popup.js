import React, { Component, ReactNode } from "react";
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  TextInput,
  ScrollView
} from "react-native";
import {
  sizeWidth,
  sizeFont,
  sizeHeight,
  moderateScale
} from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Text from "./text";
import lodash from "lodash";
import Button from "./button";
import HTML from "react-native-render-html";
import { logDebug } from "../../helpers/app.helper";

export default class NotificationPopup extends Component {
  render(): ReactNode {
    const { onConfirmNotification, notification, onClose } = this.props;
    const title = lodash.get(notification, "title");
    const body = lodash.get(notification, "body");
    logDebug("NotificationPopup", `title = ${title}, body = ${body}`);
    return (
      <View style={styles.root}>
        <TouchableOpacity
          onPress={onConfirmNotification}
          style={styles.container}
        >
          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            bounces={false}
            style={styles.content}
          >
            <HTML
              onLinkPress={onConfirmNotification}
              baseFontStyle={{
                fontFamily: font.regular,
                color: "black",
                fontSize: sizeFont(13)
              }}
              ignoredStyles={[
                "font-family",
                "font-weight",
                "letter-spacing",
                "line-height",
                "display",
                "font-size"
              ]}
              imagesMaxWidth={sizeWidth(300)}
              html={body}
            />
          </ScrollView>
        </TouchableOpacity>
        <TouchableOpacity onPress={onClose} style={styles.bottom}>
          <Image
            source={require("../../../res/icon/close-remove.png")}
            style={styles.x}
          />
          <Text style={styles.never}>Không hiển thị lại</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: sizeWidth(300),
    height: sizeWidth(400),
    borderRadius: sizeWidth(6),
    backgroundColor: "white",
    alignItems: "center"
  },
  root: {
    width: sizeWidth(300)
  },
  content: {
    maxHeight: sizeWidth(400)
  },
  overlay: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  bottom: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: moderateScale(10),
    justifyContent: "center"
  },
  x: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  never: {
    color: "white",
    marginLeft: sizeWidth(10),
    fontSize: sizeFont(12)
  }
});
