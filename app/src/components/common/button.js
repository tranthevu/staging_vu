import React, { Component } from "react";
import { View, StyleSheet, TouchableOpacity, ActivityIndicator } from "react-native";
import Text from "./text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import PropTypes from "prop-types";
import { font, appColor } from "../../constants/app.constant";

export default class Button extends Component {
  render() {
    const {
      disabled,
      text,
      style,
      textStyle,
      onPress,
      rightIcon,
      loading,
      leftIcon
    } = this.props;

    return (
      <TouchableOpacity
        disabled={disabled || loading}
        style={[styles.container, style]}
        onPress={onPress}
      >
        {(loading && this.renderLoading()) || leftIcon}
        <Text numberOfLines={1} style={[styles.text, textStyle]}>
          {text}
        </Text>
        {rightIcon}
      </TouchableOpacity>
    );
  }

  renderLoading = () => {
    const { loadingColor } = this.props;
    return (
      <View style={styles.loading}>
        <ActivityIndicator size="small" color={loadingColor || "white"} />
      </View>
    );
  };
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  textStyle: PropTypes.any,
  loading: PropTypes.bool
};

const styles = StyleSheet.create({
  container: {
    height: moderateScale(44),
    paddingHorizontal: sizeWidth(16),
    width: sizeWidth(302),
    backgroundColor: appColor.primary,
    alignSelf: "center",
    borderRadius: moderateScale(22),
    borderWidth: 1,
    borderColor: "white",
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "rgba(0.94, 0.25, 0.21, 0.2)",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 2
  },
  loading: {
    alignSelf: "center",
    position: "absolute",
    left: sizeWidth(12)
  },
  text: {
    textAlign: "center",
    color: "white",
    fontFamily: font.medium,
    fontSize: sizeFont(14)
  }
});
