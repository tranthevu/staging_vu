import React, { PureComponent, ReactNode } from "react";
import { Text, StyleSheet, View } from "react-native";
import { sizeFont, moderateScale } from "../../helpers/size.helper";
import { font, appColor, event } from "../../constants/app.constant";
import TabBarBottom from "./tab-bar-bottom";
import { connect } from "react-redux";
import { navigateToPage } from "../../actions/nav.action";
import EventRegister from "../../helpers/event-register.helper";
import AuthHelper from "../../helpers/auth.helper";

class TabBar extends PureComponent {
  constructor(props) {
    super(props);
  }

  render(): ReactNode {
    const { jumpToIndex, navigation } = this.props;
    return (
      <View style={styles.container}>
        <TabBarBottom
          {...this.props}
          jumpToIndex={async tabIndex => {
            const currentIndex = navigation.state.index;
            const isAuthenticated = await AuthHelper.isAuthenticated();
            if (
              !isAuthenticated &&
              (tabIndex === 1 || tabIndex === 2 || tabIndex === 3)
            ) {
              this.props.navigateToPage("Login", { canBack: true });
            } else {
              EventRegister.emit(event.jumpPage, tabIndex);
              jumpToIndex(tabIndex);
            }
          }}
        />
      </View>
    );
  }
}

export default connect(
  null,
  { navigateToPage }
)(TabBar);

const styles = StyleSheet.create({
  container: {
    height: moderateScale(44)
  }
});
