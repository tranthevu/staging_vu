import React, { PureComponent, ReactNode } from "react";
import { Text, StyleSheet, View, TouchableOpacity } from "react-native";
import { sizeFont, moderateScale } from "../../helpers/size.helper";
import { font, appColor, event } from "../../constants/app.constant";

export default class TabBarBottom extends PureComponent {
  navigationStateIndex = null;
  constructor(props) {
    super(props);
  }

  renderTabBarButton(route, idx) {
    const {
      activeTintColor,
      inactiveTintColor,
      navigation,
      getLabelText,
      renderIcon,
      jumpToIndex
    } = this.props;
    const currentIndex = navigation.state.index;
    const color = currentIndex === idx ? activeTintColor : inactiveTintColor;
    return (
      <TouchableOpacity
        onPress={() => jumpToIndex(idx)}
        style={styles.button}
        key={route.routeName}
      >
        {renderIcon({
          route,
          tintColor: color,
          focused: currentIndex === idx,
          index: idx
        })}
      </TouchableOpacity>
    );
  }

  render(): ReactNode {
    const { jumpToIndex, navigation } = this.props;
    const tabBarButtons = navigation.state.routes.map(
      this.renderTabBarButton.bind(this)
    );
    return <View style={styles.container}>{tabBarButtons}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    height: moderateScale(44),
    flexDirection: "row"
  },
  button: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
