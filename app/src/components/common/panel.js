import React, { Component, ReactNode } from "react";
import { View, StyleSheet } from "react-native";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import Text from "./text";
import { font } from "../../constants/app.constant";

export default class Panel extends Component {
  render(): ReactNode {
    const { label, children } = this.props;
    return (
      <View style={styles.container}>
        {!!label && <Text style={styles.label}>{label}</Text>}
        {children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: moderateScale(10),
    backgroundColor: "white"
  },
  label: {
    fontSize: sizeFont(12),
    fontFamily: font.medium,
    marginHorizontal: sizeWidth(12),
    marginTop: moderateScale(14),
    marginBottom: moderateScale(7)
  }
});
