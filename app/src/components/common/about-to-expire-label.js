import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { sizeFont, moderateScale } from "../../helpers/size.helper";
import Text from "./text";
import { font } from "../../constants/app.constant";

export default class AboutToExpireLabel extends Component {
  render() {
    return (
      <View style={styles.wrap}>
        <Text style={styles.text}>Sắp hết hạn</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrap: {
    position: "absolute",
    top: moderateScale(15),
    height: moderateScale(21),
    width: moderateScale(92),
    right: moderateScale(-22),
    backgroundColor: "#FFCA28",
    transform: [
      {
        rotate: "45deg"
      }
    ],
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    textAlign: "center",
    fontFamily: font.bold,
    color: "#564308",
    fontSize: sizeFont(11)
  }
});
