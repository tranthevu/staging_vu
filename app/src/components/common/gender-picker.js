import React, { Component } from "react";
import { View, Platform, TouchableOpacity, StyleSheet } from "react-native";
import Text from "./text";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import { appColor, font, gender } from "../../constants/app.constant";

export default class GenderPicker extends Component {
  render() {
    const { value } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Giới tính</Text>
        {value === gender.male
          ? this.renderCheck()
          : this.renderNormal(gender.male)}
        <Text style={styles.gender}>Nam</Text>
        {value === gender.female
          ? this.renderCheck()
          : this.renderNormal(gender.female)}
        <Text style={styles.gender}>Nữ</Text>
        {value === gender.other
          ? this.renderCheck()
          : this.renderNormal(gender.other)}
        <Text style={styles.gender}>Khác</Text>
      </View>
    );
  }

  renderCheck = () => {
    return (
      <TouchableOpacity style={styles.circle}>
        <View style={styles.dot} />
      </TouchableOpacity>
    );
  };

  renderNormal = gender => {
    const { onValueChange } = this.props;
    return (
      <TouchableOpacity
        onPress={() => onValueChange(gender)}
        style={styles.circle}
      />
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: sizeWidth(21),
    marginVertical: sizeWidth(4)
  },
  circle: {
    width: moderateScale(20),
    height: moderateScale(20),
    borderRadius: moderateScale(10),
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#DADADA"
  },
  dot: {
    width: moderateScale(12),
    height: moderateScale(12),
    borderRadius: moderateScale(6),
    backgroundColor: appColor.primary
  },
  gender: {
    color: "black",
    fontSize: sizeFont(13),
    marginLeft: sizeWidth(8),
    marginRight: sizeWidth(20)
  },
  label: {
    fontFamily: font.medium,
    fontSize: sizeFont(13),
    marginRight: sizeWidth(40)
  }
});
