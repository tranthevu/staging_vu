import React, { Component } from "react";
import {
  Image,
  TouchableWithoutFeedback,
  Dimensions,
  StyleSheet,
  Linking
} from "react-native";
import { connect } from "react-redux";
import { isIphoneX } from "react-native-iphone-x-helper";
import Draggable from "react-native-draggable";
import { moderateScale } from "../../helpers/size.helper";

const { width, height } = Dimensions.get("window");
class ChatbotIcon extends Component {
  render() {
    const { y, maxY } = this.props;
    return (
      <Draggable
        minX={0}
        minY={moderateScale(5)}
        maxX={width}
        maxY={maxY || height - moderateScale(isIphoneX() ? 105 : 60)}
        x={width - moderateScale(80)}
        z={1000}
        y={y || height - moderateScale(isIphoneX() ? 185 : 140)}
      >
        <TouchableWithoutFeedback onPress={this.navigateToMessenger} style={styles.desc}>
          <Image
            style={styles.icon}
            source={require("../../../res/icon/messenger.png")}
          />
        </TouchableWithoutFeedback>
      </Draggable>
    );
  }

  navigateToMessenger = () => {
    Linking.openURL("https://m.me/yollanetwork");
  };
}

export default connect(
  null,
  null
)(ChatbotIcon);

const styles = StyleSheet.create({
  desc: {
    justifyContent: "center",
    alignItems: "center"
  },
  icon: {
    width: moderateScale(58),
    height: moderateScale(58)
  }
});
