import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, Animated } from "react-native";
import Text from "./text";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import EventRegister from "../../helpers/event-register.helper";
import { event, text } from "../../constants/app.constant";
import validator from "validator";

export default class NotificationView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: text.emptyString
    };
    this.height = new Animated.Value(0);
  }

  componentDidMount = () => {
    this.shownNotificationEvent = EventRegister.on(
      event.shownNotification,
      this.onReceiveMessage
    );
  };

  register = () => {
    if (this.shownNotificationEvent) EventRegister.off(this.shownNotificationEvent);
    this.shownNotificationEvent = EventRegister.on(
      event.shownNotification,
      this.onReceiveMessage
    );
  };

  componentWillUnmount = () => {
    EventRegister.off(this.shownNotificationEvent);
  };

  onReceiveMessage = message => {
    this.setState({ message }, () => {
      Animated.timing(this.height, {
        toValue: moderateScale(45),
        duration: 300
      }).start();
    });
    if (this.showTimeout) clearTimeout(this.showTimeout);
    this.showTimeout = setTimeout(() => {
      Animated.timing(this.height, {
        toValue: 0,
        duration: 300
      }).start(() => {
        this.setState({ message: text.emptyString });
        this.showTimeout = null;
      });
    }, 3000);
  };

  render(): ReactNode {
    const { message } = this.state;
    if (validator.isEmpty(message)) return null;
    return (
      <Animated.View
        style={[
          styles.container,
          {
            height: this.height
          }
        ]}
      >
        <Image
          resizeMode="stretch"
          style={styles.image}
          source={require("../../../res/icon/notif-info.png")}
        />
        <Text style={styles.text}>{message}</Text>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#FDDD62",
    overflow: "hidden",
    justifyContent: "center",
    position: "absolute",
    top: 0,
    width: sizeWidth(320)
  },
  image: {
    width: moderateScale(18),
    height: moderateScale(18),
    marginRight: sizeWidth(11)
  },
  text: {
    color: "#653A00",
    fontSize: sizeFont(13)
  }
});
