import React, { PureComponent } from "react";
import { Image, View } from "react-native";
import styles from "./styles";
import { IMG_LINE } from "../../commons/images";

export default class BreakLine extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <View style={[styles.circle, styles.left]} />
        <Image style={styles.line} source={IMG_LINE} />
        <View style={[styles.circle, styles.right]} />
      </View>
    );
  }
}
