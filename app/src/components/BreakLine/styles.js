import { StyleSheet } from "react-native";
import { sizeWidth } from "../../helpers/size.helper";
import { appColor } from "../../constants/app.constant";

export default StyleSheet.create({
  line: {
    width: "100%",
    height: 1
  },
  circle: {
    backgroundColor: appColor.bg,
    width: sizeWidth(18),
    height: sizeWidth(18),
    borderRadius: sizeWidth(9),
    position: "absolute",
    zIndex: 1,
    overflow: "hidden"
  },
  left: {
    left: 0
  },

  right: {
    right: 0
  },
  container: {
    justifyContent: "center"
  }
});
