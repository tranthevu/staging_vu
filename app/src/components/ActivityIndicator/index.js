import React from "react";
import ReactNative, { StyleSheet, ViewPropTypes } from "react-native";
import PropTypes from "prop-types";
import { appColor } from "../../constants/app.constant";
import { moderateScale } from "../../helpers/size.helper";

const ActivityIndicator = ({ style, color }) => {
  return (
    <ReactNative.ActivityIndicator
      color={color || appColor.primary}
      style={[styles.container, style]}
    />
  );
};

export default ActivityIndicator;
// eslint-disable-next-line no-undef

ActivityIndicator.defaultProps = {
  style: PropTypes.object
};
ActivityIndicator.propTypes = {
  style: ViewPropTypes.style
};

const styles = StyleSheet.create({
  container: {
    alignSelf: "center",
    paddingVertical: moderateScale(8),
    paddingHorizontal: moderateScale(15)
  }
});
