import { StyleSheet } from "react-native";
import { moderateScale, sizeFont } from "../../../helpers/size.helper";
import { appColor, font } from "../../../constants/app.constant";

export default StyleSheet.create({
  container: {
    borderRadius: moderateScale(6),
    minHeight: moderateScale(50),
    overflow: "hidden",
    flexDirection: "row",
    backgroundColor: appColor.white,
    alignItems: "center"
  },
  left: {
    justifyContent: "center",
    alignItems: "center",
    borderRadius: moderateScale(6),
    width: moderateScale(40)
  },
  content: {
    backgroundColor: "white",
    flex: 1,
    borderRadius: moderateScale(6)
  },
  img: {
    width: moderateScale(19),
    height: moderateScale(19),
    tintColor: appColor.gray
  },
  title: {
    fontFamily: font.regular,
    fontSize: sizeFont(12),
    flex: 1,
    paddingRight: moderateScale(8)
  }
});
