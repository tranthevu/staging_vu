/* eslint-disable no-plusplus */
import React, { PureComponent } from "react";
import { View, Image } from "react-native";
import styles from "./styles";
import { IC_VECTOR_LOCATION } from "../../../commons/images";
import AppText from "../../common/text";

export default class AddressItem extends PureComponent {
  render() {
    const { style, address } = this.props;
    return (
      <View style={[styles.container, style]}>
        <View style={styles.left}>
          <Image resizeMode="contain" source={IC_VECTOR_LOCATION} style={styles.img} />
        </View>
        <AppText numberOfLines={4} style={styles.title}>
          {address}
        </AppText>
      </View>
    );
  }
}
