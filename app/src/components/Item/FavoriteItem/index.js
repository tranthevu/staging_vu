/* eslint-disable no-plusplus */
import React, { PureComponent } from "react";
import { TouchableOpacity, View, Image } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import {
  IC_HEART,
  IC_CLOSE,
  IC_CLOSE_RED,
  IC_CHECK_GREEN,
  IC_PLUS_GRAY
} from "../../../commons/images";
import AppText from "../../common/text";

export default class FavoriteItem extends PureComponent {
  static getDerivedStateFromProps(props, state) {
    if (props.isChecked !== state.prevIsChecked) {
      return { prevIsChecked: props.isChecked, isChecked: props.isChecked };
    }
    return null;
  }

  constructor(props) {
    super(props);
    this.state = {
      isChecked: false
    };
  }

  onPressToggle = isCheck => () => {
    const { onPressToggle, item } = this.props;
    if (typeof onPressToggle === "function") {
      onPressToggle(item, isCheck);
    }
  };

  NodeWhite = () => {
    const container = [];
    const Node = <View style={styles.nodeWhite} />;
    for (let index = 0; index < 12; index++) {
      container.push(Node);
    }
    return <View style={styles.node}>{container}</View>;
  };

  render() {
    const { isChecked } = this.state;
    const { style, item } = this.props;
    const { name } = item || {};
    return (
      <View style={[styles.container, style]}>
        <View style={styles.left}>
          <Image resizeMode="contain" source={IC_HEART} style={styles.img} />
          <this.NodeWhite />
        </View>
        <LinearGradient
          colors={["rgba(0, 0, 0, 0)", "rgba(0, 0, 0, 0.08)"]}
          style={styles.overlay}
        >
          <View style={styles.content}>
            <View style={styles.center}>
              <AppText numberOfLines={2} style={styles.txt}>
                Thêm
                <AppText numberOfLines={2} style={styles.title}>
                  {` "${name}" `}
                </AppText>
                vào mục yêu thích
              </AppText>
            </View>
            <TouchableOpacity
              onPress={this.onPressToggle(isChecked)}
              style={styles.right}
            >
              <Image
                resizeMode="contain"
                source={isChecked ? IC_CHECK_GREEN : IC_PLUS_GRAY}
                style={styles.imgClose}
              />
            </TouchableOpacity>
          </View>
        </LinearGradient>
      </View>
    );
  }
}
