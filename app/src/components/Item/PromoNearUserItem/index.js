/* eslint-disable no-plusplus */
import React, { PureComponent } from "react";
import { TouchableOpacity, View, Image } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import { IC_CHEVRON, IC_VECTOR_LOCATION } from "../../../commons/images";
import AppText from "../../common/text";
import { TYPE_ACTION } from "../../../constants/app.constant";

export default class PromoNearUserItem extends PureComponent {
  onPress = type => () => {
    const { onPress, item, onCheckGPSLocation } = this.props;
    if (typeof onPress === "function") {
      if (typeof onCheckGPSLocation === "function") {
        onCheckGPSLocation(() => {
          onPress(item, type);
        });
      } else {
        onPress(item, type);
      }
    }
  };

  NodeWhite = () => {
    const container = [];
    for (let index = 0; index < 12; index++) {
      container.push(<View style={styles.nodeWhite} key={index} />);
    }
    return <View style={styles.node}>{container}</View>;
  };

  render() {
    const { style } = this.props;
    return (
      <TouchableOpacity
        onPress={this.onPress(TYPE_ACTION.PRESS)}
        style={[styles.container, style]}
      >
        <View style={styles.left}>
          <Image resizeMode="contain" source={IC_VECTOR_LOCATION} style={styles.img} />
          <this.NodeWhite />
        </View>
        <LinearGradient
          colors={["rgba(0, 0, 0, 0)", "rgba(0, 0, 0, 0.08)"]}
          style={styles.overlay}
        >
          <View style={styles.content}>
            <View style={styles.center}>
              <AppText numberOfLines={2} style={styles.title}>
                Ưu đãi gần bạn
              </AppText>
              <AppText numberOfLines={2} style={styles.subTitle}>
                Khám phá ưu đãi xung quanh bạn
              </AppText>
            </View>
            <View style={styles.right}>
              <Image resizeMode="contain" source={IC_CHEVRON} style={styles.imgClose} />
            </View>
          </View>
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}
