import { StyleSheet } from "react-native";
import { moderateScale, sizeFont } from "../../../helpers/size.helper";
import { appColor, font } from "../../../constants/app.constant";

export default StyleSheet.create({
  container: {
    borderRadius: moderateScale(6),
    height: moderateScale(61),
    overflow: "hidden",
    flexDirection: "row"
  },
  left: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: appColor.green,
    borderRadius: moderateScale(6),
    width: moderateScale(60),
    height: moderateScale(60)
  },
  overlay: {
    flex: 1,
    borderRadius: moderateScale(6),
    paddingBottom: moderateScale(3)
  },
  content: {
    backgroundColor: "white",
    flex: 1,
    flexDirection: "row",
    borderRadius: moderateScale(6)
  },
  img: {
    width: moderateScale(19),
    height: moderateScale(19)
  },
  title: {
    fontFamily: font.bold,
    fontSize: sizeFont(12),
    paddingLeft: moderateScale(11)
  },
  center: {
    justifyContent: "center",
    flex: 1
  },
  right: {
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: moderateScale(14)
  },
  imgClose: {
    width: moderateScale(25),
    height: moderateScale(25)
  },
  subTitle: {
    fontFamily: font.regular,
    fontSize: sizeFont(12),
    paddingLeft: moderateScale(11),
    color: "#979797"
  },
  nodeWhite: {
    width: moderateScale(2),
    height: moderateScale(2),
    backgroundColor: "white",
    marginVertical: moderateScale(1)
  },
  node: {
    justifyContent: "center",
    alignItems: "center",
    height: "50%",
    position: "absolute",
    alignSelf: "flex-end"
  }
});
