import { StyleSheet } from "react-native";
import { sizeFont, sizeWidth, moderateScale } from "../../../helpers/size.helper";
import { font } from "../../../constants/app.constant";

export default StyleSheet.create({
  container: {
    backgroundColor: "white",
    width: sizeWidth(146),
    height: sizeWidth(160),
    borderRadius: sizeWidth(6),
    overflow: "hidden",
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    marginHorizontal: sizeWidth(4)
  },
  image: {
    width: sizeWidth(146),
    borderTopLeftRadius: sizeWidth(6),
    borderTopRightRadius: sizeWidth(6),
    height: sizeWidth(110),
    overflow: "hidden"
  },
  content: {
    paddingVertical: sizeWidth(8),
    paddingHorizontal: sizeWidth(14)
  },
  title: {
    fontSize: sizeFont(12),
    fontFamily: font.bold,
    color: "black"
  },
  count: {
    fontSize: sizeFont(12),
    marginTop: moderateScale(4),
    fontFamily: font.lightItalic,
    color: "black"
  }
});
