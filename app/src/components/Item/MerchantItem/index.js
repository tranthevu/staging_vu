import React, { PureComponent } from "react";
import { View, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { navigateToPage } from "../../../actions/nav.action";

import CacheImage from "../../common/cache-image";
import Text from "../../common/text";
import styles from "./styles";

class MerchantItem extends PureComponent {
  render() {
    const { style, item } = this.props;
    return (
      <TouchableOpacity
        onPress={this.navigateToMerchantDetail}
        style={[styles.container, style]}
      >
        <CacheImage style={styles.image} uri={item.logo_image} />
        <View style={styles.content}>
          <Text numberOfLines={1} style={styles.title}>
            {item.name}
          </Text>
          <Text numberOfLines={1} style={styles.count}>
            Có {item.product_count} ưu đãi
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  navigateToMerchantDetail = () => {
    const { item } = this.props;
    this.props.navigateToPage("MerchantDetail", { merchantCode: item.code });
  };
}
export default connect(
  null,
  { navigateToPage }
)(MerchantItem);
