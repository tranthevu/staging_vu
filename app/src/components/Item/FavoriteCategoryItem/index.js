/* eslint-disable no-plusplus */
import React, { PureComponent } from "react";
import { TouchableOpacity, View, Image } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import styles from "./styles";
import { IC_HEART, IC_CLOSE_RED } from "../../../commons/images";
import AppText from "../../common/text";
import { TYPE_ACTION } from "../../../constants/app.constant";

export default class FavoriteCategoryItem extends PureComponent {
  onPress = type => () => {
    const { onPress, item } = this.props;
    if (typeof onPress === "function") {
      onPress(item, type);
    }
  };

  NodeWhite = () => {
    const container = [];
    const Node = <View style={styles.nodeWhite} />;
    for (let index = 0; index < 12; index++) {
      container.push(Node);
    }
    return <View style={styles.node}>{container}</View>;
  };

  render() {
    const { item = {} } = this.props;
    const { name = "" } = item;
    return (
      <TouchableOpacity
        onPress={this.onPress(TYPE_ACTION.PRESS)}
        style={styles.container}
      >
        <View style={styles.left}>
          <Image resizeMode="contain" source={IC_HEART} style={styles.img} />
          <this.NodeWhite />
        </View>
        <LinearGradient
          colors={["rgba(0, 0, 0, 0)", "rgba(0, 0, 0, 0.08)"]}
          style={styles.overlay}
        >
          <View style={styles.content}>
            <View style={styles.center}>
              <AppText numberOfLines={2} style={styles.title}>
                {name}
              </AppText>
            </View>
            <TouchableOpacity
              onPress={this.onPress(TYPE_ACTION.DELETE)}
              style={styles.right}
            >
              <Image resizeMode="contain" source={IC_CLOSE_RED} style={styles.imgClose} />
            </TouchableOpacity>
          </View>
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}
