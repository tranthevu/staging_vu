/* eslint-disable no-nested-ternary */
import React, { PureComponent } from "react";
import { Text, View, Image, TouchableOpacity } from "react-native";
import styles from "./styles";
import { IC_CHEVRON, IMG_DEFAULT_IMAGE } from "../../../commons/images";

export default class MerchantInfoItem extends PureComponent {
  onPress = () => {
    const { onPress, item } = this.props;
    if (typeof onPress === "function") {
      onPress(item);
    }
  };

  render() {
    const { children, item, style, ...otherProps } = this.props;
    const { name, content, image, isBranch, sourceImg } = item || {};
    const source = isBranch ? sourceImg : image ? { uri: image } : IMG_DEFAULT_IMAGE;
    return (
      <TouchableOpacity
        {...this.props}
        onPress={this.onPress}
        style={[styles.infoBox, style]}
      >
        <View style={styles.avatarInfoBox}>
          <Image source={source} style={styles.avatarInfoBox} />
        </View>
        <View style={styles.rightView}>
          <Text numberOfLines={4} style={styles.title}>
            {name}
          </Text>
          <View style={styles.iconChevron}>
            <Image source={IC_CHEVRON} resizeMode="contain" style={styles.iconChevron} />
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

MerchantInfoItem.defaultProps = {
  item: {
    // name: "Tên Merchant",
    // content: "Có # ưu đãi mới"
    // image:
    //   "https://previews.123rf.com/images/triken/triken1608/triken160800029/61320775-male-avatar-profile-picture-default-user-avatar-guest-avatar-simply-human-head-vector-illustration-i.jpg"
  }
};
