import { StyleSheet } from "react-native";
import { moderateScale, sizeWidth, sizeFont } from "../../../helpers/size.helper";
import { appColor, font } from "../../../constants/app.constant";

export default StyleSheet.create({
  infoBox: {
    flex: 1,
    paddingVertical: sizeWidth(6),
    paddingHorizontal: sizeWidth(6),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: sizeWidth(6),
    overflow: "hidden",
    backgroundColor: "white"
  },
  title: {
    fontFamily: font.bold,
    fontSize: sizeFont(12),
    color: appColor.text,
    textAlignVertical: "center",
    flex: 1
  },
  txtContent: {
    fontFamily: font.lightItalic,
    fontSize: sizeFont(12),
    color: appColor.text,
    marginLeft: sizeWidth(6)
  },
  rightView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    flex: 1,
    paddingHorizontal: sizeWidth(6)
  },
  avatarInfoBox: {
    backgroundColor: appColor.gray,
    borderRadius: sizeWidth(20),
    width: sizeWidth(40),
    height: sizeWidth(40),
    justifyContent: "center",
    alignItems: "center"
  },
  leftContentInfo: {
    justifyContent: "center",
    flex: 1
  },
  iconChevron: {
    width: moderateScale(20),
    height: moderateScale(20)
  }
});
