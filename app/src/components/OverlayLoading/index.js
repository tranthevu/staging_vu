/* eslint-disable no-unused-expressions */
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  ActivityIndicator,
  View,
  StyleSheet,
  Dimensions,
  Text,
  BackHandler
} from "react-native";
import { appColor } from "../../constants/app.constant";

const screen = Dimensions.get("screen");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: screen.width,
    height: screen.height,
    backgroundColor: "rgba(0, 0, 0, 0.3)",
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    zIndex: 1
  },

  content: {
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 8,
    overflow: "hidden",
    padding: 16
  },
  textContent: {
    fontSize: 15,
    color: "#fff",
    textAlign: "center",
    marginTop: 8
  },
  loadingIcon: {
    width: 24,
    height: 24,
    marginLeft: 5,
    marginRight: 5
  }
});

export default class Loading extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.isVisible) {
      this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
        return true;
      });
    } else {
      this.backHandler && this.backHandler.remove();
    }
  }

  componentWillUnmount() {
    this.backHandler && this.backHandler.remove();
  }

  render() {
    const { text, isVisible, contentComponent } = this.props;
    if (isVisible) {
      return (
        <View style={styles.container}>
          <View style={styles.row}>
            {contentComponent ? (
              contentComponent()
            ) : (
              <View style={styles.content}>
                <ActivityIndicator size="large" color={appColor.primary} />
                {text && <Text style={styles.textContent}>{text}</Text>}
              </View>
            )}
          </View>
        </View>
      );
    }
    return null;
  }
}

Loading.propTypes = {
  text: PropTypes.string,
  isVisible: PropTypes.bool.isRequired,
  contentComponent: PropTypes.func
};
