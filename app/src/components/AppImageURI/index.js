import React, { PureComponent } from "react";
import { View, Image } from "react-native";
import { IMG_NO_IMAGE } from "../../commons/images";
import styles from "./styles";

export default class AppImageURI extends PureComponent {
  render() {
    const { uri, source, style, ...otherProps } = this.props;
    if (!uri) {
      return (
        <View style={[styles.container, style]}>
          <Image style={[style]} resizeMode="center" source={IMG_NO_IMAGE} />
        </View>
      );
    }
    return <Image style={style} {...otherProps} source={{ uri }} />;
  }
}
