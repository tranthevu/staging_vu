import { StyleSheet } from "react-native";

export default StyleSheet.create({
  container: {
    backgroundColor: "red",
    flex: 1,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center"
  },
  imgEmpty: {
    width: "100%",
    height: "100%"
  }
});
