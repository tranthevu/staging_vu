import { StyleSheet } from "react-native";
import { moderateScale, sizeFont, sizeWidth } from "../../../helpers/size.helper";
import { font, appColor } from "../../../constants/app.constant";

export default StyleSheet.create({
  container: {
    alignSelf: "center",
    backgroundColor: "transparent",
    width: sizeWidth(146),
    borderRadius: sizeWidth(6),
    overflow: "hidden",
    marginVertical: sizeWidth(3),
    marginHorizontal: sizeWidth(5)
  },
  header: {
    width: "100%",
    borderRadius: sizeWidth(6),
    overflow: "hidden"
  },
  logo: {
    width: moderateScale(26),
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "white",
    height: moderateScale(26),
    borderRadius: moderateScale(13)
  },
  cover: {
    width: "100%",
    height: sizeWidth(146)
  },
  info: {
    flexDirection: "row",
    height: moderateScale(36),
    width: sizeWidth(146),
    borderRadius: sizeWidth(6),
    paddingHorizontal: moderateScale(5),
    alignItems: "center",
    position: "absolute",
    bottom: 0
  },
  name: {
    flex: 1,
    marginLeft: sizeWidth(5),
    color: "white",
    fontSize: sizeFont(13),
    fontFamily: font.bold
  },
  top: {
    backgroundColor: "white",
    paddingVertical: moderateScale(8),
    paddingHorizontal: moderateScale(10)
  },
  label: {
    color: "#000000",
    fontFamily: font.medium
  },
  event: {
    fontSize: sizeFont(12),
    alignSelf: "center",
    color: appColor.blur
  },
  title: {
    color: "#444444",
    fontWeight: "500",
    height: moderateScale(32),
    fontSize: sizeFont(12)
  },
  prices: {
    flexDirection: "row",
    marginTop: moderateScale(6),
    height: moderateScale(20),
    alignItems: "flex-end"
  },
  bottom: {
    backgroundColor: "white",
    paddingHorizontal: sizeWidth(16),
    paddingVertical: sizeWidth(12)
  },
  content: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: sizeWidth(8)
  },
  icon: {
    width: moderateScale(13),
    height: moderateScale(13),
    marginRight: moderateScale(8)
  },
  line: {
    width: "94%",
    alignSelf: "center",
    height: 1
  },
  desc: {
    fontSize: sizeFont(12)
  },
  overlay: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  shadow: {
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 1,
    shadowRadius: 4,
    marginBottom: 4,
    elevation: 2,
    height: 2,
    backgroundColor: "white",
    width: "100%"
  },
  discount: {
    fontSize: sizeFont(11),
    color: "white",
    fontFamily: font.bold,
    marginLeft: sizeWidth(3)
  },
  tag: {
    width: sizeWidth(41),
    height: moderateScale(20),
    marginRight: sizeWidth(7),
    alignSelf: "center",
    justifyContent: "center"
  },
  body: {
    borderTopLeftRadius: sizeWidth(6),
    borderTopRightRadius: sizeWidth(6),
    overflow: "hidden"
  }
});
