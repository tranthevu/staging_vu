import React, { PureComponent } from "react";
import { View, Image, ImageBackground, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import moment from "moment";
import LinearGradient from "react-native-linear-gradient";
import { productClass } from "../../../constants/app.constant";
import Text from "../../common/text";
import { resetPage, navigateToPage } from "../../../actions/nav.action";
import OnlineLabel from "../../common/online-label";
import ExpireLabel from "../../common/expire-label";
import ExclusiveLabel from "../../common/exclusive-label";
import {
  IMG_DEFAULT_OFFER,
  IMG_DEFAULT_LOCATION,
  IMG_DISCOUNT_TAG
} from "../../../commons/images";
import { getProductType } from "../../../helpers/common.helper";
import styles from "./styles";

class CardOffer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  navigateToMerchantDetail = () => {
    const { item = {} } = this.props;
    const merchantCode = lodash.get(item, "merchant.code");
    this.props.navigateToPage("MerchantDetail", { merchantCode });
  };

  render() {
    const { style, item = {}, hideMerchant } = this.props;
    const logo = lodash.get(item, "merchant.logo_image.full");
    const merchant = lodash.get(item, "merchant.name");
    const cover = lodash
      .get(item, "images", [])
      .find(item => item.display_order === 0 && item.caption === "1:1");
    const endDate = moment(lodash.get(item, "attributes.date_valid_end"));
    const now = moment();
    const diff = endDate.diff(now, "days");
    const shortDescription = lodash.get(item, "attributes.short_description");
    const isExpired = lodash.get(item, "isExpired");
    const discountPercent = lodash.get(item, "dealset.percent_discount", 0);
    const isExclusive = lodash.get(item, "attributes.is_exclusive_offer");
    return (
      <TouchableOpacity
        onPress={this.navigateToOfferDetail}
        style={[styles.container, style]}
      >
        <View>
          <View style={styles.header}>
            <Image
              style={styles.cover}
              source={
                lodash.get(cover, "original.thumbnail")
                  ? { uri: cover.original.thumbnail }
                  : IMG_DEFAULT_OFFER
              }
            />
            {!!merchant && !hideMerchant && (
              <View style={styles.info}>
                <LinearGradient
                  colors={["rgba(0, 0, 0, 0)", "rgba(0, 0, 0, 0.5)"]}
                  style={styles.overlay}
                />
                <TouchableOpacity onPress={this.navigateToMerchantDetail}>
                  <Image
                    style={styles.logo}
                    source={logo ? { uri: logo } : IMG_DEFAULT_LOCATION}
                  />
                </TouchableOpacity>
                <Text style={styles.name}>{merchant}</Text>
              </View>
            )}
          </View>
          <View style={styles.body}>
            <View style={styles.top}>
              <Text numberOfLines={2} style={styles.title}>
                {item.title}
              </Text>
              <View style={styles.prices}>
                {!!discountPercent && (
                  <ImageBackground
                    style={styles.tag}
                    resizeMode="stretch"
                    source={IMG_DISCOUNT_TAG}
                  >
                    <Text style={styles.discount}>-{discountPercent}%</Text>
                  </ImageBackground>
                )}
                <Text style={styles.event}>{getProductType(item.product_class)}</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.shadow} />
        {item.product_class === productClass.affiliateOffer && <OnlineLabel />}
        {item.product_class === productClass.voucher && isExclusive && <ExclusiveLabel />}
        {isExpired && <ExpireLabel />}
      </TouchableOpacity>
    );
  }

  navigateToOfferDetail = () => {
    const { item } = this.props;
    this.props.navigateToPage("OfferDetail", { productId: item.id, productSlug: item.slug });
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(CardOffer);
