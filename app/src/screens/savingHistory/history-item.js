import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import moment from "moment";
import numeral from "numeral";
import lodash from "lodash";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { font, appColor, dateTimeFormat } from "../../constants/app.constant";
import SeparatorLine from "../../components/common/separator-line";
import CacheImage from "../../components/common/cache-image";

export default class HistoryItem extends Component {
  render() {
    const { item } = this.props;
    const timeUsed = moment(item.time_used).format(dateTimeFormat.ddmmyyyyhhmm);
    const savingMoney = numeral(parseFloat(item.saving_money)).format("0,0");
    const cover = lodash.get(item, "product_img[0].original.thumbnail");
    const merchantName = lodash.get(item, "merchant.name");
    const merchantLogo = lodash.get(item, "merchant.logo_image.thumbnail");
    return (
      <View style={styles.container}>
        <View style={styles.top}>
          <CacheImage style={styles.cover} uri={cover} />
          <View style={styles.body}>
            <Text numberOfLines={1} style={styles.title}>
              {item.code} - {timeUsed}
            </Text>
            <Text numberOfLines={2} style={styles.content}>
              {item.product_title}
            </Text>
          </View>
        </View>
        <View style={styles.bottom}>
          <CacheImage uri={merchantLogo} style={styles.avatar} />
          <Text numberOfLines={1} style={styles.merchant}>
            {merchantName}
          </Text>
          <Text style={styles.saved}>Tiết kiệm</Text>
          <Text style={styles.amount}>
            {savingMoney}
            <Text style={styles.unit}>đ</Text>
          </Text>
        </View>
        <SeparatorLine top={moderateScale(70)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    marginVertical: moderateScale(5),
    marginHorizontal: sizeWidth(9),
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    }
  },
  bottom: {
    flexDirection: "row",
    paddingHorizontal: sizeWidth(12),
    paddingVertical: sizeWidth(8),
    alignItems: "center"
  },
  avatar: {
    width: moderateScale(16),
    height: moderateScale(16),
    borderRadius: moderateScale(8),
    marginRight: sizeWidth(4)
  },
  merchant: {
    flex: 1,
    fontSize: sizeFont(12)
  },
  saved: {
    fontSize: sizeFont(12),
    marginHorizontal: sizeWidth(12)
  },
  amount: {
    color: appColor.primary,
    fontFamily: font.bold,
    fontSize: sizeFont(14)
  },
  unit: {
    color: appColor.primary,
    fontFamily: font.bold,
    fontSize: sizeFont(14),
    textDecorationLine: "underline"
  },
  top: {
    paddingVertical: moderateScale(10),
    paddingHorizontal: sizeWidth(12),
    flexDirection: "row",
    height: moderateScale(79)
  },
  cover: {
    width: moderateScale(60),
    height: moderateScale(60),
    borderRadius: moderateScale(6),
    backgroundColor: "#BDBDBD"
  },
  body: {
    flex: 1,
    marginLeft: sizeWidth(10)
  },
  title: {
    fontSize: sizeFont(12),
    color: appColor.blur
  },
  content: {
    fontSize: sizeFont(12),
    fontFamily: font.medium,
    marginTop: moderateScale(9)
  }
});
