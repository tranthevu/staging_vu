import React, { Component, ReactNode } from "react";
import { View, StyleSheet, FlatList } from "react-native";
import { connect } from "react-redux";
import _ from "lodash";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import Category from "../dashboard/category";
import PaginationList from "../../components/common/pagination-list";
import PromotionItem from "../dashboard/promotion-item";
import PromotionItemList from "../dashboard/promotion-item-list";
import {
  requestOnlineProducts,
  fetchMoreOnlineProducts,
  refreshOnlineProducts
} from "../../actions/online-products.action";
import Api from "../../api/api";
import EmptyView from "../../components/common/empty-view";

const viewType = {
  grid: 0,
  list: 1
};

class OnlinePromoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedViewType: viewType.list
    };
  }

  navigateToSearch = () => {
    this.props.navigateToPage("Search", {
      category: "uu-dai-online"
    });
  };

  renderHeader = () => {
    // const { topCategories } = this.props.topCategories;
    return (
      <View>
        {/* <View style={styles.categories}>
          <FlatList
            data={topCategories}
            horizontal
            contentContainerStyle={styles.top}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderCategory}
          />
        </View> */}
        {this.renderTitle()}
      </View>
    );
  };

  renderRecentProducts = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      onlineProducts
    } = this.props.onlineProducts;
    const { fetchMoreOnlineProducts, refreshOnlineProducts } = this.props;
    return (
      <PaginationList
        ref={ref => (this.list = ref)}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderPromotion}
        loading={loading}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        firstLoading={firstLoading}
        ListHeaderComponent={this.renderHeader()}
        onEndReached={() => fetchMoreOnlineProducts()}
        onRefresh={() => refreshOnlineProducts()}
        data={onlineProducts}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  renderCategory = ({ item }) => {
    return <Category item={item} />;
  };

  renderTitle = () => {
    const { selectedViewType } = this.state;
    return (
      <View style={styles.bar}>
        <Text style={styles.label}>Ưu đãi mới</Text>
        <TouchableIcon
          style={styles.view}
          iconStyle={styles.viewIcon}
          onPress={this.toggleViewType}
          source={
            selectedViewType === viewType.list
              ? require("../../../res/icon/list.png")
              : require("../../../res/icon/grid.png")
          }
        />
      </View>
    );
  };

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  renderPromotion = ({ item }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem item={item} />
    ) : (
      <PromotionItemList item={item} />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Toolbar
          leftStyle={[styles.part, styles.left]}
          rightStyle={[styles.part, styles.right]}
          center={<Text style={styles.header}>Ưu đãi online</Text>}
          left={<BackIcon />}
          right={
            <View style={styles.right}>
              {/* <TouchableIcon
                style={styles.wrap}
                iconStyle={styles.icon}
                source={require("../../../res/icon/sort.png")}
              /> */}
              <TouchableIcon
                style={styles.wrap}
                onPress={this.navigateToSearch}
                iconStyle={styles.icon}
                source={require("../../../res/icon/home-search.png")}
              />
            </View>
          }
        />
        {this.renderRecentProducts()}
      </View>
    );
  }

  componentDidMount = async () => {
    this.props.requestOnlineProducts();
  };
}

export default connect(
  state => ({
    onlineProducts: state.onlineProducts,
    topCategories: state.topCategories
  }),
  {
    resetPage,
    navigateToPage,
    fetchMoreOnlineProducts,
    refreshOnlineProducts,
    requestOnlineProducts
  }
)(OnlinePromoScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  icon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: "white"
  },
  wrap: {
    marginLeft: moderateScale(6)
  },
  right: {
    alignItems: "flex-end",
    paddingRight: moderateScale(5)
  },
  part: {
    // width: moderateScale(90)
  },
  left: {
    alignItems: "flex-start",
    paddingLeft: moderateScale(10)
  },
  categories: {
    marginBottom: sizeWidth(12),
    marginTop: sizeWidth(8)
  },
  label: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: appColor.blur
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(9)
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: sizeWidth(5),
    paddingHorizontal: sizeWidth(10)
  }
});
