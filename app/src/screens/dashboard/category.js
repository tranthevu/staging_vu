import React, { Component, ReactNode } from "react";
import { StyleSheet, Image, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";

class Category extends Component {
  render(): ReactNode {
    const { item } = this.props;
    return (
      <TouchableOpacity
        onPress={this.navigateToCategoryDashboard}
        style={styles.container}
      >
        <Image
          resizeMode="stretch"
          source={
            item.category.image
              ? { uri: item.category.image }
              : require("../../../res/img/default-image.png")
          }
          style={styles.image}
        />
        <Text numberOfLines={2} style={styles.name}>
          {item.category.name}
        </Text>
      </TouchableOpacity>
    );
  }

  navigateToCategoryDashboard = () => {
    const { item, isExclusive } = this.props;
    this.props.navigateToPage("CategoryDashboard", { category: item, isExclusive });
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(Category);

const styles = StyleSheet.create({
  container: {
    width: moderateScale(60),
    marginHorizontal: sizeWidth(5),
    alignItems: "center"
  },
  image: {
    width: moderateScale(50),
    height: moderateScale(50)
  },
  name: {
    textAlign: "center",
    marginVertical: sizeWidth(4)
  },
  count: {
    fontSize: sizeFont(12),
    fontStyle: "italic"
  }
});
