import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Image,
  ImageBackground,
  TouchableOpacity,
  Linking
} from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import moment from "moment";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import {
  font,
  appColor,
  productClass,
  dateTimeFormat,
  productCodeType
} from "../../constants/app.constant";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { getProductType, isEmptyPrice, formatPrice } from "../../helpers/common.helper";
import ExpireLabel from "../../components/common/expire-label";
import VoucherHelper from "../../helpers/voucher.helper";
import { IC_CALENDAR_DARKER, IC_MARKER_DARKER } from "../../commons/images";

class PromotionItemList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderContent = (discountPercent, currentPrice, oldPrice) => {
    const { item = {}, isGift } = this.props;
    if (isGift) {
      return (
        <View style={styles.prices}>
          <Text style={styles.event}>Phần thưởng</Text>
        </View>
      );
    }
    if (item.product_class === productClass.voucher) {
      return (
        <View style={styles.prices}>
          {!!discountPercent && (
            <ImageBackground
              style={styles.tag}
              resizeMode="stretch"
              source={require("../../../res/icon/discount-tag.png")}
            >
              <Text style={styles.discount}>-{discountPercent}%</Text>
            </ImageBackground>
          )}
          {!isEmptyPrice(currentPrice) ? (
            <Text style={styles.current}>
              {formatPrice(currentPrice)}
              <Text style={styles.underline}>đ</Text>
            </Text>
          ) : (
            <Text style={styles.event}>{getProductType(item.product_class)}</Text>
          )}
          {!isEmptyPrice(oldPrice) && (
            <Text style={styles.old}>{formatPrice(oldPrice)}đ</Text>
          )}
        </View>
      );
    }
    return (
      <View style={styles.prices}>
        {!!discountPercent && (
          <ImageBackground
            style={styles.tag}
            resizeMode="stretch"
            source={require("../../../res/icon/discount-tag.png")}
          >
            <Text style={styles.discount}>-{discountPercent}%</Text>
          </ImageBackground>
        )}
        <Text style={styles.event}>{getProductType(item.product_class)}</Text>
      </View>
    );
  };

  render() {
    const { style, item = {}, hideMerchant, isSavedProduct, isGift } = this.props;
    const logo = lodash.get(item, "merchant.logo_image.full");
    const merchant = lodash.get(item, "merchant.name");
    const currentPrice = lodash.get(item, "dealset.fixed_discount");
    const oldPrice = lodash.get(item, "attributes.price_old");
    const cover = lodash
      .get(item, "images", [])
      .find(item => item.display_order === 0 && item.caption === "1:1");
    const isExpired = lodash.get(item, "isExpired");
    const discountPercent = lodash.get(item, "dealset.percent_discount", 0);
    const isExclusive = lodash.get(item, "attributes.is_exclusive_offer");
    const statusShown = isSavedProduct && VoucherHelper.isShowStatus(item, isGift);
    const isAboutToExpire = lodash.get(item, "dealset.is_near_expire");
    const showAboutToExpire = isSavedProduct && isAboutToExpire;
    const image =
      lodash.get(cover, "original.thumbnail") || lodash.get(cover, "original");
    const areas = lodash.get(item, "areas");
    const validStartDate = lodash.get(item, "attributes.date_valid_start");
    const validEndDate = lodash.get(item, "attributes.date_valid_end");
    return (
      <TouchableOpacity
        style={[styles.container, style]}
        onPress={this.navigateToOfferDetail}
      >
        <View style={{ flexDirection: "row" }}>
          <ImageBackground
            style={styles.image}
            source={
              image ? { uri: image } : require("../../../res/img/default-offer.jpg")
            }
          >
            {item.product_class === productClass.voucher &&
              isExclusive &&
              !showAboutToExpire &&
              !statusShown && (
                <View style={styles.exclusive}>
                  <Text style={styles.white}>Độc quyền</Text>
                </View>
              )}
            {showAboutToExpire && !statusShown && (
              <View style={styles.due}>
                <Text style={styles.expire}>Sắp hết hạn</Text>
              </View>
            )}
            {item.product_class === productClass.affiliateOffer &&
              !showAboutToExpire &&
              !statusShown && (
                <View style={styles.online}>
                  <Text style={styles.white}>Online</Text>
                </View>
              )}
            {statusShown && this.renderStatus()}
          </ImageBackground>
          <View style={styles.body}>
            <Text style={styles.title} numberOfLines={2}>
              {item.title}
            </Text>
            {this.renderContent(discountPercent, currentPrice, oldPrice)}
            <View style={styles.bottom}>
              {!!merchant && !hideMerchant && (
                <View style={styles.left}>
                  <Image
                    style={styles.merchant}
                    source={
                      logo
                        ? { uri: logo }
                        : require("../../../res/img/default-location.jpg")
                    }
                  />
                  <Text style={styles.name}>{merchant}</Text>
                </View>
              )}
              {isGift && (
                <Image
                  style={styles.gift}
                  source={require("../../../res/icon/gift-small.png")}
                />
              )}
              {!!item.total_avaiable_count &&
                item.total_avaiable_count > 1 &&
                item.product_class !== productClass.affiliateOffer &&
                item.code_type !== productCodeType.yollaBizShareToEarn && (
                  <View style={styles.badge}>
                    <Text style={styles.count}>x{item.total_avaiable_count}</Text>
                  </View>
                )}
            </View>
          </View>
          {/* {statusShown && <View style={styles.blurCover} />}
            {statusShown && this.renderStatus()} */}
          {isExpired && <ExpireLabel />}
        </View>
        {this.renderDashLine()}
        <View style={styles.bottomCard}>
          <View style={styles.bottomCardContent}>
            <Image
              source={IC_CALENDAR_DARKER}
              resizeMode="contain"
              style={styles.bottomCardIcon}
            />
            <Text style={styles.bottomCardText}>
              {moment(validStartDate).format(dateTimeFormat.ddmmyyyy)} -{" "}
              {moment(validEndDate).format(dateTimeFormat.ddmmyyyy)}
            </Text>
          </View>
          {!lodash.isEmpty(areas) && (
            <View style={styles.bottomCardContent}>
              <Image
                source={IC_MARKER_DARKER}
                resizeMode="contain"
                style={styles.bottomCardIcon}
              />
              <Text style={styles.bottomCardText}>{areas.join(", ")}</Text>
            </View>
          )}
        </View>
      </TouchableOpacity>
    );
  }

  renderDashLine = () => (
    <View style={styles.dashline}>
      <View style={[styles.circle, styles.leftCircle]} />
      <View style={[styles.circle, styles.rightCircle]} />
      <Image style={styles.line} source={require("../../../res/icon/line.png")} />
    </View>
  );

  renderStatus = () => {
    const { item, isGift } = this.props;
    const status = VoucherHelper.getStatus(item, isGift);
    return (
      // <View style={styles.wrap}>
      //   <View style={styles.status}>
      //     <Text numberOfLines={1} style={styles.value}>
      //       {VoucherHelper.getStatus(item, isGift)}
      //     </Text>
      //   </View>
      // </View>
      <View style={styles.expired}>
        <Text style={styles.text}>{status}</Text>
      </View>
    );
  };

  navigateToVoucher = type => {
    const { item } = this.props;
    this.props.navigateToPage("Voucher", {
      productId: item.id,
      productSlug: item.slug,
      type,
      product_class: item.product_class
    });
  };

  navigateToOfferDetail = () => {
    const { item, isSavedProduct, isGift } = this.props;
    // if share2earn has campaign_has_product, show OfferDetail screen
    //    share2earn has coupon, promotion_voucher_code is true
    // else open share2earn in browser
    if (isGift) {
      this.navigateToVoucher(productClass.shareToEarn); // use productClass, it will be transformed to codeType at api called
    } else if (
      isSavedProduct &&
      (item.product_class === productClass.voucher ||
        item.product_class === productClass.shareToEarn ||
        item.product_class === productClass.yollaBizShareToEarn ||
        item.product_class === productClass.prepaidVoucher)
    ) {
      this.navigateToVoucher(item.product_class);
    } else if (
      typeof item.campaign_has_product === "undefined" ||
      item.campaign_has_product
    ) {
      this.props.navigateToPage("OfferDetail", {
        productId: item.id,
        productSlug: item.slug,
        promotion_voucher_code: item.promotion_voucher_code
      });
    } else if (this.props.info && this.props.info.url) {
      const { info } = this.props;
      Linking.openURL(info.url);
    } else {
      this.props.navigateToPage("OfferDetail", {
        productSlug: item.slug
      });
    }
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(PromotionItemList);

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    overflow: "hidden",
    width: sizeWidth(302),
    borderRadius: sizeWidth(6),
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    padding: sizeWidth(6),
    marginVertical: sizeWidth(5),
    alignSelf: "center"
  },
  gift: {
    width: moderateScale(20),
    height: moderateScale(20)
  },
  image: {
    width: moderateScale(85),
    height: moderateScale(85),
    justifyContent: "flex-end",
    marginRight: sizeWidth(10),
    borderRadius: moderateScale(6),
    overflow: "hidden"
  },
  blurCover: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    opacity: 0.5,
    backgroundColor: "white",
    right: 0
  },
  wrap: {
    position: "absolute",
    top: 0,
    left: 0,
    justifyContent: "center",
    alignItems: "center",
    bottom: 0,
    right: 0
  },
  discount: {
    fontSize: sizeFont(11),
    color: "white",
    fontFamily: font.bold,
    marginLeft: sizeWidth(3)
  },
  tag: {
    width: moderateScale(41),
    height: moderateScale(20),
    marginRight: moderateScale(7),
    alignSelf: "center",
    justifyContent: "center"
  },
  bottom: {
    flexDirection: "row",
    alignItems: "center"
  },
  merchant: {
    width: moderateScale(16),
    height: moderateScale(16),
    borderRadius: moderateScale(8),
    marginRight: sizeWidth(6)
  },
  name: {
    fontSize: sizeFont(12),
    flex: 1
  },
  title: {
    fontSize: sizeFont(12),
    fontFamily: font.medium
  },
  body: {
    flex: 1
  },
  prices: {
    flexDirection: "row",
    marginVertical: sizeWidth(8),
    alignItems: "flex-end"
  },
  event: {
    fontSize: sizeFont(12),
    alignSelf: "center",
    color: appColor.blur
  },
  current: {
    color: "#FF6961",
    fontSize: sizeFont(14),
    alignSelf: "center",
    fontFamily: font.bold,
    marginRight: sizeWidth(12)
  },
  old: {
    color: appColor.blur,
    alignSelf: "center",
    fontSize: sizeFont(12),
    textDecorationLine: "line-through"
  },
  underline: {
    textDecorationLine: "underline",
    color: "#FF6961",
    fontSize: sizeFont(14),
    fontFamily: font.bold
  },
  status: {
    backgroundColor: "rgb(56, 58, 57)",
    height: sizeWidth(28),
    justifyContent: "center",
    paddingHorizontal: sizeWidth(21),
    borderRadius: sizeWidth(14),
    alignSelf: "center"
  },
  value: {
    color: "white",
    fontSize: sizeFont(13),
    fontFamily: font.bold
  },
  left: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
    marginRight: sizeWidth(8)
  },
  badge: {
    minWidth: moderateScale(22),
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: sizeWidth(2),
    height: moderateScale(22),
    borderRadius: moderateScale(11),
    backgroundColor: appColor.primary
  },
  count: {
    color: "white",
    fontSize: sizeFont(11),
    fontFamily: font.medium
  },
  due: {
    backgroundColor: "#FFCA28",
    height: moderateScale(20),
    justifyContent: "center",
    alignItems: "center",
    width: "100%"
  },
  expire: {
    color: "#564308",
    fontSize: sizeFont(11),
    fontFamily: font.medium
  },
  online: {
    backgroundColor: "#F6792E",
    height: moderateScale(20),
    justifyContent: "center",
    alignItems: "center",
    width: "100%"
  },
  exclusive: {
    backgroundColor: "#EF4136",
    height: moderateScale(20),
    justifyContent: "center",
    alignItems: "center",
    width: "100%"
  },
  white: {
    color: "white",
    fontSize: sizeFont(11),
    fontFamily: font.medium
  },
  bottomCard: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: sizeWidth(10)
  },
  bottomCardContent: {
    flexDirection: "row",
    alignItems: "center"
  },
  bottomCardText: {
    fontSize: sizeFont(11)
  },
  bottomCardIcon: {
    width: moderateScale(13),
    height: moderateScale(13),
    marginRight: sizeWidth(2)
  },
  line: {
    marginTop: sizeWidth(15),
    width: "95%",
    alignSelf: "center",
    height: 1
  },
  leftCircle: {
    left: sizeWidth(-13),
    top: sizeWidth(6)
  },
  rightCircle: {
    right: sizeWidth(-13),
    top: sizeWidth(6)
  },
  circle: {
    backgroundColor: appColor.bg,
    width: sizeWidth(18),
    height: sizeWidth(18),
    borderRadius: sizeWidth(9),
    position: "absolute"
  },
  dashline: {
    justifyContent: "center",
    alignItems: "center"
  },
  expired: {
    backgroundColor: "#444444",
    justifyContent: "center",
    alignItems: "center",
    width: "100%"
  },
  text: {
    color: "white",
    fontSize: sizeFont(11),
    fontFamily: font.medium,
    textAlign: "center"
  }
});
