import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, ImageBackground, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import LinearGradient from "react-native-linear-gradient";
import { font, appColor, productClass } from "../../constants/app.constant";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import OnlineLabel from "../../components/common/online-label";
import ExpireLabel from "../../components/common/expire-label";
import { getProductType } from "../../helpers/common.helper";

class MediumPromotionItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  navigateToMerchantDetail = () => {
    const { item = {} } = this.props;
    const merchantCode = lodash.get(item, "merchant.code_merchant");
    this.props.navigateToPage("MerchantDetail", { merchantCode });
  };

  render(): ReactNode {
    const { style, item = {}, hideMerchant } = this.props;
    const logo = lodash.get(item, "merchant.logo_image.full");
    const merchant = lodash.get(item, "merchant.name");
    const cover = lodash
      .get(item, "images", [])
      .find(item => item.display_order === 0 && item.caption === "1:1");
    const isExpired = lodash.get(item, "isExpired");
    const discountPercent = lodash.get(item, "dealset.percent_discount", 0);
    return (
      <TouchableOpacity
        onPress={this.navigateToOfferDetail}
        style={[styles.container, style]}
      >
        <View>
          <View style={styles.header}>
            <Image
              style={styles.cover}
              source={
                lodash.get(cover, "original.thumbnail")
                  ? { uri: cover.original.thumbnail }
                  : require("../../../res/img/default-offer.jpg")
              }
            />
            {!!merchant && !hideMerchant && (
              <View style={styles.info}>
                <LinearGradient
                  colors={["rgba(0, 0, 0, 0)", "rgba(0, 0, 0, 0.5)"]}
                  style={styles.overlay}
                />
                <TouchableOpacity onPress={this.navigateToMerchantDetail}>
                  <Image
                    style={styles.logo}
                    source={
                      logo
                        ? { uri: logo }
                        : require("../../../res/img/default-location.jpg")
                    }
                  />
                </TouchableOpacity>
                <Text style={styles.name}>{merchant}</Text>
              </View>
            )}
          </View>
          <View style={styles.body}>
            <View style={styles.top}>
              <Text numberOfLines={2} style={styles.title}>
                {item.title}
              </Text>
              <View style={styles.prices}>
                {!!discountPercent && (
                  <ImageBackground
                    style={styles.tag}
                    resizeMode="stretch"
                    source={require("../../../res/icon/discount-tag.png")}
                  >
                    <Text style={styles.discount}>-{discountPercent}%</Text>
                  </ImageBackground>
                )}
                <Text numberOfLines={1} style={styles.event}>
                  {getProductType(item.product_class)}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.shadow} />
        {item.product_class === productClass.affiliateOffer && <OnlineLabel />}
        {isExpired && <ExpireLabel />}
      </TouchableOpacity>
    );
  }

  navigateToOfferDetail = () => {
    const { item } = this.props;
    this.props.navigateToPage("OfferDetail", {
      productId: item.id,
      productSlug: item.slug
    });
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(MediumPromotionItem);

const styles = StyleSheet.create({
  container: {
    alignSelf: "center",
    backgroundColor: "transparent",
    width: sizeWidth(146),
    borderRadius: sizeWidth(6),
    overflow: "hidden",
    marginVertical: sizeWidth(3),
    marginHorizontal: sizeWidth(5)
  },
  header: {
    width: "100%",
    borderRadius: sizeWidth(6),
    overflow: "hidden"
  },
  logo: {
    width: moderateScale(26),
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "white",
    height: moderateScale(26),
    borderRadius: moderateScale(13)
  },
  cover: {
    width: "100%",
    height: sizeWidth(146)
  },
  info: {
    flexDirection: "row",
    height: moderateScale(36),
    width: sizeWidth(146),
    borderRadius: sizeWidth(6),
    paddingHorizontal: moderateScale(5),
    alignItems: "center",
    position: "absolute",
    bottom: 0
  },
  name: {
    flex: 1,
    marginLeft: sizeWidth(5),
    color: "white",
    fontSize: sizeFont(13),
    fontFamily: font.bold
  },
  top: {
    backgroundColor: "white",
    paddingVertical: moderateScale(8),
    paddingHorizontal: moderateScale(10)
  },
  label: {
    color: "#000000",
    fontFamily: font.medium
  },
  event: {
    fontSize: sizeFont(12),
    alignSelf: "center",
    flex: 1,
    color: appColor.blur
  },
  title: {
    color: "#444444",
    fontWeight: "500",
    height: moderateScale(32),
    fontSize: sizeFont(12)
  },
  prices: {
    flexDirection: "row",
    marginTop: moderateScale(6),
    height: moderateScale(20),
    alignItems: "flex-end"
  },
  bottom: {
    backgroundColor: "white",
    paddingHorizontal: sizeWidth(16),
    paddingVertical: sizeWidth(12)
  },
  content: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: sizeWidth(8)
  },
  icon: {
    width: moderateScale(13),
    height: moderateScale(13),
    marginRight: moderateScale(8)
  },
  line: {
    width: "94%",
    alignSelf: "center",
    height: 1
  },
  desc: {
    fontSize: sizeFont(12)
  },
  overlay: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  shadow: {
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 1,
    shadowRadius: 4,
    marginBottom: 4,
    elevation: 2,
    height: 2,
    backgroundColor: "white",
    width: "100%"
  },
  discount: {
    fontSize: sizeFont(11),
    color: "white",
    fontFamily: font.bold,
    marginLeft: sizeWidth(3)
  },
  tag: {
    width: sizeWidth(41),
    height: moderateScale(20),
    marginRight: sizeWidth(7),
    alignSelf: "center",
    justifyContent: "center"
  },
  body: {
    borderTopLeftRadius: sizeWidth(6),
    borderTopRightRadius: sizeWidth(6),
    overflow: "hidden"
  }
});
