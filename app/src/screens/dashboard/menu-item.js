import React, { Component, ReactNode } from "react";
import { StyleSheet, Image, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeWidth } from "../../helpers/size.helper";
import { appColor } from "../../constants/app.constant";

class MenuItem extends Component {
  render(): ReactNode {
    const { text, onPress } = this.props;
    return (
      <TouchableOpacity onPress={onPress} style={styles.container}>
        <Image
          resizeMode="stretch"
          source={require("../../../res/icon/expand.png")}
          style={styles.arrow}
        />
        <Text style={styles.text}>{text}</Text>
      </TouchableOpacity>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(MenuItem);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    paddingHorizontal: sizeWidth(12),
    paddingVertical: sizeWidth(16),
    alignItems: "center"
  },
  arrow: {
    width: sizeWidth(5),
    height: sizeWidth(8),
    tintColor: appColor.text
  },
  text: {
    flex: 1,
    marginLeft: sizeWidth(12)
  }
});
