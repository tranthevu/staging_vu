import React, { Component, ReactNode } from "react";
import { View, StyleSheet, TouchableOpacity, Linking } from "react-native";
import { connect } from "react-redux";
import Swiper from "react-native-swiper";
import lodash from "lodash";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { sizeWidth, moderateScale } from "../../helpers/size.helper";
import CacheImage from "../../components/common/cache-image";

class Banner extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderItem = banner => {
    const image = lodash.get(banner, "images[0].original");
    return (
      <TouchableOpacity
        onPress={() => this.navigate(banner)}
        key={banner.id.toString()}
        style={styles.item}
      >
        <CacheImage resizeMode="stretch" uri={image} style={styles.image} />
      </TouchableOpacity>
    );
  };

  navigate = banner => {
    const url = lodash.get(banner, "url_product");
    if (url) Linking.openURL(url);
  };

  render(): ReactNode {
    const { banners } = this.props.banners;
    if (banners.length === 0) return null;
    return (
      <View style={styles.container}>
        <Swiper
          paginationStyle={styles.pagination}
          dotStyle={styles.dot}
          ref={ref => (this.swiper = ref)}
          activeDotStyle={styles.activeDot}
          style={styles.swiper}
          showsButtons={false}
          autoplay={true}
          autoplayTimeout={5}
        >
          {banners.map(item => this.renderItem(item))}
        </Swiper>
      </View>
    );
  }
}

export default connect(
  state => ({
    banners: state.banners
  }),
  { resetPage, navigateToPage }
)(Banner);

const styles = StyleSheet.create({
  container: {
    height: sizeWidth(158),
    marginBottom: moderateScale(12)
  },
  dot: {
    width: sizeWidth(6),
    height: sizeWidth(6),
    borderRadius: sizeWidth(3),
    borderWidth: 0.5,
    borderColor: "white",
    backgroundColor: "#DDDDDD"
  },
  activeDot: {
    width: sizeWidth(8),
    height: sizeWidth(8),
    borderWidth: 0.5,
    borderColor: "white",
    borderRadius: sizeWidth(4),
    backgroundColor: "#979797"
  },
  swiper: {},
  pagination: {
    marginBottom: -sizeWidth(16)
  },
  image: {
    width: sizeWidth(302),
    height: sizeWidth(158),
    borderRadius: sizeWidth(6)
  },
  item: {
    alignSelf: "center"
  }
});
