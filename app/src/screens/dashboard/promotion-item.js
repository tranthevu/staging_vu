import React, { Component, ReactNode } from "react";
import {
  View,
  Linking,
  StyleSheet,
  Image,
  ImageBackground,
  TouchableOpacity,
  Platform
} from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import LinearGradient from "react-native-linear-gradient";
import moment from "moment";
import {
  font,
  appColor,
  productClass,
  dateTimeFormat,
  productCodeType
} from "../../constants/app.constant";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { getProductType, isEmptyPrice, formatPrice } from "../../helpers/common.helper";
import OnlineLabel from "../../components/common/online-label";
import ExclusiveLabel from "../../components/common/exclusive-label";
import ExpireLabel from "../../components/common/expire-label";
import VoucherHelper from "../../helpers/voucher.helper";
import AboutToExpireLabel from "../../components/common/about-to-expire-label";
import { IC_CALENDAR_DARKER, IC_MARKER_DARKER } from "../../commons/images";

class PromotionItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  navigateToMerchantDetail = () => {
    const { item = {} } = this.props;
    const merchantCode =
      lodash.get(item, "merchant.code_merchant") || lodash.get(item, "merchant.code");
    this.props.navigateToPage("MerchantDetail", { merchantCode });
  };

  renderContent = (discountPercent, currentPrice, oldPrice) => {
    const { isGift, item = {} } = this.props;
    if (isGift) {
      return (
        <View style={styles.prices}>
          <Text style={styles.event}>Phần thưởng</Text>
        </View>
      );
    }
    if (item.product_class === productClass.voucher) {
      return (
        <View>
          <View style={styles.prices}>
            {!!discountPercent && (
              <ImageBackground
                style={styles.tag}
                resizeMode="stretch"
                source={require("../../../res/icon/discount-tag.png")}
              >
                <Text style={styles.discount}>-{discountPercent}%</Text>
              </ImageBackground>
            )}
            {!isEmptyPrice(currentPrice) && (
              <Text style={styles.current}>
                {formatPrice(currentPrice)}
                <Text style={styles.underline}>đ</Text>
              </Text>
            )}
            {!isEmptyPrice(oldPrice) && (
              <Text style={styles.old}>{formatPrice(oldPrice)}đ</Text>
            )}
            <Text style={styles.event}>{getProductType(item.product_class)}</Text>
          </View>
        </View>
      );
    }
    return (
      <View style={styles.prices}>
        {!!discountPercent && (
          <ImageBackground
            style={styles.tag}
            resizeMode="stretch"
            source={require("../../../res/icon/discount-tag.png")}
          >
            <Text style={styles.discount}>-{discountPercent}%</Text>
          </ImageBackground>
        )}
        <Text style={styles.event}>{getProductType(item.product_class)}</Text>
      </View>
    );
  };

  render(): ReactNode {
    const { style, item = {}, hideMerchant, isSavedProduct, isGift } = this.props;
    const logo = lodash.get(item, "merchant.logo_image.full");
    const merchant = lodash.get(item, "merchant.name");
    const currentPrice = lodash.get(item, "dealset.fixed_discount");
    const oldPrice = lodash.get(item, "attributes.price_old");
    const cover = lodash
      .get(item, "images", [])
      .find(item => item.display_order === 0 && item.caption === "16:9");
    const isExpired = lodash.get(item, "isExpired");
    const discountPercent = lodash.get(item, "dealset.percent_discount", 0);
    const isExclusive = lodash.get(item, "attributes.is_exclusive_offer");
    const statusShown = isSavedProduct && VoucherHelper.isShowStatus(item, isGift);
    const isAboutToExpire = lodash.get(item, "dealset.is_near_expire");
    const showAboutToExpire = isSavedProduct && isAboutToExpire;
    const areas = lodash.get(item, "areas");
    const validStartDate = lodash.get(item, "attributes.date_valid_start");
    const validEndDate = lodash.get(item, "attributes.date_valid_end");
    return (
      <TouchableOpacity
        onPress={this.navigateToOfferDetail}
        style={[styles.container, style]}
      >
        <View>
          <View style={styles.header}>
            <Image
              style={styles.cover}
              source={
                lodash.get(cover, "original.thumbnail")
                  ? { uri: cover.original.thumbnail }
                  : require("../../../res/img/default-offer.jpg")
              }
            />
            {!!merchant && !hideMerchant && (
              <View style={styles.info}>
                <LinearGradient
                  colors={["rgba(0, 0, 0, 0)", "rgba(0, 0, 0, 0.5)"]}
                  style={styles.overlay}
                />
                <TouchableOpacity onPress={this.navigateToMerchantDetail}>
                  <Image
                    style={styles.logo}
                    source={
                      logo
                        ? { uri: logo }
                        : require("../../../res/img/default-location.jpg")
                    }
                  />
                </TouchableOpacity>
                <Text style={styles.name}>{merchant}</Text>
                {isGift && (
                  <Image
                    style={styles.gift}
                    source={require("../../../res/icon/gift-big.png")}
                  />
                )}
                {!!item.total_avaiable_count &&
                  item.total_avaiable_count > 1 &&
                  item.product_class !== productClass.affiliateOffer &&
                  item.code_type !== productCodeType.yollaBizShareToEarn && (
                    <View style={styles.badge}>
                      <Text style={styles.count}>x{item.total_avaiable_count}</Text>
                    </View>
                  )}
              </View>
            )}
          </View>
          <View style={styles.body}>
            <View style={styles.top}>
              <Text numberOfLines={2} style={styles.title}>
                {item.title}
              </Text>
              {this.renderContent(discountPercent, currentPrice, oldPrice)}
              <View style={styles.bottomCard}>
                <View style={styles.bottomCardContent}>
                  <Image
                    source={IC_CALENDAR_DARKER}
                    resizeMode="contain"
                    style={styles.bottomCardIcon}
                  />
                  <Text style={styles.bottomCardText}>
                    {moment(validStartDate).format(dateTimeFormat.ddmmyyyy)} -{" "}
                    {moment(validEndDate).format(dateTimeFormat.ddmmyyyy)}
                  </Text>
                </View>
                {!lodash.isEmpty(areas) && (
                  <View style={styles.bottomCardContent}>
                    <Image
                      source={IC_MARKER_DARKER}
                      resizeMode="contain"
                      style={styles.bottomCardIcon}
                    />
                    <Text style={styles.bottomCardText}>{areas.join(", ")}</Text>
                  </View>
                )}
              </View>
            </View>
          </View>
        </View>
        <View style={styles.shadow} />
        {/* {statusShown && <View style={styles.blurCover} />} */}
        {statusShown && this.renderStatus()}
        {item.product_class === productClass.affiliateOffer &&
          !showAboutToExpire &&
          !statusShown && <OnlineLabel />}
        {item.product_class === productClass.voucher &&
          isExclusive &&
          !showAboutToExpire && <ExclusiveLabel />}
        {isExpired && !showAboutToExpire && <ExpireLabel />}
        {showAboutToExpire && <AboutToExpireLabel />}
      </TouchableOpacity>
    );
  }

  renderStatus = () => {
    const { item, isGift } = this.props;
    // const status = VoucherHelper.getStatus(item, isGift);
    var status = VoucherHelper.getStatus(item, isGift);
    if (status === "Tin hết hạn") {
      status = "Hết hạn";
    }
    const fontSize = status.length > 10 ? 6 : 11;
    return (
      // <View style={styles.status}>
      //   <Text numberOfLines={1} style={styles.value}>
      //     {status}
      //   </Text>
      // </View>
      <View style={styles.wrap}>
        <Text style={[styles.statusText, { fontSize: sizeFont(fontSize) }]}>
          {status}
        </Text>
      </View>
    );
  };

  navigateToVoucher = type => {
    const { item } = this.props;
    this.props.navigateToPage("Voucher", {
      productId: item.id,
      productSlug: item.slug,
      product_class: item.product_class,
      type
    });
  };

  navigateToOfferDetail = () => {
    const { item, isSavedProduct, isGift } = this.props;
    if (isGift) {
      this.navigateToVoucher(productClass.shareToEarn); // use productClass, it will be transformed to codeType at api called
    } else if (
      isSavedProduct &&
      (item.product_class === productClass.voucher ||
        item.product_class === productClass.shareToEarn ||
        item.product_class === productClass.yollaBizShareToEarn ||
        item.product_class === productClass.prepaidVoucher)
    ) {
      this.navigateToVoucher();
    } else if (
      typeof item.campaign_has_product === "undefined" ||
      item.campaign_has_product
    ) {
      this.props.navigateToPage("OfferDetail", {
        productId: item.id,
        productSlug: item.slug,
        promotion_voucher_code: item.promotion_voucher_code
      });
    } else if (this.props.info && this.props.info.url) {
      const { info } = this.props;
      Linking.openURL(info.url);
    } else {
      this.props.navigateToPage("OfferDetail", {
        productSlug: item.slug
      });
    }
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(PromotionItem);

const styles = StyleSheet.create({
  container: {
    alignSelf: "center",
    backgroundColor: "transparent",
    width: sizeWidth(302),
    borderRadius: sizeWidth(6),
    overflow: "hidden",
    marginVertical: sizeWidth(3)
  },
  header: {
    width: "100%",
    borderRadius: sizeWidth(6),
    overflow: "hidden"
  },
  underline: {
    textDecorationLine: "underline",
    color: "#FF6961",
    fontSize: sizeFont(14),
    fontFamily: font.bold
  },
  logo: {
    width: moderateScale(40),
    backgroundColor: "white",
    height: moderateScale(40),
    borderRadius: moderateScale(20)
  },
  cover: {
    width: "100%",
    height: sizeWidth(169)
  },
  info: {
    flexDirection: "row",
    height: moderateScale(60),
    width: sizeWidth(302),
    borderRadius: sizeWidth(6),
    paddingHorizontal: moderateScale(17),
    alignItems: "center",
    position: "absolute",
    bottom: 0
  },
  name: {
    flex: 1,
    marginHorizontal: sizeWidth(8),
    color: "white",
    fontFamily: font.bold
  },
  gift: {
    width: moderateScale(34),
    height: moderateScale(34)
  },
  top: {
    backgroundColor: "white",
    paddingVertical: moderateScale(8),
    paddingHorizontal: moderateScale(16)
  },
  label: {
    color: "#000000",
    fontFamily: font.medium
  },
  event: {
    fontSize: sizeFont(12),
    alignSelf: "center",
    color: appColor.blur
  },
  title: {
    color: "#444444",
    fontWeight: "500",
    height: moderateScale(32),
    fontSize: sizeFont(12)
  },
  prices: {
    flexDirection: "row",
    marginTop: moderateScale(6),
    height: moderateScale(20),
    alignItems: "flex-end"
  },
  current: {
    color: "#FF6961",
    fontSize: sizeFont(14),
    alignSelf: "center",
    fontFamily: font.bold,
    marginRight: moderateScale(12)
  },
  old: {
    color: appColor.blur,
    alignSelf: "center",
    marginRight: moderateScale(12),
    fontSize: sizeFont(12),
    textDecorationLine: "line-through"
  },
  bottom: {
    backgroundColor: "white",
    paddingHorizontal: sizeWidth(16),
    paddingVertical: sizeWidth(12)
  },
  content: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: sizeWidth(8)
  },
  icon: {
    width: moderateScale(13),
    height: moderateScale(13),
    marginRight: moderateScale(8)
  },
  line: {
    width: "94%",
    alignSelf: "center",
    height: 1
  },
  desc: {
    fontSize: sizeFont(12)
  },
  overlay: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  shadow: {
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 1,
    shadowRadius: 4,
    marginBottom: 4,
    height: Platform.OS === "ios" ? 2 : 0,
    backgroundColor: "white",
    width: "100%"
  },
  discount: {
    fontSize: sizeFont(11),
    color: "white",
    fontFamily: font.bold,
    marginLeft: sizeWidth(3)
  },
  tag: {
    width: sizeWidth(41),
    height: moderateScale(20),
    marginRight: sizeWidth(7),
    alignSelf: "center",
    justifyContent: "center"
  },
  body: {
    borderTopLeftRadius: sizeWidth(6),
    borderTopRightRadius: sizeWidth(6),
    overflow: "hidden"
  },
  status: {
    backgroundColor: "rgb(56, 58, 57)",
    height: sizeWidth(28),
    justifyContent: "center",
    paddingHorizontal: sizeWidth(21),
    borderRadius: sizeWidth(14),
    position: "absolute",
    alignSelf: "center",
    top: sizeWidth(71)
  },
  value: {
    color: "white",
    fontSize: sizeFont(13),
    fontFamily: font.bold
  },
  blurCover: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    opacity: 0.5,
    backgroundColor: "white",
    right: 0
  },
  badge: {
    minWidth: moderateScale(22),
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: sizeWidth(2),
    height: moderateScale(22),
    borderRadius: moderateScale(11),
    backgroundColor: appColor.primary
  },
  count: {
    color: "white",
    fontSize: sizeFont(11),
    fontFamily: font.medium
  },
  bottomCard: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: sizeWidth(5),
    marginTop: sizeWidth(7),
    borderTopColor: "#ddd",
    borderTopWidth: 1
  },
  bottomCardContent: {
    flexDirection: "row",
    alignItems: "center"
  },
  bottomCardText: {
    fontSize: sizeFont(11)
  },
  bottomCardIcon: {
    width: moderateScale(13),
    height: moderateScale(13),
    marginRight: sizeWidth(2)
  },
  wrap: {
    position: "absolute",
    top: moderateScale(12),
    height: moderateScale(21),
    width: moderateScale(92),
    right: moderateScale(-24),
    backgroundColor: "#444444",
    transform: [
      {
        rotate: "45deg"
      }
    ],
    justifyContent: "center",
    alignItems: "center"
  },
  statusText: {
    color: "white",
    textAlign: "center",
    fontFamily: font.bold
  }
});
