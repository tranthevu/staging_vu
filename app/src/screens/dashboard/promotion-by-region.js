import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import TouchableIcon from "../../components/common/touchable-icon";
import LinearGradient from "react-native-linear-gradient";

class PromotionByRegion extends Component {
  constructor(props) {
    super(props);
  }

  render(): ReactNode {
    const { style, imageStyle, item } = this.props;
    return (
      <TouchableOpacity
        onPress={this.navigateToLocationProducts}
        style={[styles.container, style]}
      >
        <Image
          resizeMode="stretch"
          source={{ uri: item.image }}
          style={[styles.image, imageStyle]}
        />
        <LinearGradient
          colors={["rgba(0, 0, 0, 0)", "rgba(0, 0, 0, 0.5)"]}
          style={styles.body}
        >
          <Text style={styles.name}>{item.name}</Text>
          <Text style={styles.count}>{item.total_product} Ưu đãi</Text>
        </LinearGradient>
      </TouchableOpacity>
    );
  }

  navigateToLocationProducts = () => {
    const { item } = this.props;
    this.props.navigateToPage("LocationProducts", { location: item });
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(PromotionByRegion);

const styles = StyleSheet.create({
  container: {
    width: sizeWidth(270),
    marginHorizontal: sizeWidth(7),
    borderRadius: sizeWidth(6),
    marginVertical: sizeWidth(8),
    backgroundColor: "white",
    overflow: "hidden"
  },
  image: {
    width: sizeWidth(270),
    height: sizeWidth(270)
  },
  name: {
    fontSize: sizeFont(16),
    fontFamily: font.medium,
    color: "white"
  },
  count: {
    fontSize: sizeFont(12),
    color: "white",
    fontFamily: font.medium
  },
  body: {
    justifyContent: "center",
    bottom: 0,
    height: 72,
    width: sizeWidth(270),
    position: "absolute",
    paddingHorizontal: sizeWidth(15)
  }
});
