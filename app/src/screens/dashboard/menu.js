import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import { resetPage, navigateToPage, toggleDrawer } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, appColor, pageLink } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import MenuItem from "./menu-item";
import AuthHelper from "../../helpers/auth.helper";

class Menu extends Component {
  constructor(props) {
    super(props);
  }

  navigateToCategoryDashboard = category => {
    this.props.navigateToPage("CategoryDashboard", { category });
  };

  navigateToProfile = async () => {
    const isAuthenticated = await AuthHelper.isAuthenticated();
    if (isAuthenticated) {
      this.props.navigateToPage("Profile");
    } else {
      this.props.navigateToPage("Login", { canBack: true });
    }
  };

  render(): ReactNode {
    const { topCategories } = this.props.topCategories;
    const { profile } = this.props.profile;
    const name = lodash.get(profile, "full_name");
    const avatar = lodash.get(profile, "avatar_img");
    const logged = !!lodash.get(profile, "full_name");
    const image = logged
      ? avatar
        ? { uri: avatar }
        : require("../../../res/icon/default-avatar.png")
      : require("../../../res/icon/user.png");
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.navigateToProfile} style={styles.header}>
          <Image source={image} style={styles.avatar} />
          <Text style={logged ? styles.name : styles.login}>
            {logged ? name : "Đăng nhập"}
          </Text>
          {logged && (
            <Image
              resizeMode="stretch"
              source={require("../../../res/icon/expand.png")}
              style={styles.arrow}
            />
          )}
        </TouchableOpacity>
        <ScrollView bounces={false}>
          <Text style={styles.category}>Danh mục</Text>
          {topCategories.map((item, index) => (
            <MenuItem
              onPress={() => this.navigateToCategoryDashboard(item)}
              key={index.toString()}
              text={`${item.category.name} (${item.category.total_product})`}
            />
          ))}
          <View style={styles.line} />
          <Text style={styles.category}>Về Yolla Network</Text>
          <MenuItem
            onPress={() =>
              this.props.navigateToPage("Document", {
                url: "https://yolla.vn/#/intro",
                title: "Giới thiệu"
              })
            }
            text="Giới thiệu"
          />
          <MenuItem
            onPress={() =>
              this.props.navigateToPage("Document", {
                url: "https://yolla.vn/#/merchant-signup",
                title: "Đăng khuyến mãi trên Yolla Network"
              })
            }
            text="Đăng khuyến mãi trên Yolla Network"
          />
          <MenuItem
            onPress={() =>
              this.props.navigateToPage("Document", {
                url: "https://yolla.vn/#/policy",
                title: "Chính sách & Điều khoản"
              })
            }
            text="Chính sách &amp; Điều khoản"
          />
          <MenuItem
            onPress={() =>
              this.props.navigateToPage("Document", {
                url: "https://yolla.vn/#/privacy",
                title: "Chính sách quyền riêng tư"
              })
            }
            text="Chính sách quyền riêng tư"
          />
          <MenuItem
            onPress={() =>
              this.props.navigateToPage("Document", {
                url: "https://yolla.vn/#/contact",
                title: "Liên hệ"
              })
            }
            text="Liên hệ"
          />
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({
    topCategories: state.topCategories,
    profile: state.profile
  }),
  { resetPage, navigateToPage, toggleDrawer }
)(Menu);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  category: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    padding: sizeWidth(12)
  },
  header: {
    height: sizeWidth(65),
    backgroundColor: appColor.primary,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: sizeWidth(12)
  },
  avatar: {
    width: sizeWidth(52),
    height: sizeWidth(52),
    borderRadius: sizeWidth(26),
    marginRight: sizeWidth(12)
  },
  arrow: {
    width: sizeWidth(7),
    height: sizeWidth(12),
    marginLeft: sizeWidth(12),
    tintColor: "white"
  },
  name: {
    color: "white",
    flex: 1,
    fontSize: sizeFont(12),
    fontFamily: font.bold
  },
  login: {
    color: "#FFCC00",
    flex: 1,
    fontSize: sizeFont(12),
    fontFamily: font.bold
  },
  line: {
    width: "100%",
    height: 1,
    backgroundColor: "#DDDDDD"
  }
});
