import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Animated,
  Image,
  FlatList,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import Dialog, { FadeAnimation } from "react-native-popup-dialog";
import { connectActionSheet } from "@expo/react-native-action-sheet";
import { resetPage, navigateToPage, toggleDrawer } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor, text, event, appMap } from "../../constants/app.constant";
import TouchableIcon from "../../components/common/touchable-icon";
import Banner from "./banner";
import Category from "./category";
import PromotionItem from "./promotion-item";
import NotificationIcon from "./notification-icon";
import PaginationList from "../../components/common/pagination-list";
import {
  fetchMoreHotProducts,
  refreshHotProducts,
  requestHotProducts
} from "../../actions/hot-products.action";
import {
  fetchMoreRecentProducts,
  refreshRecentProducts,
  requestRecentProducts
} from "../../actions/recent-products.action";
import {
  fetchMoreExpiringProducts,
  refreshExpiringProducts,
  requestExpiringProducts
} from "../../actions/expiring-products.action";
import { requestedHomeAreas } from "../../actions/home-areas.action";
import EventRegister from "../../helpers/event-register.helper";
import {
  requestAllProducts,
  refreshAllProducts,
  fetchMoreAllProducts
} from "../../actions/all-products.action";
import PromotionItemList from "./promotion-item-list";
import ChatbotIcon from "../../components/common/chatbot-icon";
import PickLocationDialog from "../profile/pick-location-dialog";
import {
  getLocation,
  saveLocation,
  getNotShowAgainNoti
} from "../../helpers/storage.helper";
import { requestedTopCategories } from "../../actions/top-categories.action";
import { requestedTopMerchants } from "../../actions/top-merchants.action";
import { requestedFilters } from "../../actions/filters.action";
import Api from "../../api/api";
import MediumPromotionItem from "./medium-promotion-item";
import NotificationView from "../../components/common/notification-view";
import HomeMenuItem from "./home-menu-item";
import { requestedBanners } from "../../actions/banners.action";
import { NEAR_PROMO_SCREEN, PAGE_NOT_FOUND_SCREEN } from "../../navigators/screensName";
import PromoNearUserItem from "../../components/Item/PromoNearUserItem";
import withLocationPermission from "../HOC/withLocationPermission";
import { appConfig } from "../../config/app.config";
import { checkApiStatus } from "../../helpers/app.helper";
import StatisticView from "./statistic-view";
import EmptyView from "../../components/common/empty-view";
import { requestedProfile } from "../../actions/profile.action";
import AuthHelper from "../../helpers/auth.helper";
import ParallaxScrollView from "react-native-parallax-scroll-view";

const PromoNearUserItemWithPermission = withLocationPermission(PromoNearUserItem);
const tabs = {
  hot: 1,
  new: 2,
  aboutToExpire: 3
};

const viewType = {
  grid: 0,
  list: 1
};

@connectActionSheet
class DashboardScreen extends Component {
  offset = 0;

  constructor(props) {
    super(props);
    const field = lodash.get(this.props.navigation.state.params, "field");
    const selectedTab = field
      ? field === "hot"
        ? tabs.hot
        : field === "expiring"
        ? tabs.aboutToExpire
        : tabs.new
      : tabs.hot;
    this.state = {
      selectedTab,
      appliedFilters: {},
      selectedViewType: viewType.list,
      updateLocationVisible: false,
      onlineOffers: [],
      onlineOffersCount: 0,
      exclusiveOffers: [],
      exclusiveOffersCount: 0,
      share2EarnOffers: [],
      share2EarnOffersCount: 0,
      isAuthenticated: false,
      headerAnim: new Animated.Value(0),
      isExpand: false
    };
  }

  toggleExpand = () => {
    const { isExpand } = this.state;
    this.setState({
      isExpand: !isExpand
    });
  };

  saveLocation = async location => {
    this.setState({
      updateLocationVisible: false
    });
    await saveLocation(location ? location.key : "toan-quoc");
    this.props.requestedTopCategories();
    this.props.requestedTopMerchants();
    this.props.requestedFilters();
    this.props.resetPage("Main");
  };

  renderTabs = () => {
    const { selectedTab } = this.state;
    return (
      <View style={styles.tabs}>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.hot })}
          style={styles.tab}
        >
          <Text style={selectedTab === tabs.hot ? styles.activeText : styles.text}>
            Ưu đãi hot
          </Text>
          {selectedTab === tabs.hot && <View style={styles.line} />}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.new })}
          style={styles.tab}
        >
          <Text style={selectedTab === tabs.new ? styles.activeText : styles.text}>
            Ưu đãi mới
          </Text>
          {selectedTab === tabs.new && <View style={styles.line} />}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.aboutToExpire })}
          style={styles.tab}
        >
          <Text
            style={selectedTab === tabs.aboutToExpire ? styles.activeText : styles.text}
          >
            Đừng bỏ lỡ
          </Text>
          {selectedTab === tabs.aboutToExpire && <View style={styles.line} />}
        </TouchableOpacity>
      </View>
    );
  };

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  renderOnline = ({ item, index }) => {
    if (index === 7)
      return (
        <TouchableOpacity onPress={this.navigateToOnlinePromo} style={styles.more}>
          <View style={styles.moreWrap}>
            <Image
              style={styles.moreIcon}
              source={require("../../../res/icon/more-online.png")}
            />
            <Text style={styles.moreText}>Xem thêm</Text>
          </View>
        </TouchableOpacity>
      );
    return <MediumPromotionItem key={index.toString()} item={item} />;
  };

  renderExclusive = ({ item, index }) => {
    if (index == 7)
      return (
        <TouchableOpacity onPress={this.navigateToExclusiveProducts} style={styles.more}>
          <View style={styles.moreWrap}>
            <Image
              style={styles.moreIcon}
              source={require("../../../res/icon/more-online.png")}
            />
            <Text style={styles.moreText}>Xem thêm</Text>
          </View>
        </TouchableOpacity>
      );
    return <PromotionItem style={styles.promotion} key={index.toString()} item={item} />;
  };

  renderShare2EarnItem = ({ item, index }) => {
    if (index == 7)
      return (
        <TouchableOpacity onPress={this.navigateToShare2Earn} style={styles.more}>
          <View style={styles.moreWrap}>
            <Image
              style={styles.moreIcon}
              source={require("../../../res/icon/more-online.png")}
            />
            <Text style={styles.moreText}>Xem thêm</Text>
          </View>
        </TouchableOpacity>
      );
    return <MediumPromotionItem key={index.toString()} item={item} />;
  };

  navigateToExclusiveProducts = () => {
    appConfig.activeApp = appMap.all;
    this.props.navigateToPage("ExclusivePromo");
  };

  navigateToPromo = () => {
    appConfig.activeApp = appMap.promo;
    this.props.navigateToPage("Promo");
  };

  navigateToCoupon = () => {
    appConfig.activeApp = appMap.eCoupon;
    this.props.navigateToPage("Coupon");
  };

  navigateToOnlinePromo = () => {
    appConfig.activeApp = appMap.onlinePromo;
    this.props.navigateToPage("OnlinePromo");
  };

  navigateToShare2Earn = () => {
    appConfig.activeApp = appMap.shareToEarn;
    this.props.navigateToPage("ShareToEarnPromo");
  };

  renderMenuBar = () => {
    return (
      <View style={styles.menuBar}>
        <HomeMenuItem
          onPress={this.navigateToPromo}
          text={`Chương trình\nkhuyến mãi`}
          icon={require("../../../res/icon/promo.png")}
        />
        <HomeMenuItem
          onPress={this.navigateToCoupon}
          text={`Ưu đãi\neCoupon`}
          icon={require("../../../res/icon/ecoupon.png")}
        />
        <HomeMenuItem
          onPress={this.navigateToOnlinePromo}
          text={`Ưu đãi\nonline`}
          icon={require("../../../res/icon/online-promo.png")}
        />
        <HomeMenuItem
          text={`Chia sẻ\nnhận thưởng`}
          onPress={this.navigateToShare2Earn}
          icon={require("../../../res/icon/share2earn.png")}
        />
      </View>
    );
  };

  onNavigateNearPromo = () => {
    this.props.navigateToPage(NEAR_PROMO_SCREEN);
  };

  renderShare2Earn = () => {
    const { share2EarnOffersCount, share2EarnOffers } = this.state;
    return (
      <View style={[styles.online, styles.shadow]}>
        <View style={styles.bar}>
          <Text style={styles.onlineTitle}>Chia sẻ nhận thưởng</Text>
          <TouchableIcon
            onPress={this.navigateToShare2Earn}
            iconStyle={styles.arrow}
            source={require("../../../res/icon/chevron-right.png")}
          />
        </View>
        <FlatList
          horizontal
          data={share2EarnOffersCount > 7 ? [...share2EarnOffers, {}] : share2EarnOffers}
          renderItem={this.renderShare2EarnItem}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={styles.top}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  };

  renderHeader = () => {
    const {
      selectedViewType,
      onlineOffers,
      onlineOffersCount,
      exclusiveOffers,
      exclusiveOffersCount
    } = this.state;
    const title = "Ưu đãi mới";
    return (
      <View style={styles.body}>
        {this.renderMenuBar()}
        <Banner />
        {this.renderShare2Earn()}
        <View style={[styles.exclusive, styles.shadow]}>
          <View style={styles.bar}>
            <Text style={styles.onlineTitle}>Top ưu đãi độc quyền</Text>
            <TouchableIcon
              onPress={this.navigateToExclusiveProducts}
              iconStyle={styles.arrow}
              source={require("../../../res/icon/chevron-right.png")}
            />
          </View>
          <FlatList
            horizontal
            data={exclusiveOffersCount > 7 ? [...exclusiveOffers, {}] : exclusiveOffers}
            renderItem={this.renderExclusive}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={styles.top}
            showsHorizontalScrollIndicator={false}
          />
        </View>
        <PromoNearUserItemWithPermission
          onPress={this.onNavigateNearPromo}
          style={styles.header}
        />
        <View style={[styles.online, styles.shadow]}>
          <View style={styles.bar}>
            <Text style={styles.onlineTitle}>Top ưu đãi online</Text>
            <TouchableIcon
              onPress={this.navigateToOnlinePromo}
              iconStyle={styles.arrow}
              source={require("../../../res/icon/chevron-right.png")}
            />
          </View>
          <FlatList
            horizontal
            data={onlineOffersCount > 7 ? [...onlineOffers, {}] : onlineOffers}
            renderItem={this.renderOnline}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={styles.top}
            showsHorizontalScrollIndicator={false}
          />
        </View>
        <View style={styles.header}>
          <Text style={styles.title}>{title}</Text>
          <TouchableIcon
            style={styles.view}
            iconStyle={styles.viewIcon}
            onPress={this.toggleViewType}
            source={
              selectedViewType === viewType.list
                ? require("../../../res/icon/list.png")
                : require("../../../res/icon/grid.png")
            }
          />
        </View>
      </View>
    );
  };

  fetchOnlineOffers = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const data = await Api.getOnlineOffers();
    this.setState({
      onlineOffers: data.results,
      onlineOffersCount: data.count
    });
  };

  fetchExclusiveOffers = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const data = await Api.getExclusiveOffers();
    this.setState({
      exclusiveOffers: data.results,
      exclusiveOffersCount: data.count
    });
  };

  fetchShare2EarnOffers = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const data = await Api.share2EarnPromo({
      page_size: 7,
      ordering: "recent",
      page: 1
    });
    this.setState({
      share2EarnOffers: data.results,
      share2EarnOffersCount: data.count
    });
  };

  checkLocation = async () => {
    const location = await getLocation();
    this.setState({ updateLocationVisible: !location });
  };

  componentDidMount = async () => {
    const isAuthenticated = await AuthHelper.isAuthenticated();
    this.setState({ isAuthenticated });
    this.checkLocation();
    this.loadInitialNotification();
    this.fetchOnlineOffers();
    this.fetchExclusiveOffers();
    this.fetchShare2EarnOffers();
    this.props.requestedBanners();
    this.props.requestedHomeAreas();
    this.props.requestAllProducts();
    this.props.requestRecentProducts();
    this.props.requestHotProducts();
    this.props.requestExpiringProducts();
    this.jumpPageEvent = EventRegister.on(event.jumpPage, this.onTabChange);
  };

  refresh = () => {
    this.fetchOnlineOffers();
    this.fetchExclusiveOffers();
    this.props.requestedBanners();
    this.props.requestedHomeAreas();
    this.props.requestAllProducts();
    this.props.requestRecentProducts();
    this.props.requestHotProducts();
    this.props.requestExpiringProducts();
  };

  onCurrentTabChange = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { headerAnim } = this.state;
    this.notificationView.register();
    Animated.timing(headerAnim, {
      toValue: 0,
      duration: 400
    }).start();
    if (this.list) {
      this.list.scrollToTop();
      this.refresh();
    }
  };

  onTabChange = tabIndex => {
    if (tabIndex === 0) {
      this.onCurrentTabChange();
    }
  };

  componentWillUnmount = () => {
    EventRegister.off(this.jumpPageEvent);
  };

  renderAllProducts = (category, merchant, ordering) => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      allProducts
    } = this.props.allProducts;
    const { refreshAllProducts, fetchMoreAllProducts } = this.props;
    return (
      <PaginationList
        ref={ref => (this.list = ref)}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderPromotion}
        loading={loading}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        scrollEventThrottle={300}
        onScroll={this.onScroll}
        firstLoading={firstLoading}
        ListHeaderComponent={this.renderHeader()}
        onEndReached={() => fetchMoreAllProducts(category, merchant, ordering)}
        onRefresh={() => refreshAllProducts(category, merchant, ordering)}
        data={allProducts}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = moderateScale(80);
    return (
      layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom
    );
  };

  onScroll = event => {
    const currentOffset = event.nativeEvent.contentOffset.y;
    const { headerAnim } = this.state;
    if (currentOffset > moderateScale(150) && currentOffset > this.offset) {
      Animated.timing(headerAnim, {
        toValue: -moderateScale(85),
        duration: 400
      }).start();
      this.setState({ isExpand: false });
    } else if (!this.isCloseToBottom(event.nativeEvent)) {
      Animated.timing(headerAnim, {
        toValue: 0,
        duration: 400
      }).start();
    }
    this.offset = currentOffset;
  };

  renderHotProducts = (category, merchant, ordering) => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      hotProducts
    } = this.props.hotProducts;
    const { refreshHotProducts, fetchMoreHotProducts } = this.props;
    return (
      <FlatList
        horizontal
        contentContainerStyle={styles.list}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderTabPromotion}
        data={hotProducts}
      />
    );
  };

  renderRecentProducts = (category, merchant, ordering) => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      recentProducts
    } = this.props.recentProducts;
    const { refreshRecentProducts, fetchMoreRecentProducts } = this.props;
    return (
      <FlatList
        horizontal
        contentContainerStyle={styles.list}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderTabPromotion}
        data={recentProducts}
      />
    );
  };

  renderExpiringProducts = (category, merchant, ordering) => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      expiringProducts
    } = this.props.expiringProducts;
    const { fetchMoreExpiringProducts, refreshExpiringProducts } = this.props;
    return (
      <FlatList
        horizontal
        contentContainerStyle={styles.list}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderTabPromotion}
        data={expiringProducts}
      />
    );
  };

  renderTabProducts = () => {
    const { selectedTab, appliedFilters } = this.state;
    const category = lodash.get(
      appliedFilters,
      "selectedCategory.slug",
      text.emptyString
    );
    const merchant = lodash
      .get(appliedFilters, "selectedMerchants", [])
      .map(item => item.code)
      .join(",");
    const ordering = lodash.get(appliedFilters, "selectedOrder.type", text.emptyString);
    switch (selectedTab) {
      case tabs.hot:
        return this.renderHotProducts(category, merchant, ordering);
      case tabs.new:
        return this.renderRecentProducts(category, merchant, ordering);
      case tabs.aboutToExpire:
        return this.renderExpiringProducts(category, merchant, ordering);
      default:
    }
  };

  renderProducts = () => {
    const { selectedTab, appliedFilters } = this.state;
    const category = lodash.get(
      appliedFilters,
      "selectedCategory.slug",
      text.emptyString
    );
    const merchant = lodash
      .get(appliedFilters, "selectedMerchants", [])
      .map(item => item.code)
      .join(",");
    const ordering = lodash.get(appliedFilters, "selectedOrder.type", text.emptyString);
    return this.renderAllProducts(category, merchant, ordering);
  };

  render() {
    const { updateLocationVisible, headerAnim, isExpand } = this.state;
    const { profile } = this.props.profile;
    const fullName = lodash.get(profile, "full_name");
    return (
      <View style={styles.container}>
        <Animated.View style={{ marginTop: headerAnim }}>
          <View style={styles.toolbar}>
            {fullName !== undefined && (
              <Text numberOfLines={1} style={styles.hello}>
                Xin chào, {fullName}
              </Text>
            )}
            {fullName === undefined && (
              <Text numberOfLines={1} style={styles.hello}>
                Xin chào
              </Text>
            )}
            <View style={styles.right}>
              <TouchableIcon
                style={styles.wrap}
                onPress={this.navigateToSearch}
                iconStyle={styles.glass}
                source={require("../../../res/icon/home-search.png")}
              />
              <NotificationIcon />
            </View>
          </View>
          <StatisticView isExpand={isExpand} toggleExpand={this.toggleExpand} />
        </Animated.View>
        <View style={styles.content}>{this.renderProducts()}</View>
        <ChatbotIcon />
        <Dialog
          visible={updateLocationVisible}
          width={sizeWidth(261)}
          onTouchOutside={() => {
            this.setState({ updateLocationVisible: false });
          }}
          dialogAnimation={
            new FadeAnimation({
              toValue: 0,
              animationDuration: 150,
              useNativeDriver: true
            })
          }
        >
          <PickLocationDialog
            onSaveLocation={this.saveLocation}
            showActionSheetWithOptions={this.props.showActionSheetWithOptions}
          />
        </Dialog>
        <NotificationView ref={ref => (this.notificationView = ref)} />
      </View>
    );
  }

  renderTabPromotion = ({ item, index }) => {
    return <PromotionItem style={styles.promotion} item={item} />;
  };

  renderPromotion = ({ item, index }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem item={item} />
    ) : (
      <PromotionItemList item={item} />
    );
  };

  onApplyFilters = appliedFilters => {
    this.setState({ appliedFilters });
    const category = lodash.get(
      appliedFilters,
      "selectedCategory.slug",
      text.emptyString
    );
    const merchant = lodash
      .get(appliedFilters, "selectedMerchants", [])
      .map(item => item.code)
      .join(",");
    const ordering = lodash.get(appliedFilters, "selectedOrder.type", text.emptyString);
    this.props.requestRecentProducts(category, merchant, ordering);
    this.props.requestHotProducts(category, merchant, ordering);
    this.props.requestExpiringProducts(category, merchant, ordering);
  };

  navigateToSearch = () => {
    appConfig.activeApp = appMap.all;
    this.props.navigateToPage("Search");
  };

  renderCategory = ({ item, index }) => {
    return <Category item={item} />;
  };

  navigateToProducts = () => {
    this.props.navigateToPage("Products");
  };

  loadInitialNotification = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    try {
      const data = await Api.getNotificationPopup();
      const popupid = lodash.get(data, "data.popupdata.popupid");
      if (!popupid) {
        return;
      }
      const popuptype = lodash.get(data, "data.popupdata.popuptype");
      const homeShowing = this.isHomeShowing();
      if (popuptype === "3" && !homeShowing) {
        // YA15-282 [Staging][Yolla Network][1.2 build 73 - 152][Notification]
        // Show only pop-up type 3 on homepage
        return;
      }
      const noShowAgainNoti = await getNotShowAgainNoti();
      const popupidInStr = popupid.toString();
      if (noShowAgainNoti.indexOf(popupidInStr) !== -1) return;

      const notification = {
        body: lodash.get(data, "data.popupdata.content"),
        title: lodash.get(data, "data.popuptitle"),
        data: lodash.get(data, "data.popupdata")
      };
      if (!isMaintenance && data.data) {
        EventRegister.emitEvent(event.showPopupNotification, notification);
      }
    } catch (err) {
      console.log("main.js loadInitialNotification having error", err);
    }
  };

  isHomeShowing = () => {
    const { nav } = this.props;
    const main = nav.routes[nav.index];
    return main.routeName === "Main" && main.index === 0;
  };
}

export default connect(
  state => ({
    nav: state.nav,
    topCategories: state.topCategories,
    hotProducts: state.hotProducts,
    recentProducts: state.recentProducts,
    expiringProducts: state.expiringProducts,
    homeAreas: state.homeAreas,
    allProducts: state.allProducts,
    profile: state.profile
  }),
  {
    resetPage,
    navigateToPage,
    toggleDrawer,
    fetchMoreHotProducts,
    refreshHotProducts,
    fetchMoreRecentProducts,
    refreshRecentProducts,
    fetchMoreExpiringProducts,
    refreshExpiringProducts,
    requestRecentProducts,
    requestExpiringProducts,
    requestedHomeAreas,
    requestHotProducts,
    requestAllProducts,
    refreshAllProducts,
    fetchMoreAllProducts,
    requestedTopCategories,
    requestedTopMerchants,
    requestedFilters,
    requestedBanners,
    requestedProfile
  }
)(DashboardScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  logo: {
    width: sizeWidth(94),
    height: sizeWidth(11)
  },
  label: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    paddingHorizontal: sizeWidth(9)
  },
  title: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  onlineTitle: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: sizeWidth(4),
    paddingHorizontal: sizeWidth(12)
  },
  arrow: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  menu: {
    width: sizeWidth(24),
    height: sizeWidth(24)
  },
  body: {},
  categories: {
    marginBottom: sizeWidth(12),
    marginTop: sizeWidth(8)
  },
  region: {
    marginTop: sizeWidth(6),
    marginBottom: sizeWidth(15)
  },
  viewAll: {
    backgroundColor: "#EAEAEA",
    width: sizeWidth(270),
    marginHorizontal: sizeWidth(7),
    height: sizeWidth(278),
    borderRadius: sizeWidth(6),
    justifyContent: "center",
    alignItems: "center"
  },
  count: {
    color: appColor.blur,
    fontSize: sizeFont(24)
  },
  all: {
    color: appColor.blur,
    fontSize: sizeFont(16),
    marginTop: sizeWidth(12)
  },
  tabs: {
    flexDirection: "row",
    height: moderateScale(44),
    backgroundColor: "white"
  },
  tab: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    color: appColor.blur,
    fontFamily: font.medium
  },
  activeText: {
    fontFamily: font.bold
  },
  line: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: sizeWidth(3),
    backgroundColor: appColor.primary
  },
  filter: {
    width: sizeWidth(20),
    height: sizeWidth(20)
  },
  find: {
    marginRight: sizeWidth(15)
  },
  glass: {
    width: moderateScale(20),
    height: moderateScale(20)
  },
  wrap: {
    marginHorizontal: moderateScale(10),
    padding: sizeWidth(2)
  },
  hello: {
    fontSize: sizeFont(13),
    color: "white",
    maxWidth: sizeWidth(160),
    fontFamily: font.medium
  },
  right: {
    flexDirection: "row",
    alignItems: "center",
    position: "absolute",
    right: moderateScale(8)
  },
  left: {
    flexDirection: "row",
    alignItems: "center",
    position: "absolute",
    left: moderateScale(8)
  },
  toolbar: {
    paddingHorizontal: moderateScale(8),
    flexDirection: "row",
    width: sizeWidth(320),
    height: moderateScale(55),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: appColor.primary
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(12)
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: sizeWidth(5),
    paddingHorizontal: sizeWidth(9)
  },
  promotion: {
    marginHorizontal: sizeWidth(5)
  },
  list: {
    paddingHorizontal: sizeWidth(5)
  },
  top: {
    paddingHorizontal: sizeWidth(5)
  },
  online: {
    backgroundColor: "white",
    paddingVertical: sizeWidth(12),
    marginBottom: moderateScale(12)
  },
  exclusive: {
    paddingVertical: sizeWidth(12),
    marginBottom: moderateScale(12)
  },
  more: {
    marginVertical: sizeWidth(3),
    marginHorizontal: sizeWidth(5)
  },
  moreWrap: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 4,
    width: sizeWidth(146),
    borderRadius: sizeWidth(6),
    flex: 1,
    borderColor: appColor.blur,
    borderStyle: "dashed",
    borderWidth: 1,
    backgroundColor: "white"
  },
  moreText: {
    color: appColor.blur,
    fontSize: sizeFont(13),
    fontFamily: font.bold
  },
  moreIcon: {
    width: moderateScale(14),
    height: moderateScale(24),
    marginBottom: moderateScale(19)
  },
  content: {
    flex: 1
  },
  menuBar: {
    marginTop: moderateScale(10),
    backgroundColor: "white",
    marginBottom: moderateScale(12),
    flexDirection: "row",
    alignItems: "flex-start",
    paddingVertical: moderateScale(11),
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1
    }
  },
  shadow: {
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1
    }
  }
});
