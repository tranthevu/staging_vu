import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";

class HomeMenuItem extends Component {
  constructor(props) {
    super(props);
  }

  render(): ReactNode {
    const { text, icon, onPress } = this.props;
    return (
      <TouchableOpacity onPress={onPress} style={styles.container}>
        <Image resizeMode="stretch" source={icon} style={styles.image} />
        <Text style={styles.text}>{text}</Text>
      </TouchableOpacity>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(HomeMenuItem);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    color: "black",
    textAlign: "center",
    fontSize: sizeFont(11)
  },
  image: {
    width: moderateScale(50),
    height: moderateScale(50),
    marginBottom: moderateScale(7)
  }
});
