import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import lodash from "lodash";

class NotificationIcon extends Component {
  constructor(props) {
    super(props);
  }

  render(): ReactNode {
    const { style, bellStyle, circleStyle } = this.props;
    const { count } = this.props.notifications;
    let unreadcount = lodash.get(count, "unreadcount");
    return (
      <TouchableOpacity
        onPress={this.navigateToNotifications}
        style={[styles.notification, style]}
      >
        <Image
          style={[styles.bell, bellStyle]}
          source={require("../../../res/icon/bell.png")}
        />
        {!!unreadcount && (
          <View style={[styles.circle, circleStyle]}>
            <Text style={styles.badge}>
              {unreadcount > 99 ? "99+" : unreadcount}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  }

  navigateToNotifications = () => {
    this.props.navigateToPage("Notifications");
  };
}

export default connect(
  state => ({
    notifications: state.notifications
  }),
  { resetPage, navigateToPage }
)(NotificationIcon);

const styles = StyleSheet.create({
  bell: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: "white"
  },
  notification: {
    width: moderateScale(27),
    height: moderateScale(24)
  },
  circle: {
    backgroundColor: "#FFCC00",
    minWidth: moderateScale(14),
    height: moderateScale(14),
    borderRadius: moderateScale(7),
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    borderWidth: 1,
    borderColor: "white",
    top: 0,
    right: 0
  },
  badge: {
    color: appColor.primary,
    fontSize: sizeFont(9)
  }
});
