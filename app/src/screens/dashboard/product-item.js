import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";

class ProductItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render(): ReactNode {
    return (
      <TouchableOpacity
        onPress={this.navigateToMerchantDetail}
        style={styles.container}
      >
        <Image
          style={styles.logo}
          source={require("../../../res/icon/chicken-logo.png")}
        />
        <View style={styles.content}>
          <Text style={styles.name}>TMV Ngọc Dung</Text>
          <Text style={styles.count}>Có 8 ưu đãi mới</Text>
        </View>
      </TouchableOpacity>
    );
  }

  navigateToMerchantDetail = () => {
    this.props.navigateToPage("MerchantDetail");
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(ProductItem);

const styles = StyleSheet.create({
  container: {
    width: sizeWidth(146),
    marginVertical: sizeWidth(5),
    borderRadius: sizeWidth(4),
    backgroundColor: "white"
  },
  logo: {
    width: sizeWidth(146),
    height: sizeWidth(110)
  },
  name: {
    fontSize: sizeFont(12),
    color: "#000000",
    fontFamily: font.bold
  },
  count: {
    color: "#000000",
    fontStyle: "italic",
    fontSize: sizeFont(12)
  },
  content: {
    padding: sizeWidth(12)
  }
});
