import React, { Component, ReactNode } from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import numeral from "numeral";
import lodash from "lodash";
import { navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeWidth, moderateScale, sizeFont } from "../../helpers/size.helper";
import { font, giftStatus, event } from "../../constants/app.constant";
import AuthHelper from "../../helpers/auth.helper";
import TouchableIcon from "../../components/common/touchable-icon";
import EventRegister from "../../helpers/event-register.helper";
import { requestedProfile } from "../../actions/profile.action";

class StatisticView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: false
    };
  }

  componentDidMount = async () => {
    const isAuthenticated = await AuthHelper.isAuthenticated();
    const { profile } = this.props.profile;
    const fullName = lodash.get(profile, "full_name");
    if (!isAuthenticated) {
      if (fullName !== undefined) {
        this.setState({
          ...this.state,
          isAuthenticated: true
        });
      } else {
        this.setState({
          ...this.state,
          isAuthenticated: false
        });
      }
    } else {
      this.setState({
        ...this.state,
        isAuthenticated: isAuthenticated
      });
    }
  };

  navigateToLogin = () => {
    this.props.navigateToPage("Login", {
      canBack: true,
      lastScreen: "Main",
      action: "thongKe"
    });
  };

  navigateToGift = () => {
    this.props.navigateToPage("Archive", {
      selectedTab: 1,
      status: giftStatus.all
    });
    EventRegister.emit(event.jumpPage, 1);
  };

  navigateToOffer = () => {
    this.props.navigateToPage("Archive", {
      selectedTab: 0
    });
    EventRegister.emit(event.jumpPage, 1);
  };

  render(): ReactNode {
    const { isExpand, toggleExpand } = this.props;
    const { profile } = this.props.profile;
    const fullName = lodash.get(profile, "full_name");
    const received = numeral(parseFloat(profile.received_money)).format("0,0");
    const saving = numeral(parseFloat(profile.saving_money)).format("0,0");
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={toggleExpand} style={styles.top}>
          <Text style={styles.label}>Thống kê</Text>
          <Image
            source={
              isExpand
                ? require("../../../res/icon/icon-eye.png")
                : require("../../../res/icon/icon-eye-off.png")
            }
            style={styles.eye}
          />
        </TouchableOpacity>
        {isExpand && (
          <>
            <View style={styles.line} />
            {fullName !== undefined ? (
              <View style={styles.body}>
                <TouchableOpacity onPress={this.navigateToOffer} style={styles.item}>
                  <Text style={styles.text}>Tiết kiệm</Text>
                  <Image
                    source={require("../../../res/icon/icon-saved.png")}
                    style={styles.image}
                  />
                  <Text style={styles.price}>
                    {saving}
                    <Text style={styles.unit}>đ</Text>
                  </Text>
                </TouchableOpacity>
                <View style={styles.separator} />
                <TouchableOpacity onPress={this.navigateToGift} style={styles.item}>
                  <Text style={styles.text}>Tiền thưởng</Text>
                  <Image
                    source={require("../../../res/icon/icon-money-gift.png")}
                    style={styles.image}
                  />
                  <Text style={styles.price}>
                    {received}
                    <Text style={styles.unit}>đ</Text>
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={styles.require}>
                <View style={styles.content}>
                  <Text style={styles.notLogin}>Bạn chưa đăng nhập</Text>
                  <Text style={styles.login}>Đăng nhập để xem thống kê</Text>
                </View>
                <TouchableIcon
                  iconStyle={styles.icon}
                  onPress={this.navigateToLogin}
                  source={require("../../../res/icon/icon-login.png")}
                />
              </View>
            )}
          </>
        )}
      </View>
    );
  }
}

export default connect(
  state => ({
    profile: state.profile
  }),
  { navigateToPage, requestedProfile }
)(StatisticView);

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FFF",
    borderRadius: moderateScale(6),
    shadowOpacity: 1,
    marginTop: -moderateScale(12),
    marginBottom: moderateScale(3),
    shadowRadius: 0,
    elevation: 2,
    marginHorizontal: sizeWidth(9),
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    }
  },
  top: {
    flexDirection: "row",
    alignItems: "center",
    height: moderateScale(42),
    paddingHorizontal: sizeWidth(10)
  },
  label: {
    fontSize: sizeFont(13),
    marginRight: sizeWidth(12),
    flex: 1,
    fontFamily: font.medium
  },
  eye: {
    width: moderateScale(16),
    height: moderateScale(16)
  },
  line: {
    height: 1,
    backgroundColor: "#DDDDDD"
  },
  body: {
    flexDirection: "row"
  },
  item: {
    padding: moderateScale(10),
    flex: 1,
    alignItems: "center"
  },
  text: {
    fontSize: sizeFont(13),
    fontFamily: font.medium
  },
  image: {
    marginVertical: moderateScale(12),
    width: moderateScale(32),
    height: moderateScale(31)
  },
  price: {
    fontSize: sizeFont(15),
    fontFamily: font.bold
  },
  unit: {
    fontSize: sizeFont(15),
    fontFamily: font.regular,
    textDecorationLine: "underline"
  },
  separator: {
    width: 1,
    backgroundColor: "#DDDDDD"
  },
  notLogin: {
    fontFamily: font.medium,
    fontSize: sizeFont(15)
  },
  login: {
    fontSize: sizeFont(13),
    marginTop: moderateScale(7)
  },
  icon: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  require: {
    flexDirection: "row",
    alignItems: "center",
    padding: moderateScale(15)
  },
  content: {
    marginRight: sizeWidth(12),
    flex: 1
  }
});
