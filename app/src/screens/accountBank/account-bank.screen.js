import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import validator from "validator";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, text } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import Input from "../../components/common/input";
import Dropdown from "../../components/common/dropdown";
import Button from "../../components/common/button";
import Api from "../../api/api";
import NotificationView from "../../components/common/notification-view";
import { showError, showMessage } from "../../helpers/error.helper";
import { checkApiStatus } from "../../helpers/app.helper";

class AccountBankScreen extends Component {
  constructor(props) {
    super(props);
    const { masterData } = this.props.masterData;
    const { country } = masterData;
    const selectedCountry = country.find(item => item.country === "Việt Nam");

    this.state = {
      account_number: text.emptyString,
      account_fullname: text.emptyString,
      bank_branch: text.emptyString,
      bank_name: text.emptyString,
      selectedCountry,
      selectedProvince: null,
      loading: true
    };
  }

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const response = await Api.getAccountBank();
    if (response.data !== undefined && response.data !== null) {
      this.state.account_fullname = response.data.account_fullname;
      this.state.account_number = response.data.account_number;
      this.state.bank_branch = response.data.bank_branch;
      this.state.bank_name = response.data.bank_name;
      this.state.selectedProvince =
        this.state.selectedCountry &&
        this.state.selectedCountry.provinces &&
        this.state.selectedCountry.provinces.find(
          item => item.province == response.data.bank_city
        );
    }
    this.state.loading = false;
    this.setState(this.state);
  };

  saveInfo = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const {
      bank_branch,
      account_fullname,
      account_number,
      bank_name,
      selectedProvince
    } = this.state;

    if (account_number && !validator.isNumeric(account_number))
      return showError("Số tài khoản không hợp lệ!");
    try {
      this.setState({ loading: true });
      const data = {
        bank_branch,
        account_fullname,
        account_number,
        bank_city: lodash.get(selectedProvince, "province"),
        bank_name
      };
      await Api.updateAccountBank(data);
      this.setState({ loading: false });
      showMessage("Cập nhật thông tin thành công");
    } catch (err) {
      this.setState({ loading: false });
    }
  };

  render(): ReactNode {
    const {
      account_fullname,
      account_number,
      bank_name,
      selectedCountry,
      selectedProvince,
      bank_branch,
      loading
    } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
          center={<Text style={styles.title}>Tài khoản nhận thưởng</Text>}
        />
        <View style={styles.body}>
          <ScrollView bounces={false}>
            <Text style={styles.desc}>
              Nhập chính xác thông tin thanh toán để nhận phần thưởng từ doanh nghiệp.
            </Text>
            <Input
              label="Chủ tài khoản"
              style={styles.name}
              onChangeText={text => this.setState({ account_fullname: text })}
              placeholder="Nhập họ và tên"
              value={account_fullname}
            />
            <Input
              label="Số tài khoản"
              onChangeText={text => this.setState({ account_number: text })}
              placeholder="Nhập số tài khoản"
              value={account_number}
            />
            <Input
              label="Ngân hàng"
              onChangeText={text => this.setState({ bank_name: text })}
              placeholder="Nhập tên ngân hàng"
              value={bank_name}
            />
            <Dropdown
              options={lodash.get(selectedCountry, "provinces")}
              path="province"
              onValueChange={value =>
                this.setState({
                  selectedProvince: value
                })
              }
              label="Tỉnh / Thành phố"
              value={lodash.get(selectedProvince, "province") || "Chọn một tùy chọn"}
            />
            <Input
              onChangeText={text => this.setState({ bank_branch: text })}
              label="Chi nhánh"
              value={bank_branch}
              placeholder="Nhập chi nhánh"
            />
          </ScrollView>
          <Button
            loading={loading}
            onPress={this.saveInfo}
            style={styles.save}
            text="CẬP NHẬT"
          />
          <NotificationView />
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    masterData: state.masterData
  }),
  { resetPage, navigateToPage, navigateBack }
)(AccountBankScreen);

const styles = StyleSheet.create({
  save: {
    marginVertical: sizeWidth(18)
  },
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  label: {
    fontFamily: font.medium,
    fontSize: sizeFont(13),
    marginVertical: sizeWidth(10),
    marginHorizontal: sizeWidth(21)
  },
  desc: {
    marginHorizontal: sizeWidth(21),
    fontSize: sizeFont(12),
    marginBottom: sizeWidth(4),
    marginTop: sizeWidth(15)
  },
  cancel: {
    color: "white"
  },
  name: {
    marginTop: sizeWidth(12)
  },
  body: {
    flex: 1
  }
});
