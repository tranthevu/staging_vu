import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, documentType, event } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import TouchableRow from "../../components/common/touchable-row";
import Switch from "../../components/common/switch";
import VersionNumber from "react-native-version-number";
import Api from "../../api/api";
import { loadedProfile } from "../../actions/profile.action";
import firebase from "react-native-firebase";
import NotificationView from "../../components/common/notification-view";
import EventRegister from "../../helpers/event-register.helper";
import { checkApiStatus } from "../../helpers/app.helper";

class SettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = async () => {
    this.jumpPageEvent = EventRegister.on(event.jumpPage, this.onTabChange);
  };

  onTabChange = async tabIndex => {
    if (tabIndex == 4) {
      const isMaintenance = await checkApiStatus();
      if (isMaintenance) {
        this.props.resetPage("Maintenance");
      }
      this.notificationView.register();
    }
  };

  componentWillUnmount = () => {
    EventRegister.off(this.jumpPageEvent);
  };

  onChangeReceiveNotification = async value => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { profile } = this.props;
    this.props.loadedProfile({ ...profile, receive_notification: value });
    const data = await Api.updateProfile({ receive_notification: value });
    this.props.loadedProfile(data.data);
    const fcmToken = await firebase.messaging().getToken();
    if (value) {
      await Api.registerTokenId(fcmToken);
    } else {
      await Api.unregisterTokenId(fcmToken);
    }
  };

  render(): ReactNode {
    const { profile } = this.props.profile;
    return (
      <View style={styles.container}>
        <Toolbar center={<Text style={styles.title}>Cài đặt</Text>} />
        {/* <Switch
          onValueChange={this.onChangeReceiveNotification}
          value={profile.receive_notification}
          label="Thông báo"
        />
        <View style={[styles.line, styles.top]} /> */}
        <View style={styles.body}>
          <TouchableRow
            onPress={() =>
              this.props.navigateToPage("Document", {
                document: documentType.introduction,
                title: "Giới thiệu"
              })
            }
            label="Giới thiệu"
          />
          <TouchableRow
            onPress={() =>
              this.props.navigateToPage("Document", {
                document: documentType.policy,
                title: "Chính sách và điều khoản"
              })
            }
            label="Chính sách và điều khoản"
          />
          <TouchableRow
            onPress={() =>
              this.props.navigateToPage("Document", {
                document: documentType.privacy,
                title: "Chính sách quyền riêng tư"
              })
            }
            label="Chính sách quyền riêng tư"
          />
          <TouchableRow
            onPress={() =>
              this.props.navigateToPage("Document", {
                document: documentType.activity,
                title: "Chính sách hoạt động"
              })
            }
            label="Chính sách hoạt động"
          />
          <TouchableRow
            onPress={() =>
              this.props.navigateToPage("Document", {
                document: documentType.contact,
                title: "Liên hệ"
              })
            }
            label="Liên hệ"
          />
          <View style={styles.row}>
            <Text style={styles.text}>Phiên bản</Text>
            <Text style={styles.version}>{VersionNumber.appVersion}</Text>
          </View>
          <View style={styles.line} />
          <NotificationView ref={ref => (this.notificationView = ref)} />
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    profile: state.profile
  }),
  { resetPage, navigateToPage, loadedProfile }
)(SettingsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F9F9F9"
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  row: {
    paddingHorizontal: sizeWidth(12),
    marginTop: moderateScale(20),
    height: moderateScale(44),
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center"
  },
  text: {
    fontSize: sizeFont(12),
    flex: 1,
    fontFamily: font.regular
  },
  version: {
    fontSize: sizeFont(12),
    marginLeft: sizeWidth(12),
    fontFamily: font.regular
  },
  line: {
    backgroundColor: "#DDDDDD",
    height: 1,
    width: "100%"
  },
  top: {
    marginBottom: moderateScale(20)
  }
});
