import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ScrollView, TouchableOpacity } from "react-native";
import lodash from "lodash";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, sizeHeight } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import Button from "../../components/common/button";
import AppText from "../../components/common/text";
import NewCheckbox from "../../components/common/new-checkbox";

class SelectionListScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      isMultipleChoice: false,
      items: []
    };
  }

  componentDidMount = async () => {
    const { title, isMultipleChoice, items } = this.props.navigation.state.params;
    this.setState({ title, isMultipleChoice, items });
  };

  render(): ReactNode {
    const { title, isMultipleChoice, items } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
          center={<Text style={styles.title}>{title}</Text>}
        />
        <View style={styles.body}>
          {!lodash.isEmpty(items) && (
            <ScrollView
              bounces={false}
              contentContainerStyle={isMultipleChoice ? styles.list : {}}
            >
              {items.map((item, index) => (
                <TouchableOpacity
                  key={`${index + 1}`}
                  style={[styles.row, index % 2 === 0 ? styles.evenRow : styles.oddRow]}
                  onPress={() => this.onSelect(item)}
                >
                  <AppText style={styles.text}>{item.text}</AppText>
                  {!isMultipleChoice && item.checked && (
                    <NewCheckbox disabled checked={item.checked} />
                  )}
                  {isMultipleChoice && (
                    <NewCheckbox
                      checked={item.checked}
                      onChange={() => this.onSelect(item)}
                    />
                  )}
                </TouchableOpacity>
              ))}
            </ScrollView>
          )}
          {isMultipleChoice && (
            <View style={styles.actions}>
              <Button onPress={this.onSave} text="LƯU" />
            </View>
          )}
        </View>
      </View>
    );
  }

  onSave = () => {
    const { items } = this.state;
    const onUpdateSelection = lodash.get(
      this.props.navigation,
      "state.params.onUpdateSelection"
    );
    const selectedItems = items.filter(item => item.checked);
    if (onUpdateSelection && typeof onUpdateSelection === "function") {
      onUpdateSelection(selectedItems);
    }
    this.props.navigateBack();
  };

  onSelect = selectedItem => {
    const { isMultipleChoice, items } = this.state;
    const onUpdateSelection = lodash.get(
      this.props.navigation,
      "state.params.onUpdateSelection"
    );
    if (!isMultipleChoice) {
      if (onUpdateSelection && typeof onUpdateSelection === "function") {
        onUpdateSelection(selectedItem);
      }
      this.props.navigateBack();
      return;
    }
    this.setState({
      items: items.map(item => {
        if (item.value === selectedItem.value) {
          item.checked = !item.checked;
        }
        return item;
      })
    });
  };
}

export default connect(
  null,
  { resetPage, navigateToPage, navigateBack }
)(SelectionListScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  cancel: {
    fontSize: sizeFont(13),
    color: "white"
  },
  list: {
    paddingBottom: sizeWidth(50)
  },
  actions: {
    position: "absolute",
    bottom: sizeWidth(12),
    width: "100%",
    flexDirection: "row",
    justifyContent: "center"
  },
  row: {
    height: sizeWidth(50),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: sizeWidth(10)
  },
  oddRow: {
    backgroundColor: "#FFF"
  },
  evenRow: {
    backgroundColor: "rgba(251, 212, 211, 0.2)"
  },
  text: {
    color: "#000",
    flex: 1,
    marginRight: sizeWidth(8)
  }
});
