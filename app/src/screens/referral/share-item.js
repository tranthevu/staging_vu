import React, { Component } from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import { sizeWidth, sizeFont } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { font } from "../../constants/app.constant";

export default class ShareItem extends Component {
  render() {
    const { onPress, style, icon, label } = this.props;
    return (
      <View>
        <TouchableOpacity onPress={onPress} style={[styles.container, style]}>
          <Image style={styles.image} source={icon} />
          <Text style={styles.label}>{label}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: sizeWidth(15),
    height: sizeWidth(34),
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center"
  },
  label: {
    fontSize: sizeFont(14),
    flex: 1,
    color: "#000000",
    fontFamily: font.regular
  },
  image: {
    width: sizeWidth(25),
    height: sizeWidth(25),
    marginRight: sizeWidth(12)
  }
});
