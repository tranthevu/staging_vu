import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import {
  resetPage,
  navigateToPage,
  navigateBack
} from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import Input from "../../components/common/input";
import Button from "../../components/common/button";
import ShareItem from "./share-item";

const viewType = {
  code: 0,
  share: 1
};

class ReferralScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedViewType: viewType.code
    };
  }

  renderCode = () => {
    return (
      <ScrollView bounces={false}>
        <Text style={styles.label}>Thêm bạn - Thêm quà</Text>
        <Text style={styles.desc}>
          Nhận ngay 3 mã giảm giá 20% áp dụng cho tất cả các nhãn hàng là đối
          tác của Yolla Card
        </Text>
        <Image
          style={styles.referral}
          source={require("../../../res/icon/referral.png")}
        />
        <Input
          right={
            <TouchableIcon
              style={styles.copy}
              iconStyle={styles.icon}
              source={require("../../../res/icon/copy.png")}
            />
          }
          label="Mã giới thiệu"
          value="ck8792"
        />
        <Button
          onPress={() => this.setState({ selectedViewType: viewType.share })}
          style={styles.button}
          text="GIỚI THIỆU"
        />
      </ScrollView>
    );
  };

  renderShare = () => {
    return (
      <ScrollView>
        <Text style={styles.share}>Chia sẻ Yolla Card với bạn bè qua:</Text>
        <ShareItem
          icon={require("../../../res/icon/telegram.png")}
          label="Facebook"
        />
        <ShareItem
          style={styles.bg}
          icon={require("../../../res/icon/telegram.png")}
          label="Messenger"
        />
        <ShareItem
          icon={require("../../../res/icon/telegram.png")}
          label="Google Plus"
        />
        <ShareItem
          style={styles.bg}
          icon={require("../../../res/icon/telegram.png")}
          label="Telegram"
        />
        <ShareItem
          icon={require("../../../res/icon/telegram.png")}
          label="Twitter"
        />
        <ShareItem
          style={styles.bg}
          icon={require("../../../res/icon/telegram.png")}
          label="Mail"
        />
        <ShareItem
          icon={require("../../../res/icon/telegram.png")}
          label="Linkedin"
        />
        <ShareItem
          style={styles.bg}
          icon={require("../../../res/icon/telegram.png")}
          label="Line"
        />
        <ShareItem
          icon={require("../../../res/icon/telegram.png")}
          label="Whatsapp"
        />
        <ShareItem
          style={styles.bg}
          icon={require("../../../res/icon/telegram.png")}
          label="Viber"
        />
        <ShareItem
          icon={require("../../../res/icon/telegram.png")}
          label="Sao chép liên kết"
        />
      </ScrollView>
    );
  };

  render(): ReactNode {
    const { selectedViewType } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Giới thiệu bạn bè</Text>}
        />
        {selectedViewType === viewType.code
          ? this.renderCode()
          : this.renderShare()}
      </View>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage, navigateBack }
)(ReferralScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  copy: {
    marginLeft: sizeWidth(12)
  },
  icon: {
    width: sizeWidth(24),
    height: sizeWidth(24)
  },
  label: {
    color: "#000000",
    fontSize: sizeFont(24),
    fontFamily: font.bold,
    marginTop: sizeWidth(20),
    marginBottom: sizeWidth(20),
    marginHorizontal: sizeWidth(15)
  },
  desc: {
    color: "#000000",
    fontSize: sizeFont(16),
    marginHorizontal: sizeWidth(15)
  },
  referral: {
    marginTop: sizeWidth(27),
    alignSelf: "flex-end",
    width: sizeWidth(250),
    height: sizeWidth(226)
  },
  button: {
    marginVertical: sizeWidth(12)
  },
  share: {
    color: "#000000",
    fontFamily: font.medium,
    margin: sizeWidth(15)
  },
  bg: {
    backgroundColor: "#F9F9F9"
  }
});
