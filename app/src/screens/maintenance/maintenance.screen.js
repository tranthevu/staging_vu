import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image } from "react-native";
import { connect } from "react-redux";
import {sizeWidth, sizeHeight, sizeFont, moderateScale} from "../../helpers/size.helper";
import { appColor } from "../../constants/app.constant";
import Text from "../../components/common/text";
import Button from "../../components/common/button";

class MaintenanceScreen extends Component {
  tryAgain = () => {
    this.props.navigation.navigate("Splash");
  };

  render(): ReactNode {
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
          <Image
            style={styles.logoImage}
            resizeMode="stretch"
            source={require("../../../res/icon/splash-logo-yolla-network.png")}
          />
        </View>
        <View style={styles.maintenance}>
          <Image
            style={styles.maintenanceImage}
            resizeMode="stretch"
            source={require("../../../res/img/maintenance.png")}
          />
        </View>
        <View style={styles.info}>
          <Text style={[styles.infoText, styles.infoText1]}>ỨNG DỤNG ĐANG NÂNG CẤP</Text>
          <Text style={[styles.infoText, styles.infoText2]}>
            Cảm ơn bạn đã sử dụng Yolla Network.{"\n"}
            Chúng tôi sẽ sớm trở lại với những bổ sung và{"\n"}hoàn thiện tốt hơn.
          </Text>
          <Text style={[styles.infoText, styles.infoText3]}>Trân trọng.</Text>
        </View>

        <Button
          onPress={this.tryAgain}
          style={styles.btnBuy}
          text="THỬ LẠI"
          textStyle={styles.textStyle}
        />
      </View>
    );
  }
}

export default connect(null, null)(MaintenanceScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: appColor.primary
  },
  logo: {
    flex: 1 / 5,
    justifyContent: "center",
    alignItems: "center"
  },
  logoImage: {
    width: sizeWidth(140),
    height: sizeHeight(30)
  },
  maintenance: {
    flex: 2 / 5,
    justifyContent: "flex-start",
    alignItems: "center"
  },
  maintenanceImage: {
    width: sizeWidth(280),
    height: sizeHeight(200)
  },
  info: {
    flex: 1 / 5,
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "center"
  },
  infoText: {
    color: "#FFFFFF",
    fontSize: sizeFont(16),
    textAlign: "center",
    alignSelf: "center"
  },
  infoText1: {
    fontSize: sizeFont(20),
    fontWeight: "bold"
  },
  infoText2: {},
  infoText3: {
    fontStyle: "italic",
    fontFamily: "Roboto-LightItalic"
  },
  textStyle: {
    color: appColor.primary
  },
  btnBuy: {
    position: "absolute",
    bottom: moderateScale(12),
    alignSelf: "center",
    backgroundColor: "#FFF"
  },
});
