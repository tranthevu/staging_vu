import React, { Component, ReactNode } from "react";
import { View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import PromotionItemList from "../dashboard/promotion-item-list";
import PromotionItem from "../dashboard/promotion-item";
import {
  requestJoinedCampaigns,
  fetchMoreJoinedCampaigns,
  refreshJoinedCampaigns
} from "../../actions/joined-campaigns.action";
import PaginationList from "../../components/common/pagination-list";
import EmptyView from "../../components/common/empty-view";

const viewType = {
  grid: 0,
  list: 1
};

class JoinedCampaignsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedViewType: viewType.list
    };
  }

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  render(): ReactNode {
    const { selectedViewType } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          center={<Text style={styles.header}>Chương trình đã tham gia</Text>}
          left={<BackIcon />}
        />
        <View style={styles.bar}>
          <Text style={styles.text}>Chương trình đã tham gia</Text>
          <TouchableIcon
            style={styles.view}
            iconStyle={styles.viewIcon}
            onPress={this.toggleViewType}
            source={
              selectedViewType === viewType.list
                ? require("../../../res/icon/list.png")
                : require("../../../res/icon/grid.png")
            }
          />
        </View>
        {this.renderList()}
      </View>
    );
  }

  renderPromotion = ({ item }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem item={item.product} />
    ) : (
      <PromotionItemList item={item.product} />
    );
  };

  renderList = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      joinedCampaigns
    } = this.props.joinedCampaigns;
    const { fetchMoreJoinedCampaigns, refreshJoinedCampaigns } = this.props;
    return (
      <PaginationList
        ref={ref => (this.list = ref)}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderPromotion}
        loading={loading}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        firstLoading={firstLoading}
        onEndReached={() => fetchMoreJoinedCampaigns()}
        onRefresh={() => refreshJoinedCampaigns()}
        data={joinedCampaigns}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  componentDidMount = () => {
    this.props.requestJoinedCampaigns();
  };
}

export default connect(
  state => ({
    joinedCampaigns: state.joinedCampaigns,
    profile: state.profile
  }),
  {
    resetPage,
    navigateToPage,
    fetchMoreJoinedCampaigns,
    refreshJoinedCampaigns,
    requestJoinedCampaigns
  }
)(JoinedCampaignsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  text: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: appColor.blur
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(9)
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: sizeWidth(5),
    paddingHorizontal: sizeWidth(10)
  }
});
