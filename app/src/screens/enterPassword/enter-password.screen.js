import React, { Component } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import firebase from "react-native-firebase";
import { connect } from "react-redux";
import validator from "validator";
import lodash from "lodash";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import Text from "../../components/common/text";
import {
  sizeWidth,
  sizeHeight,
  sizeFont,
  moderateScale
} from "../../helpers/size.helper";
import Button from "../../components/common/button";
import Input from "../../components/common/input";
import { font, text, appColor, loginType } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import TouchableIcon from "../../components/common/touchable-icon";
import Api from "../../api/api";
import { appConfig } from "../../config/app.config";
import { saveClientCas } from "../../helpers/storage.helper";
import { showError } from "../../helpers/error.helper";
import { checkApiStatus, logDebug } from "../../helpers/app.helper";
import { requestedTopCategories } from "../../actions/top-categories.action";
import { requestCountNotifications } from "../../actions/notifications.action";
import { requestedProfile } from "../../actions/profile.action";
import { requestedTopMerchants } from "../../actions/top-merchants.action";
import { requestedFilters } from "../../actions/filters.action";
import { requestedMasterData } from "../../actions/master-data.action";
import CacheImage from "../../components/common/cache-image";
import NotificationView from "../../components/common/notification-view";
import KeyboardHandlerView from "../../components/common/keyboard-handler-view";

class EnterPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: text.emptyString,
      loggingIn: false,
      passwordVisible: false
    };
  }

  toggleShowPassword = () => {
    const { passwordVisible } = this.state;
    this.setState({
      passwordVisible: !passwordVisible
    });
  };

  render() {
    const { password, loggingIn, passwordVisible } = this.state;
    const { avatar, phone } = this.props.navigation.state.params;
    return (
      <View style={styles.container}>
        <Toolbar
          center={<Text style={styles.title}>Nhập mật khẩu</Text>}
          left={<BackIcon />}
        />
        <KeyboardHandlerView
          keyboardVerticalOffset={moderateScale(20)}
          behavior="padding"
          style={styles.body}
        >
          <ScrollView>
            <CacheImage
              style={styles.avatar}
              placeholder={require("../../../res/icon/img-avatar.png")}
              uri={avatar}
            />
            <Text style={styles.phone}>{`0${phone.replace(/^0+/, "")}`}</Text>
            <Input
              ref={ref => (this.passwordRef = ref)}
              value={password}
              onChangeText={text => this.setState({ password: text })}
              secureTextEntry={!passwordVisible}
              iconStyle={styles.eye}
              right={
                <TouchableIcon
                  style={styles.visible}
                  onPress={this.toggleShowPassword}
                  iconStyle={styles.eye}
                  source={
                    passwordVisible
                      ? require("../../../res/icon/visible.png")
                      : require("../../../res/icon/eye-off.png")
                  }
                />
              }
              label="Mật khẩu"
              placeholder="Nhập mật khẩu"
            />
            <Text onPress={this.navigateToForgotPassword} style={styles.forgot}>
              Quên mật khẩu?
            </Text>
          </ScrollView>
          <Button
            loading={loggingIn}
            style={styles.button}
            onPress={this.login}
            text="TIẾP TỤC"
          />
          <NotificationView />
        </KeyboardHandlerView>
      </View>
    );
  }

  navigateToForgotPassword = () => {
    this.props.navigateToPage("ForgotPassword");
  };

  login = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    try {
      const { phone } = this.props.navigation.state.params;
      const { password } = this.state;
      if (validator.isEmpty(password)) {
        return showError("Mật khẩu không được để trống!");
      }
      if (password.length < 6 || password.length > 12) {
        return showError("Vui lòng nhập ít nhất từ 6 đến 12 kí tự");
      }
      this.setState({ loggingIn: true });
      const response = await Api.loginForApp({
        access_token: text.emptyString,
        service: appConfig.ssoService,
        provider: loginType.otp,
        type: loginType.otp,
        social_access_token: text.emptyString,
        phone,
        password
      });
      if (response.status === "SUCCESS") {
        await saveClientCas(response.client_cas);
        const fcmToken = await firebase.messaging().getToken();
        await Api.registerTokenId(fcmToken);
        // TODO: Temporary hide banner as requested
        // this.props.requestedBanners();
        this.props.requestedTopCategories();
        this.props.requestCountNotifications();
        this.props.requestedProfile();
        this.props.requestedTopMerchants();
        this.props.requestedFilters();
        this.props.requestedMasterData();

        const lastScreen = lodash.get(this.props.navigation.state.params, "lastScreen");
        const action = lodash.get(this.props.navigation.state.params, "action");
        const url = lodash.get(this.props.navigation.state.params, "url");
        if (lastScreen !== undefined)
          this.props.navigateToPage(lastScreen, { isComeback: "true", action, url });
        else this.props.resetPage("Main");
      } else {
        this.setState({ loggingIn: false });
        showError("Tên đăng nhập hoặc mật khẩu không đúng!");
      }
    } catch (err) {
      logDebug("enter-password.screen.js", "login ERR", err);
      this.setState({ loggingIn: false });
    }
  };

  componentDidMount = async () => {
    if (this.passwordRef) this.passwordRef.focus();
  };
}

export default connect(
  null,
  {
    resetPage,
    navigateToPage,
    navigateBack,
    requestedTopCategories,
    requestCountNotifications,
    requestedProfile,
    requestedTopMerchants,
    requestedFilters,
    requestedMasterData
  }
)(EnterPasswordScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF"
  },
  logo: {
    marginTop: sizeHeight(56),
    alignSelf: "center",
    width: sizeWidth(134),
    height: sizeWidth(29)
  },
  text: {
    fontSize: sizeFont(24),
    fontFamily: font.bold,
    color: "#444444",
    alignSelf: "center",
    marginTop: sizeHeight(19),
    marginBottom: sizeHeight(17)
  },
  haveAccount: {
    alignSelf: "center",
    marginTop: sizeHeight(19)
  },
  login: {
    textDecorationLine: "underline"
  },
  privacy: {
    color: "rgb(79, 166, 206)"
  },
  icon: {
    width: sizeWidth(150),
    height: sizeWidth(150)
  },
  success: {
    fontSize: sizeFont(24),
    fontFamily: font.bold,
    marginTop: sizeHeight(27),
    marginBottom: sizeHeight(18)
  },
  desc: {
    fontSize: sizeFont(16),
    textAlign: "center"
  },
  back: {
    marginBottom: sizeWidth(8)
  },
  indicator: {
    flex: 1
  },
  info: {
    flex: 1,
    paddingHorizontal: sizeWidth(12),
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  button: {
    marginVertical: moderateScale(10)
  },
  body: {
    flex: 1
  },
  avatar: {
    marginVertical: moderateScale(10),
    width: moderateScale(100),
    alignSelf: "center",
    height: moderateScale(100)
  },
  phone: {
    fontSize: sizeFont(13),
    fontFamily: font.bold,
    marginBottom: moderateScale(10),
    alignSelf: "center"
  },
  forgot: {
    fontSize: sizeFont(13),
    color: "#1E96C8",
    alignSelf: "flex-end",
    marginHorizontal: sizeWidth(16),
    marginTop: moderateScale(8)
  },
  eye: {
    width: moderateScale(14),
    tintColor: appColor.blur,
    height: moderateScale(14)
  },
  visible: {
    marginRight: moderateScale(6)
  }
});
