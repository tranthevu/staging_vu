import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image } from "react-native";
import { connect } from "react-redux";
import validator from "validator";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import Text from "../../components/common/text";
import {
  sizeWidth,
  sizeHeight,
  sizeFont,
  moderateScale
} from "../../helpers/size.helper";
import Button from "../../components/common/button";
import Input from "../../components/common/input";
import { font, text, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import TouchableIcon from "../../components/common/touchable-icon";
import { NOTICE_SCREEN } from "../../navigators/screensName";
import { showError } from "../../helpers/error.helper";
import NotificationView from "../../components/common/notification-view";
import Api from "../../api/api";
import { checkApiStatus } from "../../helpers/app.helper";

class NewPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: text.emptyString,
      retypePassword: text.emptyString,
      passwordVisible: false,
      retypePasswordVisible: false,
      loading: false
    };
  }

  toggleShowPassword = () => {
    const { passwordVisible } = this.state;
    this.setState({
      passwordVisible: !passwordVisible
    });
  };

  toggleShowRetypePassword = () => {
    const { retypePasswordVisible } = this.state;
    this.setState({
      retypePasswordVisible: !retypePasswordVisible
    });
  };

  render(): ReactNode {
    const {
      password,
      passwordVisible,
      retypePassword,
      retypePasswordVisible,
      loading
    } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
          center={<Text style={styles.title}>Tạo mật khẩu mới</Text>}
          left={<BackIcon />}
        />
        <View style={styles.body}>
          <Text style={styles.enter}>Tạo mật khẩu mới</Text>
          <Text style={styles.message}>Nhập mật khẩu mới cho tài khoản của bạn.</Text>
          <Input
            value={password}
            onChangeText={text => this.setState({ password: text })}
            secureTextEntry={!passwordVisible}
            iconStyle={styles.eye}
            right={
              <TouchableIcon
                style={styles.visible}
                onPress={this.toggleShowPassword}
                iconStyle={styles.eye}
                source={
                  passwordVisible
                    ? require("../../../res/icon/visible.png")
                    : require("../../../res/icon/eye-off.png")
                }
              />
            }
            label="Mật khẩu mới"
            placeholder="Tạo mật khẩu"
          />
          <Input
            value={retypePassword}
            onChangeText={text => this.setState({ retypePassword: text })}
            secureTextEntry={!retypePasswordVisible}
            iconStyle={styles.eye}
            right={
              <TouchableIcon
                style={styles.visible}
                onPress={this.toggleShowRetypePassword}
                iconStyle={styles.eye}
                source={
                  retypePasswordVisible
                    ? require("../../../res/icon/visible.png")
                    : require("../../../res/icon/eye-off.png")
                }
              />
            }
            label="Nhập lại mật khẩu"
            placeholder="Nhập lại mật khẩu"
          />
          <NotificationView />
        </View>
        <Button
          style={styles.button}
          loading={loading}
          disabled={loading}
          onPress={this.submitNewPassword}
          text="TẠO MẬT KHẨU MỚI"
        />
      </View>
    );
  }

  submitNewPassword = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { password, retypePassword } = this.state;
    if (validator.isEmpty(password))
      return showError("Mật khẩu mới không được để trống!");
    if (validator.isEmpty(retypePassword))
      return showError("Vui lòng nhập lại mật khẩu mới!");
    if (
      password.length < 6 ||
      password.length > 12 ||
      retypePassword.length < 6 ||
      retypePassword.length > 12
    ) {
      return showError("Vui lòng nhập ít nhất từ 6 đến 12 kí tự");
    }
    if (password !== retypePassword)
      return showError("Mật khẩu mới và mật khẩu xác nhận không khớp!");
    try {
      const { phone } = this.props.navigation.state.params;
      this.setState({ loading: true });
      await Api.newPassword(phone, password);
      this.props.navigateToPage(NOTICE_SCREEN, {
        hideCancel: true,
        title: "Thành công",
        action: () => this.props.resetPage("Login"),
        button: "QUAY LẠI ĐĂNG NHẬP",
        message: "Tạo mật khẩu mới thành công",
        desc:
          "Chúc mừng bạn đã tạo mật khẩu mới thành công. Bạn có thể đăng nhập tài khoản bằng mật khẩu mới."
      });
      this.setState({ loading: false });
    } catch (err) {
      this.setState({ loading: false });
    }
  };

  navigateToForgotPassword = () => {
    this.props.navigateToPage("ForgotPassword");
  };

  componentDidMount = async () => {};
}

export default connect(null, { resetPage, navigateToPage, navigateBack })(
  NewPasswordScreen
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF"
  },
  cancel: {
    color: "white"
  },
  logo: {
    marginTop: sizeHeight(56),
    alignSelf: "center",
    width: sizeWidth(134),
    height: sizeWidth(29)
  },
  text: {
    fontSize: sizeFont(24),
    fontFamily: font.bold,
    color: "#444444",
    alignSelf: "center",
    marginTop: sizeHeight(19),
    marginBottom: sizeHeight(17)
  },
  haveAccount: {
    alignSelf: "center",
    marginTop: sizeHeight(19)
  },
  login: {
    textDecorationLine: "underline"
  },
  privacy: {
    color: "rgb(79, 166, 206)"
  },
  icon: {
    width: sizeWidth(150),
    height: sizeWidth(150)
  },
  success: {
    fontSize: sizeFont(24),
    fontFamily: font.bold,
    marginTop: sizeHeight(27),
    marginBottom: sizeHeight(18)
  },
  desc: {
    fontSize: sizeFont(16),
    textAlign: "center"
  },
  back: {
    marginBottom: sizeWidth(8)
  },
  indicator: {
    flex: 1
  },
  info: {
    flex: 1,
    paddingHorizontal: sizeWidth(12),
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  button: {
    marginVertical: moderateScale(10)
  },
  body: {
    flex: 1
  },
  eye: {
    width: moderateScale(14),
    tintColor: appColor.blur,
    height: moderateScale(14)
  },
  visible: {
    marginRight: moderateScale(6)
  },
  message: {
    marginHorizontal: sizeWidth(9),
    marginVertical: sizeWidth(9),
    textAlign: "center",
    color: appColor.blur,
    alignSelf: "center",
    fontSize: sizeFont(13)
  },
  enter: {
    marginHorizontal: sizeWidth(9),
    fontSize: sizeFont(15),
    fontFamily: font.medium,
    alignSelf: "center",
    marginTop: sizeWidth(20)
  }
});
