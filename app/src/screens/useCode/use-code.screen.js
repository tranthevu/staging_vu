import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, ScrollView } from "react-native";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor, event } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import QRCode from "react-native-qrcode";
import lodash from "lodash";
import moment from "moment";
import { removeVoucher } from "../../actions/vouchers.action";
import Barcode from "../../components/common/barcode";
import EventRegister from "../../helpers/event-register.helper";
import NotificationView from "../../components/common/notification-view";

class UseCodeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countdown: 90
    };
  }

  componentDidMount = async () => {
    const { voucher } = this.props.navigation.state.params;
    this.countdownInterval = setInterval(() => {
      const { countdown } = this.state;
      if (countdown === 1) {
        this.props.navigateBack();
      } else {
        this.setState({
          countdown: countdown - 1
        });
      }
    }, 1000);
    this.codeUsedEvent = EventRegister.on(event.codeUsed, this.onCodeUsed);
  };

  onCodeUsed = data => {
    const { voucher, code } = this.props.navigation.state.params;
    if (voucher.id == data.id && data.code_id == code.id) this.props.navigateBack();
  };

  componentWillUnmount = () => {
    clearInterval(this.countdownInterval);
    EventRegister.off(this.codeUsedEvent);
  };

  renderConfirmCode = code => {
    return (
      <>
        <Text style={styles.confirm}>Mã xác nhận ưu đãi</Text>
        <Text style={styles.confirmCode}>{code}</Text>
        <View style={styles.separator} />
      </>
    );
  };

  render() {
    const { voucher, code } = this.props.navigation.state.params;
    const title = lodash.get(voucher, "title");
    const endDate = moment(lodash.get(voucher, "code.end_datetime"));
    const now = moment();
    const diff = endDate.startOf("day").diff(now.startOf("day"), "days");
    const used = lodash.get(voucher, "is_used");
    const isExpired = endDate.isBefore(now);
    const multiUse = lodash.get(voucher, "deal_set.usage") === "Multi-use";
    const combined = `${lodash.get(code, "code")}__${lodash.get(code, "code_confirm")}`;
    const { countdown } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          center={<Text style={styles.toolbar}>Sử dụng ưu đãi</Text>}
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
        />
        <View style={styles.middle}>
          <ScrollView bounces={false}>
            <View style={styles.body}>
              <View style={styles.header}>
                <Text style={styles.title}>{title}</Text>
              </View>
              <Image style={styles.line} source={require("../../../res/icon/line.png")} />
              <View style={styles.content}>
                <View style={styles.countdown}>
                  <Text style={styles.valid}>
                    Mã có hiệu lực trong{" "}
                    <Text style={styles.time}>
                      {moment.utc(countdown * 1000).format("mm:ss")}
                    </Text>
                  </Text>
                </View>
                <View style={styles.single}>
                  <QRCode
                    value={combined}
                    size={sizeWidth(130)}
                    bgColor={used ? "rgb(204, 204, 204)" : "black"}
                    fgColor="white"
                  />
                </View>
                <View style={used ? styles.blurWrap : styles.wrap}>
                  <Text style={used ? styles.blurCode : styles.code}>
                    {lodash.get(code, "code")}
                  </Text>
                </View>
                <Barcode
                  height={sizeWidth(35)}
                  value={combined}
                  format="CODE128"
                  width={1}
                />
                {this.renderConfirmCode(lodash.get(code, "code_confirm"))}
                <Text style={styles.desc}>Cung cấp mã code tại cửa hàng để sử dụng</Text>
              </View>
              <Image style={styles.line} source={require("../../../res/icon/line.png")} />
              <View style={styles.footer}>
                <View style={styles.row}>
                  <Text style={styles.label}>Hạn sử dụng</Text>
                  <Text style={styles.text}>
                    {used
                      ? "Đã dùng"
                      : isExpired
                      ? "Đã hết hạn"
                      : diff === 0
                      ? "Hết hạn hôm nay"
                      : `Còn ${diff} ngày`}
                  </Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.label}>Ngày hết hạn</Text>
                  <Text style={styles.text}>{endDate.format("DD/MM/YYYY")}</Text>
                </View>
              </View>
              <View style={[styles.circle, styles.left, styles.top]} />
              <View style={[styles.circle, styles.right, styles.top]} />
              <View style={[styles.circle, styles.left, styles.bottom]} />
              <View style={[styles.circle, styles.right, styles.bottom]} />
            </View>
          </ScrollView>
          <NotificationView />
        </View>
      </View>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage, navigateBack, removeVoucher }
)(UseCodeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F2F2F2"
  },
  middle: {
    flex: 1
  },
  toolbar: {
    fontFamily: font.bold,
    fontSize: sizeFont(18),
    color: "white"
  },
  body: {
    width: sizeWidth(302),
    overflow: "hidden",
    borderRadius: sizeWidth(6),
    backgroundColor: "white",
    alignSelf: "center",
    marginVertical: sizeWidth(14)
  },
  header: {
    height: sizeWidth(62),
    paddingHorizontal: sizeWidth(16),
    alignItems: "center",
    justifyContent: "center"
  },
  swiper: {
    height: sizeWidth(130)
  },
  title: {
    textAlign: "center",
    fontFamily: font.medium
  },
  footer: {
    height: sizeWidth(91),
    paddingHorizontal: sizeWidth(16),
    justifyContent: "center"
  },
  row: {
    flexDirection: "row",
    paddingVertical: sizeWidth(6)
  },
  label: {
    flex: 1,
    fontFamily: font.medium
  },
  text: {
    flex: 1
  },
  content: {
    alignItems: "center",
    overflow: "hidden",
    width: sizeWidth(302),
    paddingVertical: sizeWidth(15)
  },
  qr: {
    width: sizeWidth(130),
    height: sizeWidth(130),
    alignSelf: "center"
  },
  single: {
    width: sizeWidth(130),
    height: sizeWidth(130),
    // Fix bug https://yollateam.atlassian.net/browse/YOLLA-417
    // [Yolla-App-0.77][Detail Offer][Multiple Code] Missing text "s" on detail product
    // How to fix: https://github.com/cssivision/react-native-qrcode/issues/68#issuecomment-455791280
    overflow: "hidden"
  },
  wrap: {
    width: sizeWidth(182),
    height: sizeWidth(34),
    borderRadius: sizeWidth(17),
    borderWidth: 1,
    borderColor: "#FF6961",
    justifyContent: "center",
    alignItems: "center",
    marginVertical: moderateScale(15),
    borderStyle: "dashed"
  },
  blurWrap: {
    width: sizeWidth(182),
    height: sizeWidth(34),
    borderRadius: sizeWidth(17),
    borderWidth: 1,
    borderColor: appColor.blur,
    justifyContent: "center",
    alignItems: "center",
    marginVertical: moderateScale(15),
    borderStyle: "dashed"
  },
  blurCode: {
    fontSize: sizeFont(16),
    fontFamily: font.bold,
    color: appColor.blur,
    textDecorationLine: "line-through"
  },
  code: {
    fontSize: sizeFont(16),
    fontFamily: font.bold
  },
  desc: {
    width: sizeWidth(170),
    textAlign: "center",
    color: appColor.blur
  },
  line: {
    width: "94%",
    alignSelf: "center",
    height: 1
  },
  circle: {
    backgroundColor: appColor.bg,
    position: "absolute",
    width: sizeWidth(18),
    height: sizeWidth(18),
    borderRadius: sizeWidth(9)
  },
  left: {
    left: sizeWidth(-9)
  },
  top: {
    top: sizeWidth(53)
  },
  bottom: {
    bottom: sizeWidth(82)
  },
  right: {
    right: sizeWidth(-9)
  },
  confirm: {
    fontSize: sizeFont(14),
    fontFamily: font.bold,
    marginVertical: moderateScale(8)
  },
  confirmCode: {
    fontSize: sizeFont(16)
  },
  separator: {
    width: sizeWidth(224),
    height: 1,
    backgroundColor: appColor.blur,
    marginVertical: sizeWidth(6)
  },
  cancel: {
    color: "white",
    fontSize: sizeFont(13)
  },
  valid: {
    fontSize: sizeFont(13)
  },
  countdown: {
    width: sizeWidth(320),
    alignItems: "center",
    marginBottom: moderateScale(10)
  },
  time: {
    fontSize: sizeFont(13),
    color: appColor.primary
  }
});
