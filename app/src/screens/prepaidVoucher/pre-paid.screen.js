import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  ScrollView,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { WebView } from "react-native-webview";
import Button from "../../components/common/button";
import { text, appColor } from "../../constants/app.constant";
import { moderateScale, sizeWidth } from "../../helpers/size.helper";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import { formatPrice } from "../../helpers/common.helper";
import Api from "../../api/api";
import LoadingIndicator from "../../components/common/loading-indicator";
import InfoInput from "./info-input";
import { styles } from "./pre-paid.screen.style";
import NotificationView from "../../components/common/notification-view";
import Text from "../../components/common/text";
import { showError } from "../../helpers/error.helper";
import AuthHelper from "../../helpers/auth.helper";
import { checkApiStatus } from "../../helpers/app.helper";

class prePaid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: 1,
      webview: false,
      typeSuccess: 0,
      loading: true,
      email: text.emptyString,
      phone: text.emptyString,
      fullname: text.emptyString,
      paymenMethods: [],
      selectedPaymentMethod: null,
      updatingPaymentInfo: false,
      submitting: false,
      webviewLoading: false,
      data: null
    };
  }

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { basketID } = this.props.navigation.state.params;
    const data = await Promise.all([
      Api.getCartPayment(basketID),
      Api.getPaymentMethods()
    ]);
    this.setState({
      data: data[0].data,
      paymenMethods: data[1].data,
      selectedPaymentMethod: lodash.get(data, "[1].data[0].slug"),
      loading: false
    });
    if (this.state.typeSuccess !== 0) {
      this.props.navigateToPage("NotificationPrepaid", {});
    }
    const isAuthenticated = await AuthHelper.isAuthenticated();
    if (isAuthenticated) {
      if (this.state.data) {
        const { data } = this.state;
        const fullname = lodash.get(data, "user.full_name", "");
        const phone = lodash.get(data, "user.phone", 0);
        const email = lodash.get(data, "user.email", "");
        this.setState({
          fullname,
          phone,
          email
        });
      }
    }
  };

  decreaseAmount = () => {
    const { amount } = this.state;
    if (amount > 1) {
      this.setState({
        amount: amount - 1
      });
    }
  };

  increaseAmount = () => {
    const { amount, data } = this.state;
    if (amount === data.max_quantity_code_per_user) return;
    this.setState({
      amount: amount + 1
    });
  };

  renderAmount = () => {
    const { data } = this.state;
    const title = lodash.get(data, "title", "");
    const price = lodash.get(data, "price", 0);
    const name = lodash.get(data, "partner.partner_name", "");
    const logo = lodash.get(data, "partner.logo_image");
    const image = lodash.get(data, "images[0].original");
    return (
      <View style={styles.infoProdduct}>
        <ImageBackground style={styles.image} source={{ uri: image }} />
        <View style={styles.body}>
          <View style={{ flex: 1 }}>
            <Text style={styles.title} numberOfLines={2}>
              {title}
            </Text>
            <View style={styles.bottom}>
              <View style={styles.left}>
                <Image style={styles.merchant} source={{ uri: logo }} />
                <Text style={styles.name}>{name}</Text>
              </View>
            </View>
            <Text style={styles.price}>
              {formatPrice(price)}
              <Text style={styles.underline}>đ</Text>
            </Text>
          </View>
          <View style={styles.quantity}>
            <TouchableOpacity
              disabled={data.max_quantity_code_per_user === this.state.amount}
              style={styles.amount}
              onPress={this.increaseAmount}
            >
              <Image
                source={require("../../../res/icon/payment_plus.png")}
                style={{
                  width: moderateScale(20),
                  height: moderateScale(20)
                }}
              />
            </TouchableOpacity>
            <View style={styles.amount}>
              <Text>{this.state.amount}</Text>
            </View>
            <TouchableOpacity
              disabled={this.state.amount === 1}
              style={styles.amount}
              onPress={this.decreaseAmount}
            >
              <Image
                source={require("../../../res/icon/payment_minus.png")}
                style={{
                  width: moderateScale(20),
                  height: moderateScale(20)
                }}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  renderCost = () => {
    const { data, amount } = this.state;
    const price = lodash.get(data, "price", 0);
    const totalPrice = lodash.get(data, "total_price", 0);
    return (
      <View style={styles.infoCost}>
        <View style={styles.money}>
          <Text style={styles.costTitle}>Tạm tính</Text>
          <Text style={styles.costValue}>
            {formatPrice(price * amount)}
            <Text style={styles.costUnderline}>đ</Text>
          </Text>
        </View>
        <View style={styles.money}>
          <View style={{ flex: 1 }}>
            <Text style={styles.costTitle}>Tổng thanh toán</Text>
          </View>
          <Text style={styles.costSum}>
            {formatPrice(totalPrice * amount)}
            <Text style={styles.underline}>đ</Text>
          </Text>
        </View>
      </View>
    );
  };

  renderPay = () => {
    const { paymenMethods, selectedPaymentMethod } = this.state;
    return (
      <View style={styles.infoPay}>
        <View style={styles.body}>
          <Text style={styles.payTitle}>Hình thức thanh toán</Text>
        </View>
        {paymenMethods.map(method => {
          return (
            <TouchableOpacity
              onPress={() => this.setState({ selectedPaymentMethod: method.slug })}
              style={styles.method}
            >
              <Image
                source={
                  method.slug === selectedPaymentMethod
                    ? require("../../../res/icon/alepay_checked.png")
                    : require("../../../res/icon/alepay_unchecked.png")
                }
                style={styles.unchecked}
              />
              <View style={styles.payAlepay}>
                <Text style={styles.payText}>{method.info}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  toggleUpdatePaymentInfo = () => {
    const { updatingPaymentInfo } = this.state;
    this.setState({
      updatingPaymentInfo: !updatingPaymentInfo
    });
  };

  renderProfile = () => {
    const { updatingPaymentInfo } = this.state;
    return (
      <View style={styles.infoProfile}>
        <View style={styles.buy}>
          <View style={styles.row}>
            <Text style={styles.payProfile}>Thông tin người mua</Text>
          </View>
          <TouchableOpacity onPress={this.toggleUpdatePaymentInfo}>
            <Text style={styles.profileUpdate}>Cập nhật</Text>
          </TouchableOpacity>
        </View>
        <InfoInput
          value={this.state.fullname}
          updating={updatingPaymentInfo}
          placeholder="Cập nhật họ và tên để mua hàng"
          label="Họ và tên"
          onChangeText={text => this.setState({ fullname: text })}
          warning="Bạn cần cập nhật họ và tên để mua hàng"
        />
        <InfoInput
          value={this.state.phone}
          updating={updatingPaymentInfo}
          placeholder="Cập nhật số điện thoại để mua hàng"
          label="Số điện thoại"
          onChangeText={text => this.setState({ phone: text })}
          warning="Bạn cần cập nhật số điện thoại để mua hàng"
        />
        <InfoInput
          value={this.state.email}
          updating={updatingPaymentInfo}
          placeholder="Cập nhật email để mua hàng"
          label="Email"
          onChangeText={text => this.setState({ email: text })}
          warning="Bạn cần cập nhật email để mua hàng"
        />
      </View>
    );
  };

  btnPaid = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    try {
      const { amount } = this.state;
      let { basketID } = this.props.navigation.state.params;
      const { product } = this.props.navigation.state.params;
      if (amount !== 1) {
        const cartData = await Api.addCart({
          product_slug: product.slug,
          quantity: amount
        });
        basketID = lodash.get(cartData, "data.basket");
      }
      const { email, fullname, phone, selectedPaymentMethod } = this.state;
      if (!email) return showError("Bạn cần cập nhật email để mua hàng");
      if (!phone) return showError("Bạn cần cập nhật số điện thoại để mua hàng");
      if (!fullname) return showError("Bạn cần cập nhật họ và tên để mua hàng");
      if (!selectedPaymentMethod)
        return showError("Bạn cần chọn phương thức thanh toán để mua hàng");
      this.setState({
        submitting: true
      });
      const data = await Api.proceedPayment(basketID, {
        full_name: fullname,
        phone,
        email
      });
      const url = lodash.get(data, "data");
      this.setState({
        webview: true,
        submitting: false,
        url
      });
    } catch (err) {
      this.setState({
        submitting: false
      });
    }
  };

  onNavigationStateChange = async webViewState => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    if (webViewState.url.indexOf("uuid") !== -1) {
      const arrPaidment = await Promise.all([Api.getPaymentTransactions()]);
      const id = lodash.get(arrPaidment[0].results[0], "uuid", 0);
      const data = await Promise.all([Api.getTransaction(id)]);
      const status = lodash.get(data[0], "data.payment_status");
      this.setState({
        webview: false
      });
      this.props.navigateBack();
      this.props.navigateToPage("NotificationPrepaid", { status });
    }
  };

  renderWebview = () => {
    const { webviewLoading } = this.state;
    return (
      <View style={styles.root}>
        <WebView
          onNavigationStateChange={this.onNavigationStateChange}
          source={{ uri: this.state.url }}
          onLoadStart={() => this.setState({ webviewLoading: true })}
          onLoadEnd={() => this.setState({ webviewLoading: false })}
          style={styles.webview}
        />
        {/* {webviewLoading && (
          <View style={styles.overlay}>
            <ActivityIndicator size="large" />
          </View>
        )} */}
      </View>
    );
  };

  renderInfo = () => {
    if (this.state.webview) return this.renderWebview();
    return (
      <>
        <View style={{ flex: 1, marginBottom: sizeWidth(50) }}>
          <KeyboardAwareScrollView>
            {this.renderAmount()}
            {this.renderCost()}
            {this.renderPay()}
            {this.renderProfile()}
          </KeyboardAwareScrollView>
        </View>
        <View style={{ marginVertical: moderateScale(15) }}>
          <Button style={styles.btnPaid} text="THANH TOÁN" onPress={this.btnPaid} />
        </View>
      </>
    );
  };

  render() {
    const { submitting } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.header}>Thanh toán</Text>}
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
        />
        <View style={styles.center}>
          {this.state.loading ? (
            <LoadingIndicator style={styles.loading} />
          ) : this.state.webview ? (
            this.renderWebview()
          ) : (
            <>
              <View style={{ flex: 1, marginBottom: sizeWidth(50) }}>
                <KeyboardAwareScrollView>
                  {this.renderAmount()}
                  {this.renderCost()}
                  {this.renderPay()}
                  {this.renderProfile()}
                </KeyboardAwareScrollView>
              </View>
              <View style={{ marginVertical: moderateScale(15) }}>
                <Button
                  loading={submitting}
                  style={styles.btnPaid}
                  text="THANH TOÁN"
                  onPress={this.btnPaid}
                />
              </View>
            </>
          )}
          <NotificationView />
        </View>
      </View>
    );
  }
}
export default connect(null, { resetPage, navigateToPage, navigateBack })(prePaid);
