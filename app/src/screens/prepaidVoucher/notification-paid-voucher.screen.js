import React, { Component } from "react";
import { View, TouchableOpacity, StyleSheet, Text, Image } from "react-native";
import { connect } from "react-redux";
import Button from "../../components/common/button";
import { font, appColor, eVoucherStatus, event } from "../../constants/app.constant";
import {
  moderateScale,
  sizeFont,
  sizeWidth,
  sizeHeight
} from "../../helpers/size.helper";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import EventRegister from "../../helpers/event-register.helper";

class notiPaid extends Component {
  navigateToDashboard = () => {
    this.props.resetPage("Main");
  };

  navigateToMyOffers = () => {
    this.props.resetPage("Main");
    this.props.navigateToPage("Archive", {
      selectedTab: 0
    });
    EventRegister.emit(event.jumpPage, 1);
  };

  render() {
    const { status } = this.props.navigation.state.params;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.header}>Thông báo giao dịch</Text>}
        />
        <View style={styles.center}>
          <Image
            style={styles.image}
            source={
              status === "failed" || status === "settled" || status === "cancelled"
                ? require("../../../res/icon/page_unsuccess.png")
                : require("../../../res/icon/page_success.png")
            }
          />
          <Text style={styles.title}>
            {status === "failed" || status === "settled" || status === "cancelled"
              ? "Thất bại"
              : "Thành công"}
          </Text>
          <Text style={styles.text}>
            {status === "failed" || status === "settled" || status === "cancelled"
              ? "Giao dịch không thành công. Xin vui lòng thử lại."
              : "Giao dịch của bạn thực hiện thành công. Cảm ơn bạn đã mua hàng tại Yolla."}
          </Text>
          <View style={styles.bottom}>
            {status === "failed" || status === "settled" || status === "cancelled" ? (
              <Button onPress={this.retry} style={styles.get} text="THỬ LẠI" />
            ) : (
              <Button
                style={styles.get}
                text="XEM ƯU ĐÃI"
                onPress={this.navigateToMyOffers}
              />
            )}
            <TouchableOpacity
              onPress={() => this.navigateToDashboard()}
              style={styles.redirect}
            >
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <Text style={{ color: "#55ACEE", fontSize: sizeFont(14) }}>
                  Tiếp tục mua sắm
                </Text>
              </View>
              <Image
                style={styles.arrow}
                source={require("../../../res/icon/page_arrow_right.png")}
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  retry = () => {
    this.props.navigateBack();
  };
}
export default connect(
  null,
  { resetPage, navigateToPage, navigateBack }
)(notiPaid);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  center: {
    flex: 1
  },
  get: {
    alignSelf: "center",
    width: sizeWidth(270),
    height: sizeHeight(44),
    borderRadius: 22,
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 2,
    shadowColor: "rgba(239, 65, 54, 0.2)",
    shadowOffset: {
      width: 0,
      height: 4
    }
  },
  title: {
    fontSize: sizeWidth(18),
    fontFamily: font.medium,
    alignSelf: "center",
    marginTop: moderateScale(22),
    marginBottom: moderateScale(15)
  },
  text: {
    alignSelf: "center",
    fontSize: sizeWidth(15),
    textAlign: "center",
    paddingHorizontal: sizeWidth(20)
  },
  image: {
    alignSelf: "center",
    width: moderateScale(120),
    marginTop: moderateScale(54),
    height: moderateScale(110)
  },
  redirect: {
    justifyContent: "center",
    flexDirection: "row",
    marginVertical: sizeHeight(30)
  },

  arrow: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end"
  }
});
