import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, TextInput } from "react-native";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { appColor, font } from "../../constants/app.constant";

export default class InfoInput extends Component {
  render(): ReactNode {
    const {
      value,
      updating,
      placeholder,
      warning,
      onChangeText,
      label,
      style
    } = this.props;
    return (
      <View style={[styles.container, style]}>
        {!updating && (
          <View style={styles.item}>
            <Text style={styles.label}>{label}</Text>
            <Text style={styles.value}>{value}</Text>
          </View>
        )}
        {updating && (
          <View>
            <View style={styles.item}>
              <Text style={styles.label}>{label}</Text>
            </View>
            <TextInput
              style={styles.input}
              value={value}
              placeholderTextColor={appColor.blur}
              placeholder={placeholder}
              onChangeText={onChangeText}
            />
            {!value && <View style={styles.line} />}
            {!value && (
              <View style={styles.row}>
                <Text style={styles.changeEmail}>{warning}</Text>
                <Image
                  style={styles.warning}
                  source={require("../../../res/icon/warning.png")}
                />
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
  item: {
    flexDirection: "row",
    marginVertical: sizeWidth(4)
  },
  changeEmail: {
    flex: 1,
    color: "#EF4136",
    fontSize: sizeFont(12),
    marginRight: sizeWidth(12)
  },
  line: {
    backgroundColor: "#FF3B30",
    height: 2
  },
  input: {
    padding: 0,
    paddingBottom: moderateScale(10),
    fontSize: sizeFont(12),
    color: appColor.text
  },
  warning: { width: moderateScale(16), height: moderateScale(14) },
  row: { flexDirection: "row", marginTop: moderateScale(8), alignItems: "center" },
  value: {
    flex: 1,
    fontSize: sizeFont(13),
    textAlign: "right"
  },
  label: {
    flex: 1,
    fontSize: sizeFont(13),
    fontFamily: font.medium
  }
});
