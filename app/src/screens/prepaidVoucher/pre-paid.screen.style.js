import { StyleSheet } from "react-native";
import { appColor, font } from "../../constants/app.constant";
import {
  sizeFont,
  moderateScale,
  sizeWidth,
  sizeHeight
} from "../../helpers/size.helper";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  center: {
    flex: 1
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  cancel: {
    color: "#FFFFFF",
    fontSize: sizeFont(13)
  },
  item: {
    flexDirection: "row"
  },
  profile: {
    fontSize: sizeFont(13),
    color: "#444444",
    marginTop: moderateScale(8),
    fontWeight: "500"
  },
  profileInput: {
    flex: 1,
    textAlign: "right",
    color: "#444444",
    fontSize: sizeFont(13),
    padding: 0,
    marginRight: moderateScale(10)
  },
  infoProfile: {
    width: sizeWidth(302),
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    backgroundColor: "#FFFFFF",
    borderRadius: sizeWidth(6),
    alignSelf: "center",
    paddingHorizontal: sizeWidth(10),
    paddingVertical: moderateScale(10),
    marginTop: moderateScale(10),
    marginBottom: moderateScale(10)
  },
  payProfile: {
    fontSize: sizeFont(16),
    fontFamily: font.medium
  },
  profileUpdate: {
    color: "#55ACEE",
    fontSize: sizeFont(13)
  },
  unchecked: {
    width: moderateScale(20),
    height: moderateScale(20)
  },
  money: {
    alignItems: "center",
    flexDirection: "row"
  },
  method: {
    alignItems: "center",
    flexDirection: "row",
    marginVertical: moderateScale(5)
  },
  payTitle: {
    fontSize: sizeFont(16),
    marginBottom: sizeWidth(7),
    fontFamily: font.medium
  },
  payAlepay: {
    marginLeft: moderateScale(14)
  },
  payText: { fontSize: sizeFont(13) },
  infoPay: {
    width: sizeWidth(302),
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    backgroundColor: "#FFFFFF",
    borderRadius: sizeWidth(6),
    alignSelf: "center",
    paddingHorizontal: sizeWidth(12),
    paddingVertical: moderateScale(12),
    marginTop: moderateScale(10)
  },
  infoCost: {
    width: sizeWidth(302),
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    backgroundColor: "#FFFFFF",
    borderRadius: sizeWidth(6),
    alignSelf: "center",
    paddingHorizontal: sizeWidth(10),
    paddingVertical: moderateScale(4),
    marginTop: moderateScale(10)
  },
  value: {
    flex: 1,
    alignItems: "flex-end",
    marginHorizontal: moderateScale(10)
  },
  infoProdduct: {
    width: sizeWidth(302),
    overflow: "hidden",
    alignSelf: "center",
    marginTop: moderateScale(10),
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    backgroundColor: "#FFFFFF",
    borderRadius: sizeWidth(6),
    flexDirection: "row",
    padding: sizeWidth(6)
  },
  image: {
    width: sizeWidth(85),
    height: sizeWidth(85),
    justifyContent: "flex-end",
    marginRight: sizeWidth(10),
    borderRadius: moderateScale(6),
    overflow: "hidden"
  },
  body: {
    flex: 1,
    flexDirection: "row"
  },
  costTitle: {
    flex: 1,
    marginVertical: moderateScale(6),
    fontSize: sizeFont(13),
    fontFamily: font.medium
  },
  title: {
    fontSize: sizeFont(12),
    fontFamily: font.bold,
    color: "#444444"
  },
  bottom: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: moderateScale(15)
  },
  left: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
    marginRight: sizeWidth(8)
  },
  merchant: {
    width: sizeWidth(16),
    height: sizeWidth(16),
    borderRadius: sizeWidth(8),
    marginRight: sizeWidth(6)
  },
  name: {
    fontSize: sizeFont(12),
    flex: 1
  },
  price: {
    color: "#EF4136",
    fontFamily: font.bold,
    fontSize: sizeFont(16),
    marginTop: moderateScale(15)
  },
  costValue: {
    fontSize: sizeFont(13),
    textAlign: "right",
    flex: 1
  },
  costSum: {
    color: "#EF4136",
    fontFamily: font.bold,
    fontSize: sizeFont(16),
    flex: 1,
    textAlign: "right"
  },
  quantity: {
    width: sizeWidth(35)
  },
  amount: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  btnPaid: {
    position: "absolute",
    bottom: moderateScale(12),
    alignSelf: "center",
    backgroundColor: "#FF792E",
    width: sizeWidth(270),
    height: sizeHeight(42),
    borderRadius: 22,
    shadowOpacity: 1,
    shadowRadius: 4,
    elevation: 2,
    shadowColor: "rgba(255, 121, 46, 0.2)",
    shadowOffset: {
      width: 0,
      height: 4
    }
  },
  underline: {
    textDecorationLine: "underline",
    color: "#EF4136",
    fontSize: sizeFont(16),
    fontFamily: font.bold
  },
  costUnderline: {
    textDecorationLine: "underline",
    color: "#444444",
    fontSize: sizeFont(13)
  },
  webview: {
    flex: 1
  },
  loading: {
    width: "100%",
    height: "100%",
    backgroundColor: "white"
  },
  row: {
    flex: 1,
    marginRight: sizeWidth(6)
  },
  buy: {
    flexDirection: "row",
    marginBottom: sizeWidth(6)
  },
  root: {
    flex: 1
  },
  overlay: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  }
});
