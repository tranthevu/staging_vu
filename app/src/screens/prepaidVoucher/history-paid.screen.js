import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ImageBackground,
  ScrollView
} from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import moment from "moment";
import { resetPage, navigateBack, navigateToPage } from "../../actions/nav.action";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { appColor, font, pagination } from "../../constants/app.constant";
import { formatPrice } from "../../helpers/common.helper";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import SeparatorLine from "../../components/common/separator-line";
import CacheImage from "../../components/common/cache-image";
import Api from "../../api/api";
import PaginationList from "../../components/common/pagination-list";
import EmptyView from "../../components/common/empty-view";
import LoadingIndicator from "../../components/common/loading-indicator";
import { checkApiStatus } from "../../helpers/app.helper";

class historyPrePaid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      reachedEnd: false,
      refeshing: false,
      loading: true,
      page: 1,
      page_size: pagination.pageSize,
      firstLoading: true
    };
  }

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { page, page_size } = this.state;
    const res = await Api.getPaymentTransactions({ page, page_size });
    this.setState({
      data: res.results,
      loading: false,
      reachedEnd: !res.next,
      firstLoading: false
    });
  };

  navigateToDetailPaid = basket_id => {
    this.props.navigateToPage("DetailPrepaidScreen", { basket_id });
  };

  getInfoProduct = async basket_id => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const product = await Promise.all([Api.getCartPayment(basket_id)]);
  };

  renderHistory = () => {
    const { data, reachedEnd, refeshing, loading, firstLoading } = this.state;
    return (
      <PaginationList
        keyExtractor={(item, index) => `${index}`}
        renderItem={this.renderItem}
        reachedEnd={reachedEnd}
        refreshing={refeshing}
        loading={loading}
        firstLoading={firstLoading}
        onEndReached={() => this.onLoadMore()}
        onRefresh={() => this.onRefesh()}
        data={data}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  renderItem = ({ item }) => {
    const code = lodash.get(item, "alepay_order_code", "");
    const date = lodash.get(item, "date_created", "");
    const status = lodash.get(item, "payment_status", "");
    const price = lodash.get(item, "amount", 0);
    const uuid = lodash.get(item, "uuid", 0);
    const name = lodash.get(item, "basket.line.title");
    const partner_name = lodash.get(item, "basket.line.partner.partner_name");
    const logo_image = lodash.get(item, "basket.line.partner.logo_image");
    const image = lodash.get(item, "basket.line.images[0].original");
    const time = moment(date).format("DD/MM/YYYY HH:mm");
    return (
      <TouchableOpacity
        style={styles.infoHistory}
        onPress={() => this.navigateToDetailPaid(uuid)}
      >
        <View style={styles.top}>
          <CacheImage style={styles.cover} uri={image} />
          <View style={styles.body}>
            <Text numberOfLines={1} style={styles.title}>
              {code ? `${code} - ${time}` : time}
            </Text>
            <Text numberOfLines={2} style={styles.content}>
              {name}
            </Text>
          </View>
          {status === "success" && (
            <View style={styles.status}>
              <Image
                source={require("../../../res/icon/history_success.png")}
                style={styles.statusPayment}
              />
            </View>
          )}
          {(status === "failed" || status === "settled" || status === "cancelled") && (
            <View style={styles.status}>
              <Image
                source={require("../../../res/icon/history_unsuccess.png")}
                style={styles.statusPayment}
              />
            </View>
          )}
        </View>
        <View style={styles.bottom}>
          <CacheImage uri={logo_image} style={styles.avatar} />
          <Text numberOfLines={1} style={styles.merchant}>
            {partner_name}
          </Text>
          <Text style={styles.amount}>
            {formatPrice(price)}
            <Text style={styles.unit}>đ</Text>
          </Text>
        </View>
        <SeparatorLine top={moderateScale(70)} />
      </TouchableOpacity>
    );
  };

  onRefesh = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { page_size } = this.state;
    this.setState({
      refeshing: true,
      page: 1
    });
    const res = await Api.getPaymentTransactions({ page: 1, page_size });
    if (res) {
      this.setState({
        refeshing: false,
        data: res.results,
        reachedEnd: !res.next
      });
    }
  };

  onLoadMore = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { page, page_size, data } = this.state;
    const nextPage = page + 1;
    this.setState({ loading: true });
    const res = await Api.getPaymentTransactions({ page: nextPage, page_size });
    if (res) {
      this.setState({
        loading: false,
        data: data.concat(res.results),
        reachedEnd: !res.next,
        page: nextPage
      });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.header}>Lịch sử mua hàng</Text>}
        />
        <View style={styles.body}>{this.renderHistory()}</View>
      </View>
    );
  }
}
export default connect(null, { resetPage, navigateToPage, navigateBack })(historyPrePaid);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  infoHistory: {
    backgroundColor: "white",
    marginVertical: moderateScale(5),
    marginHorizontal: sizeWidth(9),
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    }
  },
  bottom: {
    flexDirection: "row",
    paddingHorizontal: sizeWidth(12),
    paddingVertical: sizeWidth(8),
    alignItems: "center"
  },
  avatar: {
    width: moderateScale(16),
    height: moderateScale(16),
    borderRadius: moderateScale(8),
    marginRight: sizeWidth(4)
  },
  merchant: {
    flex: 1,
    fontSize: sizeFont(12)
  },
  saved: {
    fontSize: sizeFont(12),
    marginHorizontal: sizeWidth(12)
  },
  amount: {
    color: "#FF6961",
    fontFamily: font.bold,
    fontSize: sizeFont(14)
  },
  unit: {
    color: appColor.primary,
    fontFamily: font.bold,
    fontSize: sizeFont(14),
    textDecorationLine: "underline"
  },
  top: {
    paddingVertical: moderateScale(10),
    paddingHorizontal: sizeWidth(12),
    flexDirection: "row",
    height: moderateScale(79)
  },
  cover: {
    width: moderateScale(60),
    height: moderateScale(60),
    borderRadius: moderateScale(6),
    backgroundColor: "#BDBDBD",
    marginRight: moderateScale(10)
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(12),
    color: appColor.blur,
    flexDirection: "row"
  },
  content: {
    fontSize: sizeFont(12),
    fontFamily: font.medium,
    marginTop: moderateScale(9)
  },
  loading: {
    width: "100%",
    height: "100%",
    backgroundColor: "white"
  },
  status: {
    width: sizeWidth(31),
    alignItems: "flex-end"
  },
  statusPayment: {
    width: sizeWidth(16),
    height: sizeWidth(16)
  }
});
