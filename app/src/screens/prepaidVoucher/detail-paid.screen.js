import React, { Component } from "react";
import {
  View,
  TouchableOpacity,
  StyleSheet,
  Text,
  ImageBackground,
  Image,
  TextInput,
  ScrollView
} from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import Button from "../../components/common/button";
import { font, appColor, eVoucherStatus } from "../../constants/app.constant";
import {
  moderateScale,
  sizeFont,
  sizeWidth,
  sizeHeight
} from "../../helpers/size.helper";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import { formatPrice } from "../../helpers/common.helper";
import Api from "../../api/api";
import LoadingIndicator from "../../components/common/loading-indicator";
import moment from "moment";
import { checkApiStatus } from "../../helpers/app.helper";
class detailPaid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { basket_id } = this.props.navigation.state.params;
    const product = await Promise.all([Api.getTransaction(basket_id)]);
    this.setState({ data: product[0].data, loading: false });
  };

  renderPayment = () => {
    const data = this.state;
    const partner_name = lodash.get(data, "data.basket.line.partner.partner_name");
    const logo_image = lodash.get(data, "data.basket.line.partner.logo_image");
    const image = lodash.get(data, "data.basket.line.images[0].original");
    const name = lodash.get(data, "data.basket.line.title");
    return (
      <View style={styles.infoPayment}>
        <ImageBackground style={styles.image} source={{ uri: image }} />
        <View style={styles.body}>
          <View style={{ flex: 1 }}>
            <Text style={styles.title} numberOfLines={2}>
              {name}
            </Text>
            <View style={styles.bottom}>
              <View style={styles.left}>
                <Image style={styles.merchant} source={{ uri: logo_image }} />
                <Text style={styles.name}>{partner_name}</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  };

  renderDetailPayment = () => {
    const data = this.state;
    const code = lodash.get(data, "data.alepay_order_code", "");
    const time = moment(lodash.get(data, "data.date_created")).format("DD/MM/YYYY HH:mm");
    const amount = lodash.get(data, "data.amount", "");
    const status = lodash.get(data, "data.payment_status", "");
    const paymentMethod = lodash.get(data, "data.payment_method", "");
    return (
      <View style={styles.containerPayment}>
        <View style={styles.bgWhite}>
          <View style={styles.bodyPayment}>
            <Text style={styles.titlePayment}>Thông tin giao dịch</Text>
          </View>
          <View style={styles.bodyPayment}>
            <View style={{ flex: 1 }}>
              <Text style={styles.textPayment}>Mã giao dịch</Text>
            </View>
            <View style={styles.value}>
              <Text style={styles.textInfo}>{code}</Text>
            </View>
          </View>
        </View>
        <View style={styles.bgBlur}>
          <View style={styles.bodyPayment}>
            <View style={{ flex: 1 }}>
              <Text style={styles.textPayment}>Thời gian</Text>
            </View>
            <View style={styles.value}>
              <Text style={styles.textInfo}>{time}</Text>
            </View>
          </View>
        </View>
        <View style={styles.bgWhite}>
          <View style={styles.bodyPayment}>
            <View style={{ flex: 1 }}>
              <Text style={styles.textPayment}>Trạng thái</Text>
            </View>
            <View style={styles.value}>
              <Text style={[styles.textInfo, { color: "#9acd32" }]}>
                {eVoucherStatus[status]}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.bgBlur}>
          <View style={styles.bodyPayment}>
            <View style={{ flex: 1 }}>
              <Text style={styles.textPayment}>Hình thức thanh toán</Text>
            </View>
            <View style={styles.value}>
              <Text style={styles.textInfo}>{paymentMethod}</Text>
            </View>
          </View>
        </View>
        <View style={styles.bgWhite}>
          <View style={styles.bodyPayment}>
            <View style={{ flex: 1 }}>
              <Text style={styles.textPayment}>Tổng thanh toán</Text>
            </View>
            <View style={styles.value}>
              <Text style={styles.textInfo}>
                <Text style={styles.price}>
                  {formatPrice(amount)}
                  <Text style={styles.underline}>đ</Text>
                </Text>
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  renderDetailProfile = () => {
    const data = this.state;
    const full_name = lodash.get(data, "data.user.full_name");
    const phone = lodash.get(data, "data.user.phone");
    const email = lodash.get(data, "data.user.email");

    return (
      <View style={styles.containerPayment}>
        <View style={styles.bgWhite}>
          <View style={styles.bodyPayment}>
            <Text style={styles.titlePayment}>Thông tin người mua</Text>
          </View>
          <View style={styles.bodyPayment}>
            <View style={{ flex: 1 }}>
              <Text style={styles.textPayment}>Họ và tên</Text>
            </View>
            <View style={styles.value}>
              <Text style={styles.textInfo}>{full_name}</Text>
            </View>
          </View>
        </View>
        <View style={styles.bgBlur}>
          <View style={styles.bodyPayment}>
            <View style={{ flex: 1 }}>
              <Text style={styles.textPayment}>Số điện thoại</Text>
            </View>
            <View style={styles.value}>
              <Text style={styles.textInfo}>{phone}</Text>
            </View>
          </View>
        </View>
        <View style={styles.bgWhite}>
          <View style={styles.bodyPayment}>
            <View style={{ flex: 1 }}>
              <Text style={styles.textPayment}>Email</Text>
            </View>
            <View style={styles.value}>
              <Text style={styles.textInfo}>{email}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.header}>Chi tiết giao dịch</Text>}
        />
        {this.state.loading ? (
          <LoadingIndicator style={styles.loading} />
        ) : (
          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            bounces={false}
            style={{ marginBottom: moderateScale(10) }}
          >
            {this.renderPayment()}
            {this.renderDetailPayment()}
            {this.renderDetailProfile()}
          </ScrollView>
        )}
      </View>
    );
  }
}
export default connect(null, { resetPage, navigateToPage, navigateBack })(detailPaid);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  containerPayment: {
    flex: 1,
    marginTop: moderateScale(10)
  },
  bgWhite: {
    backgroundColor: "white",
    padding: 10
  },
  bgBlur: {
    backgroundColor: appColor.bg,
    padding: 10
  },
  bodyPayment: { flexDirection: "row", padding: 10 },
  titlePayment: {
    fontSize: sizeFont(16),
    fontFamily: font.bold,
    color: "#444444",
    marginStart: moderateScale(4)
  },
  textInfo: {
    fontSize: sizeFont(14),
    color: "#444444",
    alignSelf: "flex-start"
  },
  textPayment: {
    fontSize: sizeFont(14),
    fontWeight: "500",
    color: "#444444",
    marginStart: moderateScale(7)
  },
  infoPayment: {
    width: sizeWidth(302),
    alignSelf: "center",
    marginTop: moderateScale(10),
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    backgroundColor: "#FFFFFF",
    borderRadius: sizeWidth(6),
    flexDirection: "row",
    padding: sizeWidth(6)
  },
  image: {
    width: sizeWidth(60),
    height: sizeWidth(60),
    justifyContent: "flex-end",
    marginRight: sizeWidth(10),
    borderRadius: moderateScale(6),
    overflow: "hidden"
  },
  body: {
    flex: 1,
    flexDirection: "row"
  },
  costTitle: {
    marginVertical: moderateScale(4),
    fontSize: sizeFont(13),
    fontFamily: font.bold,
    color: "#444444",
    textTransform: "capitalize"
  },
  title: {
    fontSize: sizeFont(12),
    fontFamily: font.bold,
    color: "#444444",
    textTransform: "capitalize"
  },
  bottom: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: moderateScale(15)
  },
  left: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1,
    marginRight: sizeWidth(8)
  },
  merchant: {
    width: sizeWidth(16),
    height: sizeWidth(16),
    borderRadius: sizeWidth(8),
    marginRight: sizeWidth(6)
  },
  name: {
    fontSize: sizeFont(12),
    flex: 1
  },
  price: {
    color: "#EF4136",
    fontFamily: font.bold,
    fontSize: sizeFont(16),
    marginTop: moderateScale(15)
  },
  underline: {
    textDecorationLine: "underline",
    color: "#EF4136",
    fontSize: sizeFont(16),
    fontFamily: font.bold
  },
  loading: {
    width: "100%",
    height: "100%",
    backgroundColor: "white"
  },
  value: {
    flex: 1,
    marginStart: moderateScale(8)
  }
});
