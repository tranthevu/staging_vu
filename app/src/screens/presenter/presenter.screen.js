import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, moderateScale } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import NotificationView from "../../components/common/notification-view";
import BackIcon from "../../components/common/back-icon";

class PresenterScreen extends Component {
  render(): ReactNode {
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Người giới thiệu</Text>}
        />
        <View style={styles.body}>
          <ScrollView contentContainerStyle={styles.content}>
            <Text style={styles.label}>Người giới thiệu là gì?</Text>
            <Text style={styles.text}>
              Người đầu tiên giới thiệu thành công bất kỳ 1 chương trình S2E nào đến cho 1
              người mua mới thì sẽ trở thành người giới thiệu trong thời gian x năm.
            </Text>
            <Text style={[styles.text, styles.space]}>
              Sau khi chu kỳ x năm kết thúc, thông tin người giới thiệu sẽ được thiết lập
              lại (Reset).
            </Text>
            <Text style={styles.label}>Quyền lợi của người giới thiệu</Text>
            <Text style={[styles.text, styles.space]}>
              Người giới thiệu sẽ được hệ thống ghi nhớ và tính thưởng cho tất cả các
              chương trình S2E mà người được giới thiệu tham gia trong vòng x năm bất kể
              người đó mua hàng từ link chia sẻ của bất kỳ ai.
            </Text>
            <Text style={styles.label}>Làm sao để trở thành người giới thiệu?</Text>
            <Text style={styles.step}>Cách 1:</Text>
            <Text style={styles.text}>
              - Tìm kiếm những người chưa từng tham gia bất kỳ chương trình S2E nào của
              Yolla
            </Text>
            <Text style={styles.text}>
              - Giới thiệu một chương trình S2E bất kỳ cho họ
            </Text>
            <Text style={styles.step}>Cách 2:</Text>
            <Text style={styles.text}>- Đối với người đã từng tham gia S2E</Text>
            <Text style={styles.text}>
              - Hãy chắc chắn tài khoản của họ đã kết thúc chu kỳ
            </Text>
            <Text style={styles.text}>- của Người giới thiệu cũ</Text>
            <Text style={styles.text}>
              - Trở thành người đầu tiên chia sẻ một chương trình S2E mới cho họ.
            </Text>
          </ScrollView>
          <NotificationView ref={ref => (this.notificationView = ref)} />
        </View>
      </View>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(PresenterScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F9F9F9"
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  label: {
    fontSize: sizeFont(16),
    fontFamily: font.medium,
    marginBottom: moderateScale(10)
  },
  content: {
    padding: moderateScale(12)
  },
  text: {
    fontSize: sizeFont(13),
    marginBottom: moderateScale(8)
  },
  space: {
    marginBottom: moderateScale(20)
  },
  step: {
    fontFamily: font.medium,
    fontSize: sizeFont(13),
    marginBottom: moderateScale(10)
  }
});
