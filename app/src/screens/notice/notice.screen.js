import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, ScrollView } from "react-native";
import lodash from "lodash";
import { connect } from "react-redux";
import { resetPage, navigateBack, navigateToPage } from "../../actions/nav.action";
import { sizeFont, moderateScale } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { font } from "../../constants/app.constant";
import Button from "../../components/common/button";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import NotificationView from "../../components/common/notification-view";

class NoticeScreen extends Component {
  render(): ReactNode {
    const params = lodash.get(this.props.navigation.state, "params", {}) || {};
    const {
      hideCancel,
      bottom,
      action,
      icon,
      hideBack,
      title,
      button,
      message,
      desc,
      iconStyle
    } = params;
    return (
      <View style={styles.container}>
        <Toolbar
          left={!hideBack && <BackIcon />}
          right={
            !hideCancel && (
              <Text onPress={this.props.navigateBack} style={styles.cancel}>
                Hủy
              </Text>
            )
          }
          center={<Text style={styles.title}>{title}</Text>}
        />
        <View style={styles.body}>
          <ScrollView bounces={false}>
            <Image
              source={icon || require("../../../res/icon/notice-success.png")}
              style={[styles.icon, iconStyle]}
            />
            <Text style={styles.message}>{message}</Text>
            <Text style={styles.desc}>{desc}</Text>
          </ScrollView>
          <View>
            <Button
              style={styles.action}
              onPress={action || this.props.navigateBack}
              text={button}
            />
          </View>
          {bottom}
          <NotificationView />
        </View>
      </View>
    );
  }

  componentDidMount = async () => {};
}

export default connect(
  null,
  { resetPage, navigateBack, navigateToPage }
)(NoticeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF",
    alignItems: "center"
  },
  icon: {
    marginTop: moderateScale(25),
    marginBottom: moderateScale(25),
    alignSelf: "center",
    width: moderateScale(180),
    height: moderateScale(180)
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  message: {
    fontSize: sizeFont(18),
    marginHorizontal: moderateScale(20),
    textAlign: "center",
    fontFamily: font.medium,
    alignSelf: "center",
    marginBottom: moderateScale(8)
  },
  desc: {
    fontSize: sizeFont(13),
    textAlign: "center",
    marginHorizontal: moderateScale(23)
  },
  action: {
    marginVertical: moderateScale(10)
  },
  cancel: {
    color: "white"
  },
  body: {
    flex: 1
  }
});
