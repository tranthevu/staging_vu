import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  FlatList,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import PaginationList from "../../components/common/pagination-list";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor, text } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import PromotionItem from "../dashboard/promotion-item";
import Banner from "../dashboard/banner";
import BackIcon from "../../components/common/back-icon";
import NotificationIcon from "../dashboard/notification-icon";
import SearchInput from "../../components/common/search-input";
import LoadingIndicator from "../../components/common/loading-indicator";
import Api from "../../api/api";
import {
  requestLocationProducts,
  fetchMoreLocationProducts,
  refreshLocationProducts
} from "../../actions/location-products.action";
import NotificationView from "../../components/common/notification-view";
import EmptyView from "../../components/common/empty-view";

class LocationProductsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appliedFilters: {}
    };
  }

  componentDidMount = async () => {
    const slug = lodash.get(this.props.navigation.state.params, "location.slug");
    this.props.requestLocationProducts(slug);
  };

  navigateToSearch = () => {
    this.props.navigateToPage("Search");
  };

  render(): ReactNode {
    const { firstLoading } = this.props.locationProducts;
    return (
      <View style={styles.container}>
        <Toolbar
          center={<Text style={styles.header}>Ưu đãi theo khu vực</Text>}
          left={<BackIcon />}
        />
        <View style={styles.wrap}>
          {firstLoading ? <LoadingIndicator /> : this.renderBody()}
          <NotificationView />
        </View>
      </View>
    );
  }

  renderBody = () => {
    return <View style={styles.root}>{this.renderOffers()}</View>;
  };

  renderHeader = () => {
    const location = lodash.get(this.props.navigation.state.params, "location");
    return (
      <View style={styles.body}>
        <Text numberOfLines={1} style={styles.title}>
          {location.name} <Text style={styles.count}>({location.total_product})</Text>
        </Text>
        <TouchableIcon
          style={styles.filter}
          iconStyle={styles.filterIcon}
          onPress={this.navigateToFilter}
          source={require("../../../res/icon/filter.png")}
        />
      </View>
    );
  };

  renderOffers = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      locationProducts
    } = this.props.locationProducts;
    const { fetchMoreLocationProducts, refreshLocationProducts } = this.props;
    const slug = lodash.get(this.props.navigation.state.params, "location.slug");
    const { appliedFilters } = this.state;
    const area = lodash.get(appliedFilters, "selectedArea.slug");
    const merchant = lodash
      .get(appliedFilters, "selectedMerchants", [])
      .map(item => item.code)
      .join(",");
    const ordering = lodash.get(appliedFilters, "selectedOrder.type", text.emptyString);
    return (
      <PaginationList
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderPromotion}
        loading={loading}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        firstLoading={firstLoading}
        ListHeaderComponent={this.renderHeader()}
        onEndReached={() =>
          fetchMoreLocationProducts(area || slug, text.emptyString, merchant, ordering)
        }
        onRefresh={() =>
          refreshLocationProducts(area || slug, text.emptyString, merchant, ordering)
        }
        data={locationProducts}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  navigateToFilter = () => {
    const { appliedFilters } = this.state;
    const childAreas = lodash.get(this.props.navigation.state.params, "location.childs");
    this.props.navigateToPage("Filter", {
      onApplyFilters: this.onApplyFilters,
      appliedFilters,
      childAreas
    });
  };

  onApplyFilters = appliedFilters => {
    this.setState({ appliedFilters });
    const slug = lodash.get(this.props.navigation.state.params, "location.slug");
    const area = lodash.get(appliedFilters, "selectedArea.slug");
    const merchant = lodash
      .get(appliedFilters, "selectedMerchants", [])
      .map(item => item.code)
      .join(",");
    const ordering = lodash.get(appliedFilters, "selectedOrder.type", text.emptyString);
    this.props.requestLocationProducts(
      area || slug,
      text.emptyString,
      merchant,
      ordering
    );
  };

  renderPromotion = ({ item, index }) => {
    return <PromotionItem item={item} />;
  };
}

export default connect(
  state => ({
    locationProducts: state.locationProducts
  }),
  {
    resetPage,
    navigateToPage,
    fetchMoreLocationProducts,
    refreshLocationProducts,
    requestLocationProducts
  }
)(LocationProductsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  wrap: {
    flex: 1
  },
  root: {
    flex: 1
  },
  list: {
    marginTop: sizeWidth(6),
    marginBottom: sizeWidth(15)
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },

  filter: {
    alignSelf: "center",
    marginLeft: sizeWidth(12)
  },
  filterIcon: {
    width: moderateScale(20),
    height: moderateScale(20)
  },
  content: {
    backgroundColor: "white"
  },
  body: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: sizeWidth(12),
    marginBottom: sizeWidth(5),
    paddingHorizontal: sizeWidth(9)
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    flex: 1
  },
  count: {
    fontSize: sizeFont(14),
    fontFamily: font.bold
  }
});
