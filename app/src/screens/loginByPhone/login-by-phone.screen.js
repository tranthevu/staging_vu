import React, { Component, ReactNode } from "react";
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
  StyleSheet
} from "react-native";
import validator from "validator";
import { connect } from "react-redux";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, text, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import Button from "../../components/common/button";
import Input from "../../components/common/input";
import Dropdown from "../../components/common/dropdown";
import Api from "../../api/api";
import { requestedBanners } from "../../actions/banners.action";
import { requestedTopCategories } from "../../actions/top-categories.action";
import { requestCountNotifications } from "../../actions/notifications.action";
import { requestedProfile } from "../../actions/profile.action";
import { requestedTopMerchants } from "../../actions/top-merchants.action";
import { requestedFilters } from "../../actions/filters.action";
import { requestedMasterData } from "../../actions/master-data.action";
import LoadingIndicator from "../../components/common/loading-indicator";
import NotificationView from "../../components/common/notification-view";
import { showError } from "../../helpers/error.helper";
import lodash from "lodash";
import { checkApiStatus } from "../../helpers/app.helper";

class LoginByPhoneScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: text.emptyString,
      checking: false,
      loggingIn: false
    };
  }

  componentDidMount = () => {
    if (this.phoneRef) this.phoneRef.focus();
  };

  checkExistPhone = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const lastScreen = lodash.get(this.props.navigation.state.params, "lastScreen");
    const action = lodash.get(this.props.navigation.state.params, "action");
    const url = lodash.get(this.props.navigation.state.params, "url");
    try {
      const params = this.props.navigation.state.params || {};
      const { provider, socialAccessToken, type } = params;
      const { phone } = this.state;
      if (validator.isEmpty(phone))
        return showError("Số điện thoại không được để trống!");
      if (phone.length < 9 || phone.length >= 12)
        return showError("Số điện thoại không đúng định dạng!");
      this.setState({ checking: true });
      if (!socialAccessToken) {
        const res = await Api.checkIsPassword(`84${phone.replace(/^0+/, "")}`);
        if (res.is_password) {
          this.props.navigateToPage("EnterPassword", {
            phone,
            avatar: res.avatar,
            full_name: res.full_name,
            lastScreen: lastScreen,
            action: action,
            url: url
          });
        } else {
          const otpRes = await Api.sendOTP(`84${phone.replace(/^0+/, "")}`);
          if (otpRes.status === 200) {
            this.props.navigateToPage("ConfirmPhone", {
              phone: `84${phone.replace(/^0+/, "")}`,
              lastScreen: lastScreen,
              action: action,
              url: url
            });
          } else if (otpRes.status === 400) {
            showError(otpRes.message);
          }
        }
      } else {
        this.props.navigateToPage("ConfirmPhone", {
          phone: `84${phone.replace(/^0+/, "")}`,
          provider,
          socialAccessToken,
          type,
          lastScreen: lastScreen,
          action: action,
          url: url
        });
      }
      this.setState({ checking: false });
    } catch (err) {
      this.setState({ checking: false });
    }
  };

  render(): ReactNode {
    const { phone, checking, loggingIn } = this.state;
    if (loggingIn) return <LoadingIndicator />;
    const Root = Platform.OS === "ios" ? KeyboardAvoidingView : View;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Nhập số điện thoại</Text>}
        />
        <Root
          keyboardVerticalOffset={moderateScale(20)}
          behavior="padding"
          style={styles.body}
        >
          <ScrollView bounces={false}>
            <Text style={styles.enter}>Nhập số điện thoại của bạn</Text>
            <Text style={styles.desc}>Mã xác minh sẽ được gửi qua tin nhắn SMS</Text>
            <View style={styles.input}>
              <Dropdown style={styles.prefix} value="+84" />
              <Input
                value={phone}
                ref={ref => (this.phoneRef = ref)}
                onChangeText={text => this.setState({ phone: text })}
                style={styles.phone}
                keyboardType="phone-pad"
                placeholder="Nhập số di động của bạn"
              />
            </View>
          </ScrollView>
          <Button
            style={styles.button}
            loading={checking}
            onPress={this.checkExistPhone}
            text="TIẾP TỤC"
          />
          <NotificationView />
        </Root>
      </View>
    );
  }
}

export default connect(null, {
  resetPage,
  navigateToPage,
  requestedBanners,
  requestCountNotifications,
  requestedTopCategories,
  requestedProfile,
  requestedTopMerchants,
  requestedFilters,
  requestedMasterData
})(LoginByPhoneScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  desc: {
    marginHorizontal: sizeWidth(9),
    marginVertical: sizeWidth(9),
    textAlign: "center",
    color: appColor.blur,
    alignSelf: "center",
    fontSize: sizeFont(13)
  },
  enter: {
    marginHorizontal: sizeWidth(9),
    fontSize: sizeFont(15),
    fontFamily: font.medium,
    alignSelf: "center",
    marginTop: sizeWidth(20)
  },
  input: {
    flexDirection: "row",
    alignItems: "flex-end",
    marginHorizontal: sizeWidth(12)
  },
  prefix: {
    alignSelf: "flex-end",
    width: sizeWidth(94)
  },
  phone: {
    alignSelf: "flex-end",
    flex: 1
  },
  space: {
    flex: 1
  },
  button: {
    marginVertical: moderateScale(10)
  }
});
