import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image } from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateBack, navigateToPage } from "../../actions/nav.action";

import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import Button from "../../components/common/button";
import Api from "../../api/api";
import { showError } from "../../helpers/error.helper";
import NotificationView from "../../components/common/notification-view";
import { checkApiStatus } from "../../helpers/app.helper";

class ResendCodeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  render(): ReactNode {
    const { phone } = this.props.navigation.state.params;
    const { loading } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
          center={<Text style={styles.title}>Không nhận được mã</Text>}
        />
        <View style={styles.body}>
          <Text style={styles.label}>Nếu bạn chưa nhận được mã:</Text>
          <View style={styles.row}>
            <Image
              source={require("../../../res/icon/smartphone.png")}
              style={styles.icon}
            />
            <Text style={styles.text}>
              Xác minh <Text style={styles.bold}>{phone}</Text> là số di động của bạn.{" "}
              <Text onPress={this.modifyPhone} style={styles.underline}>
                Chỉnh sửa.
              </Text>
            </Text>
          </View>
          <View style={styles.row}>
            <Image
              style={styles.icon}
              source={require("../../../res/icon/chat-message.png")}
            />
            <Text style={styles.text}>Kiểm tra hộp thư SMS của bạn</Text>
          </View>
          <Button
            loading={loading}
            style={styles.resend}
            onPress={this.resendCode}
            text="GỬI LẠI MÃ"
          />
          <NotificationView />
        </View>
      </View>
    );
  }

  modifyPhone = () => {
    this.props.navigateBack();
    this.props.navigateBack();
  };

  resendCode = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { phone } = this.props.navigation.state.params;
    try {
      this.setState({ loading: true });
      const otpRes = await Api.sendOTP(phone);
      if (otpRes.status === 200) {
        this.props.navigateBack();
      } else if (otpRes.status === 400) {
        showError(otpRes.message);
      }
      this.setState({ loading: false });
    } catch (err) {
      this.setState({ loading: false });
    }
  };
}

export default connect(null, { resetPage, navigateBack, navigateToPage })(
  ResendCodeScreen
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: sizeWidth(15),
    marginVertical: sizeWidth(6)
  },
  icon: {
    width: sizeWidth(24),
    height: sizeWidth(24),
    tintColor: appColor.text,
    marginRight: sizeWidth(12)
  },
  text: {
    flex: 1
  },
  label: {
    marginVertical: sizeWidth(10),
    paddingHorizontal: sizeWidth(8),
    fontFamily: font.medium
  },
  resend: {
    marginTop: sizeWidth(10)
  },
  bold: {
    fontFamily: font.medium
  },
  underline: {
    textDecorationLine: "underline"
  },
  cancel: {
    color: "white"
  },
  body: {
    flex: 1
  }
});
