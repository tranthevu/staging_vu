import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import Button from "../../components/common/button";
import Input from "../../components/common/input";
import Dropdown from "../../components/common/dropdown";

class ConfirmedPhoneScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render(): ReactNode {
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Đã xác minh tài khoản</Text>}
        />
        <Text style={styles.desc}>
          Nhấn vào TIẾP TỤC để chấp nhận{" "}
          <Text style={styles.underline}>Điều khoản</Text>,{" "}
          <Text style={styles.underline}>Chính sách dữ liệu</Text>, sử dụng
          <Text style={styles.underline}>cookie</Text> của Facebook và Chính
          sách quyên riêng tư của Yolla.{" "}
          <Text style={styles.underline}>Tìm hiểu thêm</Text> về cách tài khoản
          được xác minh
        </Text>
        <Button onPress={this.navigateToCompleteProfile} text="TIẾP TỤC" />
      </View>
    );
  }

  navigateToCompleteProfile = () => {
    this.props.navigateToPage("CompleteProfile");
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(ConfirmedPhoneScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  desc: {
    fontSize: sizeFont(13),
    textAlign: "center",
    marginHorizontal: sizeWidth(9),
    marginVertical: sizeWidth(20)
  },
  underline: {
    textDecorationLine: "underline"
  }
});
