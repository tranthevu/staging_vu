import React, { Component, ReactNode } from "react";
import { connect } from "react-redux";
import { View, StyleSheet, ScrollView } from "react-native";
import lodash from "lodash";
import validator from "validator";
import { resetPage, navigateBack, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor, documentType, text } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import Button from "../../components/common/button";
import Input from "../../components/common/input";
import Dropdown from "../../components/common/dropdown";
import { NOTICE_SCREEN } from "../../navigators/screensName";
import Api from "../../api/api";
import { showError } from "../../helpers/error.helper";
import NotificationView from "../../components/common/notification-view";
import KeyboardHandlerView from "../../components/common/keyboard-handler-view";
import LoadingIndicator from "../../components/common/loading-indicator";
import { checkApiStatus } from "../../helpers/app.helper";

class ConfirmPhoneScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      digit1: text.emptyString,
      digit2: text.emptyString,
      digit3: text.emptyString,
      digit4: text.emptyString,
      digit5: text.emptyString,
      digit6: text.emptyString
    };
  }

  onChangeText = (text, index) => {
    if (text.length <= 1) {
      this.setState({
        [`digit${index}`]: text
      });
    }
    if (text.length >= 1 && index < 6) {
      this[`digit${index + 1}Ref`].focus();
    } else if (text.length === 0 && index > 1) {
      this[`digit${index - 1}Ref`].focus();
    }
  };

  componentDidMount = () => {
    // if (this.digit1Ref) this.digit1Ref.focus();
    this.bypassOTP();
  };

  bypassOTP = async () => {
    const fromForgotPassword = lodash.get(
      this.props.navigation.state.params,
      "fromForgotPassword"
    );
    const { phone } = this.props.navigation.state.params;
    if (fromForgotPassword) {
      this.props.navigateBack();
      this.props.navigateBack();
      this.props.navigateToPage("NewPassword", { phone });
    } else {
      this.props.navigateBack();
      this.props.navigateToPage(NOTICE_SCREEN, {
        hideCancel: true,
        title: "Đã xác minh tài khoản",
        action: this.completeProfile,
        button: "TIẾP TỤC",
        message: "Xác minh thành công",
        desc: (
          <>
            Nhấn vào TIẾP TỤC để chấp nhận{" "}
            <Text onPress={this.navigateToPolicyScreen} style={styles.underline}>
              Chính sách & Điều khoản
            </Text>
            ,{" "}
            <Text onPress={this.navigateToPrivacyScreen} style={styles.underline}>
              Chính sách bảo mật
            </Text>{" "}
            của Yolla Network
          </>
        )
      });
    }
  };

  render(): ReactNode {
    const fromForgotPassword = lodash.get(
      this.props.navigation.state.params,
      "fromForgotPassword"
    );
    const { loading, digit1, digit2, digit3, digit4, digit5, digit6 } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
          center={<Text style={styles.title}>Xác minh số điện thoại</Text>}
        />
        <KeyboardHandlerView
          behavior="padding"
          keyboardVerticalOffset={moderateScale(20)}
          style={styles.body}
        >
          {loading ? (
            <LoadingIndicator />
          ) : (
            <>
              <ScrollView>
                <Text style={styles.enter}>Nhập mã xác minh</Text>
                <Text style={styles.message}>
                  Nhập mã xác minh được gửi qua tin nhắn SMS để xác minh số điện thoại của
                  bạn
                </Text>
                <View style={styles.top}>
                  <Input
                    value={digit1}
                    ref={ref => (this.digit1Ref = ref)}
                    onChangeText={text => this.onChangeText(text, 1)}
                    style={styles.input}
                    keyboardType="number-pad"
                    inputStyle={styles.text}
                  />
                  <Input
                    value={digit2}
                    keyboardType="number-pad"
                    onChangeText={text => this.onChangeText(text, 2)}
                    ref={ref => (this.digit2Ref = ref)}
                    style={styles.input}
                    inputStyle={styles.text}
                  />
                  <Input
                    value={digit3}
                    keyboardType="number-pad"
                    onChangeText={text => this.onChangeText(text, 3)}
                    ref={ref => (this.digit3Ref = ref)}
                    style={styles.input}
                    inputStyle={styles.text}
                  />
                  <Input
                    value={digit4}
                    onChangeText={text => this.onChangeText(text, 4)}
                    ref={ref => (this.digit4Ref = ref)}
                    style={styles.input}
                    keyboardType="number-pad"
                    inputStyle={styles.text}
                  />
                  <Input
                    value={digit5}
                    keyboardType="number-pad"
                    onChangeText={text => this.onChangeText(text, 5)}
                    ref={ref => (this.digit5Ref = ref)}
                    style={styles.input}
                    inputStyle={styles.text}
                  />
                  <Input
                    value={digit6}
                    keyboardType="number-pad"
                    onChangeText={text => this.onChangeText(text, 6)}
                    ref={ref => (this.digit6Ref = ref)}
                    style={styles.input}
                    inputStyle={styles.text}
                  />
                </View>
                <Text onPress={this.navigateToResendCode} style={styles.notReceive}>
                  Tôi không nhận được mã
                </Text>
                {!fromForgotPassword && (
                  <Text style={styles.desc}>
                    Nhấn vào TIẾP TỤC để chấp nhận{" "}
                    <Text onPress={this.navigateToPolicyScreen} style={styles.underline}>
                      Chính sách & Điều khoản
                    </Text>
                    ,{" "}
                    <Text onPress={this.navigateToPrivacyScreen} style={styles.underline}>
                      Chính sách bảo mật
                    </Text>{" "}
                    của Yolla.
                  </Text>
                )}
              </ScrollView>
              <Button
                loading={loading}
                style={styles.button}
                onPress={this.checkOtp}
                text="TIẾP TỤC"
              />
            </>
          )}
          <NotificationView />
        </KeyboardHandlerView>
      </View>
    );
  }

  navigateToResendCode = () => {
    const { phone } = this.props.navigation.state.params;
    this.props.navigateToPage("ResendCode", { phone });
  };

  navigateToPolicyScreen = () => {
    this.props.navigateToPage("Document", {
      document: documentType.policy,
      title: "Chính sách và điều khoản"
    });
  };

  navigateToPrivacyScreen = () => {
    this.props.navigateToPage("Document", {
      document: documentType.privacy,
      title: "Chính sách quyền riêng tư"
    });
  };

  completeProfile = async () => {
    const lastScreen = lodash.get(this.props.navigation.state.params, "lastScreen");
    const action = lodash.get(this.props.navigation.state.params, "action");
    const url = lodash.get(this.props.navigation.state.params, "url");
    const {
      phone,
      provider,
      socialAccessToken,
      type
    } = this.props.navigation.state.params;
    this.props.navigateBack();
    this.props.navigateBack();
    this.props.navigateToPage("CompleteProfile", {
      phone,
      provider,
      socialAccessToken,
      type,
      isFromSocial: !!socialAccessToken,
      lastScreen: lastScreen,
      action: action,
      url: url
    });
  };

  checkOtpLogin = async () => {
    try {
      const isMaintenance = await checkApiStatus();
      if (isMaintenance) {
        this.props.resetPage("Maintenance");
      }
      const { digit1, digit2, digit3, digit4, digit5, digit6 } = this.state;
      if (
        validator.isEmpty(digit1) ||
        validator.isEmpty(digit2) ||
        validator.isEmpty(digit3) ||
        validator.isEmpty(digit4) ||
        validator.isEmpty(digit5) ||
        validator.isEmpty(digit6)
      ) {
        return showError("Vui lòng nhập mã OTP");
      }
      this.setState({ loading: true });
      const { phone } = this.props.navigation.state.params;
      const otp = `${digit1}${digit2}${digit3}${digit4}${digit5}${digit6}`;
      const res = await Api.verifyOTP({ phone, otp });
      if (res.verify) {
        this.props.navigateToPage(NOTICE_SCREEN, {
          hideCancel: true,
          title: "Đã xác minh tài khoản",
          action: this.completeProfile,
          button: "TIẾP TỤC",
          message: "Xác minh thành công",
          desc: (
            <>
              Nhấn vào TIẾP TỤC để chấp nhận{" "}
              <Text onPress={this.navigateToPolicyScreen} style={styles.underline}>
                Chính sách & Điều khoản
              </Text>
              ,{" "}
              <Text onPress={this.navigateToPrivacyScreen} style={styles.underline}>
                Chính sách bảo mật
              </Text>{" "}
              của Yolla Network
            </>
          )
        });
      } else {
        this.props.navigateToPage(NOTICE_SCREEN, {
          title: "Không thể xác minh",
          icon: require("../../../res/icon/not-identify.png"),
          iconStyle: styles.icon,
          button: "GỬI LẠI MÃ",
          action: this.resendCode,
          message: <Text style={styles.cannot}>Không thể xác minh số điện thoại</Text>,
          desc: `Chúng tôi không thể xác minh số điện thoại ${phone}`,
          bottom: (
            <Text onPress={this.changePhoneNumber} style={styles.change}>
              Thay đổi số điện thoại
            </Text>
          )
        });
      }
      this.setState({ loading: false });
    } catch (err) {
      this.setState({ loading: false });
    }
  };

  changePhoneNumber = () => {
    this.props.navigateBack();
    this.props.navigateBack();
  };

  resendCode = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { phone } = this.props.navigation.state.params;
    const otpRes = await Api.sendOTP(phone);
    if (otpRes.status === 200) {
      this.props.navigateBack();
      this.setState({
        digit1: text.emptyString,
        digit2: text.emptyString,
        digit3: text.emptyString,
        digit4: text.emptyString,
        digit5: text.emptyString,
        digit6: text.emptyString
      });
    } else if (otpRes.status === 400) {
      showError(otpRes.message);
    }
  };

  checkOtp = () => {
    const fromForgotPassword = lodash.get(
      this.props.navigation.state.params,
      "fromForgotPassword"
    );
    if (fromForgotPassword) {
      this.checkOtpForgotPassword();
    } else {
      this.checkOtpLogin();
    }
  };

  checkOtpForgotPassword = async () => {
    try {
      const isMaintenance = await checkApiStatus();
      if (isMaintenance) {
        this.props.resetPage("Maintenance");
      }
      const { digit1, digit2, digit3, digit4, digit5, digit6 } = this.state;
      if (
        validator.isEmpty(digit1) ||
        validator.isEmpty(digit2) ||
        validator.isEmpty(digit3) ||
        validator.isEmpty(digit4) ||
        validator.isEmpty(digit5) ||
        validator.isEmpty(digit6)
      ) {
        return showError("Vui lòng nhập mã OTP");
      }
      this.setState({ loading: true });
      const { phone } = this.props.navigation.state.params;
      const otp = `${digit1}${digit2}${digit3}${digit4}${digit5}${digit6}`;
      const res = await Api.verifyOTP({ phone, otp });
      if (res.verify) {
        const { phone } = this.props.navigation.state.params;
        this.props.navigateBack();
        this.props.navigateBack();
        this.props.navigateToPage("NewPassword", { phone });
      } else {
        this.props.navigateToPage(NOTICE_SCREEN, {
          title: "Không thể xác minh",
          icon: require("../../../res/icon/not-identify.png"),
          iconStyle: styles.icon,
          button: "GỬI LẠI MÃ",
          action: this.resendCode,
          message: <Text style={styles.cannot}>Không thể xác minh số điện thoại</Text>,
          desc: `Chúng tôi không thể xác minh số điện thoại ${phone}`,
          bottom: (
            <Text onPress={this.changePhoneNumber} style={styles.change}>
              Thay đổi số điện thoại
            </Text>
          )
        });
      }
      this.setState({ loading: false });
    } catch (err) {
      this.setState({ loading: false });
    }
  };
}

export default connect(null, { resetPage, navigateToPage, navigateBack })(
  ConfirmPhoneScreen
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  cancel: {
    color: "white"
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  desc: {
    fontSize: sizeFont(13),
    textAlign: "center",
    marginHorizontal: sizeWidth(9),
    marginVertical: sizeWidth(20)
  },
  message: {
    marginHorizontal: sizeWidth(9),
    marginVertical: sizeWidth(9),
    textAlign: "center",
    color: appColor.blur,
    alignSelf: "center",
    fontSize: sizeFont(13)
  },
  cannot: {
    color: appColor.primary,
    fontSize: sizeFont(18),
    fontFamily: font.medium
  },
  enter: {
    marginHorizontal: sizeWidth(9),
    fontSize: sizeFont(15),
    fontFamily: font.medium,
    alignSelf: "center",
    marginTop: sizeWidth(20)
  },
  notReceive: {
    textDecorationStyle: "solid",
    marginTop: sizeWidth(23),
    textDecorationLine: "underline",
    alignSelf: "center"
  },
  icon: {
    width: moderateScale(141),
    height: moderateScale(180)
  },
  underline: {
    fontSize: sizeFont(13),
    textDecorationLine: "underline"
  },
  input: {
    width: sizeWidth(30),
    paddingHorizontal: 0,
    marginHorizontal: sizeWidth(8)
  },
  text: {
    textAlign: "center",
    fontSize: sizeFont(20),
    fontFamily: font.bold,
    textAlignVertical: "center"
  },
  top: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    marginVertical: moderateScale(10)
  },
  change: {
    alignSelf: "center",
    marginVertical: moderateScale(20),
    fontSize: sizeFont(12),
    textDecorationLine: "underline"
  }
});
