import React, { Component, ReactNode } from "react";
import { View, ScrollView, StyleSheet } from "react-native";
import { connect } from "react-redux";
import moment from "moment";
import lodash from "lodash";
import validator from "validator";
import firebase from "react-native-firebase";
import Toast from "@remobile/react-native-toast";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, text, appColor, loginType } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import Button from "../../components/common/button";
import Input from "../../components/common/input";
import Dropdown from "../../components/common/dropdown";
import DateTimePicker from "../../components/common/date-time-picker";

import Api from "../../api/api";
import { requestedProfile } from "../../actions/profile.action";
import { requestedBanners } from "../../actions/banners.action";
import { requestedTopCategories } from "../../actions/top-categories.action";
import { requestCountNotifications } from "../../actions/notifications.action";
import { requestedTopMerchants } from "../../actions/top-merchants.action";
import { requestedFilters } from "../../actions/filters.action";
import { requestedMasterData } from "../../actions/master-data.action";
import NotificationView from "../../components/common/notification-view";
import { showError } from "../../helpers/error.helper";
import { appConfig } from "../../config/app.config";
import { saveClientCas } from "../../helpers/storage.helper";
import { checkApiStatus, logDebug } from "../../helpers/app.helper";

class CompleteProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullName: text.emptyString,
      dateOfBirth: null,
      loading: false,
      passwordVisible: false,
      password: text.emptyString,
      retypePasswordVisible: false,
      retypePassword: text.emptyString
    };
  }

  toggleShowPassword = () => {
    const { passwordVisible } = this.state;
    this.setState({
      passwordVisible: !passwordVisible
    });
  };

  toggleShowRetypePassword = () => {
    const { retypePasswordVisible } = this.state;
    this.setState({
      retypePasswordVisible: !retypePasswordVisible
    });
  };

  componentDidMount = () => {
    if (this.nameRef) this.nameRef.focus();
  };

  render(): ReactNode {
    const {
      fullName,
      dateOfBirth,
      password,
      passwordVisible,
      retypePassword,
      retypePasswordVisible,
      loading
    } = this.state;
    const maxDate = moment().format("YYYY-MM-DD");
    const isFromSocial = lodash.get(this.props.navigation.state.params, "isFromSocial");
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Cập nhật thông tin</Text>}
        />
        <View style={styles.body}>
          <ScrollView>
            <Input
              style={styles.name}
              label="Họ và Tên"
              value={fullName}
              ref={ref => (this.nameRef = ref)}
              onChangeText={text => this.setState({ fullName: text })}
            />
            {!isFromSocial && (
              <>
                <Input
                  value={password}
                  style={styles.input}
                  onChangeText={text => this.setState({ password: text })}
                  secureTextEntry={!passwordVisible}
                  right={
                    <TouchableIcon
                      style={styles.visible}
                      onPress={this.toggleShowPassword}
                      iconStyle={styles.eye}
                      source={
                        passwordVisible
                          ? require("../../../res/icon/visible.png")
                          : require("../../../res/icon/eye-off.png")
                      }
                    />
                  }
                  label="Mật khẩu"
                  placeholder="Tạo mật khẩu"
                />
                <Input
                  value={retypePassword}
                  style={styles.input}
                  onChangeText={text => this.setState({ retypePassword: text })}
                  secureTextEntry={!retypePasswordVisible}
                  right={
                    <TouchableIcon
                      style={styles.visible}
                      onPress={this.toggleShowRetypePassword}
                      iconStyle={styles.eye}
                      source={
                        retypePasswordVisible
                          ? require("../../../res/icon/visible.png")
                          : require("../../../res/icon/eye-off.png")
                      }
                    />
                  }
                  label="Nhập lại mật khẩu"
                  placeholder="Nhập lại mật khẩu"
                />
              </>
            )}
            {/* <DateTimePicker
            style={styles.date}
            placeholder="dd/mm/yyyy"
            maxDate={maxDate}
            label="Ngày sinh"
            mode="date"
            onDateChange={value => {
              this.setState({ dateOfBirth: moment(value) });
            }}
            value={dateOfBirth}
          />
          <Text style={styles.desc}>
            Nhập chính xác ngày sinh của bạn. Ngày sinh sẽ bị giới hạn thay đổi sau khi
            nhập.
          </Text> */}
            <Button
              style={styles.button}
              loading={loading}
              onPress={this.completeProfile}
              text="HOÀN TẤT"
            />
          </ScrollView>

          <NotificationView />
        </View>
      </View>
    );
  }

  completeProfile = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { fullName, password, retypePassword } = this.state;
    const {
      isFromSocial,
      provider,
      socialAccessToken,
      type,
      phone
    } = this.props.navigation.state.params;
    if (validator.isEmpty(fullName)) return showError("Họ và Tên không được để trống!");
    if (fullName.trim().split(" ").length < 2)
      return showError("Họ và Tên nhập ít nhất 2 từ!");
    if (!isFromSocial && validator.isEmpty(password))
      return showError("Mật khẩu không được để trống!");
    if (!isFromSocial && validator.isEmpty(retypePassword))
      return showError("Vui lòng nhập lại mật khẩu!");
    if (!isFromSocial && password !== retypePassword)
      return showError("Mật khẩu và mật khẩu xác nhận không khớp!");

    try {
      this.setState({ loading: true });
      const response = await Api.loginForApp({
        access_token: text.emptyString,
        service: appConfig.ssoService,
        provider: provider || loginType.otp,
        type: type || loginType.otp,
        social_access_token: socialAccessToken || text.emptyString,
        phone,
        password: isFromSocial ? undefined : password,
        full_name: fullName
      });
      if (response.status === "SUCCESS") {
        await saveClientCas(response.client_cas);
        const fcmToken = await firebase.messaging().getToken();
        await Api.registerTokenId(fcmToken);
        // TODO: Temporary hide banner as requested
        // this.props.requestedBanners();
        this.props.requestedTopCategories();
        this.props.requestCountNotifications();
        this.props.requestedProfile();
        this.props.requestedTopMerchants();
        this.props.requestedFilters();
        this.props.requestedMasterData();

        const lastScreen = lodash.get(this.props.navigation.state.params, "lastScreen");
        const action = lodash.get(this.props.navigation.state.params, "action");
        const url = lodash.get(this.props.navigation.state.params, "url");
        if (lastScreen !== undefined)
          this.props.navigateToPage(lastScreen, {
            isComeback: "true",
            action: action,
            url: url
          });
        else this.props.resetPage("Main");
      } else {
        this.setState({ loading: false });
        showError("Đăng nhập thất bại. Vui lòng thử lại!");
      }
    } catch (err) {
      logDebug("complete-profile.screen.js", "login ERR", err);
      this.setState({ loading: false });
    }
  };

  navigateToDashboard = () => {
    this.props.resetPage("Main");
  };
}

export default connect(null, {
  resetPage,
  navigateToPage,
  requestedBanners,
  requestedTopMerchants,
  requestedFilters,
  requestedMasterData,
  requestedTopCategories,
  requestCountNotifications,
  requestedProfile
})(CompleteProfileScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  name: {
    marginTop: sizeWidth(8),
    paddingHorizontal: 0
  },
  input: {
    paddingHorizontal: 0
  },
  eye: {
    width: moderateScale(14),
    tintColor: appColor.blur,
    height: moderateScale(14)
  },
  visible: {
    marginRight: moderateScale(6)
  },
  date: {
    paddingHorizontal: 0
  },
  desc: {
    fontStyle: "italic",
    color: "#000000",
    textAlign: "center",
    fontSize: sizeFont(12),
    paddingHorizontal: sizeWidth(9),
    marginTop: sizeWidth(4),
    marginBottom: sizeWidth(23)
  },
  calendar: {
    width: sizeWidth(15),
    height: sizeWidth(15)
  },
  button: {
    marginTop: moderateScale(10)
  }
});
