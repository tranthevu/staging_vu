import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import moment from "moment";
import lodash from "lodash";
import validator from "validator";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, text } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import Input from "../../components/common/input";
import Dropdown from "../../components/common/dropdown";
import DateTimePicker from "../../components/common/date-time-picker";
import GenderPicker from "../../components/common/gender-picker";
import Button from "../../components/common/button";
import Api from "../../api/api";
import { requestedProfile } from "../../actions/profile.action";
import { showError } from "../../helpers/error.helper";
import NotificationView from "../../components/common/notification-view";
import { checkApiStatus } from "../../helpers/app.helper";

class ContactScreen extends Component {
  constructor(props) {
    super(props);
    const { profile } = this.props.profile;
    const { masterData } = this.props.masterData;
    const { country } = masterData;
    const selectedCountry =
      profile.country && country.find(item => item.country == profile.country);

    const selectedProvince =
      selectedCountry &&
      profile.city &&
      selectedCountry.provinces &&
      selectedCountry.provinces.find(item => item.province == profile.city);

    const selectedDistrict =
      selectedProvince &&
      profile.district &&
      selectedProvince.districts &&
      selectedProvince.districts.find(item => item.district == profile.district);

    const selectedWard =
      selectedDistrict &&
      profile.ward &&
      selectedDistrict.wards &&
      selectedDistrict.wards.find(item => item.ward == profile.ward);

    this.state = {
      name: profile.full_name || text.emptyString,
      email: profile.email || text.emptyString,
      birthday: profile.dateofbirth && moment(profile.dateofbirth),
      address: profile.address || text.emptyString,
      selectedCountry,
      selectedProvince,
      selectedDistrict,
      selectedWard,
      loading: false,
      gender: profile.gender
    };
  }

  saveInfo = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const {
      name,
      email,
      birthday,
      address,
      selectedCountry,
      selectedDistrict,
      selectedProvince,
      selectedWard,
      gender
    } = this.state;
    if (validator.isEmpty(name)) return showError("Họ và Tên không được để trống!");
    if (/\d/.test(name)) return showError("Vui lòng nhập chữ!");
    if (name.trim().split(" ").length < 2)
      return showError("Họ và Tên nhập ít nhất 2 từ!");

    if (!validator.isEmail(email)) return showError("Địa chỉ Email không hợp lệ!");
    if (!selectedCountry) return showError("Vui lòng chọn quốc gia!");
    if (!selectedProvince) return showError("Vui lòng chọn tỉnh / thành phố!");
    if (!selectedDistrict) return showError("Vui lòng chọn quận!");
    if (!selectedWard) return showError("Vui lòng chọn phường!");
    if (validator.isEmpty(address)) return showError("Vui lòng nhập địa chỉ!");
    try {
      this.setState({ loading: true });
      const data = {
        email,
        full_name: name,
        gender,
        country: lodash.get(selectedCountry, "country"),
        city: lodash.get(selectedProvince, "province"),
        district: lodash.get(selectedDistrict, "district"),
        ward: lodash.get(selectedWard, "ward"),
        address,
        dateofbirth: birthday && birthday.format("YYYY-MM-DDTHH:mm:ssZ")
      };
      await Api.updateProfile(data);
      this.props.requestedProfile();
      this.setState({ loading: false });
      this.props.navigateBack();
    } catch (err) {
      this.setState({ loading: false });
    }
  };

  render(): ReactNode {
    const {
      name,
      email,
      birthday,
      address,
      selectedCountry,
      selectedProvince,
      selectedDistrict,
      selectedWard,
      loading,
      gender
    } = this.state;
    const { masterData } = this.props.masterData;
    const { country } = masterData;
    const { profile } = this.props.profile;
    const { disabled_birthday_update } = profile;
    const maxDate = moment().format("YYYY-MM-DD");
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
          center={<Text style={styles.title}>Thông tin tài khoản</Text>}
        />
        <View style={styles.body}>
          <ScrollView bounces={false}>
            <Input
              label="Họ và Tên"
              style={styles.name}
              onChangeText={text => this.setState({ name: text })}
              placeholder="Nhập họ và tên"
              value={name}
            />
            <Input
              label="Email"
              onChangeText={text => this.setState({ email: text })}
              placeholder="Nhập Email"
              value={email}
            />
            <GenderPicker
              value={gender}
              onValueChange={value => this.setState({ gender: value })}
            />
            <DateTimePicker
              value={birthday}
              disabled={disabled_birthday_update}
              mode="date"
              maxDate={maxDate}
              onDateChange={date => this.setState({ birthday: moment(date) })}
              label="Ngày sinh"
            />
            <Text style={styles.desc}>
              Nhập chính xác ngày sinh để nhận quà sinh nhật. Ngày sinh sẽ không thể thay
              đổi sau nhập.
            </Text>
            <Dropdown
              options={country}
              path="country"
              onValueChange={value =>
                this.setState({
                  selectedCountry: value,
                  selectedProvince: null,
                  selectedDistrict: null,
                  selectedWard: null
                })
              }
              label="Quốc gia"
              value={lodash.get(selectedCountry, "country") || "Chọn một tùy chọn"}
            />
            <Dropdown
              options={lodash.get(selectedCountry, "provinces")}
              path="province"
              onValueChange={value =>
                this.setState({
                  selectedProvince: value,
                  selectedDistrict: null,
                  selectedWard: null
                })
              }
              label="Tỉnh / Thành phố"
              value={lodash.get(selectedProvince, "province") || "Chọn một tùy chọn"}
            />
            <Dropdown
              options={lodash.get(selectedProvince, "districts")}
              path="district"
              onValueChange={value =>
                this.setState({ selectedDistrict: value, selectedWard: null })
              }
              label="Quận"
              value={lodash.get(selectedDistrict, "district") || "Chọn một tùy chọn"}
            />
            <Dropdown
              options={lodash.get(selectedDistrict, "wards")}
              path="ward"
              onValueChange={value => this.setState({ selectedWard: value })}
              label="Phường"
              value={lodash.get(selectedWard, "ward") || "Chọn một tùy chọn"}
            />
            <Input
              onChangeText={text => this.setState({ address: text })}
              label="Địa chỉ"
              value={address}
              placeholder="Nhập địa chỉ"
            />
          </ScrollView>
          <Button
            loading={loading}
            onPress={this.saveInfo}
            style={styles.save}
            text="Lưu"
          />
          <NotificationView />
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    profile: state.profile,
    masterData: state.masterData
  }),
  { resetPage, navigateToPage, navigateBack, requestedProfile }
)(ContactScreen);

const styles = StyleSheet.create({
  save: {
    marginVertical: sizeWidth(18)
  },
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  label: {
    fontFamily: font.medium,
    fontSize: sizeFont(13),
    marginVertical: sizeWidth(10),
    marginHorizontal: sizeWidth(21)
  },
  desc: {
    marginHorizontal: sizeWidth(21),
    fontSize: sizeFont(12),
    fontStyle: "italic",
    marginBottom: sizeWidth(4)
  },
  cancel: {
    color: "white"
  },
  name: {
    marginTop: sizeWidth(12)
  },
  body: {
    flex: 1
  }
});
