import React, { Component, ReactNode } from "react";
import { View, StyleSheet, FlatList } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor, keywordType } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import PromotionItem from "../dashboard/promotion-item";
import BackIcon from "../../components/common/back-icon";
import Brand from "../categoryDashboard/brand";
import {
  requestSearchProducts,
  fetchMoreSearchProducts,
  refreshSearchProducts
} from "../../actions/search-products.action";
import PaginationList from "../../components/common/pagination-list";
import NotificationView from "../../components/common/notification-view";
import { appConfig } from "../../config/app.config";
import EmptyView from "../../components/common/empty-view";
import { checkApiStatus } from "../../helpers/app.helper";

class SearchResultsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRemoveCriteria: false
    };
  }

  renderHeader = () => {
    const { topMerchants } = this.props.topMerchants;
    if (topMerchants.length === 0) return;
    return (
      <View style={styles.body}>
        <Text style={styles.label}>Thương hiệu</Text>
        <View style={styles.list}>
          <FlatList
            data={topMerchants}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderBrand}
          />
        </View>
        <Text style={styles.title}>Ưu đãi</Text>
      </View>
    );
  };

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    this.searchProducts();
  };

  searchProducts = () => {
    const { keyword, category } = this.props.navigation.state.params;
    const params = this.buildRequestParams(keyword, category);
    this.props.requestSearchProducts(params);
  };

  buildRequestParams = (keyword, category) => {
    const data = {};
    if (keyword.type === keywordType.keyword) data.q = lodash.get(keyword, "value");

    const { isRemoveCriteria } = this.state;
    if (keyword.type === keywordType.category) {
      if (!isRemoveCriteria) {
        data.category = lodash.get(keyword, "slug");
      }
      data.q = lodash.get(keyword, "value");
    }

    if (category) {
      data.category = category;
    }
    data.type = appConfig.activeApp.key;
    // Currently not support search for category or area. Commented out for now
    // if (keyword.type === keywordType.area) {
    //   if (!isRemoveCriteria) {
    //     data.area = lodash.get(keyword, "slug");
    //   }
    //   data.q = lodash.get(keyword, "value");
    // }
    return data;
  };

  renderBrand = ({ item, index }) => {
    return <Brand item={item} />;
  };

  removeCriteria = () => {
    this.setState({ isRemoveCriteria: true }, this.searchProducts);
  };

  renderCriteria = () => {
    const { keyword } = this.props.navigation.state.params;

    return (
      <View style={styles.criteria}>
        <Text style={styles.text}>{lodash.get(keyword, "name")}</Text>
        <TouchableIcon
          onPress={this.removeCriteria}
          iconStyle={styles.x}
          source={require("../../../res/icon/x-tag.png")}
        />
      </View>
    );
  };

  clearCriteria = () => {
    this.setState({ criteria: null });
  };

  render(): ReactNode {
    const { keyword } = this.props.navigation.state.params;
    const value = lodash.get(keyword, "value");
    const name = lodash.get(keyword, "name");
    const { count, loading } = this.props.searchProducts;
    const { isRemoveCriteria } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          center={<Text style={styles.header}>Kết quả tìm kiếm</Text>}
          left={<BackIcon />}
        />
        <View style={styles.wrap}>
          {!loading && (
            <Text numberOfLines={2} style={styles.results}>
              Có <Text style={styles.keyword}>"{count}"</Text> Kết quả tìm kiếm cho{" "}
              <Text style={styles.keyword}>"{value}"</Text>
            </Text>
          )}
          {!loading &&
            !isRemoveCriteria &&
            keyword.type !== keywordType.keyword &&
            this.renderCriteria()}
          {this.renderList()}
          <NotificationView />
        </View>
      </View>
    );
  }

  renderList = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      searchProducts
    } = this.props.searchProducts;
    const { fetchMoreSearchProducts, refreshSearchProducts } = this.props;
    const { keyword } = this.props.navigation.state.params;
    const params = this.buildRequestParams(keyword);
    return (
      <PaginationList
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderPromotion}
        loading={loading}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        firstLoading={firstLoading}
        onEndReached={() => fetchMoreSearchProducts(params)}
        onRefresh={() => refreshSearchProducts(params)}
        data={searchProducts}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  renderPromotion = ({ item, index }) => {
    return <PromotionItem item={item} />;
  };
}

export default connect(
  state => ({
    topMerchants: state.topMerchants,
    searchProducts: state.searchProducts
  }),
  {
    resetPage,
    navigateToPage,
    refreshSearchProducts,
    requestSearchProducts,
    fetchMoreSearchProducts
  }
)(SearchResultsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  wrap: {
    flex: 1
  },
  label: {
    color: appColor.blur,
    fontFamily: font.medium,
    marginTop: sizeWidth(10),
    marginBottom: sizeWidth(5),
    paddingHorizontal: sizeWidth(9)
  },
  title: {
    color: appColor.blur,
    fontFamily: font.medium,
    paddingHorizontal: sizeWidth(9),
    marginBottom: sizeWidth(5)
  },
  menu: {
    width: sizeWidth(24),
    height: sizeWidth(24)
  },
  body: {},
  list: {
    marginTop: sizeWidth(6),
    marginBottom: sizeWidth(15)
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  count: {
    fontSize: sizeFont(14),
    fontFamily: font.bold
  },
  results: {
    marginTop: sizeWidth(16),
    marginBottom: sizeWidth(10),
    marginHorizontal: sizeWidth(9),
    fontSize: sizeFont(14)
  },
  keyword: {
    fontSize: sizeFont(14),
    fontFamily: font.medium,
    color: "#444444"
  },
  criteria: {
    paddingHorizontal: sizeWidth(10),
    height: moderateScale(24),
    borderRadius: moderateScale(12),
    borderWidth: 1,
    borderColor: appColor.blur,
    marginBottom: sizeWidth(7),
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: sizeWidth(8),
    alignSelf: "flex-start"
  },
  text: {
    fontSize: sizeFont(12),
    marginRight: sizeWidth(6)
  },
  x: {
    width: moderateScale(9),
    height: moderateScale(9)
  }
});
