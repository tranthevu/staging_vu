import React, { Component } from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import _ from "lodash";
import { navigateToPage } from "../../../actions/nav.action";
import Text from "../../../components/common/text";

import Toolbar from "../../../components/common/toolbar";
import BackIcon from "../../../components/common/back-icon";
import NotificationView from "../../../components/common/notification-view";
import MerchantItem from "../../follow/merchant-item";
import { getListMerchantByAddressSelector } from "../../../selector/merchantSelectors";
import styles from "./styles";
import { searchListMerchantByAddressSubmit } from "../../../actions/merchant.actions";
import FlatListAutoLoadMore from "../../../components/FlatListAutoLoadMore";
import { getIsFetching } from "../../../selector/loadingSelector";
import AddressItem from "../../../components/Item/AddressItem";

class ListMerchantByAddressScreen extends Component {
  constructor(props) {
    super(props);
    this.params = props.navigation.state.params || {};
    this.state = {
      data: []
    };
  }

  renderMerchant = ({ item }) => {
    return <MerchantItem style={styles.item} item={item} />;
  };

  onRefresh = (nextPage, showLoading) => {
    this.props.searchListMerchantByAddressSubmit({
      callback: this.onSearchListMerchantResponse,
      page: nextPage,
      showLoading,
      point: this.pointAddress
    });
  };

  get pointAddress() {
    const { item } = this.params;
    const { location } = item || {};
    const { lat, long } = location || {};
    return `${long},${lat}`;
  }

  onFetch = (nextPage, isRefresh) => {
    this.onRefresh(nextPage);
  };

  renderList = () => {
    const { isFetching } = this.props;
    const { data } = this.state;
    return (
      <FlatListAutoLoadMore
        isFetching={isFetching}
        onFetch={this.onFetch}
        numColumns={2}
        contentContainerStyle={styles.content}
        renderItem={this.renderMerchant}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        data={data}
      />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Toolbar
          center={<Text style={styles.header}>Doanh nghiệp</Text>}
          left={<BackIcon />}
        />
        <View style={styles.body}>
          <AddressItem
            address={_.get(this.params, "item.address", "Khu vực có nhiều ưu đãi")}
          />
          {this.renderList()}
          <NotificationView />
        </View>
      </View>
    );
  }

  onSearchListMerchantResponse = (err, res) => {
    if (!err) {
      const { results } = res;
      this.setState({ data: results });
    }
  };

  componentDidMount = () => {
    this.onRefresh(1, true);
  };
}

export default connect(
  state => ({
    listMerchant: getListMerchantByAddressSelector(state),
    isFetching: getIsFetching(state)
  }),
  {
    navigateToPage,
    searchListMerchantByAddressSubmit
  }
)(ListMerchantByAddressScreen);
