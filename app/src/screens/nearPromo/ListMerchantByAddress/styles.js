import { StyleSheet } from "react-native";
import { sizeFont, sizeWidth } from "../../../helpers/size.helper";
import { font, appColor } from "../../../constants/app.constant";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  content: {
    paddingHorizontal: sizeWidth(6),
    paddingVertical: sizeWidth(5)
  },
  item: {
    marginVertical: sizeWidth(5)
  },
  body: {
    flex: 1
  }
});
