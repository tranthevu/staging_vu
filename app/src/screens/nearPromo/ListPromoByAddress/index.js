import React, { Component } from "react";
import { View, FlatList } from "react-native";
import { connect } from "react-redux";
import _ from "lodash";
import { navigateToPage } from "../../../actions/nav.action";
import Text from "../../../components/common/text";

import Toolbar from "../../../components/common/toolbar";
import TouchableIcon from "../../../components/common/touchable-icon";
import BackIcon from "../../../components/common/back-icon";
import PromotionItem from "../../dashboard/promotion-item";
import PromotionItemList from "../../dashboard/promotion-item-list";
import MerchantItem from "../../../components/Item/MerchantItem";
import { IC_LIST, IC_GRID, IC_CHEVRON } from "../../../commons/images";
import styles from "./styles";
import { LIST_MERCHANT_BY_ADDRESS_SCREEN } from "../../../navigators/screensName";
import { getListTopMerchantByAddressSelector } from "../../../selector/merchantSelectors";
import {
  getTopMerchantByAddressSubmit,
  clearTopMerchantByAddress
} from "../../../actions/merchant.actions";
import { getListPromoByAddressSubmit } from "../../../actions/promo-products.action";
import { getIsFetching } from "../../../selector/loadingSelector";
import FlatListAutoLoadMore from "../../../components/FlatListAutoLoadMore";
import AddressItem from "../../../components/Item/AddressItem";
import { checkApiStatus } from "../../../helpers/app.helper";

const viewType = {
  grid: 0,
  list: 1
};

class ListPromoByAddressScreen extends Component {
  constructor(props) {
    super(props);
    this.params = props.navigation.state.params || {};
    this.state = {
      selectedViewType: viewType.list,
      partnerProducts: []
    };
  }

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    this.onRefresh(1);
  };

  componentWillMount() {
    this.props.clearTopMerchantByAddress();
  }

  onFetch = (nextPage, isRefresh, showLoading) => {
    if (isRefresh) {
      this.onRefresh();
      return;
    }
    this.props.getListPromoByAddressSubmit({
      showLoading,
      callback: this.onGetListPromotResponse,
      page: nextPage,
      point: this.pointAddress
    });
  };

  onGetListPromotResponse = (err, data) => {
    if (!err) {
      this.setState({ partnerProducts: data });
    }
  };

  get pointAddress() {
    const { item } = this.params;
    const { location } = item || {};
    const { lat, long } = location || {};
    return `${long},${lat}`;
  }

  onRefresh = page => {
    this.props.getTopMerchantByAddressSubmit({
      point: this.pointAddress
    });
    this.onFetch(page, false, true);
  };

  navigateToListMerchant = () => {
    this.props.navigateToPage(LIST_MERCHANT_BY_ADDRESS_SCREEN, {
      item: this.params.item
    });
  };

  renderHeader = () => {
    const { merchants } = this.props;
    return (
      <View>
        {Array.isArray(merchants) && merchants.length > 0 && (
          <View style={[styles.list, styles.shadow]}>
            <View style={styles.bar}>
              <Text style={styles.label}>Doanh nghiệp</Text>
              <TouchableIcon
                iconStyle={styles.arrow}
                onPress={this.navigateToListMerchant}
                source={IC_CHEVRON}
              />
            </View>
            <FlatList
              horizontal
              data={merchants}
              renderItem={this.renderMerchant}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={styles.top}
              showsHorizontalScrollIndicator={false}
            />
          </View>
        )}
        {this.renderTitle()}
      </View>
    );
  };

  renderMerchant = ({ item }) => {
    return <MerchantItem item={item} />;
  };

  renderProducts = () => {
    const { partnerProducts } = this.state;
    const { isFetching } = this.props;

    return (
      <FlatListAutoLoadMore
        isFetching={isFetching}
        onFetch={this.onFetch}
        renderItem={this.renderPromotion}
        ListHeaderComponent={this.renderHeader()}
        data={partnerProducts}
        ItemSeparatorComponent={null}
      />
    );
  };

  renderTitle = () => {
    const { selectedViewType } = this.state;
    return (
      <View style={styles.bar}>
        <Text style={styles.label}>Ưu đãi</Text>
        <TouchableIcon
          style={styles.view}
          iconStyle={styles.viewIcon}
          onPress={this.toggleViewType}
          source={selectedViewType === viewType.list ? IC_LIST : IC_GRID}
        />
      </View>
    );
  };

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  renderPromotion = ({ item }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem item={item} />
    ) : (
      <PromotionItemList item={item} />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Toolbar
          center={<Text style={styles.header}>Ưu đãi gần bạn</Text>}
          left={<BackIcon />}
        />
        <AddressItem
          address={_.get(this.params, "item.address", "Khu vực có nhiều ưu đãi")}
        />
        {this.renderProducts()}
      </View>
    );
  }
}

export default connect(
  state => ({
    partnerProducts: state.partnerProducts,
    merchants: getListTopMerchantByAddressSelector(state),
    isFetching: getIsFetching(state)
  }),
  {
    navigateToPage,
    getTopMerchantByAddressSubmit,
    clearTopMerchantByAddress,
    getListPromoByAddressSubmit
  }
)(ListPromoByAddressScreen);
