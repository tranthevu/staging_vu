import { StyleSheet } from "react-native";
import { sizeFont, sizeWidth, moderateScale } from "../../../helpers/size.helper";
import { font, appColor } from "../../../constants/app.constant";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  icon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: "white"
  },
  top: {
    paddingHorizontal: sizeWidth(5)
  },
  label: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: appColor.blur
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(9)
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: sizeWidth(5),
    paddingHorizontal: sizeWidth(10)
  },
  arrow: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  list: {
    paddingVertical: sizeWidth(4),
    marginBottom: moderateScale(12)
  },
  shadow: {
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1
    }
  }
});
