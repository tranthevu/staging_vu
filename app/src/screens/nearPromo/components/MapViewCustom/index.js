/* eslint-disable no-unused-expressions */
import React, { Component } from "react";
import _ from "lodash";
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";

export default class MapViewCustom extends Component {
  shouldComponentUpdate(nextProps) {
    const { update = {} } = this.props;
    if (update === _.get(nextProps, "update", undefined) && update !== 0) {
      return false;
    }
    return true;
  }

  animateCamera = latLng => {
    this.mapView &&
      this.mapView.animateCamera({ center: latLng, zoom: 14 }, { duration: 1000 });
  };

  render() {
    return (
      <MapView
        ref={ref => {
          this.mapView = ref;
        }}
        provider={PROVIDER_GOOGLE}
        showsUserLocation
        showsMyLocationButton
        {...this.props}
      >
        {this.props.children}
      </MapView>
    );
  }
}
