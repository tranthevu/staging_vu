import React, { Component } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  SectionList,
  ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import { PROVIDER_GOOGLE, Circle } from "react-native-maps";
import _ from "lodash";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeWidth } from "../../helpers/size.helper";
import { appColor, LATITUDE_DELTA, LONGITUDE_DELTA } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import PromotionItem from "../dashboard/promotion-item";
import PromotionItemList from "../dashboard/promotion-item-list";
import {
  IC_SORT,
  IC_LIST,
  IC_GRID,
  IC_LOCATION,
  IC_ZOOM_IN,
  IC_ZOOM_OUT,
  IC_MARKER_PLUS_RED
} from "../../commons/images";
import { getCurrentPosition } from "../../helpers/permission-helper";
import styles from "./styles";
import MarkerMerchant from "../../components/MarkerMerchant";
import MerchantInfoItem from "../../components/Item/MerchantInfoItem";
import {
  getProductNearBySubmit,
  clearProductsNearBy
} from "../../actions/search-products.action";
import {
  FILTER_PRODUCT_NEAR_BY_SCREEN,
  MERCHANT_DETAIL_SCREEN,
  LIST_PROMO_BY_ADDRESS_SCREEN
} from "../../navigators/screensName";
import { getIsFetching } from "../../selector/loadingSelector";
import MapViewCustom from "./components/MapViewCustom";
import { getPlaceUserByLocationSubmit } from "../../actions/profile.action";
import { getPlaceUserSelector } from "../../selector/profileSelectors";
import { showAlertNotification } from "../../ultis/arlert";
import { checkApiStatus } from "../../helpers/app.helper";

const viewType = {
  grid: 0,
  list: 1
};

class NearPromoScreen extends Component {
  constructor(props) {
    super(props);
    this.params = props.navigation.state.params || {};
    this.state = {
      selectedViewType: viewType.list,
      isZoom: false,
      isShowInfoMerchant: false,
      heightTop: 0,
      coords: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      markers: [],
      dataProducts: [],
      appliedFilters: {
        selectedRadius: {
          type: 0.25,
          name: "250m"
        },
        selectedCategory: this.params.category || {
          id: 1,
          name: "Tất cả",
          slug: ""
        }
      }
    };
    this.page = 1;
  }

  componentWillUnmount() {
    this.props.clearProductsNearBy();
  }

  onGetPlaceUser = () => {
    const { coords } = this.state;
    this.props.getPlaceUserByLocationSubmit({
      lat: coords.latitude,
      lng: coords.longitude,
      callback: (err, data) => {}
    });
  };

  onResponseLocationSuccess = async coords => {
    const { markers } = this.state;
    this.setState({ coords }, () => {
      this.setState({ markers: [...markers, { coordinate: coords }] }, () => {
        this.mounted = true;
        this.onGetPlaceUser();
        this.onRequestFilter(1);
      });
    });
  };

  onUserLocationError = () => {
    showAlertNotification(
      {
        message:
          "Vui lòng đảm bảo bạn đã kích hoạt GPS và cấp quyền truy cập vị trí để sử dụng tính năng này."
      },
      () => {
        this.props.navigation.goBack();
      }
    );
  };

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    await navigator.geolocation.getCurrentPosition(currentPosition => {
      const coords = {
        latitude: currentPosition.coords.latitude,
        longitude: currentPosition.coords.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      };
      this.setState({
        ...this.state,
        coords: coords
      });
    });
    await getCurrentPosition(this.onResponseLocationSuccess, this.onUserLocationError);
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { productsNearBy } = this.props;
    const { dataProducts } = this.state;
    if (nextProps.productsNearBy !== productsNearBy) {
      const newDataProducts = nextProps.productsNearBy.data;
      if (Array.isArray(newDataProducts)) {
        const dataConvert = newDataProducts.map(e => {
          const { long = 0, lat = 0 } = e.location;
          const coordinate = {
            latitude: parseFloat(lat) || 0,
            longitude: parseFloat(long) || 0
          };
          return { ...e, coordinate };
        });
        this.setState({ dataProducts: [...dataProducts, ...dataConvert] });
      }
    }
  }

  renderHeader = ({ section }) => {
    const { selectedViewType } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: appColor.bg }}>
        <View style={styles.bar}>
          <Text style={styles.label}>Ưu đãi gần bạn</Text>
          <TouchableIcon
            style={styles.view}
            iconStyle={styles.viewIcon}
            onPress={this.toggleViewType}
            source={selectedViewType === viewType.list ? IC_LIST : IC_GRID}
          />
        </View>
      </View>
    );
  };

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  renderPromotion = ({ item, index }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem item={item} />
    ) : (
      <PromotionItemList item={item} />
    );
  };

  onRefresh = () => {
    this.setState({ dataProducts: [] }, () => {
      this.onGetPlaceUser();
      this.onRequestFilter(1);
    });
  };

  renderInfoCurrentLocation = () => {
    const { appliedFilters } = this.state;
    const { selectedRadius = {}, selectedCategory = {} } = appliedFilters || {};
    let formatted_address = _.get(this.props.placeUser, "formatted_address");
    if (formatted_address && formatted_address.length > 25) {
      // YA15-252 [Yolla Network App][1.2 build 58 - 139][Location] The button "Lam moi" is disable on iOS devices
      formatted_address = `${formatted_address.substr(0, 25)}...`;
    }
    return (
      <View style={styles.viewInfoLocation}>
        <View style={styles.viewLeftInfoLocation}>
          <Text style={styles.labelLocation} numberOfLines={1}>
            {formatted_address}
          </Text>
          <TouchableOpacity onPress={this.onRefresh}>
            <Text style={styles.txtReload}>Làm mới</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.rightInfoLocation}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Image source={IC_LOCATION} style={styles.icLocation} />
            <Text style={styles.labelLocation}>{selectedRadius.name}</Text>
          </View>
        </View>
      </View>
    );
  };

  onPressZoomMap = () => {
    const { isZoom } = this.state;
    this.setState({ isZoom: !isZoom, isShowInfoMerchant: false });
  };

  onApplyFilters = ({ selectedCategory, selectedRadius }) => {
    this.setState(
      { appliedFilters: { selectedCategory, selectedRadius }, dataProducts: [] },
      () => {
        this.onRequestFilter(1);
      }
    );
  };

  onPressMarker = item => {
    this.setState({ isShowInfoMerchant: true, itemMerchantShow: item });
  };

  navigateToPageMerchantDetail = item => {
    this.setState({ isShowInfoMerchant: false }, () => {
      const { isBranch } = item;
      if (isBranch) {
        this.props.navigateToPage(LIST_PROMO_BY_ADDRESS_SCREEN, {
          item
        });
        return;
      }
      this.props.navigateToPage(MERCHANT_DETAIL_SCREEN, {
        merchantCode: _.get(item, "merchant.code_merchant")
      });
    });
  };

  onPressMap = event => {
    if (event.nativeEvent.action !== "marker-press") {
      this.setState({ isShowInfoMerchant: false });
    }
  };

  filterProductByBranch = () => {
    const { dataProducts = [] } = this.state;
    const result = [];
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < dataProducts.length; i++) {
      const element = dataProducts[i];
      const itemFind = result.find(e => {
        const { coordinate } = e;
        const { coordinate: coordinate1 } = element;
        return (
          coordinate.latitude === coordinate1.latitude &&
          coordinate.longitude === coordinate1.longitude
        );
      });
      if (itemFind) {
        itemFind.isBranch = true;
        itemFind.countMerchant = itemFind.countMerchant ? itemFind.countMerchant + 1 : 1;
        itemFind.sourceImg = IC_MARKER_PLUS_RED;
        itemFind.merchant.name = itemFind.address || "Khu vực có nhiều ưu đãi";
      } else {
        result.push(element);
      }
    }
    return result;
  };

  renderMap = () => {
    const { isZoom, heightMap = 60, isShowInfoMerchant, itemMerchantShow } = this.state;
    const data = this.filterProductByBranch();
    const { appliedFilters } = this.state;
    const { selectedRadius = {} } = appliedFilters || {};
    let selectedRadiusName = 250;
    if (selectedRadius.name.indexOf("km") === -1) {
      selectedRadiusName = parseInt(selectedRadius.name.replace("m", ""), 10);
    } else {
      const selectedRadiusString = selectedRadius.name.replace("km", "");
      selectedRadiusName = selectedRadiusString * 1000;
    }
    return (
      <View style={{ flex: 1 }}>
        {this.renderInfoCurrentLocation()}
        <View style={styles.map}>
          <TouchableOpacity onPress={this.onPressZoomMap} style={styles.iconZoom}>
            <Image style={styles.zoomIc} source={isZoom ? IC_ZOOM_IN : IC_ZOOM_OUT} />
          </TouchableOpacity>
          {isShowInfoMerchant && (
            <MerchantInfoItem
              onPress={this.navigateToPageMerchantDetail}
              item={{
                ...itemMerchantShow,
                name: _.get(itemMerchantShow, "merchant.name", ""),
                content: "",
                image: _.get(itemMerchantShow, "merchant.logo_image.thumbnail")
              }}
              style={{
                position: "absolute",
                top: heightMap * 0.2,
                left: sizeWidth(9),
                right: sizeWidth(9),
                zIndex: 5
              }}
            />
          )}
          <MapViewCustom
            onPress={this.onPressMap}
            ref={ref => {
              this.mapView = ref;
            }}
            onLayout={evt => {
              this.setState({ heightMap: evt.nativeEvent.layout.height });
            }}
            provider={PROVIDER_GOOGLE}
            style={styles.map}
            loadingEnabled
            showsUserLocation
            showsMyLocationButton
            followsUserLocation
            region={this.state.coords}
            update={data.length}
          >
            {data.map((item, index) => (
              <MarkerMerchant
                item={item}
                onPress={this.onPressMarker}
                key={index.toString()}
                coordinate={item.coordinate}
                image={{ uri: _.get(item, "merchant.logo_image.thumbnail") }}
              />
            ))}
            {this.state.coords.longitude !== 0 ? (
              <Circle
                center={{
                  latitude: this.state.coords.latitude,
                  longitude: this.state.coords.longitude
                }}
                radius={selectedRadiusName}
                fillColor={"rgba(0, 0, 0, 0.1)"}
                strokeColor={"rgba(76, 135, 248, 0.5)"}
              />
            ) : null}
          </MapViewCustom>
        </View>
      </View>
    );
  };

  onPressFilter = () => {
    const { appliedFilters } = this.state;
    this.props.navigateToPage(FILTER_PRODUCT_NEAR_BY_SCREEN, {
      onApplyFilters: this.onApplyFilters,
      appliedFilters
    });
  };

  onRequestFilter = (page, showLoading) => {
    const { type } = this.props.navigation.state.params || {};
    this.page = page;
    this.setState({ isShowInfoMerchant: false }, () => {
      const { appliedFilters, coords } = this.state;
      const { latitude, longitude } = coords;
      const { selectedRadius, selectedCategory = {} } = appliedFilters;
      this.props.getProductNearBySubmit({
        point: `${longitude},${latitude}`,
        radius: selectedRadius.type,
        category: selectedCategory.slug,
        page,
        type,
        showLoading,
        callback: (err, data) => {
          // console.log(">>>>>>>", { err, data });
        }
      });
    });
  };

  onEndReachedLoadMore = () => {
    const { loading, productsNearBy } = this.props;
    if (!loading && productsNearBy.next && this.mounted) {
      this.onRequestFilter(this.page + 1, false);
    }
  };

  renderFooterComponent = () => {
    const { loading } = this.props;
    const { dataProducts } = this.state;
    if (loading && dataProducts.length > 0) {
      return (
        <ActivityIndicator
          style={styles.loadingIndicator}
          animating
          size="large"
          color={appColor.primary}
        />
      );
    }
    return null;
  };

  keyExtractor = (item, index) => (item.id || index).toString();

  ListEmptyComponent = () => <Text style={styles.txtEmpty}>Không tìm thấy ưu đãi.</Text>;

  render() {
    const { isZoom, dataProducts } = this.state;
    // dataProducts will return dupplicated products (Map allows dup product while list won't allow)
    const filteredProducts = _.uniqBy(dataProducts, "id");
    return (
      <View style={styles.container}>
        <Toolbar
          onLayout={evt => {
            this.setState({ heightTop: evt.nativeEvent.layout.height });
          }}
          leftStyle={[styles.part, styles.left]}
          rightStyle={styles.part}
          center={
            <Text numberOfLines={1} style={styles.header}>
              Ưu đãi gần bạn
            </Text>
          }
          left={<BackIcon />}
          right={
            <TouchableIcon
              style={styles.wrap}
              iconStyle={styles.icon}
              source={IC_SORT}
              onPress={this.onPressFilter}
            />
          }
        />
        <SectionList
          ListHeaderComponent={this.renderMap}
          stickySectionHeadersEnabled
          stickyHeaderIndices={[0]}
          renderSectionHeader={this.renderHeader}
          sections={
            filteredProducts.length > 0
              ? [{ tittle: "Ưu đãi gần bạn", data: filteredProducts }]
              : []
          }
          renderItem={this.renderPromotion}
          showsVerticalScrollIndicator={false}
          onEndReachedThreshold={0.2}
          onEndReached={this.onEndReachedLoadMore}
          ListFooterComponent={this.renderFooterComponent}
          keyExtractor={this.keyExtractor}
          ListEmptyComponent={this.ListEmptyComponent}
          // onRefresh={() => refreshExclusiveProducts()}
        />
        {isZoom && (
          <View
            style={{
              height: "100%",
              width: "100%",
              position: "absolute",
              top: this.state.heightTop
              // display: isZoom ? "flex" : "none"
            }}
          >
            {this.renderMap()}
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  topCategories: state.topCategories,
  productsNearBy: state.searchProducts.productsNearBy,
  loading: getIsFetching(state),
  placeUser: getPlaceUserSelector(state)
});

const mapDispatchToProps = {
  resetPage,
  navigateToPage,
  getProductNearBySubmit,
  clearProductsNearBy,
  getPlaceUserByLocationSubmit
};

export default connect(mapStateToProps, mapDispatchToProps)(NearPromoScreen);
