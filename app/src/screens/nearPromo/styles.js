import { StyleSheet } from "react-native";
import { appColor, font } from "../../constants/app.constant";
import { moderateScale, sizeFont } from "../../helpers/size.helper";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  map: {
    flex: 1,
    height: moderateScale(325)
  },
  body: {
    flex: 1
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  icon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: "white"
  },
  wrap: {
    alignSelf: "flex-end",
    marginRight: moderateScale(6)
  },
  right: {
    // backgroundColor: "blue"
  },
  part: {
    // width: moderateScale(90)
  },
  left: {
    alignItems: "flex-start",
    paddingLeft: moderateScale(10)
  },
  categories: {
    marginBottom: moderateScale(12),
    marginTop: moderateScale(8)
  },
  label: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.bold
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: appColor.blur
  },
  view: {
    alignSelf: "center",
    marginLeft: moderateScale(9)
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: moderateScale(5),
    paddingHorizontal: moderateScale(10)
  },
  icLocation: {
    width: moderateScale(12),
    height: moderateScale(12),
    marginRight: moderateScale(8)
  },
  rightInfoLocation: {
    alignItems: "center",
    justifyContent: "flex-end",
    flexDirection: "row",
    flex: 1
  },
  viewInfoLocation: {
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: moderateScale(8),
    paddingVertical: moderateScale(6),
    height: moderateScale(25),
    borderBottomColor: appColor.gray,
    borderBottomWidth: 1
  },
  labelLocation: {
    fontFamily: font.medium,
    fontSize: sizeFont(12),
    marginRight: moderateScale(6)
  },
  infoDistance: {
    flexDirection: "row"
  },
  txtReload: {
    fontSize: sizeFont(12),
    fontFamily: font.medium,
    color: appColor.blue
  },
  viewLeftInfoLocation: {
    width: moderateScale(160),
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between"
  },
  iconZoom: {
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    borderRadius: moderateScale(30),
    width: moderateScale(30),
    height: moderateScale(30),
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 10,
    top: moderateScale(8),
    left: moderateScale(8)
  },
  zoomIc: {
    width: moderateScale(18),
    height: moderateScale(18)
  },
  loadingIndicator: {
    marginVertical: moderateScale(4)
  },
  txtEmpty: {
    textAlign: "center",
    textAlignVertical: "center",
    flex: 1,
    flexDirection: "row",
    padding: moderateScale(18)
  }
});
