import React, { Component } from "react";
import { View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import PaginationList from "../../components/common/pagination-list";
import PromotionItem from "../dashboard/promotion-item";
import PromotionItemList from "../dashboard/promotion-item-list";
import {
  requestShare2EarnPromo,
  fetchMoreShare2EarnPromo,
  refreshShare2EarnPromo
} from "../../actions/share2earn-promo.action";
import EmptyView from "../../components/common/empty-view";
import Banner from "../../components/Banner";
import { getBannerShareToEarnPromo } from "../../selector/bannerSelectors";
import { getShareToEarnBannersSubmit } from "../../actions/banners.action";
import { checkApiStatus } from "../../helpers/app.helper";

const viewType = {
  grid: 0,
  list: 1
};

class ShareToEarnPromoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedViewType: viewType.list
    };
  }

  navigateToSearch = () => {
    this.props.navigateToPage("Search");
  };

  renderHeader = () => {
    const { bannerShareToEarnPromo } = this.props;
    return (
      <View>
        <Banner data={bannerShareToEarnPromo} />
        {this.renderTitle()}
      </View>
    );
  };

  renderRecentProducts = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      share2EarnPromo
    } = this.props.share2EarnPromo;
    const { fetchMoreShare2EarnPromo, refreshShare2EarnPromo } = this.props;
    return (
      <PaginationList
        ref={ref => {
          this.list = ref;
        }}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderPromotion}
        loading={loading}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        firstLoading={firstLoading}
        ListHeaderComponent={this.renderHeader()}
        onEndReached={() => fetchMoreShare2EarnPromo()}
        onRefresh={() => refreshShare2EarnPromo()}
        data={share2EarnPromo}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  renderTitle = () => {
    const { selectedViewType } = this.state;
    return (
      <View style={styles.bar}>
        <Text style={styles.label}>Ưu đãi mới</Text>
        <TouchableIcon
          style={styles.view}
          iconStyle={styles.viewIcon}
          onPress={this.toggleViewType}
          source={
            selectedViewType === viewType.list
              ? require("../../../res/icon/list.png")
              : require("../../../res/icon/grid.png")
          }
        />
      </View>
    );
  };

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  renderPromotion = ({ item }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem item={item} />
    ) : (
      <PromotionItemList item={item} />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Toolbar
          leftStyle={[styles.part, styles.left]}
          rightStyle={[styles.part, styles.right]}
          center={<Text style={styles.header}>Chia sẻ nhận thưởng</Text>}
          left={<BackIcon />}
          right={
            <TouchableIcon
              style={styles.wrap}
              onPress={this.navigateToSearch}
              iconStyle={styles.icon}
              source={require("../../../res/icon/home-search.png")}
            />
          }
        />
        {this.renderRecentProducts()}
      </View>
    );
  }

  componentDidMount = () => {
    this.props.getShareToEarnBannersSubmit();
    this.props.requestShare2EarnPromo();
  };
}

export default connect(
  state => ({
    share2EarnPromo: state.share2EarnPromo,
    bannerShareToEarnPromo: getBannerShareToEarnPromo(state)
  }),
  {
    resetPage,
    navigateToPage,
    fetchMoreShare2EarnPromo,
    refreshShare2EarnPromo,
    requestShare2EarnPromo,
    getShareToEarnBannersSubmit
  }
)(ShareToEarnPromoScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  header: {
    textAlign: "center",
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  icon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: "white"
  },
  wrap: {},
  right: {
    paddingRight: moderateScale(10),
    alignItems: "flex-end"
  },
  part: {
    // width: moderateScale(90)
  },
  left: {
    alignItems: "flex-start",
    paddingLeft: moderateScale(10)
  },
  label: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: appColor.blur
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(9)
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: sizeWidth(5),
    paddingHorizontal: sizeWidth(10)
  }
});
