import React, { Component, ReactNode } from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";

import CacheImage from "../../components/common/cache-image";

class MerchantItem extends Component {
  render(): ReactNode {
    const { style, item } = this.props;
    return (
      <TouchableOpacity
        onPress={this.navigateToMerchantDetail}
        style={[styles.container, style]}
      >
        <CacheImage style={styles.image} uri={item.logo_image} />
        <View style={styles.content}>
          <Text numberOfLines={1} style={styles.title}>
            {item.name}
          </Text>
          <Text numberOfLines={1} style={styles.count}>
            Có {item.product_count} ưu đãi
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  navigateToMerchantDetail = () => {
    const { item } = this.props;
    this.props.navigateToPage("MerchantDetail", { merchantCode: item.code });
  };
}
export default connect(
  null,
  { navigateToPage }
)(MerchantItem);

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    width: sizeWidth(146),
    height: sizeWidth(160),
    borderRadius: sizeWidth(6),
    overflow: "hidden",
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    marginHorizontal: sizeWidth(4)
  },
  image: {
    width: sizeWidth(146),
    borderTopLeftRadius: sizeWidth(6),
    borderTopRightRadius: sizeWidth(6),
    height: sizeWidth(110),
    overflow: "hidden"
  },
  content: {
    paddingVertical: sizeWidth(8),
    paddingHorizontal: sizeWidth(14)
  },
  title: {
    fontSize: sizeFont(12),
    fontFamily: font.bold,
    color: "black"
  },
  count: {
    fontSize: sizeFont(12),
    marginTop: moderateScale(4),
    fontFamily: font.lightItalic,
    color: "black"
  }
});
