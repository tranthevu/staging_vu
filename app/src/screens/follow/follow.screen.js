import React, { Component, ReactNode } from "react";
import { View, StyleSheet, FlatList } from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import PaginationList from "../../components/common/pagination-list";
import PromotionItem from "../dashboard/promotion-item";
import PromotionItemList from "../dashboard/promotion-item-list";
import MerchantItem from "./merchant-item";
import Api from "../../api/api";
import {
  refreshPartnerProducts,
  requestPartnerProducts,
  fetchMorePartnerProducts
} from "../../actions/partner-products.action";
import EmptyView from "../../components/common/empty-view";
import { checkApiStatus } from "../../helpers/app.helper";

const viewType = {
  grid: 0,
  list: 1
};

class FollowScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedViewType: viewType.list,
      merchants: []
    };
  }

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    this.props.requestPartnerProducts();
    const res = await Api.getFollowMerchants({
      page: 1,
      page_size: 7
    });
    this.setState({
      merchants: res.results
    });
  };

  navigateToFollowMerchant = () => {
    this.props.navigateToPage("FollowMerchant");
  };

  renderHeader = () => {
    const { merchants } = this.state;
    return (
      <View>
        {Array.isArray(merchants) && merchants.length > 0 && (
          <View style={[styles.list, styles.shadow]}>
            <View style={styles.bar}>
              <Text style={styles.label}>Đang theo dõi</Text>
              <TouchableIcon
                iconStyle={styles.arrow}
                onPress={this.navigateToFollowMerchant}
                source={require("../../../res/icon/chevron-right.png")}
              />
            </View>
            <FlatList
              horizontal
              data={merchants}
              renderItem={this.renderMerchant}
              keyExtractor={(item, index) => index.toString()}
              contentContainerStyle={styles.top}
              showsHorizontalScrollIndicator={false}
            />
          </View>
        )}
        {this.renderTitle()}
      </View>
    );
  };

  renderMerchant = ({ item }) => {
    return <MerchantItem item={item} />;
  };

  renderRecentProducts = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      partnerProducts
    } = this.props.partnerProducts;
    const { refreshPartnerProducts, fetchMorePartnerProducts } = this.props;
    return (
      <PaginationList
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderPromotion}
        loading={loading}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        firstLoading={firstLoading}
        ListHeaderComponent={this.renderHeader()}
        onEndReached={() => fetchMorePartnerProducts()}
        onRefresh={() => refreshPartnerProducts()}
        data={partnerProducts}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  renderTitle = () => {
    const { selectedViewType } = this.state;
    return (
      <View style={styles.bar}>
        <Text style={styles.label}>Ưu đãi mới</Text>
        <TouchableIcon
          style={styles.view}
          iconStyle={styles.viewIcon}
          onPress={this.toggleViewType}
          source={
            selectedViewType === viewType.list
              ? require("../../../res/icon/list.png")
              : require("../../../res/icon/grid.png")
          }
        />
      </View>
    );
  };

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  renderPromotion = ({ item }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem item={item} />
    ) : (
      <PromotionItemList item={item} />
    );
  };

  render(): ReactNode {
    return (
      <View style={styles.container}>
        <Toolbar
          center={<Text style={styles.header}>Theo dõi</Text>}
          left={<BackIcon />}
        />
        {this.renderRecentProducts()}
      </View>
    );
  }
}

export default connect(
  state => ({
    partnerProducts: state.partnerProducts
  }),
  {
    resetPage,
    navigateToPage,
    refreshPartnerProducts,
    fetchMorePartnerProducts,
    requestPartnerProducts
  }
)(FollowScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  icon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: "white"
  },
  top: {
    paddingHorizontal: sizeWidth(5)
  },
  label: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: appColor.blur
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(9)
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: sizeWidth(5),
    paddingHorizontal: sizeWidth(10)
  },
  arrow: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  list: {
    paddingVertical: sizeWidth(4),
    marginBottom: moderateScale(12)
  },
  shadow: {
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1
    }
  }
});
