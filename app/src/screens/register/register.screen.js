import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView
} from "react-native";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeWidth, sizeHeight, sizeFont } from "../../helpers/size.helper";
import Button from "../../components/common/button";
import Input from "../../components/common/input";
import { font, text } from "../../constants/app.constant";
import Api from "../../api/api";
import { saveToken } from "../../helpers/storage.helper";
import KeyboardHandlerView from "../../components/common/keyboard-handler-view";
import TouchableIcon from "../../components/common/touchable-icon";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Checkbox from "../../components/common/checkbox";

class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      settings: {},
      username: text.emptyString,
      password: text.emptyString,
      registerSuccessful: false
    };
  }

  render(): ReactNode {
    const { username, password, registerSuccessful } = this.state;
    if (registerSuccessful) return this.renderSuccessIndicator();
    return (
      <View style={styles.container}>
        <View>
          <Image style={styles.logo} source={require("../../../res/icon/app-logo.png")} />
          <Text style={styles.text}>Đăng ký</Text>
          <Input label="User ID" placeholder="Nhập tên đăng nhập" />
          <Input label="Email" placeholder="Nhập email" />
          <Input secureTextEntry={true} label="Mật khẩu" placeholder="Nhập mật khẩu" />
          <Input
            secureTextEntry={true}
            label="Xác nhận"
            placeholder="Xác nhận mật khẩu"
          />
          <Input label="Giới thiệu" placeholder="Nhập mã người giới thiệu" />
          <Checkbox
            label={
              <Text>
                Tôi đồng ý với
                <Text style={styles.privacy}>{" Chính sách và điều khoản "}</Text>
                của chương trình thành viên
              </Text>
            }
          />
          <Button onPress={this.register} text="Đăng ký" />
          <Text onPress={this.props.navigateBack} style={styles.haveAccount}>
            Đã có tài khoản?
            <Text style={styles.login}>Đăng nhập</Text>
          </Text>
        </View>
      </View>
    );
  }

  renderSuccessIndicator = () => {
    return (
      <View style={styles.indicator}>
        <View style={styles.info}>
          <Image style={styles.icon} source={require("../../../res/icon/success.png")} />
          <Text style={styles.success}>Thành công</Text>
          <Text style={styles.desc}>
            Vui lòng kiểm tra email để kích hoạt tài khoản và xác thực email
          </Text>
        </View>
        <Button onPress={this.props.navigateBack} style={styles.back} text="Đăng nhập" />
      </View>
    );
  };

  componentDidMount = async () => {};

  register = async () => {
    this.setState({ registerSuccessful: true });
  };
}

export default connect(
  null,
  { resetPage, navigateToPage, navigateBack }
)(RegisterScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: sizeHeight(12),
    backgroundColor: "#FFFFFF"
  },
  logo: {
    marginTop: sizeHeight(56),
    alignSelf: "center",
    width: sizeWidth(134),
    height: sizeWidth(29)
  },
  text: {
    fontSize: sizeFont(24),
    fontFamily: font.bold,
    color: "#444444",
    alignSelf: "center",
    marginTop: sizeHeight(19),
    marginBottom: sizeHeight(17)
  },
  haveAccount: {
    alignSelf: "center",
    marginTop: sizeHeight(19)
  },
  login: {
    textDecorationLine: "underline"
  },
  privacy: {
    color: "rgb(79, 166, 206)"
  },
  icon: {
    width: sizeWidth(150),
    height: sizeWidth(150)
  },
  success: {
    fontSize: sizeFont(24),
    fontFamily: font.bold,
    marginTop: sizeHeight(27),
    marginBottom: sizeHeight(18)
  },
  desc: {
    fontSize: sizeFont(16),
    textAlign: "center"
  },
  back: {
    marginBottom: sizeWidth(8)
  },
  indicator: {
    flex: 1
  },
  info: {
    flex: 1,
    paddingHorizontal: sizeWidth(12),
    justifyContent: "center",
    alignItems: "center"
  }
});
