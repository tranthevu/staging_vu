import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import Input from "../../components/common/input";
import NotificationView from "../../components/common/notification-view";

class AccountScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render(): ReactNode {
    const { profile } = this.props.profile;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Thông tin tài khoản</Text>}
        />
        <View style={styles.body}>
          <Input
            editable={false}
            style={styles.code}
            label="Mã thành viên"
            value={profile.username}
          />
          <Input editable={false} label="Điện thoại" value={profile.phone} />
          <NotificationView />
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    profile: state.profile
  }),
  { resetPage, navigateToPage }
)(AccountScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  body: {
    flex: 1
  },
  title: {
    fontFamily: font.bold,
    fontSize: sizeFont(18),
    color: "white"
  },
  code: {
    marginTop: moderateScale(16)
  }
});
