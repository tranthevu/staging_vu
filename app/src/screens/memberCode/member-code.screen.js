import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  FlatList,
  ScrollView,
  Image,
  TouchableOpacity
} from "react-native";
import _ from "lodash";
import { resetPage, navigateBack, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import {
  sizeWidth,
  sizeHeight,
  sizeFont,
  moderateScale
} from "../../helpers/size.helper";
import Toolbar from "../../components/common/toolbar";
import Text from "../../components/common/text";
import {
  appColor,
  font,
  event,
  environmentMap,
  text
} from "../../constants/app.constant";
import Barcode from "../../components/common/barcode";
import QRCode from "react-native-qrcode";
import { appConfig } from "../../config/app.config";
import { checkApiStatus, getLogs } from "../../helpers/app.helper";
import NotificationView from "../../components/common/notification-view";
import EventRegister from "../../helpers/event-register.helper";
import TouchableIcon from "../../components/common/touchable-icon";

class MemberCodeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = async () => {
    this.jumpPageEvent = EventRegister.on(event.jumpPage, this.onTabChange);
  };

  onTabChange = async tabIndex => {
    if (tabIndex == 2) {
      const isMaintenance = await checkApiStatus();
      if (isMaintenance) {
        this.props.resetPage("Maintenance");
      } else {
        this.notificationView.register();
      }
    }
  };

  componentWillUnmount = () => {
    EventRegister.off(this.jumpPageEvent);
  };

  render(): ReactNode {
    const { profile } = this.props.profile;
    const code = _.get(profile, "username");
    if (appConfig.environment === environmentMap.development) {
      const logs = getLogs();
      return (
        <View>
          <FlatList
            data={logs}
            renderItem={({ item, index }) => {
              return <Text key={index}>{item}</Text>;
            }}
          />
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <Toolbar center={<Text style={styles.header}>Mã thành viên</Text>} />
        <ScrollView bounces={false} contentContainerStyle={styles.body}>
          <View style={styles.content}>
            <QRCode value={code} size={sizeWidth(160)} bgColor="black" fgColor="white" />
            <Text style={styles.desc}>
              Cung cấp mã thành viên cho nhân viên cửa hàng để tích điểm
            </Text>
          </View>
          <Image style={styles.line} source={require("../../../res/icon/line.png")} />
          {!!code && (
            <View style={styles.bottom}>
              <Barcode
                width={code.length > 20 ? 0.8 : code.length > 15 ? 1.4 : 2}
                height={sizeWidth(40)}
                value={code}
                format="CODE128"
              />
              <Text>{code}</Text>
            </View>
          )}
          <View style={[styles.circle, styles.left, styles.top]} />
          <View style={[styles.circle, styles.right, styles.top]} />
          <NotificationView ref={ref => (this.notificationView = ref)} />
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({
    profile: state.profile
  }),
  {
    resetPage,
    navigateBack,
    navigateToPage
  }
)(MemberCodeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F2F2F2"
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  bottom: {
    marginBottom: sizeWidth(25),
    marginTop: sizeWidth(18),
    alignItems: "center"
  },
  body: {
    marginVertical: sizeWidth(14),
    marginHorizontal: sizeWidth(9),
    alignItems: "center",
    borderRadius: sizeWidth(6),
    backgroundColor: "white"
  },
  desc: {
    fontSize: sizeFont(13),
    color: "black",
    paddingHorizontal: sizeWidth(32),
    textAlign: "center",
    marginTop: sizeWidth(35)
  },
  icon: {
    width: moderateScale(20),
    height: moderateScale(20)
  },
  line: {
    width: "94%",
    alignSelf: "center",
    height: 1
  },
  circle: {
    backgroundColor: "#F2F2F2",
    position: "absolute",
    width: sizeWidth(18),
    height: sizeWidth(18),
    borderRadius: sizeWidth(9)
  },
  right: {
    right: sizeWidth(-9)
  },
  left: {
    left: sizeWidth(-9)
  },
  top: {
    top: sizeWidth(288)
  },
  content: {
    height: sizeWidth(297),
    justifyContent: "center",
    alignItems: "center"
  }
});
