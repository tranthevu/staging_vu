import React, { Component, ReactNode } from "react";
import { View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import NotificationView from "../../components/common/notification-view";
import HistoryItem from "./history-item";
import PaginationList from "../../components/common/pagination-list";
import {
  refreshRedemptionHistory,
  fetchMoreRedemptionHistory,
  requestRedemptionHistory
} from "../../actions/redemption-history.action";
import EmptyView from "../../components/common/empty-view";
import { checkApiStatus } from "../../helpers/app.helper";

class RedemptionHistoryScreen extends Component {
  renderList = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      redemptionHistory
    } = this.props.redemptionHistory;
    const { fetchMoreRedemptionHistory, refreshRedemptionHistory } = this.props;
    return (
      <PaginationList
        loading={loading}
        contentContainerStyle={styles.content}
        renderItem={this.renderHistoryItem}
        keyExtractor={(item, index) => index.toString()}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        firstLoading={firstLoading}
        onEndReached={() => fetchMoreRedemptionHistory()}
        onRefresh={() => refreshRedemptionHistory()}
        data={redemptionHistory}
        EmptyComponent={<EmptyView label="Bạn chưa có lịch sử đổi thưởng nào" />}
      />
    );
  };

  render(): ReactNode {
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Lịch sử đổi thưởng</Text>}
        />
        <View style={styles.body}>
          {this.renderList()}
          <NotificationView />
        </View>
      </View>
    );
  }

  renderHistoryItem = ({ item }) => {
    return <HistoryItem item={item} />;
  };

  componentDidMount = () => {
    this.props.requestRedemptionHistory();
  };
}

export default connect(state => ({ redemptionHistory: state.redemptionHistory }), {
  resetPage,
  navigateToPage,
  navigateBack,
  requestRedemptionHistory,
  refreshRedemptionHistory,
  fetchMoreRedemptionHistory
})(RedemptionHistoryScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  content: {
    paddingVertical: moderateScale(5)
  }
});
