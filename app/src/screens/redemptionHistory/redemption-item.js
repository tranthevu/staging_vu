import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import numeral from "numeral";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { font, appColor } from "../../constants/app.constant";
import SeparatorLine from "../../components/common/separator-line";
import CacheImage from "../../components/common/cache-image";

export default class RedemptionItem extends Component {
  render() {
    const { item } = this.props;
    const price = numeral(parseFloat(item.price)).format("0,0");
    return (
      <View style={styles.container}>
        <View style={styles.top}>
          <View style={styles.body}>
            <Text numberOfLines={2} style={styles.content}>
              {item.campaign_name}
            </Text>
            <Text style={styles.x}>x{item.total_count}</Text>
          </View>
        </View>
        <View style={styles.bottom}>
          <CacheImage uri={item.partner_logo_url} style={styles.avatar} />
          <Text numberOfLines={1} style={styles.merchant}>
            {item.partner_name}
          </Text>
          <Text style={styles.amount}>
            +{price}
            <Text style={styles.unit}>đ</Text>
          </Text>
        </View>
        <SeparatorLine top={moderateScale(45)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    marginVertical: moderateScale(5),
    marginHorizontal: sizeWidth(9),
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    }
  },
  bottom: {
    flexDirection: "row",
    paddingHorizontal: sizeWidth(12),
    paddingVertical: sizeWidth(8),
    alignItems: "center"
  },
  avatar: {
    backgroundColor: "#C4C4C4",
    width: moderateScale(16),
    height: moderateScale(16),
    borderRadius: moderateScale(8),
    marginRight: sizeWidth(4)
  },
  merchant: {
    flex: 1,
    fontSize: sizeFont(12)
  },
  saved: {
    fontSize: sizeFont(12),
    marginHorizontal: sizeWidth(12)
  },
  amount: {
    marginLeft: sizeWidth(12),
    color: appColor.primary,
    fontFamily: font.bold,
    fontSize: sizeFont(14)
  },
  unit: {
    color: appColor.primary,
    fontFamily: font.bold,
    fontSize: sizeFont(14),
    textDecorationLine: "underline"
  },
  top: {
    paddingVertical: moderateScale(10),
    paddingHorizontal: sizeWidth(12),
    flexDirection: "row",
    height: moderateScale(54)
  },
  body: {
    flex: 1,
    flexDirection: "row"
  },
  content: {
    flex: 1,
    marginRight: sizeWidth(6),
    fontSize: sizeFont(12),
    fontFamily: font.medium
  },
  x: {
    fontSize: sizeFont(12),
    alignSelf: "flex-end"
  }
});
