import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import numeral from "numeral";
import moment from "moment";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, moderateScale, sizeWidth } from "../../helpers/size.helper";
import { font, appColor, dateTimeFormat } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import NotificationView from "../../components/common/notification-view";
import RedemptionItem from "./redemption-item";
import Api from "../../api/api";
import LoadingIndicator from "../../components/common/loading-indicator";
import { checkApiStatus } from "../../helpers/app.helper";

class RedemptionDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }

  componentDidMount = async () => {
    await this.loadRedemptionDetail();
  };

  loadRedemptionDetail = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { payCode } = this.props.navigation.state.params;
    const response = await Api.redemptionDetail(payCode);
    this.setState({
      detail: response.data.data,
      loading: false
    });
  };

  render(): ReactNode {
    const { detail, loading } = this.state;
    if (loading) return <LoadingIndicator />;
    const totalPrice = numeral(parseFloat(detail.total_price)).format("0,0");
    const createDate = moment(detail.create_date).format(dateTimeFormat.ddmmyyyyhhmm);
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Chi tiết</Text>}
        />
        <View style={styles.body}>
          <View style={styles.header}>
            <Text style={styles.code}>{detail.pay_code}</Text>
            <Text style={styles.amount}>
              +{totalPrice}
              <Text style={styles.unit}>đ</Text>
            </Text>
            <Text style={styles.date}>{createDate}</Text>
          </View>
          <Text style={styles.label}>Chi tiết trả thưởng</Text>
          <ScrollView>
            {detail.details &&
              detail.details.map((item, index) => (
                <RedemptionItem key={index.toString()} item={item} />
              ))}
          </ScrollView>
          <NotificationView />
        </View>
      </View>
    );
  }
}

export default connect(null, { resetPage, navigateToPage, navigateBack })(
  RedemptionDetailScreen
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  header: {
    alignItems: "center",
    backgroundColor: "white",
    padding: moderateScale(12)
  },
  code: {
    fontSize: sizeFont(13),
    fontFamily: font.medium
  },
  amount: {
    marginVertical: moderateScale(7),
    fontSize: sizeFont(18),
    fontFamily: font.medium,
    color: appColor.primary
  },
  date: {
    fontSize: sizeFont(12)
  },
  unit: {
    fontSize: sizeFont(18),
    fontFamily: font.medium,
    color: appColor.primary,
    textDecorationLine: "underline"
  },
  label: {
    fontFamily: font.medium,
    fontSize: sizeFont(16),
    marginHorizontal: sizeWidth(14),
    marginTop: moderateScale(10),
    marginBottom: moderateScale(5)
  }
});
