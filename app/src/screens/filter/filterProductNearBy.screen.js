import React, { Component } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import Button from "../../components/common/button";
import Dropdown from "../../components/common/dropdown";
import NotificationView from "../../components/common/notification-view";
import BackIcon from "../../components/common/back-icon";
import { showError } from "../../helpers/error.helper";

const dataRadius = [
  {
    type: 0.25,
    name: "250m"
  },
  {
    type: 0.5,
    name: "500m"
  },
  {
    type: 1,
    name: "1km"
  },
  {
    type: 1.5,
    name: "1.5km"
  },
  {
    type: 2,
    name: "2km"
  }
];

class FilterProductNearByScreen extends Component {
  constructor(props) {
    super(props);
    const appliedFilters = lodash.get(
      this.props.navigation.state,
      "params.appliedFilters",
      {}
    );
    this.state = {
      selectedCategory: appliedFilters.selectedCategory,
      selectedRadius: appliedFilters.selectedRadius
    };
  }

  // componentDidMount = async () => {
  //   const category = lodash.get(this.props.navigation.state, "params.category");
  //   const data = await Api.getFilters(category);
  //   this.setState({
  //     categoryMerchants: data.data.merchants,
  //     categoryMerchantLoaded: true
  //   });
  // };

  onValueChangeCategories = value => {
    const { selectedCategory } = this.state;
    if (lodash.get(selectedCategory, "slug") !== value.slug) {
      this.setState({
        selectedCategory: value
      });
    }
  };

  onValueChangeRadius = value => this.setState({ selectedRadius: value });

  render() {
    const { filters } = this.props.filters;
    const { selectedCategory, selectedRadius } = this.state;

    let categories = lodash.get(this.props.navigation.state, "params.categories");
    if (categories) categories = { self: categories };
    if (!categories) categories = filters.categories;
    const categoryOptions = [
      {
        name: "Tất cả",
        slug: ""
      },
      ...lodash.get(categories, "self")
    ];
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
          center={<Text style={styles.title}>Bộ lọc</Text>}
        />
        <View style={styles.body}>
          <ScrollView bounces={false} contentContainerStyle={styles.list}>
            <Dropdown
              options={dataRadius}
              path="name"
              label="Bán kính"
              onValueChange={this.onValueChangeRadius}
              value={lodash.get(selectedRadius, "name", "Chọn phương bán kính")}
            />
            <Dropdown
              options={categoryOptions}
              label="Danh mục"
              path="name"
              onValueChange={this.onValueChangeCategories}
              value={lodash.get(selectedCategory, "name", "Tất cả")}
            />
          </ScrollView>
          <Button style={styles.apply} onPress={this.applyFilter} text="ÁP DỤNG" />
          <NotificationView />
        </View>
      </View>
    );
  }

  applyFilter = () => {
    const onApplyFilters = lodash.get(
      this.props.navigation.state,
      "params.onApplyFilters"
    );
    const { selectedCategory, selectedRadius } = this.state;

    if (!selectedRadius) {
      showError("Vui lòng chọn bán kính.");
    } else {
      if (onApplyFilters)
        onApplyFilters({
          selectedCategory,
          selectedRadius
        });
      this.props.navigateBack();
    }
  };
}

export default connect(
  state => ({
    filters: state.filters,
    topCategories: state.topCategories
  }),
  { resetPage, navigateToPage, navigateBack }
)(FilterProductNearByScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  cancel: {
    fontSize: sizeFont(13),
    color: "white"
  },
  apply: {
    position: "absolute",
    bottom: sizeWidth(12)
  },
  list: {
    paddingBottom: sizeWidth(40)
  }
});
