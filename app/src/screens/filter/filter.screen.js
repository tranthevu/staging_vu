import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import lodash from "lodash";
import { connect } from "react-redux";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import Button from "../../components/common/button";
import Dropdown from "../../components/common/dropdown";
import Multichoice from "../../components/common/multichoice";
import Api from "../../api/api";
import NotificationView from "../../components/common/notification-view";
import { checkApiStatus } from "../../helpers/app.helper";

class FilterScreen extends Component {
  constructor(props) {
    super(props);
    const appliedFilters = lodash.get(
      this.props.navigation.state,
      "params.appliedFilters",
      {}
    );
    this.state = {
      selectedCategory: appliedFilters.selectedCategory,
      selectedMerchants: appliedFilters.selectedMerchants,
      selectedOrder: appliedFilters.selectedOrder,
      selectedArea: appliedFilters.selectedArea,
      categoryMerchants: [],
      categoryMerchantLoaded: false,
      isExclusive: false
    };
  }

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const category = lodash.get(this.props.navigation.state, "params.category");
    const isExclusive = lodash.get(this.props.navigation.state, "params.isExclusive");
    const data = await Api.getFilters(category, isExclusive);
    this.setState({
      categoryMerchants: data.data.merchants,
      categoryMerchantLoaded: true,
      isExclusive
    });
  };

  loadCategoryMerchants = async category => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { isExclusive } = this.state;
    this.setState({ categoryMerchantLoaded: false, categoryMerchants: [] });
    const data = await Api.getFilters(category, isExclusive);
    this.setState({
      categoryMerchants: data.data.merchants,
      categoryMerchantLoaded: true
    });
  };

  render(): ReactNode {
    const { filters } = this.props.filters;
    const {
      selectedCategory,
      selectedMerchants,
      selectedArea,
      categoryMerchants,
      categoryMerchantLoaded
    } = this.state;

    let categories = lodash.get(this.props.navigation.state, "params.categories");
    if (categories) categories = { self: categories };
    if (!categories) categories = filters.categories;

    const { merchants } = filters;
    const category = lodash.get(this.props.navigation.state, "params.category");

    let merchantLoading = false;
    if (category) {
      merchantLoading = !categoryMerchantLoaded;
    }

    const childAreas = lodash.get(this.props.navigation.state, "params.childAreas");
    const isSelected =
      (!lodash.isEmpty(selectedCategory) &&
        lodash.get(selectedCategory, "name") !== "Tất cả") ||
      !lodash.isEmpty(selectedArea) ||
      !lodash.isEmpty(selectedMerchants);
    return (
      <View style={styles.container}>
        <Toolbar
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
          center={<Text style={styles.title}>Bộ lọc</Text>}
        />
        <View style={styles.body}>
          <ScrollView bounces={false} contentContainerStyle={styles.list}>
            {!childAreas && (
              <Dropdown
                options={lodash.get(categories, "self")}
                label="Chuyên mục"
                path="name"
                onValueChange={value => {
                  if (lodash.get(selectedCategory, "slug") !== value.slug) {
                    this.setState({
                      selectedCategory: value,
                      selectedMerchants: []
                    });
                  }
                  this.loadCategoryMerchants(value.slug);
                }}
                value={lodash.get(selectedCategory, "name", "Chọn chuyên mục")}
                onPress={this.onNavigateCategorySelection}
              />
            )}
            {childAreas && (
              <Dropdown
                options={childAreas}
                label="Khu vực"
                path="name"
                onValueChange={value => this.setState({ selectedArea: value })}
                value={lodash.get(selectedArea, "name", "Chọn khu vực")}
                onPress={this.onNavigateCategorySelection}
              />
            )}
            <Multichoice
              merchantLoading={merchantLoading}
              options={!category ? merchants : categoryMerchants}
              path="name"
              onValueChange={value => this.setState({ selectedMerchants: value })}
              placeholder="Chọn nhãn hàng"
              value={selectedMerchants || []}
              label="Nhãn hàng"
              onPress={this.onNavigateMerchantSelection}
            />
          </ScrollView>
          <View style={styles.actions}>
            {isSelected && (
              <Button
                style={[styles.halfSize, styles.btnReset]}
                textStyle={styles.btnResetText}
                onPress={this.onResetFilter}
                text="ĐẶT LẠI"
              />
            )}
            <Button
              style={isSelected ? styles.halfSize : {}}
              onPress={this.applyFilter}
              text="ÁP DỤNG"
            />
          </View>
          <NotificationView />
        </View>
      </View>
    );
  }

  applyFilter = () => {
    const onApplyFilters = lodash.get(
      this.props.navigation.state,
      "params.onApplyFilters"
    );
    const {
      selectedCategory,
      selectedArea,
      selectedMerchants,
      selectedOrder
    } = this.state;
    if (onApplyFilters)
      onApplyFilters({
        selectedCategory,
        selectedArea,
        selectedMerchants,
        selectedOrder
      });
    this.props.navigateBack();
  };

  onResetFilter = () => {
    const { filters } = this.props;
    let categories = lodash.get(this.props.navigation.state, "params.categories");
    if (categories) categories = { self: categories };
    if (!categories) categories = filters.categories;
    this.setState({
      selectedCategory: categories.self.find(item => item.name === "Tất cả"),
      selectedArea: undefined,
      selectedMerchants: []
    });
  };

  onNavigateCategorySelection = () => {
    const { navigateToPage } = this.props;
    const { filters } = this.props.filters;
    let categories = lodash.get(this.props.navigation.state, "params.categories");
    if (categories) categories = { self: categories };
    if (!categories) categories = filters.categories;
    navigateToPage("SelectionList", {
      title: "Chuyên mục",
      isMultipleChoice: false,
      items: lodash.get(categories, "self")
        ? categories.self.map(item => {
            return {
              ...item,
              text: item.name,
              value: item.slug,
              checked: lodash.get(this.state.selectedCategory, "slug") === item.slug
            };
          })
        : [],
      onUpdateSelection: selectedItem => {
        const { selectedCategory } = this.state;
        if (lodash.get(selectedCategory, "slug") !== selectedItem.slug) {
          this.setState({
            selectedCategory: selectedItem,
            selectedMerchants: []
          });
        }
        this.loadCategoryMerchants(selectedItem.slug);
      }
    });
  };

  onNavigateAreaSelection = () => {
    const { navigateToPage } = this.props;
    const childAreas = lodash.get(this.props.navigation.state, "params.childAreas");
    navigateToPage("SelectionList", {
      title: "Khu vực",
      isMultipleChoice: false,
      items: !lodash.isEmpty(childAreas)
        ? childAreas.map(item => {
            return {
              ...item,
              text: item.name,
              value: item.key
            };
          })
        : [],
      onUpdateSelection: selectedArea => {
        this.setState({ selectedArea });
      }
    });
  };

  onNavigateMerchantSelection = () => {
    const { categoryMerchants, selectedMerchants } = this.state;
    const { navigateToPage } = this.props;
    const { filters } = this.props.filters;
    const { merchants } = filters;
    const category = lodash.get(this.props.navigation.state, "params.category");
    const items = !category ? merchants : categoryMerchants;
    navigateToPage("SelectionList", {
      title: "Nhãn hàng",
      isMultipleChoice: true,
      items: items
        ? items.map(item => {
            return {
              ...item,
              text: item.name,
              value: item.code,
              checked: selectedMerchants
                ? selectedMerchants.findIndex(mer => mer.code === item.code) > -1
                : false
            };
          })
        : [],
      onUpdateSelection: selectedMerchants => {
        this.setState({ selectedMerchants });
      }
    });
  };
}

export default connect(
  state => ({
    filters: state.filters
  }),
  { resetPage, navigateToPage, navigateBack }
)(FilterScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  cancel: {
    fontSize: sizeFont(13),
    color: "white"
  },
  // apply: {
  //   position: "absolute",
  //   bottom: sizeWidth(12)
  // },
  list: {
    paddingBottom: sizeWidth(40)
  },
  actions: {
    position: "absolute",
    bottom: sizeWidth(12),
    width: "100%",
    flexDirection: "row",
    justifyContent: "center"
  },
  btnReset: {
    backgroundColor: "#FFF",
    borderWidth: 1,
    borderColor: "#DDD"
  },
  btnResetText: {
    color: "#444"
  },
  halfSize: {
    width: sizeWidth(146),
    paddingHorizontal: sizeWidth(8),
    marginHorizontal: sizeWidth(6)
  }
});
