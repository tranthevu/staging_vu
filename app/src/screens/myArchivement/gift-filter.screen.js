import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import lodash from "lodash";
import { connect } from "react-redux";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, GIFT_STATUS } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import Button from "../../components/common/button";
// eslint-disable-next-line
import Dropdown from "../../components/common/dropdown";

class GiftFilterScreen extends Component {
  constructor(props) {
    super(props);
    const appliedGiftFilters = lodash.get(
      this.props.navigation.state,
      "params.appliedGiftFilters",
      {}
    );
    const status = GIFT_STATUS.find(
      e => e.status === appliedGiftFilters.selectedStatus.status
    );
    this.state = {
      selectedStatus: status
    };
  }

  render(): ReactNode {
    const { selectedStatus } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
          center={<Text style={styles.title}>Bộ lọc</Text>}
        />
        <ScrollView bounces={false} contentContainerStyle={styles.list}>
          <Dropdown
            options={GIFT_STATUS}
            path="name"
            onValueChange={value => this.onStatusChange(value)}
            value={lodash.get(selectedStatus, "name", "Chọn tuỳ chọn")}
            label="Lọc theo"
          />
        </ScrollView>
        <Button style={styles.apply} onPress={this.applyFilter} text="ÁP DỤNG" />
      </View>
    );
  }

  onStatusChange = value => {
    this.setState({
      selectedStatus: value
    });
  };

  applyFilter = () => {
    const onApplyFilters = lodash.get(
      this.props.navigation.state,
      "params.onApplyFilters"
    );
    const { selectedStatus } = this.state;
    if (onApplyFilters)
      onApplyFilters({
        selectedStatus
      });
    this.props.navigateBack();
  };
}

export default connect(
  null,
  { resetPage, navigateToPage, navigateBack }
)(GiftFilterScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  cancel: {
    fontSize: sizeFont(13),
    color: "white"
  },
  apply: {
    position: "absolute",
    bottom: sizeWidth(12)
  },
  list: {
    paddingBottom: sizeWidth(40)
  }
});
