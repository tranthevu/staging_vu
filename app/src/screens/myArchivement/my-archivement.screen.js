import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, TouchableOpacity, ScrollView } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import numeral from "numeral";
import { resetPage, navigateBack, navigateToPage } from "../../actions/nav.action";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { appColor, font, event, text, productClass } from "../../constants/app.constant";
import {
  fetchMoreSavedOffers,
  refreshSavedOffers,
  requestSavedOffers
} from "../../actions/saved-offers.action";
import {
  fetchMoreUnusedVouchers,
  refreshUnusedVouchers,
  requestUnusedVouchers
} from "../../actions/unused-vouchers.action";
import {
  fetchMoreUsedVouchers,
  refreshUsedVouchers,
  requestUsedVouchers
} from "../../actions/used-vouchers.action";
import PaginationList from "../../components/common/pagination-list";
import EventRegister from "../../helpers/event-register.helper";
import TouchableIcon from "../../components/common/touchable-icon";
import ChatbotIcon from "../../components/common/chatbot-icon";
import NotificationView from "../../components/common/notification-view";
import {
  requestSavedGifts,
  refreshSavedGifts,
  fetchMoreSavedGifts
} from "../../actions/saved-gifts.action";
import PromotionItem from "../dashboard/promotion-item";
import PromotionItemList from "../dashboard/promotion-item-list";
import GiftItem from "./gift-item";
import EmptyView from "../../components/common/empty-view";
import { requestedProfile } from "../../actions/profile.action";
import Api from "../../api/api";
import { checkApiStatus } from "../../helpers/app.helper";

export const tabs = {
  all: 0,
  gift: 1
};

const viewType = {
  grid: 0,
  list: 1
};

class MyArchivementScreen extends Component {
  constructor(props) {
    super(props);
    const selectedTab = lodash.get(this.props.navigation.state, "params.selectedTab");
    const giftStatus = lodash.get(this.props.navigation.state, "params.status", 0) || 0;
    this.state = {
      selectedTab: selectedTab || tabs.all,
      appliedFilters: {},
      appliedGiftFilters: {
        selectedStatus: {
          status: giftStatus
        }
      },
      selectedViewType: viewType.list
    };
  }

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  componentWillReceiveProps = nextProps => {
    const selectedTab = lodash.get(nextProps.navigation.state, "params.selectedTab");
    const giftStatus = lodash.get(nextProps.navigation.state, "params.status", 0) || 0;
    if (
      nextProps.navigation.state !== this.props.navigation.state &&
      selectedTab !== undefined
    ) {
      this.setState({
        selectedTab,
        appliedFilters: {},
        appliedGiftFilters: {
          selectedStatus: {
            status: giftStatus
          }
        },
        selectedViewType: viewType.grid
      });
      if (selectedTab === tabs.gift) {
        this.props.requestSavedGifts(giftStatus);
      } else {
        this.props.requestSavedOffers();
      }
    }
  };

  renderTabs = () => {
    const { selectedTab } = this.state;
    return (
      <View style={styles.tabs}>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.all })}
          style={styles.tab}
        >
          <Text style={selectedTab === tabs.all ? styles.activeText : styles.text}>
            Ưu đãi
          </Text>
          {selectedTab === tabs.all && <View style={styles.line} />}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.gift })}
          style={styles.tab}
        >
          <Text style={selectedTab === tabs.gift ? styles.activeText : styles.text}>
            Phần thưởng
          </Text>
          {/* <View style={styles.circle}>
            <Text style={styles.badge}>1</Text>
          </View> */}
          {selectedTab === tabs.gift && <View style={styles.line} />}
        </TouchableOpacity>
      </View>
    );
  };

  renderHeader = () => {
    const { selectedTab } = this.state;
    const { profile } = this.props.profile;
    const amount =
      selectedTab === tabs.all
        ? numeral(parseFloat(profile.saving_money)).format("0,0")
        : numeral(parseFloat(profile.received_money)).format("0,0");
    return (
      <View style={styles.header}>
        <Text style={styles.saved}>
          {selectedTab === tabs.all ? "Bạn đã tiết kiệm" : "Bạn đã kiếm được"}
        </Text>
        <Text style={styles.amount}>
          {amount}
          <Text style={styles.under}>đ</Text>
        </Text>
        <TouchableOpacity
          onPress={
            selectedTab === tabs.all
              ? this.navigateToSavingHistory
              : this.navigateToRedemptionHistory
          }
          style={styles.wrap}
        >
          <Text style={styles.history}>
            {selectedTab === tabs.all ? "Lịch sử tiết kiệm" : "Lịch sử đổi thưởng"}
          </Text>
          <Image
            style={styles.arrow}
            source={require("../../../res/icon/icon-my-arrow-right.png")}
          />
        </TouchableOpacity>
      </View>
    );
  };

  navigateToSavingHistory = () => {
    this.props.navigateToPage("SavingHistory");
  };

  navigateToRedemptionHistory = () => {
    this.props.navigateToPage("RedemptionHistory");
  };

  render(): ReactNode {
    const { selectedTab } = this.state;
    return (
      <View style={styles.container}>
        {this.renderHeader()}
        {this.renderTabs()}
        <View style={styles.body}>
          {selectedTab === tabs.all ? this.renderSavedOffers() : this.renderGifts()}
          <NotificationView ref={ref => (this.notificationView = ref)} />
        </View>
        <ChatbotIcon />
      </View>
    );
  }

  renderGifts = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      savedGifts
    } = this.props.savedGifts;
    const { refreshSavedGifts, fetchMoreSavedGifts } = this.props;
    const { appliedGiftFilters, selectedViewType } = this.state;
    const status = lodash.get(appliedGiftFilters, "selectedStatus.status");
    return (
      <View style={styles.content}>
        <View style={[styles.bar, styles.barTop]}>
          <Text style={styles.label}>Phần thưởng</Text>
          {/* <TouchableIcon
            style={styles.view}
            iconStyle={styles.viewIcon}
            onPress={this.toggleViewType}
            source={
              selectedViewType === viewType.list
                ? require("../../../res/icon/list.png")
                : require("../../../res/icon/grid.png")
            }
          /> */}
          {/* <TouchableIcon
            style={styles.view}
            onPress={this.navigateToGiftFilter}
            iconStyle={styles.viewIcon}
            source={require("../../../res/icon/sort.png")}
          /> */}
        </View>
        <PaginationList
          ref={ref => (this.list = ref)}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderGift}
          loading={loading}
          EmptyComponent={<EmptyView label="Bạn chưa có phần thưởng nào" />}
          reachedEnd={reachedEnd}
          refreshing={refreshing}
          firstLoading={firstLoading}
          onEndReached={() => fetchMoreSavedGifts(status)}
          onRefresh={() => refreshSavedGifts(status)}
          data={savedGifts}
        />
      </View>
    );
  };

  navigateToMySavedFilter = () => {
    const { appliedFilters } = this.state;
    this.props.navigateToPage("MySavedFilter", {
      onApplyFilters: this.onApplyFilters,
      appliedFilters
    });
  };

  navigateToGiftFilter = () => {
    const { appliedGiftFilters } = this.state;
    this.props.navigateToPage("GiftFilter", {
      onApplyFilters: this.onApplyGiftFilters,
      appliedGiftFilters
    });
  };

  onApplyFilters = appliedFilters => {
    this.setState({ appliedFilters });
    const status = lodash.get(appliedFilters, "selectedStatus.type", text.emptyString);
    const productType = lodash.get(
      appliedFilters,
      "selectedProductType.type",
      text.emptyString
    );
    this.props.requestSavedOffers(status, productType);
  };

  onApplyGiftFilters = appliedGiftFilters => {
    this.setState({ appliedGiftFilters });
    const status = lodash.get(appliedGiftFilters, "selectedStatus.status");
    this.props.requestSavedGifts(status);
  };

  resetFilter = () => {
    this.setState({
      appliedFilters: {}
    });
    this.props.requestSavedOffers(text.emptyString, text.emptyString);
  };

  resetStatus = () => {
    const { appliedFilters } = this.state;
    const productType = lodash.get(
      appliedFilters,
      "selectedProductType.type",
      text.emptyString
    );
    this.setState({
      appliedFilters: {
        ...appliedFilters,
        selectedStatus: {}
      }
    });
    this.props.requestSavedOffers(text.emptyString, productType);
  };

  renderEmpty = () => {
    const { appliedFilters } = this.state;
    const status = lodash.get(appliedFilters, "selectedStatus.type");
    const productType = lodash.get(appliedFilters, "selectedProductType.type");

    return status || productType ? (
      <View style={styles.empty}>
        <Text style={styles.textEmpty}>
          Không có ưu đãi nào phù hợp với tiêu chí bộ lọc
        </Text>
        <Text onPress={this.resetFilter} style={styles.reset}>
          Đặt lại mặc định
        </Text>
      </View>
    ) : (
      <View style={styles.empty}>
        <Text style={styles.noResults}>Bạn chưa có ưu đãi nào</Text>
      </View>
    );
  };

  renderSavedOffers = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      savedOffers
    } = this.props.savedOffers;
    const { refreshSavedOffers, fetchMoreSavedOffers } = this.props;
    const { appliedFilters, selectedViewType } = this.state;
    const status = lodash.get(appliedFilters, "selectedStatus.type");
    const productType = lodash.get(appliedFilters, "selectedProductType.type");
    const selectedFilter = lodash.get(appliedFilters, "selectedProductType.name");
    const selectedStatus = lodash.get(appliedFilters, "selectedStatus.name");
    return (
      <View style={styles.content}>
        <View style={styles.bar}>
          <Text style={styles.label}>Ưu đãi</Text>
          <TouchableIcon
            style={styles.view}
            iconStyle={styles.viewIcon}
            onPress={this.toggleViewType}
            source={
              selectedViewType === viewType.list
                ? require("../../../res/icon/list.png")
                : require("../../../res/icon/grid.png")
            }
          />
          <TouchableIcon
            style={styles.view}
            onPress={this.navigateToMySavedFilter}
            iconStyle={styles.viewIcon}
            source={require("../../../res/icon/sort.png")}
          />
        </View>
        {productType && selectedFilter && (
          <View style={styles.selectedFilters}>
            <ScrollView
              horizontal={true}
              contentContainerStyle={styles.selectedFiltersScroll}
              bounces={false}
              showsHorizontalScrollIndicator={false}
            >
              {this.renderSelectedFilter(selectedFilter, false)}
              {selectedStatus && this.renderSelectedFilter(selectedStatus, true)}
            </ScrollView>
          </View>
        )}
        <PaginationList
          ref={ref => (this.list = ref)}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderPromotion}
          loading={loading}
          EmptyComponent={this.renderEmpty()}
          reachedEnd={reachedEnd}
          refreshing={refreshing}
          firstLoading={firstLoading}
          onEndReached={() => fetchMoreSavedOffers(status, productType)}
          onRefresh={() => refreshSavedOffers(status, productType)}
          data={savedOffers}
        />
      </View>
    );
  };

  renderSelectedFilter = (selectedText, isStatus) => {
    return (
      <View style={styles.selectedFilter}>
        <Text style={styles.selectedFilterText}>{selectedText}</Text>
        <TouchableOpacity
          style={styles.btnCloseFilter}
          onPress={isStatus ? this.resetStatus : this.resetFilter}
        >
          <Image
            style={styles.filterCloseIco}
            source={require("../../../res/icon/x-tag.png")}
          />
        </TouchableOpacity>
      </View>
    );
  };

  renderGift = ({ item, index }) => {
    return <GiftItem item={item} key={index} />;
  };

  renderPromotion = ({ item, index, isGift }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem
        isSavedProduct
        isGift={isGift}
        item={item}
        key={index.toString()}
        style={styles.promotion}
      />
    ) : (
      <PromotionItemList
        isSavedProduct
        isGift={isGift}
        item={item}
        key={index.toString()}
        style={styles.promotion}
      />
    );
  };

  componentDidMount = async () => {
    const status = lodash.get(this.props.navigation.state, "params.status");
    this.props.requestSavedOffers();
    this.props.requestSavedGifts(status);
    this.savedWishlistEvent = EventRegister.on(event.savedWishlist, this.refreshVouchers);
    this.removedWishlistEvent = EventRegister.on(
      event.removedWishlist,
      this.refreshVouchers
    );
    this.jumpPageEvent = EventRegister.on(event.jumpPage, this.onTabChange);
  };

  onCurrentTabChange = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    this.props.requestedProfile();
    this.notificationView.register();
    this.onApplyFilters(this.state.appliedFilters);
    const status = lodash.get(this.props.navigation.state, "params.status");
    this.props.requestSavedGifts(status);
  };

  onTabChange = tabIndex => {
    if (tabIndex === 1) {
      this.onCurrentTabChange();
    }
  };

  componentWillUnmount = () => {
    EventRegister.off(this.savedWishlistEvent);
    EventRegister.off(this.jumpPageEvent);
    EventRegister.off(this.removedWishlistEvent);
  };

  refreshVouchers = () => {
    const { appliedFilters } = this.state;
    const status = lodash.get(appliedFilters, "selectedStatus.type");
    this.props.refreshSavedOffers(status);
  };
}

export default connect(
  state => ({
    usedVouchers: state.usedVouchers,
    unusedVouchers: state.unusedVouchers,
    savedOffers: state.savedOffers,
    savedGifts: state.savedGifts,
    profile: state.profile
  }),
  {
    resetPage,
    navigateBack,
    navigateToPage,
    fetchMoreUsedVouchers,
    refreshUsedVouchers,
    fetchMoreUnusedVouchers,
    refreshUnusedVouchers,
    requestUnusedVouchers,
    requestUsedVouchers,
    requestSavedOffers,
    refreshSavedOffers,
    fetchMoreSavedOffers,
    requestedProfile,
    requestSavedGifts,
    refreshSavedGifts,
    fetchMoreSavedGifts
  }
)(MyArchivementScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F9F9F9"
  },
  body: {
    flex: 1
  },

  tabs: {
    flexDirection: "row",
    height: sizeWidth(44),
    backgroundColor: "white"
  },
  tab: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    color: appColor.blur,
    fontFamily: font.medium
  },
  activeText: {
    fontFamily: font.bold
  },
  line: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: sizeWidth(3),
    backgroundColor: appColor.primary
  },
  filter: {
    tintColor: "white"
  },
  empty: {
    alignItems: "center",
    padding: sizeWidth(12)
  },
  noResults: {
    color: "#979797",
    fontSize: sizeFont(13),
    fontStyle: "italic"
  },
  textEmpty: {
    fontSize: sizeFont(13)
  },
  reset: {
    color: "#55ACEE",
    fontSize: sizeFont(13),
    marginTop: sizeWidth(12)
  },
  circle: {
    backgroundColor: appColor.primary,
    minWidth: moderateScale(14),
    height: moderateScale(14),
    borderRadius: moderateScale(7),
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    borderWidth: 1,
    borderColor: "white",
    top: moderateScale(8),
    right: moderateScale(31)
  },
  badge: {
    color: "white",
    fontSize: sizeFont(9)
  },
  label: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: appColor.blur
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(9)
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: sizeWidth(5),
    paddingHorizontal: sizeWidth(10)
  },
  barTop: {
    marginVertical: sizeWidth(12)
  },
  selectedFilters: {
    flexDirection: "row",
    marginBottom: sizeWidth(5)
  },
  selectedFiltersScroll: {
    paddingHorizontal: sizeWidth(10)
  },
  selectedFilter: {
    paddingLeft: sizeWidth(13),
    paddingRight: sizeWidth(9),
    height: sizeWidth(22),
    marginRight: sizeWidth(5),
    backgroundColor: "#FFF",
    borderWidth: 1,
    borderColor: "#DDD",
    borderRadius: sizeWidth(15),
    flexDirection: "row",
    alignItems: "center"
  },
  selectedFilterText: {
    fontSize: sizeFont(13)
  },
  btnCloseFilter: {
    width: moderateScale(15),
    height: moderateScale(15),
    marginLeft: moderateScale(7),
    justifyContent: "center",
    alignItems: "center",
    marginBottom: sizeWidth(1)
  },
  filterCloseIco: {
    width: moderateScale(8),
    height: moderateScale(8)
  },
  content: {
    flex: 1
  },
  promotion: {
    marginVertical: moderateScale(5)
  },
  header: {
    backgroundColor: appColor.primary,
    alignItems: "center",
    padding: moderateScale(10)
  },
  saved: {
    fontSize: sizeFont(13),
    fontFamily: font.medium,
    color: "white"
  },
  amount: {
    fontSize: sizeFont(21),
    fontFamily: font.bold,
    color: "white",
    marginVertical: moderateScale(8)
  },
  under: {
    textDecorationLine: "underline",
    color: "white",
    fontSize: sizeFont(21),
    fontFamily: font.bold
  },
  wrap: {
    flexDirection: "row",
    alignItems: "center"
  },
  history: {
    fontSize: sizeFont(12),
    color: "white"
  },
  arrow: {
    width: moderateScale(16),
    height: moderateScale(16),
    marginLeft: sizeWidth(4)
  }
});
