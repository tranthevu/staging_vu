import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ImageBackground, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import numeral from "numeral";
import moment from "moment";
import { font, appColor, dateTimeFormat } from "../../constants/app.constant";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import CacheImage from "../../components/common/cache-image";
import { getProductType } from "../../helpers/common.helper";

class GiftItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render(): ReactNode {
    const { item } = this.props;
    const reward = lodash.get(item, "deal_set_reward.total_reward_value");
    const name = lodash.get(item, "merchant.name");
    const logo = lodash.get(item, "merchant.logo_image.thumbnail");
    const title = lodash.get(item, "deal_set_reward.deal_reward.name");
    const images = lodash.get(item, "deal_set_reward.deal_reward.images", []);
    const image = images.find(el => el.caption === "1:1" && el.display_order === 0);
    const cover = lodash.get(image, "original.thumbnail");
    const endDate = lodash.get(item, "deal_set_reward.deal_reward.end_datetime");
    const discountPercent = lodash.get(item, "percent_discount", 0);
    return (
      <TouchableOpacity style={styles.container} onPress={this.navigateToOfferDetail}>
        <View style={styles.body}>
          <View style={styles.content}>
            <Text style={styles.title} numberOfLines={2}>
              {title}
            </Text>
            {!discountPercent ? (
              <View style={styles.money}>
                <Text style={styles.reward}>Tổng tiền thưởng</Text>
                <Text style={styles.amount}>
                  {numeral(reward).format("0,0")}
                  <Text style={styles.unit}>đ</Text>
                </Text>
              </View>
            ) : (
              <View style={{ flexDirection: "row" }}>
                <ImageBackground
                  style={styles.tag}
                  resizeMode="stretch"
                  source={require("../../../res/icon/discount-tag.png")}
                >
                  <Text style={styles.discount}>-{discountPercent}%</Text>
                </ImageBackground>
                <Text style={styles.event}>{getProductType(item.deal_set_type)}</Text>
              </View>
            )}
          </View>
          <View style={styles.cover}>
            <CacheImage uri={cover} style={styles.image} />
            {!!item.total_count && item.total_count > 1 && (
              <View style={styles.badge}>
                <Text style={styles.count}>x{item.total_count}</Text>
              </View>
            )}
          </View>
        </View>
        <View style={styles.separator} />
        <View style={styles.bottom}>
          <CacheImage uri={logo} style={styles.merchant} />
          <Text style={styles.name}>{name}</Text>
          {!!endDate && (
            <Text style={styles.due}>
              Hết hạn: {moment(endDate).format(dateTimeFormat.ddmmyyyy)}
            </Text>
          )}
        </View>
      </TouchableOpacity>
    );
  }

  navigateToVoucher = () => {
    const { item } = this.props;
    this.props.navigateToPage("Voucher", {
      productId: item.id,
      productSlug: item.product_slug,
      isGift: true
    });
  };

  navigateToOfferDetail = () => {
    this.navigateToVoucher();
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(GiftItem);

const styles = StyleSheet.create({
  container: {
    marginHorizontal: sizeWidth(9),
    backgroundColor: "white",
    marginVertical: moderateScale(5),
    paddingHorizontal: sizeWidth(10)
  },
  body: {
    flexDirection: "row",
    paddingVertical: moderateScale(10)
  },
  content: {
    flex: 1,
    marginRight: sizeWidth(10)
  },
  title: {
    fontSize: sizeFont(12),
    fontFamily: font.medium,
    marginBottom: moderateScale(8)
  },
  money: {
    flexDirection: "row",
    alignItems: "center"
  },
  reward: {
    fontSize: sizeFont(12),
    marginRight: sizeWidth(12)
  },
  amount: {
    fontFamily: font.bold,
    flex: 1,
    color: appColor.primary
  },
  unit: {
    fontFamily: font.bold,
    color: appColor.primary,
    textDecorationLine: "underline"
  },
  cover: {
    width: moderateScale(60),
    height: moderateScale(60),
    borderRadius: moderateScale(6),
    borderWidth: 1,
    borderColor: "#dddddd",
    overflow: "hidden"
  },
  image: {
    width: moderateScale(60),
    height: moderateScale(60)
  },
  badge: {
    minWidth: moderateScale(22),
    position: "absolute",
    bottom: moderateScale(5),
    right: moderateScale(5),
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: sizeWidth(2),
    height: moderateScale(22),
    borderRadius: moderateScale(11),
    backgroundColor: appColor.primary,
    borderWidth: 1,
    borderColor: "white"
  },
  count: {
    color: "white",
    fontSize: sizeFont(11),
    fontFamily: font.medium
  },
  separator: {
    height: 1,
    backgroundColor: "#dddddd"
  },
  bottom: {
    flexDirection: "row",
    paddingVertical: moderateScale(7),
    alignItems: "center"
  },
  merchant: {
    width: moderateScale(16),
    height: moderateScale(16),
    borderRadius: moderateScale(8),
    marginRight: sizeWidth(6)
  },
  name: {
    flex: 1,
    fontSize: sizeFont(12)
  },
  due: {
    marginLeft: sizeWidth(8),
    fontSize: sizeFont(12)
  },
  discount: {
    fontSize: sizeFont(11),
    color: "white",
    fontFamily: font.bold,
    marginLeft: sizeWidth(3)
  },
  tag: {
    width: moderateScale(41),
    height: moderateScale(20),
    marginRight: moderateScale(7),
    justifyContent: "center"
  },
  event: {
    fontSize: sizeFont(12),
    alignSelf: "center",
    color: appColor.blur
  }
});
