import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import lodash from "lodash";
import moment from "moment";
import numeral from "numeral";

class Voucher extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render(): ReactNode {
    const { style, item } = this.props;
    const price = numeral(
      lodash.get(item, "product.dealset.fixed_discount", 0)
    ).format("0,0");
    const title = lodash.get(item, "product.title");
    const endDate = moment(lodash.get(item, "code.end_datetime"));
    const now = moment();
    const diff = endDate.diff(now, "days");
    const used = lodash.get(item, "is_used");
    const isExpired = !used && endDate.isBefore(now);
    const showExpiring = !used && !isExpired && diff < 6;
    const logo = lodash.get(item, "product.merchant.logo_image");

    return (
      <TouchableOpacity
        onPress={this.navigateToVoucherDetail}
        style={[styles.container, style]}
      >
        <View style={styles.body}>
          <View style={styles.top}>
            <View style={styles.voucher}>
              <Text style={styles.label}>Voucher trả sau</Text>
              <Text style={styles.current}>
                {price}
                <Text style={styles.underline}>đ</Text>
              </Text>
            </View>
            <Image
              style={styles.image}
              source={
                logo
                  ? { uri: logo }
                  : require("../../../res/img/default-image.png")
              }
            />
          </View>
          <Image
            style={styles.line}
            source={require("../../../res/icon/line.png")}
          />
          <View style={styles.bottom}>
            <Text numberOfLines={2} style={styles.label}>
              {title}
            </Text>
            <View style={styles.content}>
              <Image
                style={styles.icon}
                source={
                  used || isExpired
                    ? require("../../../res/icon/calendar.png")
                    : require("../../../res/icon/time.png")
                }
              />
              {used ? (
                this.renderUsedTime(endDate)
              ) : isExpired ? (
                this.renderExpireTime(endDate)
              ) : (
                <Text style={styles.text}>
                  Còn{" "}
                  <Text style={showExpiring ? styles.red : styles.normal}>
                    {diff}
                  </Text>{" "}
                  ngày
                </Text>
              )}

              {showExpiring && this.renderExpiring()}
              {used && this.renderUsed()}
              {isExpired && this.renderExpired()}
            </View>
          </View>
          <View style={styles.left} />
          <View style={styles.right} />
        </View>
      </TouchableOpacity>
    );
  }

  renderUsedTime = endDate => {
    return (
      <Text style={styles.text}>
        Hạn dùng: {endDate && endDate.format("DD/MM/YYYY")}
      </Text>
    );
  };

  renderExpireTime = endDate => {
    return (
      <Text style={styles.text}>
        Hạn dùng: {endDate && endDate.format("DD/MM/YYYY")}
      </Text>
    );
  };

  renderExpired = () => {
    return (
      <View style={styles.wrap}>
        <Image
          style={styles.tag}
          source={require("../../../res/icon/remove-tag.png")}
        />
        <Text style={styles.status}>Hết hạn</Text>
      </View>
    );
  };

  renderUsed = () => {
    return (
      <View style={styles.wrap}>
        <Image
          style={styles.tag}
          source={require("../../../res/icon/remove-tag.png")}
        />
        <Text style={styles.status}>Đã dùng</Text>
      </View>
    );
  };

  renderExpiring = () => {
    return (
      <View style={styles.overtime}>
        <Image
          style={styles.tag}
          source={require("../../../res/icon/stopwatch.png")}
        />
        <Text style={styles.expiring}>Sắp hết hạn</Text>
      </View>
    );
  };

  navigateToVoucherDetail = () => {
    const { item } = this.props;
    this.props.navigateToPage("Voucher", { productId: item.id, productSlug: item.slug });
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(Voucher);

const styles = StyleSheet.create({
  container: {
    alignSelf: "center",
    width: sizeWidth(302),
    borderRadius: sizeWidth(6),
    overflow: "hidden",
    marginVertical: sizeWidth(8)
  },
  top: {
    backgroundColor: "white",
    height: sizeWidth(78),
    flexDirection: "row",
    paddingHorizontal: sizeWidth(16),
    alignItems: "center"
  },
  label: {
    color: "#000000",
    fontFamily: font.medium
  },
  underline: {
    textDecorationLine: "underline",
    color: "#FF6961",
    fontSize: sizeFont(24),
    fontFamily: font.bold
  },
  image: {
    width: sizeWidth(40),
    height: sizeWidth(40),
    borderWidth: 1,
    borderColor: "#DADADA",
    borderRadius: sizeWidth(20),
    marginLeft: sizeWidth(16)
  },
  current: {
    color: "#FF6961",
    fontSize: sizeFont(24),
    fontFamily: font.bold
  },
  voucher: {
    flex: 1
  },
  bottom: {
    backgroundColor: "white",
    paddingHorizontal: sizeWidth(16),
    paddingVertical: sizeWidth(12)
  },
  content: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: sizeWidth(8)
  },
  text: {
    fontSize: sizeFont(11),
    flex: 1
  },
  red: {
    color: appColor.primary,
    fontSize: sizeFont(11)
  },
  normal: {
    fontSize: sizeFont(11)
  },
  icon: {
    width: sizeWidth(13),
    height: sizeWidth(13),
    marginRight: sizeWidth(8)
  },

  line: {
    width: "94%",
    alignSelf: "center",
    height: 1
  },
  left: {
    backgroundColor: appColor.bg,
    position: "absolute",
    width: sizeWidth(18),
    height: sizeWidth(18),
    borderRadius: sizeWidth(9),
    top: sizeWidth(69),
    left: sizeWidth(-9)
  },
  right: {
    backgroundColor: appColor.bg,
    position: "absolute",
    width: sizeWidth(18),
    height: sizeWidth(18),
    borderRadius: sizeWidth(9),
    top: sizeWidth(69),
    right: sizeWidth(-9)
  },
  wrap: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    height: sizeWidth(24),
    width: sizeWidth(91),
    borderRadius: sizeWidth(12),
    backgroundColor: "#DDDDDD"
  },
  status: {
    color: appColor.blur,
    fontSize: sizeFont(11)
  },
  overtime: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    height: sizeWidth(24),
    width: sizeWidth(100),
    borderRadius: sizeWidth(12),
    backgroundColor: "#FFCC00"
  },
  tag: {
    width: sizeWidth(17),
    height: sizeWidth(17),
    marginRight: sizeWidth(7)
  },
  expiring: {
    color: "#FF6961",
    fontSize: sizeFont(11)
  }
});
