import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  ImageBackground,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import {
  font,
  appColor,
  productClass,
  text,
  voucherStatus
} from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import lodash from "lodash";
import moment from "moment";
import LinearGradient from "react-native-linear-gradient";
import numeral from "numeral";
import OnlineLabel from "../../components/common/online-label";
import { getProductType, isEmptyPrice, formatPrice } from "../../helpers/common.helper";
import ExclusiveLabel from "../../components/common/exclusive-label";

class Offer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render(): ReactNode {
    const { style, item = {} } = this.props;
    const logo = lodash.get(item, "merchant.logo_image.full");
    const merchant = lodash.get(item, "merchant.name");
    const currentPrice = lodash.get(item, "dealset.fixed_discount");
    const oldPrice = lodash.get(item, "attributes.price_old");
    const cover = lodash
      .get(item, "images", [])
      .find(item => item.display_order === 0 && item.caption === "16:9");
    const endDate = moment(lodash.get(item, "attributes.date_valid_end"));
    const now = moment();
    const diff = endDate.diff(now, "days");
    const used = lodash.get(item, "status") === voucherStatus.used;
    const shortDescription = lodash.get(item, "attributes.short_description");
    const expireDate = moment(lodash.get(item, "code.end_datetime"));
    const isExpired = lodash.get(item, "status") === voucherStatus.isExpired;
    const isOfferExpired = lodash.get(item, "status") === voucherStatus.isOfferExpired;
    const statusShown = isOfferExpired || isExpired || used;
    const discountPercent = lodash.get(item, "dealset.percent_discount", 0);
    const isExclusive = lodash.get(item, "attributes.is_exclusive_offer");
    return (
      <TouchableOpacity
        onPress={this.navigateToVoucherDetail}
        style={[styles.container, style]}
      >
        <View style={styles.root}>
          <View style={styles.header}>
            <Image
              style={styles.cover}
              source={
                lodash.get(cover, "original.thumbnail")
                  ? { uri: cover.original.thumbnail }
                  : require("../../../res/img/default-offer.jpg")
              }
            />
            {!!merchant && (
              <View style={styles.info}>
                <LinearGradient
                  colors={["rgba(0, 0, 0, 0)", "rgba(0, 0, 0, 0.5)"]}
                  style={styles.overlay}
                />
                <TouchableOpacity onPress={this.navigateToMerchantDetail}>
                  <Image
                    style={styles.logo}
                    source={
                      logo
                        ? { uri: logo }
                        : require("../../../res/img/default-location.jpg")
                    }
                  />
                </TouchableOpacity>
                <Text style={styles.name}>{merchant}</Text>
                {/* <View style={[styles.badge]}>
                  <Text style={styles.x}>x4</Text>
                </View> */}
              </View>
            )}
          </View>
          <View style={styles.body}>
            <View style={styles.top}>
              <Text numberOfLines={2} style={styles.title}>
                {item.title}
              </Text>
              {item.product_class === productClass.voucher ? (
                <View style={styles.prices}>
                  {!!discountPercent && (
                    <ImageBackground
                      style={styles.tag}
                      resizeMode="stretch"
                      source={require("../../../res/icon/discount-tag.png")}
                    >
                      <Text style={styles.discount}>-{discountPercent}%</Text>
                    </ImageBackground>
                  )}
                  {!isEmptyPrice(currentPrice) && (
                    <Text style={styles.current}>
                      {formatPrice(currentPrice)}
                      <Text style={styles.underline}>đ</Text>
                    </Text>
                  )}
                  {!isEmptyPrice(oldPrice) && (
                    <Text style={styles.old}>{formatPrice(oldPrice)}đ</Text>
                  )}
                </View>
              ) : (
                <View style={styles.prices}>
                  {!!discountPercent && (
                    <ImageBackground
                      style={styles.tag}
                      resizeMode="stretch"
                      source={require("../../../res/icon/discount-tag.png")}
                    >
                      <Text style={styles.discount}>-{discountPercent}%</Text>
                    </ImageBackground>
                  )}
                  <Text style={styles.event}>{getProductType(item.product_class)}</Text>
                </View>
              )}
            </View>
          </View>
        </View>
        <View style={styles.shadow} />
        {statusShown && <View style={styles.blurCover} />}
        {statusShown && this.renderStatus()}
        {item.product_class == productClass.affiliateOffer && <OnlineLabel />}
        {item.product_class == productClass.voucher && isExclusive && <ExclusiveLabel />}
      </TouchableOpacity>
    );
  }

  navigateToMerchantDetail = () => {
    const { item = {} } = this.props;
    const merchantCode = lodash.get(item, "merchant.code");
    this.props.navigateToPage("MerchantDetail", { merchantCode });
  };

  renderStatus = () => {
    const { item } = this.props;
    const endDate = moment(lodash.get(item, "attributes.date_valid_end"));
    const expireDate = moment(lodash.get(item, "code.end_datetime"));
    const now = moment();
    const used = lodash.get(item, "status") === voucherStatus.used;
    const isExpired = lodash.get(item, "status") === voucherStatus.isExpired;
    const isOfferExpired = lodash.get(item, "status") === voucherStatus.isOfferExpired;
    return (
      <View style={styles.status}>
        <Text numberOfLines={1} style={styles.value}>
          {used
            ? "Đã dùng"
            : isExpired
            ? "Hết hạn dùng"
            : isOfferExpired
            ? "Tin hết hạn"
            : text.emptyString}
        </Text>
      </View>
    );
  };

  navigateToVoucherDetail = () => {
    const { item } = this.props;
    if (
      item.product_class === productClass.voucher ||
      item.product_class === productClass.shareToEarn
    ) {
      this.props.navigateToPage("Voucher", {
        productId: item.id,
        productSlug: item.slug,
        product_class: item.product_class
      });
    } else {
      this.props.navigateToPage("OfferDetail", {
        productId: item.id,
        productSlug: item.slug
      });
    }
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(Offer);

const styles = StyleSheet.create({
  root: {
    overflow: "hidden"
  },
  container: {
    alignSelf: "center",
    backgroundColor: "transparent",
    width: sizeWidth(302),
    borderRadius: sizeWidth(6),
    overflow: "hidden",
    marginVertical: sizeWidth(8)
  },
  header: {
    width: "100%",
    borderRadius: sizeWidth(6),
    overflow: "hidden"
  },
  underline: {
    textDecorationLine: "underline",
    color: "#FF6961",
    fontSize: sizeFont(14),
    fontFamily: font.bold
  },
  logo: {
    width: moderateScale(40),
    backgroundColor: "white",
    height: moderateScale(40),
    borderRadius: moderateScale(20)
  },
  cover: {
    width: "100%",
    height: sizeWidth(169),
    borderRadius: sizeWidth(6)
  },
  info: {
    flexDirection: "row",
    height: moderateScale(60),
    width: sizeWidth(302),
    borderRadius: sizeWidth(6),
    paddingHorizontal: moderateScale(17),
    alignItems: "center",
    position: "absolute",
    bottom: 0
  },
  name: {
    flex: 1,
    marginHorizontal: sizeWidth(8),
    color: "white",
    fontFamily: font.bold
  },
  top: {
    backgroundColor: "white",
    paddingVertical: moderateScale(8),
    paddingHorizontal: moderateScale(16)
  },
  label: {
    color: "#000000",
    fontFamily: font.medium
  },
  event: {
    fontSize: sizeFont(12),
    alignSelf: "center",
    color: appColor.blur
  },
  title: {
    color: "#444444",
    fontWeight: "500",
    fontSize: sizeFont(12)
  },
  prices: {
    flexDirection: "row",
    marginTop: moderateScale(6),
    alignItems: "flex-end"
  },
  current: {
    color: "#FF6961",
    fontSize: sizeFont(14),
    alignSelf: "center",
    fontFamily: font.bold,
    marginRight: sizeWidth(12)
  },
  old: {
    color: appColor.blur,
    alignSelf: "center",
    fontSize: sizeFont(12),
    textDecorationLine: "line-through"
  },
  bottom: {
    backgroundColor: "white",
    paddingHorizontal: moderateScale(16),
    paddingVertical: moderateScale(12)
  },
  content: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: moderateScale(8)
  },
  icon: {
    width: moderateScale(13),
    height: moderateScale(13),
    marginRight: moderateScale(8)
  },
  cart: {
    marginLeft: sizeWidth(24)
  },
  line: {
    width: "94%",
    alignSelf: "center",
    height: 1
  },
  left: {
    backgroundColor: appColor.bg,
    position: "absolute",
    width: sizeWidth(18),
    height: sizeWidth(18),
    borderRadius: sizeWidth(9),
    top: sizeWidth(69),
    left: sizeWidth(-9),
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 1,
    shadowRadius: 4
  },
  right: {
    backgroundColor: appColor.bg,
    position: "absolute",
    width: sizeWidth(18),
    height: sizeWidth(18),
    borderRadius: sizeWidth(9),
    top: sizeWidth(69),
    right: sizeWidth(-9),
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 1,
    shadowRadius: 4
  },
  desc: {
    fontSize: sizeFont(12)
  },
  overlay: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  blurCover: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    opacity: 0.5,
    backgroundColor: "white",
    right: 0
  },
  shadow: {
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 1,
    shadowRadius: 4,
    marginBottom: 4,
    elevation: 2,
    height: 2,
    backgroundColor: "white",
    width: "100%"
  },
  discount: {
    fontSize: sizeFont(11),
    color: "white",
    fontFamily: font.bold,
    marginLeft: sizeWidth(3)
  },
  tag: {
    width: sizeWidth(41),
    height: moderateScale(20),
    marginRight: sizeWidth(7),
    alignSelf: "center",
    justifyContent: "center"
  },
  body: {
    borderTopLeftRadius: sizeWidth(6),
    borderTopRightRadius: sizeWidth(6),
    overflow: "hidden"
  },
  status: {
    backgroundColor: "rgb(56, 58, 57)",
    height: sizeWidth(28),
    justifyContent: "center",
    paddingHorizontal: sizeWidth(21),
    borderRadius: sizeWidth(14),
    position: "absolute",
    alignSelf: "center",
    top: sizeWidth(71)
  },
  value: {
    color: "white",
    fontSize: sizeFont(13),
    fontFamily: font.bold
  },
  badge: {
    minWidth: moderateScale(22),
    height: moderateScale(22),
    borderRadius: moderateScale(11),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: appColor.primary
  },
  x: {
    color: "white",
    fontSize: sizeFont(11),
    fontFamily: font.medium
  }
});
