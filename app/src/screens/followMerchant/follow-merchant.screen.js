import React, { Component, ReactNode } from "react";
import { View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import NotificationView from "../../components/common/notification-view";
import MerchantItem from "../follow/merchant-item";
import PaginationList from "../../components/common/pagination-list";
import {
  refreshFollowMerchants,
  fetchMoreFollowMerchants,
  requestFollowMerchants
} from "../../actions/follow-merchants.action";
import EmptyView from "../../components/common/empty-view";

class FollowMerchantScreen extends Component {
  renderMerchant = ({ item }) => {
    return <MerchantItem style={styles.item} item={item} />;
  };

  renderList = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      followMerchants
    } = this.props.followMerchants;
    const { fetchMoreFollowMerchants, refreshFollowMerchants } = this.props;
    return (
      <PaginationList
        loading={loading}
        numColumns={2}
        contentContainerStyle={styles.content}
        renderItem={this.renderMerchant}
        keyExtractor={(item, index) => index.toString()}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        firstLoading={firstLoading}
        onEndReached={() => fetchMoreFollowMerchants()}
        onRefresh={() => refreshFollowMerchants()}
        data={followMerchants}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  render(): ReactNode {
    return (
      <View style={styles.container}>
        <Toolbar
          center={<Text style={styles.header}>Đang theo dõi</Text>}
          left={<BackIcon />}
        />
        <View style={styles.body}>
          {this.renderList()}
          <NotificationView />
        </View>
      </View>
    );
  }

  componentDidMount = async () => {
    this.props.requestFollowMerchants();
  };
}

export default connect(
  state => ({ followMerchants: state.followMerchants }),
  {
    resetPage,
    navigateToPage,
    requestFollowMerchants,
    refreshFollowMerchants,
    fetchMoreFollowMerchants
  }
)(FollowMerchantScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  content: {
    paddingHorizontal: sizeWidth(6),
    paddingVertical: sizeWidth(5)
  },
  item: {
    marginVertical: sizeWidth(5)
  },
  body: {
    flex: 1
  }
});
