import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  FlatList,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import TouchableIcon from "../../components/common/touchable-icon";
import NearOfferItem from "./near-offer-item";
import CardOffer from "../../components/Card/CardOffer";

class NearOfferList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { offers, count } = this.props;
    return (
      <View style={[styles.container, styles.shadow]}>
        <View style={styles.bar}>
          <Text style={styles.title}>Ưu đãi gần bạn</Text>
          <TouchableIcon
            onPress={this.navigateToNearbyOffers}
            iconStyle={styles.arrow}
            source={require("../../../res/icon/chevron-right.png")}
          />
        </View>
        <FlatList
          horizontal
          data={count > 7 ? [...offers, {}] : offers}
          renderItem={this.renderNearOffer}
          keyExtractor={(item, index) => (item.id || index).toString()}
          contentContainerStyle={styles.top}
          showsHorizontalScrollIndicator={false}
        />
      </View>
    );
  }

  navigateToNearbyOffers = () => {
    this.props.navigateToPage("NearbyOffers");
  };

  renderNearOffer = ({ item, index }) => {
    if (index === 7)
      return (
        <TouchableOpacity onPress={this.navigateToNearbyOffers} style={styles.more}>
          <View style={styles.moreWrap}>
            <Image
              style={styles.moreIcon}
              source={require("../../../res/icon/more-online.png")}
            />
            <Text style={styles.moreText}>Xem thêm</Text>
          </View>
        </TouchableOpacity>
      );
    return <CardOffer item={item} />;
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(NearOfferList);

const styles = StyleSheet.create({
  shadow: {
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1
    }
  },
  title: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: sizeWidth(4),
    paddingHorizontal: sizeWidth(12)
  },
  arrow: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  top: {
    paddingHorizontal: sizeWidth(5)
  },
  container: {
    backgroundColor: "white",
    paddingVertical: sizeWidth(12),
    marginBottom: moderateScale(12)
  },
  more: {
    marginVertical: sizeWidth(3),
    marginHorizontal: sizeWidth(5)
  },
  moreWrap: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 4,
    width: sizeWidth(146),
    borderRadius: sizeWidth(6),
    flex: 1,
    borderColor: appColor.blur,
    borderStyle: "dashed",
    borderWidth: 1,
    backgroundColor: "white"
  },
  moreText: {
    color: appColor.blur,
    fontSize: sizeFont(13),
    fontFamily: font.bold
  },
  moreIcon: {
    width: moderateScale(14),
    height: moderateScale(24),
    marginBottom: moderateScale(19)
  }
});
