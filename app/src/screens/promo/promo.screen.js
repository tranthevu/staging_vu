import React, { Component } from "react";
import { View, StyleSheet, FlatList } from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import Category from "../dashboard/category";
import PaginationList from "../../components/common/pagination-list";
import PromotionItem from "../dashboard/promotion-item";
import PromotionItemList from "../dashboard/promotion-item-list";
import {
  requestPromoProducts,
  fetchMorePromoProducts,
  refreshPromoProducts
} from "../../actions/promo-products.action";
import { NEAR_OFFER } from "../HOC/withNearByOffer";
import { NEAR_PROMO_SCREEN } from "../../navigators/screensName";
import Banner from "../../components/Banner/index";
import ExclusivePreference from "../../components/ExclusivePreference/index";
import Api from "../../api/api";
import EmptyView from "../../components/common/empty-view";
import { checkApiStatus } from "../../helpers/app.helper";

const viewType = {
  grid: 0,
  list: 1
};

class PromoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedViewType: viewType.list,
      nearOfferData: [],
      exclusiveOffers: [],
      exclusiveOffersCount: 0
    };
  }

  navigateToSearch = () => {
    this.props.navigateToPage("Search");
  };

  onPressViewMore = () => {
    this.props.navigateToPage(NEAR_PROMO_SCREEN, { type: NEAR_OFFER.PROMOTION });
  };

  onNavigateNearPromo = () => {
    this.props.navigateToPage(NEAR_PROMO_SCREEN, { type: NEAR_OFFER.PROMOTION });
  };

  fetchExclusiveOffers = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const data = await Api.getExclusiveOffers();
    this.setState({
      exclusiveOffers: data.results,
      exclusiveOffersCount: data.count
    });
  };

  renderHeader = () => {
    const { banners, bannerPromos } = this.props.banners;
    const { topCategories } = this.props.topCategories;
    const { exclusiveOffers, exclusiveOffersCount } = this.state;
    return (
      <View>
        <Banner data={bannerPromos} />
        <View style={styles.categories}>
          <FlatList
            data={topCategories}
            horizontal
            contentContainerStyle={styles.top}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderCategory}
          />
        </View>
        {/* <ListOffer
          isLoading={isLoadingNearOffer}
          onPressViewMore={this.onPressViewMore}
          offers={nearOffers}
        /> */}
        <ExclusivePreference
          exclusiveOffersCount={exclusiveOffersCount}
          exclusiveOffers={exclusiveOffers}
        />
        {this.renderTitle()}
      </View>
    );
  };

  renderRecentProducts = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      promoProducts
    } = this.props.promoProducts;
    const { fetchMorePromoProducts, refreshPromoProducts } = this.props;
    return (
      <PaginationList
        ref={ref => (this.list = ref)}
        keyExtractor={(item, index) => (item.id || index).toString()}
        renderItem={this.renderPromotion}
        loading={loading}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        firstLoading={firstLoading}
        ListHeaderComponent={this.renderHeader()}
        onEndReached={() => fetchMorePromoProducts()}
        onRefresh={() => refreshPromoProducts()}
        data={promoProducts}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  renderCategory = ({ item }) => {
    return <Category item={item} />;
  };

  renderTitle = () => {
    const { selectedViewType } = this.state;
    return (
      <View style={styles.bar}>
        <Text style={styles.label}>Ưu đãi mới</Text>
        <TouchableIcon
          style={styles.view}
          iconStyle={styles.viewIcon}
          onPress={this.toggleViewType}
          source={
            selectedViewType === viewType.list
              ? require("../../../res/icon/list.png")
              : require("../../../res/icon/grid.png")
          }
        />
      </View>
    );
  };

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  renderPromotion = ({ item }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem item={item} />
    ) : (
      <PromotionItemList item={item} />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <Toolbar
          leftStyle={[styles.part, styles.left]}
          rightStyle={[styles.part, styles.right]}
          center={<Text style={styles.header}>Chương trình khuyến mãi</Text>}
          left={<BackIcon />}
          right={
            <View style={styles.right}>
              {/* <TouchableIcon
                style={styles.wrap}
                iconStyle={styles.icon}
                source={require("../../../res/icon/sort.png")}
              /> */}
              <TouchableIcon
                style={styles.wrap}
                onPress={this.navigateToSearch}
                iconStyle={styles.icon}
                source={require("../../../res/icon/home-search.png")}
              />
            </View>
          }
        />
        {this.renderRecentProducts()}
      </View>
    );
  }

  componentDidMount = () => {
    this.props.requestPromoProducts();
    this.fetchExclusiveOffers();
  };
}

export default connect(
  state => ({
    promoProducts: state.promoProducts,
    topCategories: state.topCategories,
    banners: state.banners
  }),
  {
    resetPage,
    navigateToPage,
    fetchMorePromoProducts,
    refreshPromoProducts,
    requestPromoProducts
  }
)(PromoScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  header: {
    textAlign: "center",
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  icon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: "white"
  },
  wrap: {
    marginLeft: moderateScale(6)
  },
  right: {
    alignItems: "flex-end",
    paddingRight: moderateScale(5)
  },
  part: {
    // width: moderateScale(90)
  },
  left: {
    alignItems: "flex-start",
    paddingLeft: moderateScale(10)
  },
  categories: {
    marginBottom: sizeWidth(12),
    marginTop: sizeWidth(8)
  },
  label: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: appColor.blur
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(9)
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: sizeWidth(5),
    paddingHorizontal: sizeWidth(10)
  },
  nearby: {
    paddingHorizontal: moderateScale(10),
    marginTop: moderateScale(10)
  }
});
