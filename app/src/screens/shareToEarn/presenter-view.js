import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import { connect } from "react-redux";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { font, appColor } from "../../constants/app.constant";
import TouchableIcon from "../../components/common/touchable-icon";
import { navigateToPage } from "../../actions/nav.action";
import CacheImage from "../../components/common/cache-image";

class PresenterView extends Component {
  render() {
    const { reference } = this.props;
    return (
      <View style={styles.container}>
        <CacheImage uri={reference.avatar} style={styles.image} />
        <View style={styles.body}>
          <Text style={styles.label}>Người giới thiệu</Text>
          <Text style={styles.name}>{reference.full_name}</Text>
        </View>
        <TouchableIcon
          onPress={this.navigateToGuide}
          iconStyle={styles.icon}
          source={require("../../../res/icon/icon-question-mark.png")}
        />
      </View>
    );
  }

  navigateToGuide = () => {
    this.props.navigateToPage("Presenter");
  };
}

export default connect(
  null,
  {
    navigateToPage
  }
)(PresenterView);

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flexDirection: "row",
    borderRadius: moderateScale(6),
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    marginHorizontal: sizeWidth(8),
    marginTop: -moderateScale(36),
    marginBottom: moderateScale(16),
    alignItems: "center",
    padding: moderateScale(10)
  },
  image: {
    width: moderateScale(40),
    height: moderateScale(40),
    borderRadius: moderateScale(20)
  },
  body: {
    marginHorizontal: sizeWidth(7),
    flex: 1
  },
  label: {
    fontSize: sizeFont(12),
    color: appColor.blur,
    marginBottom: moderateScale(4)
  },
  name: {
    fontSize: sizeFont(14),
    fontFamily: font.medium
  },
  icon: {
    width: moderateScale(24),
    height: moderateScale(24)
  }
});
