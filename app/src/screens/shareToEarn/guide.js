import React, { Component } from "react";
import { View, StyleSheet, Dimensions, ScrollView, Image } from "react-native";
import Toolbar from "../../components/common/toolbar";
import Text from "../../components/common/text";
import { sizeFont, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import BackIcon from "../../components/common/back-icon";
import {
  GUIDE_MOUSE,
  GUIDE_LOGIN,
  GUIDE_SHARE,
  GUIDE_COUPON,
  GUIDE_GIFT,
  GUIDE_QR
} from "../../commons/images";

const WIDTH = Dimensions.get("window").width;
export default class guide extends Component {
  // eslint-disable-next-line class-methods-use-this
  renderItemGuide(img, title, description, warning = "") {
    return (
      <View style={styles.itemGuide}>
        <View style={styles.step}>
          <View style={styles.line} />
          <View style={styles.circle}>
            <Image source={img} resizeMode="contain" style={styles.icon} />
          </View>
        </View>
        <View style={styles.content}>
          <View style={styles.item}>
            <Text style={styles.label}>{title}</Text>
            <Text style={styles.info}>{description} </Text>
            {!!warning && <Text style={styles.text}>{warning}</Text>}
          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Toolbar
          center={<Text style={styles.header}>Hướng dẫn</Text>}
          left={<BackIcon />}
        />
        <View style={styles.body}>
          <ScrollView contentContainerStyle={styles.list}>
            {this.renderItemGuide(
              GUIDE_MOUSE,
              "Truy cập trang chương trình",
              "Truy cập vào liên kết chương trình trên Yolla Network hoặc website chương trình"
            )}
            <View style={styles.itemGuide}>
              <View style={styles.step}>
                <View style={styles.line} />
              </View>
              <View style={styles.space} />
            </View>
            {this.renderItemGuide(
              GUIDE_LOGIN,
              "Đăng nhập",
              "Đăng nhập tài khoản Yolla Network bằng số điện thoại hoặc Facebook"
            )}
            <View style={styles.itemGuide}>
              <View style={styles.step}>
                <View style={styles.line} />
              </View>
              <View style={styles.space} />
            </View>
            {this.renderItemGuide(
              GUIDE_COUPON,
              "Nhận ưu đãi miễn phí",
              `Chọn vào nút "Nhận mã ưu đãi" để nhận ưu đãi từ chương trình cho bạn`
            )}
            <View style={styles.itemGuide}>
              <View style={styles.step}>
                <View style={styles.line} />
              </View>
              <View style={styles.space} />
            </View>
            {this.renderItemGuide(
              GUIDE_SHARE,
              "Chia sẻ chương trình",
              "Chọn nút Chia sẻ hoặc Copy liên kết và chia sẻ cho chương trình cho bạn bè. Bạn cũng có thể chia sẻ trực tiếp cho bạn bè băng QR code có sẵn trên trang.",
              "Lưu ý: chia sẻ link từ trang chiến dịch hoặc từ website Yolla Network để có thể đạt điều kiện nhận thưởng."
            )}
            <View style={styles.itemGuide}>
              <View style={styles.step}>
                <View style={styles.line} />
              </View>
              <View style={styles.space} />
            </View>
            {this.renderItemGuide(
              GUIDE_GIFT,
              "Ưu đãi và phần thưởng",
              "Truy cập mục Ưu đãi của tôi để xem mã ưu đãi & mã nhận thưởng. Mã nhận thưởng sẽ được trao tự động khi bạn đạt đủ điều kiện nhận thưởng theo chi tiết của chương trình."
            )}
            <View style={styles.itemGuide}>
              <View style={styles.step}>
                <View style={styles.line} />
              </View>
              <View style={styles.space} />
            </View>
            <View style={styles.itemGuide}>
              <View style={styles.step}>
                <View style={styles.circle}>
                  <Image source={GUIDE_QR} resizeMode="contain" style={styles.qr} />
                </View>
              </View>
              <View style={styles.content}>
                <View style={styles.item}>
                  <Text style={styles.label}>Sử dụng phần thưởng</Text>
                  <Text style={styles.info}>
                    Đến cửa hàng và sử dụng mã phần thưởng của mình bằng cách cung cấp mã
                    phần thưởng cho nhân viên cửa hàng để nhận thưởng.
                  </Text>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  qr: {
    width: moderateScale(18),
    height: moderateScale(18)
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  list: {
    paddingVertical: moderateScale(26)
  },
  label: {
    fontSize: sizeFont(14),
    fontFamily: font.bold,
    marginBottom: moderateScale(10)
  },
  itemGuide: {
    width: WIDTH,
    flexDirection: "row"
  },
  info: {
    fontSize: sizeFont(13)
  },
  step: {
    flex: 1,
    alignItems: "center",
    position: "relative"
  },
  space: {
    flex: 4,
    marginRight: 10,
    padding: 5
  },
  content: {
    flex: 4,
    padding: 5,
    backgroundColor: "#fff",
    marginRight: 10,
    borderRadius: 6,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.18,
    shadowRadius: 1.0,
    elevation: 1
  },
  line: {
    height: "100%",
    position: "absolute",
    width: 1,
    backgroundColor: "#A9C9DD"
  },
  circle: {
    width: moderateScale(36),
    height: moderateScale(36),
    borderRadius: moderateScale(18),
    backgroundColor: "#00ACFF",
    justifyContent: "center",
    alignItems: "center"
  },
  icon: {
    width: moderateScale(18),
    height: moderateScale(16)
  },
  text: {
    fontSize: sizeFont(13),
    color: "#828FA2",
    marginTop: moderateScale(10),
    fontFamily: font.lightItalic
  },
  item: {
    padding: moderateScale(11)
  }
});
