import React, { Component, ReactNode } from "react";
import { connect } from "react-redux";
import { View, StyleSheet, Image, TouchableOpacity } from "react-native";
import lodash from "lodash";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";

import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor, text, giftStatus } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import TouchableIcon from "../../components/common/touchable-icon";
import PromotionItemList from "../dashboard/promotion-item-list";
import SearchInput from "../../components/common/search-input";
import PaginationList from "../../components/common/pagination-list";
import PromotionItem from "../dashboard/promotion-item";
import {
  requestShare2EarnProducts,
  fetchMoreShare2EarnProducts,
  refreshShare2EarnProducts
} from "../../actions/share2earn-products.action";
import Api from "../../api/api";
import AuthHelper from "../../helpers/auth.helper";
import { tabs } from "../myArchivement/my-archivement.screen";
import { SHARE_TO_EARN_FILTER_SCREEN } from "../../navigators/screensName";
import PresenterView from "./presenter-view";
import EmptyView from "../../components/common/empty-view";
import { checkApiStatus } from "../../helpers/app.helper";

const viewType = {
  grid: 0,
  list: 1
};

class ShareToEarnScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedViewType: viewType.list,
      receivedRewards: 0,
      campaignsJoined: 0,
      appliedFilters: {
        selectedStatus: {
          type: "",
          name: "Tất cả"
        }
      },
      usedRewards: 0,
      keyword: text.emptyString,
      isAuthenticated: false,
      ref: null
    };
  }

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  navigateToJoinedCampaigns = async () => {
    const isAuthenticated = await AuthHelper.isAuthenticated();
    if (isAuthenticated) {
      this.props.navigateToPage("JoinedCampaigns");
    } else {
      const lastScreen = this.props.navigation.state.routeName;
      this.props.navigateToPage("Login", {
        canBack: true,
        lastScreen,
        action: "navigateToJoinedCampaigns"
      });
    }
  };

  navigateToGift = status => {
    this.props.resetPage("Main");
    this.props.navigateToPage("Archive", { selectedTab: tabs.gift, status });
  };

  navigateToRedemptionHistory = () => {
    this.props.navigateToPage("RedemptionHistory");
  };

  renderGiftIcon = () => {
    const { receivedRewards } = this.state;
    return (
      <TouchableOpacity
        onPress={() => this.navigateToGift(giftStatus.all)}
        style={styles.wrap}
      >
        <Image style={styles.gift} source={require("../../../res/icon/gift-icon.png")} />
        {!!receivedRewards && (
          <View style={styles.circle}>
            <Text style={styles.badge}>{receivedRewards}</Text>
          </View>
        )}
      </TouchableOpacity>
    );
  };

  navigateToFilter = () => {
    const { appliedFilters } = this.state;
    this.props.navigateToPage(SHARE_TO_EARN_FILTER_SCREEN, {
      onApplyFilters: this.onApplyFilters,
      appliedFilters
    });
  };

  onApplyFilters = appliedFilters => {
    const { keyword } = this.state;
    this.setState({ appliedFilters });
    this.props.requestShare2EarnProducts({
      title: keyword,
      status: appliedFilters.selectedStatus.type
    });
  };

  render(): ReactNode {
    const {
      selectedViewType,
      receivedRewards,
      campaignsJoined,
      usedRewards,
      keyword,
      isAuthenticated,
      appliedFilters,
      ref
    } = this.state;
    return (
      <View style={styles.container}>
        <View
          style={[
            isAuthenticated ? styles.header : styles.anonymousHeader,
            !ref && isAuthenticated && styles.noRefHeader
          ]}
        >
          <Toolbar
            left={<BackIcon />}
            center={<Text style={styles.title}>Chia sẻ nhận thưởng</Text>}
            right={isAuthenticated && this.renderGiftIcon()}
          />
          {isAuthenticated && (
            <View style={styles.summary}>
              <TouchableOpacity
                onPress={this.navigateToJoinedCampaigns}
                style={styles.column}
              >
                <Text style={styles.count}>{campaignsJoined}</Text>
                <Text style={styles.label}>Chương trình đã tham gia</Text>
              </TouchableOpacity>
              <View style={styles.line} />
              <TouchableOpacity
                onPress={() => this.navigateToGift(giftStatus.all)}
                style={styles.column}
              >
                <Text style={styles.count}>{receivedRewards}</Text>
                <Text style={styles.label}>Phần thưởng đã nhận</Text>
              </TouchableOpacity>
              <View style={styles.line} />
              <TouchableOpacity
                onPress={this.navigateToRedemptionHistory}
                style={styles.column}
              >
                <Text style={styles.count}>{usedRewards}</Text>
                <Text style={styles.label}>Phần thưởng đã sử dụng</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
        {isAuthenticated && !!ref && <PresenterView reference={ref} />}
        <SearchInput
          value={keyword}
          autoFocus={false}
          onIconPress={() =>
            this.props.requestShare2EarnProducts({
              title: keyword,
              status: appliedFilters.selectedStatus.type
            })
          }
          onChangeText={value => this.setState({ keyword: value })}
          style={styles.search}
          iconStyle={styles.icon}
          inputStyle={styles.input}
          placeholder="Tìm kiếm"
        />
        <View style={styles.bar}>
          <Text style={styles.text}>Chia sẻ nhận thưởng</Text>
          <TouchableIcon
            style={styles.view}
            iconStyle={styles.viewIcon}
            onPress={this.navigateToFilter}
            source={require("../../../res/icon/sort.png")}
          />
          <TouchableIcon
            style={styles.view}
            iconStyle={styles.viewIcon}
            onPress={this.toggleViewType}
            source={
              selectedViewType === viewType.list
                ? require("../../../res/icon/list.png")
                : require("../../../res/icon/grid.png")
            }
          />
        </View>
        {this.renderList()}
      </View>
    );
  }

  renderPromotion = ({ item }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem
        item={item.product}
        info={item.info}
        promotion_voucher_code={item.promotion_voucher_code}
      />
    ) : (
      <PromotionItemList
        item={item.product}
        info={item.info}
        promotion_voucher_code={item.promotion_voucher_code}
      />
    );
  };

  renderList = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      share2EarnProducts
    } = this.props.share2EarnProducts;
    const { keyword, appliedFilters } = this.state;
    const { fetchMoreShare2EarnProducts, refreshShare2EarnProducts } = this.props;
    return (
      <PaginationList
        ref={ref => (this.list = ref)}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderPromotion}
        loading={loading}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        firstLoading={firstLoading}
        onEndReached={() =>
          fetchMoreShare2EarnProducts({
            title: keyword,
            status: appliedFilters.selectedStatus.type
          })
        }
        onRefresh={() =>
          refreshShare2EarnProducts({
            title: keyword,
            status: appliedFilters.selectedStatus.type
          })
        }
        data={share2EarnProducts}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  getStatus = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const res = await Api.statusShare2Earn();
    this.setState({
      receivedRewards: res.data.received_rewards,
      campaignsJoined: res.data.s2e_campaigns_joined,
      usedRewards: res.data.used_rewards
    });
  };

  componentDidMount = async () => {
    const { keyword, appliedFilters } = this.state;
    const isAuthenticated = await AuthHelper.isAuthenticated();
    this.setState({ isAuthenticated: isAuthenticated });
    this.props.requestShare2EarnProducts({
      title: keyword,
      status: appliedFilters.selectedStatus.type
    });
    this.loadUserRef();
    if (isAuthenticated) {
      this.getStatus();
      const action = lodash.get(this.props.navigation.state.params, "action");
      if (action === "navigateToJoinedCampaigns") {
        this.navigateToJoinedCampaigns();
      }
    }
  };

  loadUserRef = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { profile } = this.props.profile;
    const res = await Api.userRef(profile.username);
    this.setState({
      ref: res.data
    });
  };
}

export default connect(
  state => ({
    share2EarnProducts: state.share2EarnProducts,
    profile: state.profile
  }),
  {
    resetPage,
    navigateToPage,
    navigateBack,
    fetchMoreShare2EarnProducts,
    refreshShare2EarnProducts,
    requestShare2EarnProducts
  }
)(ShareToEarnScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  header: {
    height: moderateScale(180),
    width: sizeWidth(320),
    backgroundColor: appColor.primary
  },
  noRefHeader: {
    height: moderateScale(140)
  },
  anonymousHeader: {
    height: moderateScale(52),
    width: sizeWidth(320),
    backgroundColor: appColor.primary
  },
  gift: {
    width: moderateScale(20),
    height: moderateScale(20)
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  summary: {
    alignItems: "center",
    flexDirection: "row",
    marginTop: moderateScale(8)
  },
  column: {
    flex: 1,
    paddingHorizontal: sizeWidth(20),
    justifyContent: "center",
    alignItems: "center"
  },
  count: {
    color: "white",
    fontSize: sizeFont(28),
    fontFamily: font.medium
  },
  label: {
    color: "white",
    textAlign: "center",
    fontSize: sizeFont(11),
    marginTop: moderateScale(6)
  },
  line: {
    backgroundColor: "#D9DDDD",
    width: 1,
    height: moderateScale(54)
  },
  search: {
    width: sizeWidth(320),
    alignSelf: "center",
    marginBottom: moderateScale(4)
  },
  icon: {
    tintColor: appColor.primary
  },
  input: {
    fontSize: sizeFont(12)
  },
  text: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: appColor.blur
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(9)
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: sizeWidth(5),
    paddingHorizontal: sizeWidth(10)
  },
  circle: {
    backgroundColor: "#FFCC00",
    minWidth: moderateScale(14),
    height: moderateScale(14),
    borderRadius: moderateScale(7),
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    borderWidth: 1,
    borderColor: "white",
    top: 0,
    right: 0
  },
  badge: {
    color: appColor.primary,
    fontSize: sizeFont(9)
  },
  wrap: {
    width: moderateScale(23),
    height: moderateScale(20)
  }
});
