import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import { navigateBack } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import Button from "../../components/common/button";
import Dropdown from "../../components/common/dropdown";
import NotificationView from "../../components/common/notification-view";

const options = [
  {
    type: "",
    name: "Tất cả"
  },
  {
    type: "-joined",
    name: "Chưa tham gia"
  },
  {
    type: "joined",
    name: "Đã tham gia"
  }
];

class ShareToEarnFilterScreen extends Component {
  constructor(props) {
    super(props);
    const appliedFilters = lodash.get(
      this.props.navigation.state,
      "params.appliedFilters",
      {}
    );
    this.state = {
      selectedStatus: appliedFilters.selectedStatus
    };
  }

  render(): ReactNode {
    const { selectedStatus } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
          center={<Text style={styles.title}>Bộ lọc</Text>}
        />
        <View style={styles.body}>
          <ScrollView bounces={false} contentContainerStyle={styles.list}>
            <Dropdown
              options={options}
              label="Lọc theo"
              path="name"
              onValueChange={value => this.setState({ selectedStatus: value })}
              value={lodash.get(selectedStatus, "name", "Chọn một tuỳ chọn")}
            />
          </ScrollView>
          <Button style={styles.apply} onPress={this.applyFilter} text="ÁP DỤNG" />
          <NotificationView />
        </View>
      </View>
    );
  }

  applyFilter = () => {
    const onApplyFilters = lodash.get(
      this.props.navigation.state,
      "params.onApplyFilters"
    );
    const { selectedStatus } = this.state;
    if (onApplyFilters)
      onApplyFilters({
        selectedStatus
      });
    this.props.navigateBack();
  };
}

export default connect(
  null,
  { navigateBack }
)(ShareToEarnFilterScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  cancel: {
    fontSize: sizeFont(13),
    color: "white"
  },
  apply: {
    position: "absolute",
    bottom: sizeWidth(12)
  },
  list: {
    paddingBottom: sizeWidth(40)
  }
});
