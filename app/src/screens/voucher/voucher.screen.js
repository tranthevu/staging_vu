/* eslint-disable no-lonely-if */
import React, { Component } from "react";
import {
  View,
  Image,
  Linking,
  ScrollView,
  Platform,
  TouchableOpacity,
  Clipboard
} from "react-native";
import { connect } from "react-redux";
import QRCode from "react-native-qrcode";
import lodash from "lodash";
import moment from "moment";
import numeral from "numeral";
import Dialog, { FadeAnimation } from "react-native-popup-dialog";
import Swiper from "react-native-swiper";
import Barcode from "../../components/common/barcode";
import {
  resetPage,
  navigateToPage,
  navigateBack,
  replace
} from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeWidth, moderateScale } from "../../helpers/size.helper";
import {
  event,
  productClass,
  VOUCHER_TYPE,
  productCodeType
} from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import DeleteOfferDialog from "./delete-offer-dialog";
import Api from "../../api/api";
import { removeVoucher } from "../../actions/vouchers.action";
import NotificationView from "../../components/common/notification-view";
import LoadingIndicator from "../../components/common/loading-indicator";
import EventRegister from "../../helpers/event-register.helper";
import CacheImage from "../../components/common/cache-image";
import { IC_HOTLINE_RED, IC_FANPAGE_RED } from "../../commons/images";
import { makePhoneCall, openLinkUrl, getProductType } from "../../helpers/common.helper";
import styles from "./vocher.styles";
import AppImageURI from "../../components/AppImageURI";
import SeparatorLine from "../../components/common/separator-line";
import { PAGE_NOT_FOUND_SCREEN } from "../../navigators/screensName";
import TouchableIcon from "../../components/common/touchable-icon";
import { showMessage } from "../../helpers/error.helper";
import Button from "../../components/common/button";
import UseOfferDialog from "./use-offer-dialog";
import { checkApiStatus } from "../../helpers/app.helper";

class VoucherScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popupVisible: false,
      removing: false,
      listCode: [],
      selectedCodeIndex: 0,
      codeConfirm: null,
      voucher: {},
      loading: true,
      using: false,
      product: {},
      info: {},
      isFromNotification: false,
      is_check_otp: false,
      otpCode: null,
      otpRemainingTime: 0,
      is_customer_marked_use: false,
      item: "",
      type: ""
    };
  }

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    this.mounted = true;
    try {
      this.setState({ loading: true });
      const {
        codeId,
        productSlug,
        type,
        isGift,
        isFromNotification
      } = this.props.navigation.state.params;
      let listCodeData = [];
      if (codeId) {
        if (isGift) {
          listCodeData = await Api.getSavedProduct(
            productSlug,
            productCodeType.rewardCode
          );
        } else {
          listCodeData = await Api.getSavedProduct(productSlug);
        }
        const res = await Api.getSavedCode(productSlug, codeId);
        listCodeData = {
          data: {
            ...listCodeData.data,
            listcode: [res.data]
          }
        };
      } else {
        if (isGift) {
          listCodeData = await Api.getSavedProduct(
            productSlug,
            productCodeType.rewardCode
          );
        } else {
          listCodeData = await Api.getSavedProduct(productSlug);
        }
      }
      const listCode = listCodeData.data.listcode;
      // is_customer_marked_use = true => used
      const is_customer_marked_use = lodash.get(
        listCodeData,
        "data.is_customer_marked_use"
      );
      const {
        dealset,
        product,
        deal_reward,
        is_check_otp,
        campaign_type
      } = listCodeData.data;
      const voucher = { dealset };
      const title = lodash.get(listCodeData, "data.title");
      if (title) voucher.title = title;
      const codeIndex = listCode.findIndex(e => e.id == codeId);
      let selectedCodeIndex = 0;
      if (codeId) selectedCodeIndex = codeIndex < 0 ? 0 : codeIndex;

      let otpData;
      if (is_check_otp) {
        const code = lodash.get(listCode, `[${selectedCodeIndex}].code`);
        otpData = await Api.checkOtp(code);
      }
      this.setState({
        listCode,
        is_customer_marked_use,
        type: campaign_type,
        voucher,
        loading: false,
        product: lodash.get(product, "product")
          ? lodash.get(product, "product")
          : product,
        info: lodash.get(product, "info"),
        selectedCodeIndex,
        isFromNotification,
        deal_reward,
        is_check_otp,
        otpCode: lodash.get(otpData, "data[0]"),
        otpRemainingTime: lodash.get(otpData, "data[1]"),
        pClass: type,
        isGift
      });
      if (is_check_otp) this.startRemainingCountdown();
      this.codeUsedEvent = EventRegister.on(event.codeUsed, this.onCodeUsed);
    } catch (err) {
      this.props.replace(PAGE_NOT_FOUND_SCREEN);
      // eslint-disable-next-line no-unused-expressions
      this.mounted && this.setState({ loading: false });
    }
  };

  onCodeUsed = async data => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    try {
      const { listCode } = this.state;
      const { productSlug } = this.props.navigation.state.params;
      EventRegister.emit(event.removedCode, productSlug);
      const code = listCode.find(item => item.id == data.code_id);
      if (code) {
        function sleep(ms) {
          return new Promise(resolve => setTimeout(resolve, ms));
        }
        // To fix below bug. This only happen sometimes on iPhone devices. Not sure sleep technique will help here
        // YOLLA-499
        // Yolla-App-99-84][Redeem code] Should be switched the screen when the code has been used
        await sleep(2000);
        const res = await Api.getSavedCode(data.slug, data.code_id);
        const updateCode = res.data;
        this.setState({
          listCode: listCode.map(item => {
            if (item.id === updateCode.id) {
              // update status when receive notification
              item.status = updateCode.status;
            }
            return item;
          })
        });
      }
    } catch (err) {}
  };

  componentWillUnmount = () => {
    this.mounted = false;
    if (this.remainingInterval) clearInterval(this.remainingInterval);
    EventRegister.off(this.codeUsedEvent);
  };

  navigateToOfferDetail = () => {
    const { product, info } = this.state;
    if (!product) return;
    if (
      typeof product.campaign_has_product === "undefined" ||
      product.campaign_has_product
    ) {
      this.props.navigateToPage("OfferDetail", {
        productId: product.id,
        productSlug: product.slug
      });
    } else {
      const url =
        lodash.get(info, "url") || lodash.get(product, "attributes.campaign_page_url");
      if (url) Linking.openURL(url);
    }
  };

  navigateToUseCode = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    try {
      this.setState({ using: true });
      const { listCode, selectedCodeIndex } = this.state;
      const code_id = lodash.get(listCode, `[${selectedCodeIndex}].id`);
      const data = await Api.useMultiCode({ code_id });
      const { voucher } = this.state;
      this.props.navigateToPage("UseCode", { voucher, code: data.data });
      this.setState({ using: false });
    } catch (err) {
      this.setState({ using: false });
    }
  };

  renderLoading = () => {
    return <LoadingIndicator />;
  };

  // renderHeaderReward = () => {
  //   const { deal_reward } = this.state;
  //   return (
  //     <View style={styles.rwConainer}>
  //       <AppImageURI
  //         uri={lodash.get(deal_reward, "deal_reward_img", "")}
  //         resizeMode="cover"
  //         style={styles.rwImage}
  //       />
  //       <Text style={styles.rwlabel}>
  //         {lodash.get(deal_reward, "deal_reward_title", "")}
  //       </Text>
  //     </View>
  //   );
  // };

  getRemainingTime = time => {
    const minutes = Math.floor(time / 60);
    const seconds = time - minutes * 60;
    return `${`0${minutes}`.slice(-2)}:${`0${seconds}`.slice(-2)}`;
  };

  startRemainingCountdown = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    if (this.remainingInterval) clearInterval(this.remainingInterval);
    this.remainingInterval = setInterval(async () => {
      const { otpRemainingTime, selectedCodeIndex, listCode } = this.state;
      if (otpRemainingTime > 0) {
        this.setState({
          otpRemainingTime: otpRemainingTime - 1
        });
      } else {
        clearInterval(this.remainingInterval);
        const code = lodash.get(listCode, `[${selectedCodeIndex}].code`);
        const otpData = await Api.checkOtp(code);
        this.setState({
          otpCode: lodash.get(otpData, "data[0]"),
          otpRemainingTime: lodash.get(otpData, "data[1]")
        });
        this.startRemainingCountdown();
      }
    }, 1000);
  };

  onIndexChanged = async index => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { is_check_otp, listCode } = this.state;
    this.setState({ selectedCodeIndex: index });
    if (is_check_otp) {
      const code = lodash.get(listCode, `[${index}].code`);
      const otpData = await Api.checkOtp(code);
      this.setState({
        otpCode: lodash.get(otpData, "data[0]"),
        otpRemainingTime: lodash.get(otpData, "data[1]")
      });
      this.startRemainingCountdown();
    }
  };

  render() {
    const {
      popupVisible,
      listCode,
      selectedCodeIndex,
      loading,
      is_check_otp,
      otpCode,
      otpRemainingTime,
      is_customer_marked_use,
      item,
      type,
      isGift
    } = this.state;
    const endDate = moment(lodash.get(listCode, `[${selectedCodeIndex}].end_datetime`));
    const now = moment();
    const diff = endDate.startOf("day").diff(now.startOf("day"), "days");
    const used = lodash.get(listCode, `[${selectedCodeIndex}].status`) === "Used";
    const isExpired = lodash.get(listCode, `[${selectedCodeIndex}].status`) === "Expired";
    const { product_class } = this.props.navigation.state.params;
    const isShowUse =
      (product_class === productClass.voucher ||
        product_class === productClass.shareToEarn ||
        product_class === productClass.yollaBizShareToEarn ||
        type === VOUCHER_TYPE.shareToEarn ||
        type === VOUCHER_TYPE.yollaBizShareToEarn ||
        type === VOUCHER_TYPE.eCoupon) &&
      is_customer_marked_use &&
      lodash.get(listCode, `[${selectedCodeIndex}].code`) !== item &&
      !used &&
      !isExpired;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.toolbar}>Chi tiết</Text>}
        />
        <View style={styles.middle}>
          {loading ? (
            this.renderLoading()
          ) : (
            <>
              <ScrollView contentContainerStyle={styles.root} bounces={false}>
                <View style={styles.body}>
                  <View style={styles.content}>
                    {is_check_otp && !used && !isExpired && (
                      <Text style={styles.period}>
                        Mã có hiệu lực trong{" "}
                        <Text style={styles.time}>
                          {this.getRemainingTime(otpRemainingTime)}
                        </Text>
                      </Text>
                    )}
                    {listCode.length > 1 ? (
                      <View style={styles.swiper}>
                        <Swiper
                          key={listCode.length}
                          ref={ref => (this.swiper = ref)}
                          nextButton={
                            <Image
                              style={styles.next}
                              source={require("../../../res/icon/next.png")}
                            />
                          }
                          prevButton={
                            <Image
                              source={require("../../../res/icon/previous.png")}
                              style={styles.previous}
                            />
                          }
                          style={Platform.OS === "android" ? styles.list : null}
                          loop={false}
                          index={selectedCodeIndex}
                          onIndexChanged={this.onIndexChanged}
                          showsPagination={false}
                          showsButtons
                        >
                          {listCode.map(child => (
                            <View key={child.code} style={styles.item}>
                              <View style={styles.qr}>
                                <QRCode
                                  value={child.code}
                                  size={sizeWidth(130)}
                                  bgColor={
                                    child.code === item || used || isExpired
                                      ? "rgb(204, 204, 204)"
                                      : "black"
                                  }
                                  fgColor="white"
                                />
                              </View>
                            </View>
                          ))}
                        </Swiper>
                      </View>
                    ) : (
                      <View style={styles.single}>
                        <QRCode
                          value={lodash.get(listCode, "[0].code")}
                          size={sizeWidth(130)}
                          bgColor={
                            lodash.get(listCode, "[0].code") === item || used || isExpired
                              ? "rgb(204, 204, 204)"
                              : "black"
                          }
                          fgColor="white"
                        />
                      </View>
                    )}
                    <View
                      style={
                        lodash.get(listCode, `[${selectedCodeIndex}].code`) === item ||
                        used ||
                        isExpired
                          ? styles.blurWrap
                          : styles.wrap
                      }
                    >
                      <Text
                        style={
                          lodash.get(listCode, `[${selectedCodeIndex}].code`) === item ||
                          used ||
                          isExpired
                            ? styles.blurCode
                            : styles.code
                        }
                      >
                        {lodash.get(listCode, `[${selectedCodeIndex}].code`)}
                      </Text>
                      {lodash.get(listCode, `[${selectedCodeIndex}].code`) === item ||
                      used ||
                      isExpired ? (
                        <View />
                      ) : (
                        <TouchableIcon
                          source={require("../../../res/icon/copy.png")}
                          style={styles.clipboardIcon}
                          iconStyle={styles.clipboardIconImg}
                          onPress={async () => {
                            await Clipboard.setString(
                              lodash.get(listCode, `[${selectedCodeIndex}].code`)
                            );
                            await showMessage("Saved to clipboard");
                          }}
                        />
                      )}
                    </View>
                    <Barcode
                      height={sizeWidth(35)}
                      lineColor={
                        lodash.get(listCode, `[${selectedCodeIndex}].code`) === item ||
                        isExpired ||
                        used
                          ? "rgb(204, 204, 204)"
                          : "black"
                      }
                      value={lodash.get(listCode, `[${selectedCodeIndex}].code`)}
                      format="CODE128"
                      width={1.5}
                    />
                    {is_check_otp &&
                      lodash.get(listCode, `[${selectedCodeIndex}].code`) !== item &&
                      !isExpired && (
                        <>
                          <Text style={styles.confirm}>Mã xác nhận sử dụng</Text>
                          <Text style={styles.otp}>{otpCode}</Text>
                          <View style={styles.separator} />
                        </>
                      )}
                    {/* <Text style={styles.desc}>
                      Cung cấp mã code tại cửa hàng để sử dụng
                    </Text> */}
                  </View>
                  <SeparatorLine />
                  {this.renderVoucherInfo()}
                  <SeparatorLine />
                  <View style={styles.footer}>
                    <View style={styles.row}>
                      <Text style={styles.label}>Hạn sử dụng</Text>
                      <Text style={styles.text}>
                        {used ||
                        lodash.get(listCode, `[${selectedCodeIndex}].code`) === item
                          ? "Đã sử dụng"
                          : isExpired
                          ? "Đã hết hạn"
                          : diff === 0
                          ? "Hết hạn hôm nay"
                          : `Còn ${diff} ngày`}
                      </Text>
                    </View>
                    <View style={styles.row}>
                      <Text style={styles.label}>Ngày hết hạn</Text>
                      <Text style={styles.text}>{endDate.format("DD/MM/YYYY")}</Text>
                    </View>
                  </View>
                </View>
                <View
                  style={[
                    styles.body,
                    { marginBottom: isShowUse ? moderateScale(66) : sizeWidth(7) }
                  ]}
                >
                  {isGift && this.renderCampaignInfo()}
                  {this.renderMerchantInfo()}
                </View>
              </ScrollView>
              {isShowUse && (
                <Button
                  style={styles.btnUse}
                  text="SỬ DỤNG ƯU ĐÃI"
                  onPress={() => this.showUseDialog()}
                />
              )}
              <Dialog
                visible={popupVisible}
                width={sizeWidth(261)}
                onTouchOutside={() => {
                  this.setState({ popupVisible: false });
                }}
                dialogAnimation={
                  new FadeAnimation({
                    toValue: 0,
                    animationDuration: 150,
                    useNativeDriver: true
                  })
                }
              >
                {/*
                <DeleteOfferDialog
                  title={title}
                  onCancelPress={() => this.setState({ popupVisible: false })}
                  onDeletePress={this.removeOffer}
                />
                */}
                <UseOfferDialog
                  code={
                    listCode.length > 1
                      ? lodash.get(listCode, `[${selectedCodeIndex}].code`)
                      : lodash.get(listCode, "[0].code")
                  }
                  type={product_class}
                  onDone={() => {
                    this.setState({
                      popupVisible: false,
                      item:
                        listCode.length > 1
                          ? lodash.get(listCode, `[${selectedCodeIndex}].code`)
                          : lodash.get(listCode, "[0].code")
                    });
                  }}
                />
              </Dialog>
            </>
          )}
          <NotificationView />
        </View>
      </View>
    );
  }

  renderVoucherInfo = () => {
    const {
      voucher,
      listCode,
      selectedCodeIndex,
      product,
      pClass,
      isGift,
      deal_reward
    } = this.state;
    const valueReward = lodash.get(voucher, "dealset.value_reward");
    const isOnline = lodash.get(product, "campaign_type") === "online";
    let cover = {};
    let title = "";
    if (isGift) {
      cover = lodash.get(deal_reward, "deal_reward_img");
      title = lodash.get(listCode, `[${selectedCodeIndex}].name`);
    } else {
      const images = lodash.get(product, "images", []);
      const image = images.find(el => el.caption === "1:1" && el.display_order === 0);
      cover = lodash.get(image, "original.thumbnail") || lodash.get(image, "original");
      title = lodash.get(product, "title");
    }
    return (
      <TouchableOpacity onPress={this.navigateToOfferDetail}>
        <View style={styles.infoContainer}>
          <View style={styles.infoBody}>
            <View style={styles.infoContent}>
              <Text style={styles.infoTitle} numberOfLines={2}>
                {title}
              </Text>
              {isGift || valueReward ? (
                <View style={styles.infoMoney}>
                  <Text style={styles.infoReward}>
                    {isOnline ? "eVoucher" : "Tiền thưởng"}
                  </Text>
                  <Text style={styles.infoAmount}>
                    {numeral(lodash.get(voucher, "dealset.value_reward")).format("0,0")}
                    <Text style={styles.infoUnit}>đ</Text>
                  </Text>
                </View>
              ) : (
                <View style={styles.infoMoney}>
                  <Text style={styles.infoReward}>{getProductType(pClass)}</Text>
                </View>
              )}
            </View>
            <View style={styles.infoCover}>
              <CacheImage uri={cover} style={styles.infoImage} />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  renderCampaignInfo = () => {
    const { product, isGift, pClass } = this.state;
    const images = lodash.get(product, "images", []);
    const image = images.find(el => el.caption === "1:1" && el.display_order === 0);
    const cover =
      lodash.get(image, "original.thumbnail") || lodash.get(image, "original");
    return (
      <>
        <TouchableOpacity
          onPress={this.navigateToOfferDetail}
          style={{ marginVertical: sizeWidth(9) }}
        >
          <View style={[styles.infoContainer]}>
            <View style={styles.infoBody}>
              <View style={styles.infoContent}>
                <Text style={styles.infoTitle}>{lodash.get(product, "title", "")}</Text>
                {!isGift && (
                  <View style={styles.infoMoney}>
                    <Text style={styles.infoReward}>{getProductType(pClass)}</Text>
                  </View>
                )}
              </View>
              <View style={styles.infoCover}>
                <CacheImage uri={cover} resizeMode="cover" style={styles.infoImage} />
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <SeparatorLine />
      </>
    );
  };

  renderMerchantInfo = () => {
    const { product } = this.state;
    const logo = lodash.get(product, "merchant.logo_image.thumbnail");
    const hotline = lodash.get(product, "merchant.hot_line");
    const fanPage = lodash.get(product, "merchant.fan_page");
    const merchantCode = lodash.get(product, "merchant.code_merchant");
    const productCount = lodash.get(product, "merchant.total_product");
    return (
      <View style={styles.infoContainer}>
        <View style={styles.merchantWrapper}>
          <TouchableOpacity onPress={() => this.navigateToMerchantDetail(merchantCode)}>
            <View style={styles.merchantBody}>
              <View style={styles.merchantContent}>
                <View style={styles.merchantLogoWrap}>
                  <Image
                    style={styles.merchantLogo}
                    resizeMode="stretch"
                    source={
                      logo ? { uri: logo } : require("../../../res/img/default-image.png")
                    }
                  />
                </View>
                <View>
                  <Text style={styles.merchantName}>
                    {lodash.get(product, "merchant.name")}
                  </Text>
                  {productCount && (
                    <Text
                      style={styles.merchantOfferCount}
                    >{`${productCount} ưu đãi`}</Text>
                  )}
                </View>
              </View>
              <Image
                style={styles.merchantShop}
                source={require("../../../res/icon/merchant-arrow-right.png")}
              />
            </View>
          </TouchableOpacity>
          {(!!hotline || !!fanPage) && (
            <View style={styles.merchantBottom}>
              {!!hotline && (
                <View style={styles.row}>
                  <View style={{ flexDirection: "row" }}>
                    <Image
                      source={IC_HOTLINE_RED}
                      resizeMode="contain"
                      style={styles.bottomIcon}
                    />
                    <Text style={styles.merchantLabel}>Hotline</Text>
                  </View>
                  <TouchableOpacity onPress={() => makePhoneCall(hotline)}>
                    <Text style={styles.merchantValue}>{hotline}</Text>
                  </TouchableOpacity>
                </View>
              )}
              {!!hotline && !!fanPage && <View style={styles.merchantLine} />}
              {!!fanPage && (
                <View style={styles.row}>
                  <View style={{ flexDirection: "row" }}>
                    <Image
                      source={IC_FANPAGE_RED}
                      resizeMode="contain"
                      style={styles.bottomIcon}
                    />
                    <Text style={styles.merchantLabel}>Fanpage</Text>
                  </View>
                  <TouchableOpacity onPress={() => openLinkUrl(fanPage)}>
                    <Text style={styles.merchantValue} numberOfLines={1}>
                      {lodash.get(product, "merchant.username_fan_page")}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
          )}
        </View>
      </View>
    );
  };

  removeOffer = async () => {
    const { listCode, voucher, selectedCodeIndex } = this.state;
    const { type } = this.props.navigation.state.params;
    const codeId = lodash.get(listCode, `[${selectedCodeIndex}].id`);
    this.setState({ removing: true, popupVisible: false });
    try {
      const { productSlug } = this.props.navigation.state.params;
      await Api.removeCode(productSlug, codeId);

      const data = await Api.getSavedProduct(productSlug);
      EventRegister.emit(event.removedCode, productSlug);
      const listCode = data.data.listcode;
      if (listCode.length === 0) {
        this.props.removeVoucher(voucher);
        this.props.navigateBack();
      } else {
        this.setState({ listCode, selectedCodeIndex: 0 });
      }
    } catch (err) {
      this.setState({ removing: false });
    }
  };

  showUseDialog = () => {
    this.setState({
      ...this.state,
      popupVisible: true
    });
  };

  showDeleteDialog = () => {
    this.setState({
      popupVisible: true
    });
  };

  navigateToMerchantDetail = merchantCode => {
    this.props.navigateToPage("MerchantDetail", { merchantCode });
  };
}

export default connect(
  null,
  {
    resetPage,
    navigateToPage,
    navigateBack,
    removeVoucher,
    replace
  }
)(VoucherScreen);
