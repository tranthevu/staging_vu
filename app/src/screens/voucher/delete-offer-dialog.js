import React, { Component } from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import { sizeWidth, sizeFont } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { font, appColor } from "../../constants/app.constant";

export default class DeleteOfferDialog extends Component {
  render() {
    const { onDeletePress, onCancelPress, title } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Xoá ưu đãi</Text>
        <Text style={styles.text}>
          Bạn có chắc muốn xóa ưu đãi{" "}
          <Text style={styles.highlight}>{title}</Text> không?
        </Text>
        <View style={styles.bottom}>
          <Text onPress={onCancelPress} style={styles.no}>
            Không
          </Text>
          <Text style={styles.delete} onPress={onDeletePress}>
            Xóa
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: sizeWidth(15),
    justifyContent: "center",
    height: sizeWidth(159)
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.medium
  },
  text: {
    marginVertical: sizeWidth(12)
  },
  highlight: {
    fontFamily: font.medium
  },
  bottom: {
    alignSelf: "flex-end",
    flexDirection: "row"
  },
  no: {
    fontFamily: font.medium,
    marginRight: sizeWidth(35)
  },
  delete: {
    fontFamily: font.medium,
    color: appColor.primary
  }
});
