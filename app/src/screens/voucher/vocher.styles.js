import { StyleSheet, Dimensions } from "react-native";
import { font, appColor } from "../../constants/app.constant";
import { sizeFont, moderateScale, sizeWidth } from "../../helpers/size.helper";

const screen = Dimensions.get("window");

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F2F2F2"
  },
  middle: {
    flex: 1
  },
  toolbar: {
    fontFamily: font.bold,
    fontSize: sizeFont(18),
    color: "white"
  },
  delete: {
    alignSelf: "center",
    color: appColor.blur,
    fontFamily: font.medium,
    marginBottom: moderateScale(16)
  },
  body: {
    width: sizeWidth(302),
    overflow: "hidden",
    borderRadius: sizeWidth(6),
    backgroundColor: appColor.white,
    alignSelf: "center",
    marginTop: sizeWidth(7)
  },
  header: {
    height: sizeWidth(95),
    paddingHorizontal: sizeWidth(16),
    alignItems: "center",
    justifyContent: "center"
  },
  swiper: {
    height: sizeWidth(130)
  },
  list: {
    width: sizeWidth(320)
  },
  image: {
    backgroundColor: "red",
    width: sizeWidth(130),
    height: sizeWidth(130)
  },
  title: {
    textAlign: "center",
    fontFamily: font.medium
  },
  more: {
    color: "#55ACEE",
    marginTop: sizeWidth(8)
  },
  footer: {
    paddingHorizontal: sizeWidth(16),
    paddingVertical: sizeWidth(12),
    justifyContent: "center"
  },
  row: {
    width: "100%",
    flexDirection: "row",
    paddingVertical: sizeWidth(6),
    justifyContent: "space-between"
  },
  label: {
    fontSize: sizeFont(12),
    fontFamily: font.medium
  },
  text: {
    fontSize: sizeFont(12)
  },
  content: {
    alignItems: "center",
    overflow: "hidden",
    marginLeft: sizeWidth(-9),
    width: sizeWidth(320),
    paddingVertical: sizeWidth(15)
  },
  root: {
    paddingHorizontal: sizeWidth(9),
    paddingVertical: sizeWidth(7)
  },
  qr: {
    width: sizeWidth(130),
    height: sizeWidth(130)
  },
  item: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  single: {
    width: sizeWidth(130),
    height: sizeWidth(130)
  },
  wrap: {
    width: sizeWidth(200),
    height: sizeWidth(34),
    borderRadius: sizeWidth(17),
    borderWidth: 1,
    borderColor: "#DDDDDD",
    justifyContent: "center",
    alignItems: "center",
    marginVertical: moderateScale(15),
    borderStyle: "solid",
    flexDirection: "row",
    paddingHorizontal: sizeWidth(10)
  },
  blurWrap: {
    width: sizeWidth(200),
    height: sizeWidth(34),
    borderRadius: sizeWidth(17),
    borderWidth: 1,
    borderColor: appColor.blur,
    justifyContent: "center",
    alignItems: "center",
    marginVertical: moderateScale(15),
    borderStyle: "solid",
    flexDirection: "row",
    paddingHorizontal: sizeWidth(10)
  },
  blurCode: {
    fontSize: sizeFont(14),
    fontFamily: font.bold,
    color: appColor.blur,
    textDecorationLine: "line-through"
  },
  btnUse: {
    width: screen.width - 50,
    alignSelf: "center",
    position: "absolute",
    backgroundColor: appColor.primary,
    bottom: sizeWidth(14)
  },
  btnUse_blur: {
    width: screen.width - 50,
    alignSelf: "center",
    backgroundColor: appColor.blur,
    marginVertical: sizeWidth(3)
  },
  code: {
    fontSize: sizeFont(14),
    fontFamily: font.medium
  },
  clipboardIcon: {
    position: "absolute",
    alignSelf: "center",
    right: 10
  },
  clipboardIconImg: {
    width: moderateScale(16),
    height: moderateScale(16)
  },
  desc: {
    width: sizeWidth(170),
    textAlign: "center",
    color: appColor.blur,
    marginTop: moderateScale(10)
  },
  line: {
    width: "94%",
    alignSelf: "center",
    height: 1
  },
  circle: {
    backgroundColor: appColor.bg,
    position: "absolute",
    width: sizeWidth(18),
    height: sizeWidth(18),
    borderRadius: sizeWidth(9)
  },
  left: {
    left: sizeWidth(-9)
  },
  top: {
    top: sizeWidth(83)
  },
  bottom: {
    bottom: sizeWidth(72)
  },
  right: {
    right: sizeWidth(-9)
  },
  use: {
    position: "absolute",
    bottom: moderateScale(15)
  },
  next: {
    width: moderateScale(13),
    height: moderateScale(21),
    margin: moderateScale(10)
  },
  previous: {
    width: moderateScale(13),
    height: moderateScale(21),
    margin: moderateScale(10)
  },
  offer: {
    backgroundColor: "white",
    flexDirection: "row",
    borderRadius: moderateScale(6),
    marginTop: moderateScale(10),
    padding: moderateScale(10)
  },
  cover: {
    width: moderateScale(60),
    height: moderateScale(60),
    borderRadius: moderateScale(6),
    borderWidth: 1,
    borderColor: "#DDDDDD"
  },
  info: {
    flex: 1,
    marginLeft: moderateScale(10)
  },
  name: {
    fontSize: sizeFont(13),
    fontFamily: font.medium
  },
  merchant: {
    marginTop: moderateScale(11),
    flexDirection: "row",
    alignItems: "center"
  },
  avatar: {
    width: moderateScale(16),
    height: moderateScale(16),
    borderRadius: moderateScale(8)
  },
  value: {
    flex: 1,
    fontSize: sizeFont(12),
    marginLeft: sizeWidth(5)
  },
  bottomIcon: {
    width: moderateScale(18),
    height: moderateScale(18)
  },
  detailBottom: {
    width: "100%",
    flexDirection: "row",
    marginTop: sizeWidth(5),
    marginBottom: sizeWidth(14)
  },
  bottomSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: moderateScale(30)
  },
  hotline: {
    borderRightWidth: 0.5,
    borderRightColor: "#DDDDDD"
  },
  fanpage: {
    borderLeftWidth: 0.5,
    borderLeftColor: "#DDDDDD"
  },
  rwConainer: {
    padding: sizeWidth(10),
    width: sizeWidth(302)
  },
  rwImage: {
    width: sizeWidth(282),
    backgroundColor: appColor.gray,
    borderRadius: sizeWidth(3),
    aspectRatio: 1.77
  },
  rwlabel: {
    marginTop: moderateScale(7),
    fontFamily: font.medium
  },
  confirm: {
    fontSize: sizeFont(14),
    fontFamily: font.bold
  },
  otp: {
    fontSize: sizeFont(16),
    marginTop: sizeWidth(8),
    letterSpacing: sizeWidth(5),
    marginBottom: sizeWidth(4)
  },
  separator: {
    width: sizeWidth(224),
    backgroundColor: "#DDDDDD",
    height: 1,
    marginBottom: sizeWidth(8)
  },
  period: {
    marginBottom: sizeWidth(12),
    fontSize: sizeFont(13)
  },
  time: {
    color: appColor.primary,
    fontSize: sizeFont(13)
  },

  infoContainer: {
    backgroundColor: "white",
    paddingHorizontal: sizeWidth(16),
    marginVertical: sizeWidth(9)
  },
  infoBody: {
    flexDirection: "row",
    paddingVertical: moderateScale(4)
  },
  infoContent: {
    flex: 1,
    marginRight: sizeWidth(10)
  },
  infoTitle: {
    fontSize: sizeFont(12),
    fontFamily: font.medium,
    marginBottom: moderateScale(8)
  },
  infoMoney: {
    flexDirection: "row",
    alignItems: "center"
  },
  infoReward: {
    fontSize: sizeFont(12),
    marginRight: sizeWidth(12)
  },
  infoAmount: {
    fontFamily: font.bold,
    flex: 1,
    color: appColor.primary
  },
  infoUnit: {
    fontFamily: font.bold,
    color: appColor.primary,
    textDecorationLine: "underline"
  },
  infoCover: {
    width: moderateScale(60),
    height: moderateScale(60),
    borderRadius: moderateScale(6),
    borderWidth: 1,
    borderColor: "#dddddd",
    overflow: "hidden"
  },
  infoImage: {
    width: moderateScale(60),
    height: moderateScale(60)
  },
  merchantBody: {
    flexDirection: "row",
    backgroundColor: "white",
    alignItems: "center",
    paddingVertical: moderateScale(15)
  },
  merchantContent: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center"
  },
  merchantLogo: {
    width: moderateScale(50),
    height: moderateScale(50),
    borderRadius: moderateScale(25)
  },
  merchantLogoWrap: {
    width: moderateScale(50),
    borderRadius: moderateScale(25),
    height: moderateScale(50),
    overflow: "hidden",
    marginRight: sizeWidth(8),
    borderWidth: 1,
    borderColor: "#DADADA"
  },
  merchantName: {
    fontSize: sizeFont(12),
    fontFamily: font.medium
  },
  merchantOfferCount: {
    fontSize: sizeFont(12)
  },
  merchantShop: {
    width: moderateScale(7),
    height: moderateScale(12)
  },
  merchantLabel: {
    marginLeft: sizeWidth(8),
    fontSize: sizeFont(12)
  },
  merchantValue: {
    fontSize: sizeFont(12),
    color: "#4080FF",
    width: moderateScale(160),
    textAlign: "right"
  },
  merchantLine: {
    width: "100%",
    alignSelf: "center",
    height: 1,
    backgroundColor: "#F1F1F1",
    marginVertical: sizeWidth(6)
  }
});
