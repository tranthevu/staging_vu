import React, { Component } from "react";
import { StyleSheet, View, TextInput, TouchableOpacity } from "react-native";
import { sizeWidth, sizeFont, moderateScale } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { font, appColor, productClass } from "../../constants/app.constant";
import Button from "../../components/common/button";
import Api from "../../api/api";
import { showMessage } from "../../helpers/error.helper";

export default class UseOfferDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      id: ""
    };
  }

  performAction = async (code, id, value_bill) => {
    if (id === undefined) {
      await Api.redeemptEcoupon(code, "", value_bill);
    } else {
      await Api.redeemptEcoupon(code, id, value_bill);
    }
  };

  renderForS2E = (value, id, code) => {
    return (
      <View style={styles.containerS2E}>
        <Text style={styles.title}>Giá trị hoá đơn *</Text>
        <TextInput
          placeholder={"Nhập giá trị hoá đơn"}
          placeholderTextColor={"#666"}
          value={value}
          keyboardType={"number-pad"}
          style={styles.text}
          onChangeText={value => {
            this.setState({
              ...this.state,
              value: value
            });
          }}
        />
        <Text style={styles.title}>Mã hoá đơn *</Text>
        <TextInput
          placeholder={"Nhập mã hoá đơn"}
          placeholderTextColor={"#666"}
          value={id}
          style={styles.text}
          onChangeText={value => {
            this.setState({
              ...this.state,
              id: value
            });
          }}
        />
        <View style={styles.bottom}>
          <Button
            text={"XONG"}
            onPress={() => {
              if (
                id !== null &&
                id !== undefined &&
                id !== "" &&
                value !== null &&
                value !== undefined &&
                value !== ""
              ) {
                this.props.onDone();
                this.performAction(code, id, value);
              } else if (
                id === null ||
                id === undefined ||
                id === "" ||
                value === null ||
                value === undefined ||
                value === ""
              ) {
                showMessage("Vui lòng cung cấp đầy đủ thông tin !");
              }
            }}
            style={styles.button}
          />
        </View>
      </View>
    );
  };

  renderForECoupon = (value, code) => {
    return (
      <View style={styles.containerECoupon}>
        <Text style={styles.title}>Giá trị hoá đơn (không bắt buộc)</Text>
        <TextInput
          placeholder={"Nhập giá trị hoá đơn"}
          placeholderTextColor={"#666"}
          value={value}
          keyboardType={"number-pad"}
          style={styles.text}
          onChangeText={value => {
            this.setState({
              ...this.state,
              value: value
            });
          }}
        />
        <View style={styles.bottom}>
          <Button
            text={"XONG"}
            onPress={() => {
              this.props.onDone();
              this.performAction(code, "", value);
            }}
            style={styles.button}
          />
        </View>
      </View>
    );
  };

  render() {
    const { value, id } = this.state;
    const { code, type } = this.props;
    return (
      <View>
        {type === productClass.shareToEarn
          ? this.renderForS2E(value, id, code)
          : this.renderForECoupon(value, code)}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerECoupon: {
    paddingHorizontal: sizeWidth(15),
    paddingTop: sizeWidth(30),
    height: sizeWidth(179)
  },
  containerS2E: {
    paddingHorizontal: sizeWidth(15),
    paddingTop: sizeWidth(30),
    height: sizeWidth(239)
  },
  title: {
    fontSize: sizeFont(15),
    fontFamily: font.bold
  },
  text: {
    marginVertical: sizeWidth(12),
    borderBottomWidth: 0.2,
    borderBottomColor: "#333",
    paddingVertical: sizeWidth(12)
  },
  highlight: {
    fontFamily: font.medium
  },
  bottom: {
    flexDirection: "row",
    justifyContent: "center",
    position: "absolute",
    bottom: 20
  },
  no: {
    fontFamily: font.medium,
    marginRight: sizeWidth(35)
  },
  delete: {
    fontFamily: font.medium,
    color: appColor.primary
  },
  button: {
    width: sizeWidth(240),
    marginHorizontal: moderateScale(30),
    alignSelf: "center"
  }
});
