import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, text, locations } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import Button from "../../components/common/button";
import Dropdown from "../../components/common/dropdown";
import moment from "moment";
import validator from "validator";
import Toast from "@remobile/react-native-toast";
import Api from "../../api/api";
import lodash from "lodash";
import { saveLocation, getLocation } from "../../helpers/storage.helper";
import { requestedTopCategories } from "../../actions/top-categories.action";
import { requestedTopMerchants } from "../../actions/top-merchants.action";
import { requestedFilters } from "../../actions/filters.action";
import NotificationView from "../../components/common/notification-view";

class UpdateLocationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedLocation: null
    };
  }

  saveLocation = async () => {
    const { selectedLocation } = this.state;
    await saveLocation(lodash.get(selectedLocation, "key"));
    this.props.requestedTopCategories();
    this.props.requestedTopMerchants();
    this.props.requestedFilters();
    this.props.resetPage("Main");
  };

  componentDidMount = async () => {
    const location = await getLocation();
    const selectedLocation = locations.find(item => item.key === location);
    this.setState({
      selectedLocation
    });
  };

  render(): ReactNode {
    const { selectedLocation } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Vị trí của bạn</Text>}
        />
        <View style={styles.body}>
          <Dropdown
            options={locations}
            path="name"
            onValueChange={value => this.setState({ selectedLocation: value })}
            label="Vị trí của bạn"
            value={lodash.get(selectedLocation, "name") || "Toàn quốc"}
            onPress={this.onNavigateAreaSelection}
          />
          <Button style={styles.button} onPress={this.saveLocation} text="LƯU" />
          <NotificationView />
        </View>
      </View>
    );
  }

  onNavigateAreaSelection = () => {
    const { navigateToPage } = this.props;
    navigateToPage("SelectionList", {
      title: "Vị trí của bạn",
      isMultipleChoice: false,
      items: !lodash.isEmpty(locations)
        ? locations.map(item => {
            return {
              ...item,
              text: item.name,
              value: item.key
            };
          })
        : [],
      onUpdateSelection: selectedLocation => {
        this.setState({ selectedLocation });
      }
    });
  };
}

export default connect(
  null,
  {
    resetPage,
    navigateToPage,
    navigateBack,
    requestedTopCategories,
    requestedTopMerchants,
    requestedFilters
  }
)(UpdateLocationScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  body: {
    flex: 1,
    justifyContent: "space-between"
  },
  button: {
    marginBottom: sizeWidth(12)
  }
});
