import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, ScrollView, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import HTML from "react-native-render-html";
import LinearGradient from "react-native-linear-gradient";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import PromotionItem from "../dashboard/promotion-item";
import TouchableIcon from "../../components/common/touchable-icon";
import PromotionItemList from "../dashboard/promotion-item-list";

const viewType = {
  grid: 0,
  list: 1
};

class OverviewTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedViewType: viewType.list
    };
  }

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  render(): ReactNode {
    const { selectedViewType } = this.state;
    const { onReadMorePress, merchant } = this.props;
    const introduction = lodash.get(merchant, "introduction") || "";
    const products = lodash.get(merchant, "list_product", []);
    const showReadMore = typeof introduction === "string" && introduction.length > 300;
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.content}>
          <View style={styles.info}>
            <Text style={styles.introduce}>Giới thiệu</Text>
            <View style={styles.html}>
              {!!introduction && (
                <HTML
                  baseFontStyle={{
                    fontFamily: font.regular,
                    color: appColor.text,
                    fontSize: sizeFont(14)
                  }}
                  ignoredStyles={[
                    "font-family",
                    "font-weight",
                    "letter-spacing",
                    "line-height",
                    "display",
                    "font-size",
                    "flex"
                  ]}
                  html={introduction}
                  imagesMaxWidth={sizeWidth(302)}
                />
              )}
            </View>
            {showReadMore && (
              <LinearGradient
                colors={["rgba(255, 255, 255, 0.5)", "#FFFFFF"]}
                style={styles.overlay}
              />
            )}
          </View>
          {showReadMore && (
            <TouchableOpacity onPress={onReadMorePress} style={styles.more}>
              <Text style={styles.see}>Xem thêm</Text>
              <Image
                style={styles.arrow}
                source={require("../../../res/icon/expand.png")}
              />
            </TouchableOpacity>
          )}
        </View>
        <View style={styles.header}>
          <Text style={styles.label}>Ưu đãi</Text>
          <TouchableIcon
            style={styles.view}
            iconStyle={styles.viewIcon}
            onPress={this.toggleViewType}
            source={
              selectedViewType === viewType.list
                ? require("../../../res/icon/list.png")
                : require("../../../res/icon/grid.png")
            }
          />
        </View>
        {products.map(this.renderPromotion)}
      </ScrollView>
    );
  }

  renderPromotion = (item, index) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem
        hideMerchant
        item={item}
        key={index.toString()}
        style={styles.promotion}
      />
    ) : (
      <PromotionItemList
        hideMerchant
        item={item}
        key={index.toString()}
        style={styles.promotion}
      />
    );
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(OverviewTab);

const styles = StyleSheet.create({
  container: {
    backgroundColor: appColor.bg
  },
  list: {
    marginBottom: sizeWidth(12)
  },
  content: {
    backgroundColor: "white",
    borderRadius: sizeWidth(6),
    marginTop: moderateScale(10),
    alignSelf: "center",
    width: sizeWidth(302),
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 2,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    }
  },
  html: {
    height: moderateScale(105),
    overflow: "hidden"
  },
  info: {
    paddingVertical: moderateScale(15),
    paddingHorizontal: sizeWidth(11)
  },
  introduce: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    marginBottom: moderateScale(19)
  },
  desc: {
    fontSize: sizeFont(13),
    marginTop: moderateScale(5),
    lineHeight: sizeWidth(19)
  },
  title: {
    fontFamily: font.bold,
    fontSize: sizeFont(24),
    color: "#000000",
    paddingHorizontal: sizeWidth(11),
    marginVertical: sizeWidth(16)
  },
  line: {
    height: 1,
    backgroundColor: "#DADADA",
    width: "100%"
  },
  arrow: {
    width: sizeWidth(7),
    height: sizeWidth(11),
    tintColor: "#DDDDDD",
    marginLeft: sizeWidth(7)
  },
  see: {
    color: appColor.blur,
    fontSize: sizeFont(13)
  },
  more: {
    height: moderateScale(48),
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  promotion: {
    marginVertical: moderateScale(5)
  },
  label: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    flex: 1,
    marginRight: sizeWidth(12)
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(12)
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: sizeWidth(11),
    marginVertical: moderateScale(10)
  },
  overlay: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: moderateScale(60)
  }
});
