import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import StoreItem from "../offerDetail/store-item";

class StoreTab extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render(): ReactNode {
    const { partners } = this.props;
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <Text style={styles.title}>Cửa hàng</Text>
        {partners.map(item => (
          <StoreItem key={item.id} item={{ partnerstore: item }} />
        ))}
      </ScrollView>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(StoreTab);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: sizeWidth(9),
    paddingVertical: moderateScale(6)
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    marginTop: moderateScale(5),
    marginBottom: moderateScale(10)
  }
});
