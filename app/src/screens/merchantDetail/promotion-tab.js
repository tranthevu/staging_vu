import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";
import PromotionItem from "../dashboard/promotion-item";
import TouchableIcon from "../../components/common/touchable-icon";
import PaginationList from "../../components/common/pagination-list";
import PromotionItemList from "../dashboard/promotion-item-list";
import {
  requestMerchantProducts,
  fetchMoreMerchantProducts,
  refreshMerchantProducts
} from "../../actions/merchant-products.action";
import EmptyView from "../../components/common/empty-view";

const viewType = {
  grid: 0,
  list: 1
};

class PromotionTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedViewType: viewType.list
    };
  }

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  componentDidMount = () => {
    const { merchant } = this.props;
    if (merchant) this.props.requestMerchantProducts({ merchant: merchant.code });
  };

  renderHeader = () => {
    const { selectedViewType } = this.state;
    return (
      <View style={styles.header}>
        <Text style={styles.title}>Ưu đãi</Text>
        <TouchableIcon
          style={styles.view}
          iconStyle={styles.viewIcon}
          onPress={this.toggleViewType}
          source={
            selectedViewType === viewType.list
              ? require("../../../res/icon/list.png")
              : require("../../../res/icon/grid.png")
          }
        />
      </View>
    );
  };

  render(): ReactNode {
    const { selectedViewType } = this.state;
    const { merchant } = this.props;
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      merchantProducts
    } = this.props.merchantProducts;
    const { fetchMoreMerchantProducts, refreshMerchantProducts } = this.props;
    return (
      <View style={styles.container}>
        {!!merchant && (
          <PaginationList
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderPromotion}
            loading={loading}
            showsHorizontalScrollIndicator={false}
            ListHeaderComponent={this.renderHeader()}
            reachedEnd={reachedEnd}
            refreshing={refreshing}
            firstLoading={firstLoading}
            onEndReached={() => fetchMoreMerchantProducts({ merchant: merchant.code })}
            onRefresh={() => refreshMerchantProducts({ merchant: merchant.code })}
            data={merchantProducts}
            EmptyComponent={<EmptyView />}
          />
        )}
      </View>
    );
  }

  renderPromotion = ({ item }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem hideMerchant item={item} style={styles.promotion} />
    ) : (
      <PromotionItemList hideMerchant item={item} style={styles.promotion} />
    );
  };
}

export default connect(
  state => ({
    merchantProducts: state.merchantProducts
  }),
  {
    resetPage,
    navigateToPage,
    requestMerchantProducts,
    fetchMoreMerchantProducts,
    refreshMerchantProducts
  }
)(PromotionTab);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: sizeWidth(9),
    paddingVertical: moderateScale(6),
    flex: 1
  },
  promotion: {
    marginVertical: moderateScale(5)
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    flex: 1,
    marginRight: sizeWidth(12)
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(12)
  },
  header: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: moderateScale(5),
    marginBottom: moderateScale(10)
  }
});
