import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import HTML from "react-native-render-html";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";

class InfoTab extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render(): ReactNode {
    const { merchant } = this.props;
    const introduction = lodash.get(merchant, "introduction") || "";
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <Text style={styles.label}>Giới thiệu</Text>
        <View style={styles.content}>
          {!!introduction && (
            <HTML
              baseFontStyle={{
                fontFamily: font.regular,
                color: appColor.text,
                fontSize: sizeFont(14)
              }}
              ignoredStyles={[
                "font-family",
                "font-weight",
                "letter-spacing",
                "line-height",
                "display",
                "font-size"
              ]}
              html={introduction}
              imagesMaxWidth={sizeWidth(296)}
            />
          )}
        </View>
      </ScrollView>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(InfoTab);

const styles = StyleSheet.create({
  container: {
    paddingTop: moderateScale(6)
  },
  label: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    paddingHorizontal: sizeWidth(9),
    marginTop: moderateScale(5),
    marginBottom: moderateScale(10)
  },
  content: {
    backgroundColor: "white",
    padding: sizeWidth(12)
  }
});
