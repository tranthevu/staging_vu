import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, TouchableOpacity, AsyncStorage } from "react-native";
import { connect } from "react-redux";
import LinearGradient from "react-native-linear-gradient";
import lodash from "lodash";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import StoreTab from "./store-tab";
import InfoTab from "./info-tab";
import OverviewTab from "./overview-tab";
import PromotionTab from "./promotion-tab";
import SearchToolbar from "../../components/common/search-toolbar";
import Api from "../../api/api";
import CacheImage from "../../components/common/cache-image";
import LoadingIndicator from "../../components/common/loading-indicator";
import NotificationView from "../../components/common/notification-view";
import AuthHelper from "../../helpers/auth.helper";
import { checkApiStatus } from "../../helpers/app.helper";

const tabs = {
  overview: 0,
  promotion: 1,
  store: 2,
  info: 3
};

class MerchantDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: tabs.overview,
      merchant: null,
      loading: true,
      partners: [],
      following: false,
      isComeback: false
    };
  }

  getMerchantPartners = async merchantCode => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const data = await Api.getMerchantPartners(merchantCode);
    const areaPartners = Object.values(data.data);
    let partners = [];
    // eslint-disable-next-line no-plusplus
    for (let index = 0; index < areaPartners.length; index++) {
      partners = [...partners, ...areaPartners[index]];
    }
    return partners;
  };

  componentDidMount = async () => {
    try {
      this.setState({
        loading: true
      });
      const merchantCode = lodash.get(this.props.navigation.state.params, "merchantCode");
      const isComeback = lodash.get(this.props.navigation.state.params, "isComeback");

      if (isComeback === undefined) {
        await AsyncStorage.removeItem("beforeLogin:merchantId").then(res => {
          AsyncStorage.setItem("beforeLogin:merchantId", merchantCode);
        });
      }
      await AsyncStorage.getItem("beforeLogin:merchantId").then(async res => {
        const merchantCode = res;
        const isMaintenance = await checkApiStatus();
        if (isMaintenance) {
          this.props.resetPage("Maintenance");
        }
        const data = await Api.getMerchantDetail(merchantCode);
        const partners = await this.getMerchantPartners(merchantCode);
        const isAuthenticated = await AuthHelper.isAuthenticated();

        this.setState({
          merchant: {
            ...data.data,
            is_favorite: isAuthenticated ? data.data.is_favorite : false
          },
          loading: false,
          partners
        });
        //auto make the action before
        if (isAuthenticated) {
          const action = lodash.get(this.props.navigation.state.params, "action");
          if (action === "followMerchant") {
            this.setState({
              ...this.state,
              isComeback: true
            });
            this.followMerchant();
          }
        }
      });
    } catch (err) {
      this.setState({
        loading: false
      });
    }
  };

  followMerchant = async () => {
    try {
      const isMaintenance = await checkApiStatus();
      if (isMaintenance) {
        this.props.resetPage("Maintenance");
      }
      const isAuthenticated = await AuthHelper.isAuthenticated();
      if (!isAuthenticated) {
        const lastScreen = this.props.navigation.state.routeName;
        return this.props.navigateToPage("Login", {
          canBack: true,
          lastScreen: lastScreen,
          action: "followMerchant"
        });
      }
      const { merchant } = this.state;
      this.setState({
        following: true
      });
      await Api.toggleFavorite({
        merchantCode: merchant.code,
        isFavorite: !merchant.is_favorite
      });
      this.setState({
        following: false,
        merchant: {
          ...merchant,
          is_favorite: !merchant.is_favorite
        }
      });
    } catch (err) {
      this.setState({
        following: false
      });
    }
  };

  render(): ReactNode {
    const { merchant, loading, following, isComeback } = this.state;
    const name = lodash.get(merchant, "name");
    const cover = lodash.get(merchant, "image_cover_mobile");
    const logo = lodash.get(merchant, "logo_image");
    const isFavorite = lodash.get(merchant, "is_favorite");
    return (
      <View style={styles.container}>
        <SearchToolbar isComeback={isComeback} />
        <View style={styles.body}>
          {loading ? (
            <LoadingIndicator />
          ) : (
            <>
              <View style={styles.header}>
                <CacheImage
                  style={styles.cover}
                  uri={cover}
                  placeholder={require("../../../res/img/default-image.png")}
                />
                <View style={styles.info}>
                  <LinearGradient
                    colors={["rgba(0, 0, 0, 0)", "rgba(0, 0, 0, 0.5)"]}
                    style={styles.overlay}
                  />
                  <CacheImage
                    style={styles.logo}
                    uri={logo}
                    resizeMode="stretch"
                    placeholder={require("../../../res/img/default-image.png")}
                  />
                  <View style={styles.content}>
                    <Text style={styles.name}>{name}</Text>
                  </View>
                  <TouchableOpacity
                    disabled={following}
                    onPress={this.followMerchant}
                    style={styles.wrap}
                  >
                    <Image
                      style={styles.icon}
                      source={
                        isFavorite
                          ? require("../../../res/icon/tick-merchant.png")
                          : require("../../../res/icon/merchant-follow.png")
                      }
                    />
                    <Text style={styles.follow}>Theo dõi</Text>
                  </TouchableOpacity>
                </View>
              </View>
              {this.renderTabs()}
              {this.renderBody()}
            </>
          )}
          <NotificationView />
        </View>
      </View>
    );
  }

  renderTabs = () => {
    const { selectedTab } = this.state;
    return (
      <View style={styles.tabs}>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.overview })}
          style={styles.tab}
        >
          <Text style={selectedTab === tabs.overview ? styles.activeText : styles.text}>
            Tổng quan
          </Text>
          {selectedTab === tabs.overview && <View style={styles.line} />}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.promotion })}
          style={styles.tab}
        >
          <Text style={selectedTab === tabs.promotion ? styles.activeText : styles.text}>
            Ưu đãi
          </Text>
          {selectedTab === tabs.promotion && <View style={styles.line} />}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.store })}
          style={styles.tab}
        >
          <Text style={selectedTab === tabs.store ? styles.activeText : styles.text}>
            Cửa hàng
          </Text>
          {selectedTab === tabs.store && <View style={styles.line} />}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.info })}
          style={styles.tab}
        >
          <Text style={selectedTab === tabs.info ? styles.activeText : styles.text}>
            Giới thiệu
          </Text>
          {selectedTab === tabs.info && <View style={styles.line} />}
        </TouchableOpacity>
      </View>
    );
  };

  renderBody = () => {
    const { selectedTab, merchant, partners } = this.state;
    switch (selectedTab) {
      case tabs.overview:
        return (
          <OverviewTab
            merchant={merchant}
            onReadMorePress={() => this.setState({ selectedTab: tabs.info })}
          />
        );
      case tabs.promotion:
        return <PromotionTab merchant={merchant} />;
      case tabs.store:
        return <StoreTab partners={partners} merchant={merchant} />;
      case tabs.info:
        return <InfoTab merchant={merchant} />;
      default:
        return null;
    }
  };
}

export default connect(null, { resetPage, navigateToPage })(MerchantDetailScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  cover: {
    width: sizeWidth(320),
    height: sizeWidth(180),
    position: "absolute"
  },
  info: {
    width: sizeWidth(320),
    height: moderateScale(80),
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: sizeWidth(9)
  },
  logo: {
    width: moderateScale(60),
    borderRadius: moderateScale(30),
    height: moderateScale(60),
    marginRight: sizeWidth(11)
  },
  content: {
    flex: 1
  },
  name: {
    color: "#FFFFFF",
    fontSize: sizeFont(14),
    fontFamily: font.bold
  },
  header: {
    justifyContent: "flex-end",
    width: sizeWidth(320),
    height: sizeWidth(180)
  },
  tabs: {
    flexDirection: "row",
    height: moderateScale(44),
    backgroundColor: "white"
  },
  tab: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    color: appColor.blur,
    fontFamily: font.medium
  },
  activeText: {
    fontFamily: font.medium
  },
  line: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: sizeWidth(3),
    backgroundColor: appColor.primary
  },
  overlay: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  icon: {
    width: moderateScale(16),
    height: moderateScale(16)
  },
  follow: {
    color: "white",
    marginLeft: sizeWidth(5),
    fontSize: sizeFont(12)
  },
  wrap: {
    borderWidth: 1,
    borderColor: "white",
    backgroundColor: "rgba(0, 0, 0, 0.3)",
    flexDirection: "row",
    alignItems: "center",
    height: sizeWidth(24),
    borderRadius: sizeWidth(12),
    paddingHorizontal: sizeWidth(10)
  }
});
