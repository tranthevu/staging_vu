import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Dimensions, TouchableOpacity } from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import {
  sizeFont,
  sizeWidth,
  sizeHeight,
  moderateScale
} from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import Rate from "../../components/common/rate";
import GoogleStaticMap from "react-native-google-static-map";
import lodash from "lodash";
import { appConfig } from "../../config/app.config";
import openMap from "react-native-open-maps";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import StoreItem from "../offerDetail/store-item";
import NotificationView from "../../components/common/notification-view";

class StoreMapScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  openDeviceMap = (latitude, longitude, name) => {
    openMap({
      latitude: parseFloat(latitude),
      longitude: parseFloat(longitude),
      query: name
    });
  };

  render(): ReactNode {
    const { store } = this.props.navigation.state.params;
    const name = lodash.get(store, "partnerstore.name");
    const address = lodash.get(store, "partnerstore.address");
    const latitude = parseFloat(lodash.get(store, "partnerstore.latitude"));
    const longitude = parseFloat(lodash.get(store, "partnerstore.longitude"));
    let { width, height } = Dimensions.get("window");
    width = Math.round(width);
    height = Math.round(height);
    return (
      <View style={styles.container}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          loadingEnabled={true}
          showsUserLocation={true}
          initialRegion={{
            latitude: latitude,
            longitude: longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121
          }}
        >
          <Marker
            title={name}
            description={address}
            coordinate={{ latitude: latitude, longitude: longitude }}
          />
        </MapView>
        <BackIcon
          source={require("../../../res/icon/back-store.png")}
          iconStyle={styles.backIcon}
          style={styles.back}
        />
        <StoreItem hideArrow={true} style={styles.store} item={store} />
        <NotificationView />
      </View>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(StoreMapScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  bottom: {
    width: sizeWidth(302),
    padding: sizeWidth(10),
    backgroundColor: "white",
    justifyContent: "center",
    position: "absolute",
    alignSelf: "center",
    borderRadius: sizeWidth(6),
    bottom: moderateScale(16)
  },
  name: {
    fontSize: sizeFont(18),
    fontFamily: font.bold
  },
  address: {
    marginTop: sizeWidth(4),
    fontSize: sizeFont(13)
  },
  backIcon: {
    width: moderateScale(10),
    height: moderateScale(17),
    marginRight: moderateScale(3)
  },
  back: {
    position: "absolute",
    top: moderateScale(32),
    left: moderateScale(11),
    width: moderateScale(30),
    height: moderateScale(30),
    borderRadius: moderateScale(15),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    padding: 0
  },
  map: {
    flex: 1
  },
  store: {
    position: "absolute",
    bottom: moderateScale(21)
  }
});
