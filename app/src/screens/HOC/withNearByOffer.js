/* eslint-disable no-unused-expressions */
import React from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { getIsFetching } from "../../selector/loadingSelector";
import { navigateToPage } from "../../actions/nav.action";
import {
  getProductNearBySubmit,
  clearProductsNearBy
} from "../../actions/search-products.action";
import { getCurrentPosition } from "../../helpers/permission-helper";
import { showError } from "../../helpers/error.helper";
import { RADIUS_INIT } from "../../constants/app.constant";
import { checkApiStatus } from "../../helpers/app.helper";

export const NEAR_OFFER = {
  PROMOTION: "event",
  E_COUPON: "voucher"
};
export default function withNearByOffer(
  WrapComponent,
  options = { title: "Ưu đãi gần bạn", type: NEAR_OFFER.PROMOTION }
) {
  class HOC extends React.PureComponent {
    constructor(props) {
      super(props);
      this.state = {
        data: [],
        coords: undefined,
        isLoading: true
      };
    }

    onResponseLocationSuccess = coords => {
      this.setState({ coords }, () => {
        this.onRequest();
      });
    };

    onUserLocationError = e => {
      showError("Lỗi định vị");
    };

    async componentDidMount() {
      const isMaintenance = await checkApiStatus();
      if (isMaintenance) {
        this.props.resetPage("Maintenance");
      }
      this.mounted = true;
      getCurrentPosition(this.onResponseLocationSuccess, this.onUserLocationError);
    }

    componentWillUnmount() {
      this.mounted = false;
    }

    onRequest = () => {
      const { coords } = this.state;
      const { latitude, longitude } = coords;
      this.setState({ isLoading: true }, () => {
        this.props.getProductNearBySubmit({
          point: `${longitude},${latitude}`,
          radius: RADIUS_INIT,
          // category: selectedCategory.slug,
          page: 1,
          type: options.type,
          showLoading: false,
          callback: (err, response) => {
            this.setState({ isLoading: false });
            if (response) {
              this.mounted && this.setState({ data: _.get(response, "data", []) });
            }
          }
        });
      });
    };

    render() {
      const { data, isLoading } = this.state;
      return (
        <WrapComponent
          {...this.props}
          options={options}
          nearOffers={data}
          isLoadingNearOffer={isLoading}
        />
      );
    }
  }

  const mapStateToProps = state => ({
    topCategories: state.topCategories,
    productsNearBy: state.searchProducts.productsNearBy,
    loading: getIsFetching(state)
  });

  const mapDispatchToProps = {
    navigateToPage,
    getProductNearBySubmit,
    clearProductsNearBy
  };

  return connect(mapStateToProps, mapDispatchToProps)(HOC);
}
