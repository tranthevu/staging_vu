/* eslint-disable no-unused-expressions */
import React from "react";
import { connect } from "react-redux";
import { navigateToPage } from "../../actions/nav.action";
import { checkGPSLocation } from "../../helpers/permission-helper";

export const NEAR_OFFER = {
  PROMOTION: "event",
  E_COUPON: "voucher"
};
export default function withLocationPermission(WrapComponent) {
  class HOC extends React.PureComponent {
    constructor(props) {
      super(props);
      this.state = {};
    }

    onPress = (onSuccess, onError) => {
      checkGPSLocation(onSuccess, onError);
    };

    render() {
      return <WrapComponent {...this.props} onCheckGPSLocation={this.onPress} />;
    }
  }

  const mapStateToProps = state => ({});

  const mapDispatchToProps = {
    navigateToPage
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps
  )(HOC);
}
