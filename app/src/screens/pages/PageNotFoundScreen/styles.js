import { StyleSheet } from "react-native";
import { sizeFont, moderateScale } from "../../../helpers/size.helper";
import { font, appColor } from "../../../constants/app.constant";

export default StyleSheet.create({
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  content: {
    flex: 1,
    alignItems: "center",
    marginTop: moderateScale(60)
  },
  separator: {
    height: moderateScale(10)
  },
  button: {
    width: moderateScale(262)
  },
  txtButton: {
    fontFamily: font.bold
  },
  img: {
    alignSelf: "center",
    // width: moderateScale(262),
    height: moderateScale(147)
  },
  txtTitle: {
    fontSize: sizeFont(18),
    color: appColor.primary,
    fontFamily: font.bold,
    marginTop: moderateScale(20),
    marginBottom: moderateScale(10)
  },
  txtSubTitle: {
    fontSize: sizeFont(14),
    color: appColor.text,
    fontFamily: font.regular,
    marginBottom: moderateScale(20)
  }
});
