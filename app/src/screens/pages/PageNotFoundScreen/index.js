import React, { PureComponent } from "react";
import { View, Image } from "react-native";
import { connect } from "react-redux";
import styles from "./styles";
import Toolbar from "../../../components/common/toolbar";
import BackIcon from "../../../components/common/back-icon";
import Button from "../../../components/common/button";
import { IMG_NOT_FOUND } from "../../../commons/images";
import Text from "../../../components/common/text";

class PageNotFoundScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { title = "Chi tiết" } = this.props;
    return (
      <View style={styles.container}>
        <Toolbar
          leftStyle={[styles.part, styles.left]}
          rightStyle={styles.part}
          center={<Text style={styles.header}>{title}</Text>}
          left={<BackIcon />}
        />
        <View style={styles.content}>
          <Image source={IMG_NOT_FOUND} resizeMode="contain" style={styles.img} />
          <Text style={styles.txtTitle}>Có gì đó sai sai</Text>
          <Text style={styles.txtSubTitle}>Nội dung bạn truy cập không tồn tại</Text>
          <Button
            onPress={() => {
              this.props.navigation.goBack();
            }}
            textStyle={styles.txtButton}
            style={styles.button}
            text="QUAY LẠI"
          />
        </View>
      </View>
    );
  }
}
export default connect(
  () => ({}),
  {}
)(PageNotFoundScreen);
