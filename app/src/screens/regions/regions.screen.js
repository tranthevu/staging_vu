import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  FlatList
} from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import PaginationList from "../../components/common/pagination-list";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import NotificationIcon from "../dashboard/notification-icon";
import PromotionByRegion from "../dashboard/promotion-by-region";
import SearchInput from "../../components/common/search-input";
import { refreshAreas, fetchMoreAreas, requestAreas } from "../../actions/areas.action";
import NotificationView from "../../components/common/notification-view";
import EmptyView from "../../components/common/empty-view";

// TODO: This screen is not used anymore
// Will be cleaned up later
class RegionsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appliedFilters: {}
    };
  }

  navigateToSearch = () => {
    this.props.navigateToPage("Search");
  };

  componentDidMount = () => {
    this.props.requestAreas();
  };

  render(): ReactNode {
    const { areas, loading, reachedEnd, refreshing, firstLoading } = this.props.areas;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Ưu đãi theo khu vực</Text>}
          right={<NotificationIcon />}
        />
        <View style={styles.body}>
          <View style={styles.search}>
            <SearchInput
              onPress={this.navigateToSearch}
              style={styles.find}
              placeholder="Tìm kiếm"
            />
            <TouchableIcon
              style={styles.filter}
              onPress={this.navigateToFilter}
              source={require("../../../res/icon/filter.png")}
            />
          </View>
          <Text style={styles.label}>Ưu đãi theo khu vực</Text>
          <PaginationList
            data={areas}
            loading={loading}
            reachedEnd={reachedEnd}
            refreshing={refreshing}
            firstLoading={firstLoading}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderRegion}
            onEndReached={fetchMoreAreas}
            onRefresh={refreshAreas}
            EmptyComponent={<EmptyView />}
          />
          <NotificationView />
        </View>
      </View>
    );
  }

  navigateToFilter = () => {
    const { appliedFilters } = this.state;
    this.props.navigateToPage("Filter", {
      onApplyFilters: this.onApplyFilters,
      appliedFilters
    });
  };

  onApplyFilters = appliedFilters => {
    this.setState({ appliedFilters });
  };

  renderRegion = ({ item, index }) => {
    return (
      <PromotionByRegion item={item} style={styles.region} imageStyle={styles.image} />
    );
  };
}

export default connect(
  state => ({
    areas: state.areas
  }),
  { resetPage, fetchMoreAreas, refreshAreas, requestAreas, navigateToPage }
)(RegionsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "#000000"
  },
  label: {
    fontSize: sizeFont(24),
    fontFamily: font.bold,
    marginVertical: sizeWidth(8),
    paddingHorizontal: sizeWidth(9)
  },
  region: {
    width: sizeWidth(302),
    height: sizeWidth(311)
  },
  image: {
    width: sizeWidth(302),
    height: sizeWidth(227)
  },
  filter: {
    width: sizeWidth(20),
    height: sizeWidth(20)
  },
  search: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: sizeWidth(10)
  },
  find: {
    marginRight: sizeWidth(15)
  }
});
