import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, FlatList } from "react-native";
import { connect } from "react-redux";
import { resetPage, navigateBack, navigateToPage } from "../../actions/nav.action";
import { sizeWidth, sizeHeight, sizeFont } from "../../helpers/size.helper";
import Toolbar from "../../components/common/toolbar";
import Text from "../../components/common/text";
import { font } from "../../constants/app.constant";
import SearchInput from "../../components/common/search-input";
import TouchableIcon from "../../components/common/touchable-icon";

class PromotionScreen extends Component {
  render() {
    return <View style={styles.container} />;
  }

  componentDidMount = async () => {};
}

export default connect(
  null,
  { resetPage, navigateBack, navigateToPage }
)(PromotionScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#E5E5E5"
  },
  title: {
    color: "#000000",
    fontSize: sizeFont(18),
    fontFamily: font.bold
  },
  separator: {
    width: sizeWidth(320),
    height: 1,
    backgroundColor: "#DDDDDD"
  },
  search: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "white",
    padding: sizeWidth(11)
  }
});
