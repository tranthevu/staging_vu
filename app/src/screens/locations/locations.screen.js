import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  FlatList,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import Banner from "../dashboard/banner";
import BackIcon from "../../components/common/back-icon";
import NotificationIcon from "../dashboard/notification-icon";
import StoreItem from "../offerDetail/store-item";
import lodash from "lodash";
import NotificationView from "../../components/common/notification-view";

const tabs = {
  all: 0,
  northern: 1,
  central: 2,
  south: 3
};

class LocationsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: tabs.all
    };
  }

  render(): ReactNode {
    const locations = lodash.get(
      this.props.navigation.state,
      "params.locations"
    );
    return (
      <View style={styles.container}>
        <Toolbar
          center={<Text style={styles.header}>Địa điểm áp dụng</Text>}
          left={<BackIcon />}
          right={<NotificationIcon />}
        />
        <View style={styles.body}>
          <FlatList
            data={locations}
            contentContainerStyle={styles.list}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderLocation}
          />
          <NotificationView />
        </View>
      </View>
    );
  }

  renderLocation = ({ item, index }) => {
    return <StoreItem item={item} />;
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(LocationsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  tabs: {
    flexDirection: "row",
    height: sizeWidth(44),
    marginBottom: sizeWidth(12),
    backgroundColor: "white"
  },
  tab: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    color: appColor.blur,
    fontFamily: font.medium
  },
  activeText: {
    fontFamily: font.bold
  },
  line: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: sizeWidth(3),
    backgroundColor: appColor.primary
  },
  list: {
    paddingVertical: sizeWidth(8)
  }
});
