import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ScrollView, Image } from "react-native";
import validator from "validator";
import { connect } from "react-redux";
import { resetPage, navigateBack, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import {
  sizeWidth,
  sizeHeight,
  sizeFont,
  moderateScale
} from "../../helpers/size.helper";
import Button from "../../components/common/button";
import Input from "../../components/common/input";
import { font, text, appColor } from "../../constants/app.constant";
import Api from "../../api/api";
import { saveToken } from "../../helpers/storage.helper";
import TouchableIcon from "../../components/common/touchable-icon";
import NotificationView from "../../components/common/notification-view";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import Dropdown from "../../components/common/dropdown";
import { showError } from "../../helpers/error.helper";
import KeyboardHandlerView from "../../components/common/keyboard-handler-view";

class ForgotPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requestSuccessful: false,
      phone: text.emptyString
    };
  }

  componentDidMount = () => {
    if (this.phoneRef) this.phoneRef.focus();
  };

  render(): ReactNode {
    const { requestSuccessful, phone } = this.state;
    if (requestSuccessful) return this.renderSuccessIndicator();
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Quên mật khẩu</Text>}
        />
        <KeyboardHandlerView
          keyboardVerticalOffset={moderateScale(20)}
          behavior="padding"
          style={styles.body}
        >
          <ScrollView bounces={false}>
            <Text style={styles.notice}>Nhập số điện thoại của bạn</Text>
            <Text style={styles.text}>
              Nhập số điện thoại đã đăng ký vào ô bên dưới, hệ thống sẽ gửi bạn mã xác
              minh qua SMS để đặt lại mật khẩu
            </Text>
            <View style={styles.input}>
              <Dropdown style={styles.prefix} value="+84" />
              <Input
                value={phone}
                ref={ref => (this.phoneRef = ref)}
                onChangeText={text => this.setState({ phone: text })}
                style={styles.phone}
                keyboardType="phone-pad"
                placeholder="Nhập số di động của bạn"
              />
            </View>
          </ScrollView>
          <Button style={styles.button} onPress={this.recoverPassword} text="TIẾP TỤC" />
        </KeyboardHandlerView>
        <NotificationView />
      </View>
    );
  }

  recoverPassword = () => {
    const { phone } = this.state;
    if (validator.isEmpty(phone)) return showError("Số điện thoại không được để trống!");
    if (phone.length < 9 || phone.length >= 12)
      return showError("Số điện thoại không đúng định dạng!");
    this.props.navigateToPage("ConfirmPhone", {
      phone: `84${phone.replace(/^0+/, "")}`,
      fromForgotPassword: true
    });
  };

  renderSuccessIndicator = () => {
    return (
      <View style={styles.indicator}>
        <View style={styles.info}>
          <Image style={styles.icon} source={require("../../../res/icon/success.png")} />
          <Text style={styles.success}>Thành công</Text>
          <Text style={styles.desc}>
            Chúng tôi đã gửi cho bạn một email đặt lại mật khẩu mới.
          </Text>
        </View>
        <Button onPress={this.props.navigateBack} style={styles.login} text="Đăng nhập" />
      </View>
    );
  };
}

export default connect(
  null,
  { resetPage, navigateToPage, navigateBack }
)(ForgotPasswordScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF"
  },
  body: {
    flex: 1,
    paddingHorizontal: sizeWidth(22)
  },
  back: {
    marginVertical: sizeHeight(22),
    alignSelf: "center"
  },
  notice: {
    fontSize: sizeFont(15),
    fontFamily: font.bold,
    textAlign: "center",
    marginTop: moderateScale(16)
  },
  phone: {
    flex: 1,
    alignSelf: "flex-end"
  },
  text: {
    textAlign: "center",
    color: appColor.blur,
    fontSize: sizeFont(13),
    marginHorizontal: moderateScale(12),
    marginVertical: moderateScale(10)
  },
  icon: {
    width: sizeWidth(150),
    height: sizeWidth(150)
  },
  success: {
    fontSize: sizeFont(24),
    fontFamily: font.bold,
    marginTop: sizeHeight(27),
    marginBottom: sizeHeight(18)
  },
  desc: {
    fontSize: sizeFont(16),
    textAlign: "center"
  },
  login: {
    marginBottom: sizeWidth(8)
  },
  indicator: {
    flex: 1
  },
  info: {
    flex: 1,
    paddingHorizontal: sizeWidth(12),
    justifyContent: "center",
    alignItems: "center"
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  button: {
    marginVertical: moderateScale(10)
  },
  input: {
    flexDirection: "row",
    alignItems: "flex-end"
  },
  prefix: {
    alignSelf: "flex-end",
    width: sizeWidth(94)
  }
});
