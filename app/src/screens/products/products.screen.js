import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import ProductItem from "../dashboard/product-item";
import BackIcon from "../../components/common/back-icon";
import SearchInput from "../../components/common/search-input";

class ProductsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render(): ReactNode {
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Nhãn hàng</Text>}
          right={
            <TouchableIcon
              iconStyle={styles.cart}
              source={require("../../../res/icon/cart.png")}
            />
          }
        />
        <SearchInput style={styles.search} placeholder="Tìm nhãn hàng" />
        <ScrollView contentContainerStyle={styles.body}>
          <View style={styles.products}>
            <ProductItem />
            <ProductItem />
            <ProductItem />
            <ProductItem />
            <ProductItem />
            <ProductItem />
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(ProductsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  cart: {
    width: sizeWidth(25),
    height: sizeWidth(22)
  },
  logo: {
    width: sizeWidth(94),
    height: sizeWidth(11)
  },
  body: {
    padding: sizeWidth(12)
  },
  products: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between"
  },
  search: {
    marginTop: sizeWidth(11)
  },
  title: {
    fontSize: sizeFont(18),
    color: "#000000",
    fontFamily: font.bold
  }
});
