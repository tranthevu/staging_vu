import { StyleSheet } from "react-native";
import { sizeFont, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";

export default StyleSheet.create({
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  content: {
    flex: 1,
    paddingHorizontal: moderateScale(7),
    paddingVertical: moderateScale(10)
  },
  separator: {
    height: moderateScale(10)
  }
});
