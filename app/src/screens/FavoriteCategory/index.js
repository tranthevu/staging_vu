import React, { PureComponent } from "react";
import { Text, View } from "react-native";
import { connect } from "react-redux";
import _ from "lodash";
import styles from "./styles";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import FavoriteCategoryItem from "../../components/Item/FavoriteCategoryItem";
import FavoriteItem from "../../components/Item/FavoriteItem";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import {
  getFavoriteCategories,
  updateFavoriteCategory
} from "../../actions/profile.action";
import FlatListAutoLoadMore from "../../components/FlatListAutoLoadMore";
import { getIsFetching } from "../../selector/loadingSelector";
import { PAGE_LIMIT, TYPE_ACTION } from "../../constants/app.constant";
import { showAlertConfirm } from "../../ultis/arlert";
import { checkApiStatus } from "../../helpers/app.helper";

class FavoriteCategoryScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      favoriteCategory: {}
    };
  }

  componentDidMount() {
    this.onRefresh(1, true);
  }

  onFetch = (nextPage, isRefresh) => {
    if (_.get(this.state.favoriteCategory, "next") !== null || isRefresh)
      this.onRefresh(nextPage);
  };

  onRefresh = async (nextPage, showLoading = false) => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    this.props.getFavoriteCategories({
      page: nextPage,
      page_size: PAGE_LIMIT,
      showLoading,
      callback: (err, response) => {
        if (response) {
          this.setState({ favoriteCategory: response });
        }
      }
    });
  };

  deleteFavoriteCategory = async slug => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    this.props.updateFavoriteCategory({
      slug,
      isFavorite: false,
      callback: (err, result) => {
        if (result) {
          this.flatListAutoLoadMore.clearData(() => this.onRefresh(1, true));
          // this.setState({ data: this.data.filter(e => e.slug !== slug) });
        }
      }
    });
  };

  onPressItem = (item, type) => {
    if (type === TYPE_ACTION.DELETE) {
      showAlertConfirm({ message: "Bạn có muốn xóa không?" }, () => {
        this.deleteFavoriteCategory(item.slug);
      });
    } else {
      this.props.navigateToPage("CategoryDashboard", {
        category: { category: { ...item } }
      });
    }
  };

  renderItem = ({ item }) => {
    return <FavoriteCategoryItem item={item} onPress={this.onPressItem} />;
  };

  ItemSeparatorComponent = () => <View style={styles.separator} />;

  keyExtractor = (item, index) => item.slug + index;

  render() {
    const { isFetching } = this.props;
    const { favoriteCategory = {} } = this.state;
    const data = favoriteCategory.results || [];
    return (
      <View style={styles.container}>
        <Toolbar
          leftStyle={[styles.part, styles.left]}
          rightStyle={styles.part}
          center={<Text style={styles.header}>Chủ đề yêu thích</Text>}
          left={<BackIcon />}
        />
        <View style={styles.content}>
          <FlatListAutoLoadMore
            ItemSeparatorComponent={this.ItemSeparatorComponent}
            renderItem={this.renderItem}
            data={data}
            keyExtractor={this.keyExtractor}
            ref={ref => {
              this.flatListAutoLoadMore = ref;
            }}
            isFetching={isFetching}
            onFetch={this.onFetch}
          />
        </View>
        {null}
      </View>
    );
  }
}
export default connect(
  state => ({
    promoProducts: state.promoProducts,
    topCategories: state.topCategories,
    isFetching: getIsFetching(state)
  }),
  {
    resetPage,
    navigateToPage,
    getFavoriteCategories,
    updateFavoriteCategory
  }
)(FavoriteCategoryScreen);
