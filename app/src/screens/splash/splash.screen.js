import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  Linking,
  PushNotificationIOS,
  Platform
} from "react-native";
import { connect } from "react-redux";
import VersionNumber from "react-native-version-number";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { sizeWidth, sizeHeight, sizeFont } from "../../helpers/size.helper";
import { appColor, cookie } from "../../constants/app.constant";
import Text from "../../components/common/text";
import { requestedBanners } from "../../actions/banners.action";
import { requestedTopCategories } from "../../actions/top-categories.action";
import { requestCountNotifications } from "../../actions/notifications.action";
import { requestedProfile } from "../../actions/profile.action";
import { requestedTopMerchants } from "../../actions/top-merchants.action";
import { requestedFilters } from "../../actions/filters.action";
import { requestedMasterData } from "../../actions/master-data.action";
import AuthHelper from "../../helpers/auth.helper";
import { checkApiStatus } from "../../helpers/app.helper";
import { getDidNavigate } from "../../helpers/notification-navigate.helper";
import NotificationView from "../../components/common/notification-view";
import { saveClientCas } from "../../helpers/storage.helper";
import { appConfig } from "../../config/app.config";
import Cookie from "react-native-cookie";

class SplashScreen extends Component {
  render(): ReactNode {
    return (
      <View style={styles.container}>
        <Image
          style={styles.image}
          resizeMode="stretch"
          source={require("../../../res/icon/splash-logo-yolla-network.png")}
        />
        <Text style={styles.text}>Phiên bản {VersionNumber.appVersion}</Text>
        <NotificationView />
      </View>
    );
  }

  componentDidMount = async () => {
    // await saveClientCas("z9yovx0ncgrytknwcio79zxao0428p2i");
    // await Cookie.set(
    //   appConfig.rootUrl,
    //   cookie.clientCas,
    //   "z9yovx0ncgrytknwcio79zxao0428p2i"
    // );
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    } else {
      const isAuthenticated = await AuthHelper.isAuthenticated();
      if (!isAuthenticated && Platform.OS === "ios") {
        PushNotificationIOS.setApplicationIconBadgeNumber(0);
      }
      // TODO: Temporary hide banner as requested
      // this.props.requestedBanners();
      this.props.requestedTopCategories();
      this.props.requestedTopMerchants();
      this.props.requestedFilters();
      this.props.requestedMasterData();
      setTimeout(() => {
        const didNavigate = getDidNavigate();
        if (isAuthenticated) {
          this.props.requestedProfile();
          this.props.requestCountNotifications();
          if (!didNavigate) this.props.resetPage("Main");
        } else if (!didNavigate) this.props.resetPage("Login");
      }, 3000);
    }
  };
}

export default connect(
  null,
  {
    resetPage,
    requestedBanners,
    requestCountNotifications,
    requestedTopCategories,
    requestedProfile,
    requestedTopMerchants,
    requestedFilters,
    requestedMasterData,
    navigateToPage
  }
)(SplashScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: appColor.primary
  },
  image: {
    width: sizeWidth(182),
    height: sizeHeight(39)
  },
  text: {
    color: "#FFFFFF",
    fontSize: sizeFont(12),
    position: "absolute",
    bottom: sizeWidth(12),
    alignSelf: "center"
  }
});
