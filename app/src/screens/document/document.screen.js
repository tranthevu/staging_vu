import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { WebView } from "react-native-webview";
import { connect } from "react-redux";
import HTML from "react-native-render-html";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, documentType, text, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import BackIcon from "../../components/common/back-icon";
import LoadingIndicator from "../../components/common/loading-indicator";
import Api from "../../api/api";
import NotificationView from "../../components/common/notification-view";
import { checkApiStatus } from "../../helpers/app.helper";

class DocumentScreen extends Component {
  constructor(props) {
    super(props);
    const { content } = this.props.navigation.state.params;
    this.state = {
      loading: false,
      content: content || text.emptyString
    };
  }

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    try {
      const { document } = this.props.navigation.state.params;
      if (!document) return;
      this.setState({ loading: true });
      let response;
      switch (document) {
        case documentType.introduction:
          response = await Api.getStaticIntroduction();
          break;
        case documentType.contact:
          response = await Api.getStaticContact();
          break;
        case documentType.policy:
          response = await Api.getStaticPolicy();
          break;
        case documentType.activity:
          response = await Api.getActivityPolicy();
          break;
        case documentType.privacy:
          response = await Api.getStaticPrivacy();
          break;
        case documentType.faqs:
          response = await Api.getStaticFAQs();
          break;
        default:
          break;
      }
      const data = response.data.content;
      this.setState({ loading: false, content: data });
    } catch (err) {
      this.setState({ loading: false });
    }
  };

  renderBody = () => {
    const { url } = this.props.navigation.state.params;
    const { content } = this.state;
    if (url)
      return (
        <WebView
          onLoadStart={() => this.setState({ loading: true })}
          onLoadEnd={() => this.setState({ loading: false })}
          source={{ uri: url }}
          style={styles.webview}
        />
      );

    return (
      <ScrollView contentContainerStyle={styles.html} bounces={false}>
        <HTML
          baseFontStyle={{
            fontFamily: font.regular,
            color: appColor.text,
            fontSize: sizeFont(14)
          }}
          ignoredStyles={[
            "font-family",
            "letter-spacing",
            "font-weight",
            "display",
            "font-size"
          ]}
          html={content}
          imagesMaxWidth={sizeWidth(320)}
        />
      </ScrollView>
    );
  };

  render(): ReactNode {
    const { title } = this.props.navigation.state.params;
    const { loading } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={
            <Text numberOfLines={1} style={styles.title}>
              {title}
            </Text>
          }
        />
        <View style={styles.content}>
          {this.renderBody()}
          {loading && <LoadingIndicator style={styles.loading} />}
          <NotificationView />
        </View>
      </View>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(DocumentScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  webview: {
    flex: 1
  },
  loading: {
    position: "absolute",
    width: "100%",
    height: "100%",
    backgroundColor: "white"
  },
  content: {
    flex: 1
  },
  html: {
    padding: sizeWidth(12)
  }
});
