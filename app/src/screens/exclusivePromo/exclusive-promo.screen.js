import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  FlatList,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import Banner from "../dashboard/banner";
import BackIcon from "../../components/common/back-icon";
import NotificationView from "../../components/common/notification-view";
import Category from "../dashboard/category";
import NearOfferList from "../promo/near-offer-list";
import PaginationList from "../../components/common/pagination-list";
import PromotionItem from "../dashboard/promotion-item";
import PromotionItemList from "../dashboard/promotion-item-list";
import {
  requestExclusiveProducts,
  fetchMoreExclusiveProducts,
  refreshExclusiveProducts
} from "../../actions/exclusive-products.action";
import EmptyView from "../../components/common/empty-view";

const viewType = {
  grid: 0,
  list: 1
};
class ExclusivePromoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedViewType: viewType.list
    };
  }

  navigateToSearch = () => {
    this.props.navigateToPage("Search");
  };

  renderHeader = () => {
    const { topCategories } = this.props.topCategories;
    return (
      <View>
        <View style={styles.categories}>
          <FlatList
            data={topCategories}
            horizontal
            contentContainerStyle={styles.top}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderCategory}
          />
        </View>
        {/* <NearOfferList
          offers={[item, item, item, item, item, item, item]}
          count={8}
        /> */}
        {this.renderTitle()}
      </View>
    );
  };

  renderRecentProducts = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      exclusiveProducts
    } = this.props.exclusiveProducts;
    const { fetchMoreExclusiveProducts, refreshExclusiveProducts } = this.props;
    return (
      <PaginationList
        ref={ref => (this.list = ref)}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderPromotion}
        loading={loading}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        firstLoading={firstLoading}
        ListHeaderComponent={this.renderHeader()}
        onEndReached={() => fetchMoreExclusiveProducts()}
        onRefresh={() => refreshExclusiveProducts()}
        data={exclusiveProducts}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  renderCategory = ({ item, index }) => {
    return <Category isExclusive item={item} />;
  };

  renderTitle = () => {
    const { selectedViewType } = this.state;
    return (
      <View style={styles.bar}>
        <Text style={styles.label}>Ưu đãi mới</Text>
        <TouchableIcon
          style={styles.view}
          iconStyle={styles.viewIcon}
          onPress={this.toggleViewType}
          source={
            selectedViewType === viewType.list
              ? require("../../../res/icon/list.png")
              : require("../../../res/icon/grid.png")
          }
        />
      </View>
    );
  };

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  renderPromotion = ({ item, index }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem item={item} />
    ) : (
      <PromotionItemList item={item} />
    );
  };

  render(): ReactNode {
    return (
      <View style={styles.container}>
        <Toolbar
          leftStyle={[styles.part, styles.left]}
          rightStyle={[styles.part, styles.right]}
          center={
            <Text numberOfLines={1} style={styles.header}>
              Ưu đãi độc quyền
            </Text>
          }
          left={<BackIcon />}
          right={
            <TouchableIcon
              style={styles.wrap}
              onPress={this.navigateToSearch}
              iconStyle={styles.icon}
              source={require("../../../res/icon/home-search.png")}
            />
          }
        />
        {this.renderRecentProducts()}
      </View>
    );
  }

  componentDidMount = () => {
    this.props.requestExclusiveProducts();
  };
}

export default connect(
  state => ({
    exclusiveProducts: state.exclusiveProducts,
    topCategories: state.topCategories
  }),
  {
    resetPage,
    navigateToPage,
    fetchMoreExclusiveProducts,
    refreshExclusiveProducts,
    requestExclusiveProducts
  }
)(ExclusivePromoScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  body: {
    flex: 1
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  icon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: "white"
  },
  wrap: {
    marginRight: moderateScale(6)
  },
  right: {
    alignItems: "flex-end"
  },
  part: {
    // width: moderateScale(90)
  },
  left: {
    alignItems: "flex-start",
    paddingLeft: moderateScale(10)
  },
  categories: {
    marginBottom: sizeWidth(12),
    marginTop: sizeWidth(8)
  },
  label: {
    fontSize: sizeFont(16),
    flex: 1,
    fontFamily: font.medium
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: appColor.blur
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(9)
  },
  bar: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: sizeWidth(5),
    paddingHorizontal: sizeWidth(10)
  }
});
