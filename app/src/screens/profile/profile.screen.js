import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  PushNotificationIOS,
  Platform
} from "react-native";
import { GoogleSignin } from "react-native-google-signin";
import { LoginManager } from "react-native-fbsdk";
import { connect } from "react-redux";
import lodash from "lodash";
import moment from "moment";
import { connectActionSheet } from "@expo/react-native-action-sheet";
import firebase from "react-native-firebase";
import BadgeAndroid from "react-native-android-badge";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor, event, appMap, giftStatus } from "../../constants/app.constant";
import TouchableRow from "../../components/common/touchable-row";
import NotificationIcon from "../dashboard/notification-icon";
import { removeClientCas, removeUserInfoForSentry } from "../../helpers/storage.helper";
import {
  loadedProfile,
  requestedProfile,
  resetProfile
} from "../../actions/profile.action";
import Api from "../../api/api";
import { pickPictureFromGallery, capturePicture } from "../../helpers/image-helper";
import { countedNotifications } from "../../actions/notifications.action";
import NotificationView from "../../components/common/notification-view";
import EventRegister from "../../helpers/event-register.helper";
import { IC_HEART, IC_HEART_GRAY } from "../../commons/images";
import {
  FAVORITE_CATEGORY_SCREEN,
  YOLLA_BIZ_SHARE_TO_EARN_SCREEN
} from "../../navigators/screensName";
import { appConfig } from "../../config/app.config";
import Panel from "../../components/common/panel";
import TouchableIcon from "../../components/common/touchable-icon";
import { checkApiStatus } from "../../helpers/app.helper";

@connectActionSheet
class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uploading: false,
      loggingOut: false
    };
  }

  componentDidMount = async () => {
    this.jumpPageEvent = EventRegister.on(event.jumpPage, this.onTabChange);
  };

  onTabChange = async tabIndex => {
    if (tabIndex == 3) {
      const isMaintenance = await checkApiStatus();
      if (isMaintenance) {
        this.props.resetPage("Maintenance");
      }
      this.notificationView.register();
    }
  };

  componentWillUnmount = () => {
    EventRegister.off(this.jumpPageEvent);
  };

  navigateToRedemptionHistory = () => {
    this.props.navigateToPage("RedemptionHistory");
  };

  navigateToSavingHistory = () => {
    this.props.navigateToPage("SavingHistory");
  };

  navigateToMemberCode = () => {
    this.props.navigateToPage("MemberCode");
    EventRegister.emit(event.jumpPage, 2);
  };

  navigateToGift = () => {
    this.props.navigateToPage("Archive", {
      selectedTab: 1,
      status: giftStatus.all
    });
    EventRegister.emit(event.jumpPage, 1);
  };

  navigateToOffer = () => {
    this.props.navigateToPage("Archive", {
      selectedTab: 0
    });
    EventRegister.emit(event.jumpPage, 1);
  };
  navigateToHistoryPrepaid = () => {
    this.props.navigateToPage("HistoryPrepaidScreen", {});
  };

  render(): ReactNode {
    const { profile } = this.props.profile;
    const name = lodash.get(profile, "full_name");
    const avatar = lodash.get(profile, "avatar.thumbnail");
    const phone = lodash.get(profile, "phone");
    const date_joined = lodash.get(profile, "date_joined");
    const joinedAt = date_joined ? moment(date_joined).format("MM/YYYY") : "";
    const { uploading, loggingOut } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          {uploading ? (
            <View style={styles.uploading}>
              <ActivityIndicator animating />
            </View>
          ) : (
            <View>
              <Image
                style={styles.avatar}
                source={
                  avatar
                    ? { uri: avatar }
                    : require("../../../res/icon/default-avatar.png")
                }
              />
              <TouchableOpacity onPress={this.showOptionPicker} style={styles.touch}>
                <Image
                  style={styles.camera}
                  source={require("../../../res/icon/icon-camera.png")}
                />
              </TouchableOpacity>
            </View>
          )}
          <View style={styles.body}>
            <Text style={styles.name}>{name || phone}</Text>
            {!!phone && <Text style={styles.phone}>{phone}</Text>}
          </View>
          <TouchableIcon
            onPress={this.navigateToMemberCode}
            iconStyle={styles.qr}
            source={require("../../../res/icon/icon-profile-qr.png")}
          />
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          bounces={false}
        >
          <Panel label="Tài khoản">
            <TouchableRow
              icon={require("../../../res/icon/account.png")}
              label="Thông tin tài khoản"
              onPress={this.navigateToAccount}
            />
            <TouchableRow
              icon={require("../../../res/icon/contact.png")}
              onPress={this.navigateToContact}
              label="Thông tin liên hệ"
            />
            <TouchableRow
              icon={require("../../../res/icon/credit-card.png")}
              onPress={this.navigateToAccountBank}
              label="Tài khoản nhận thưởng"
            />
          </Panel>
          <Panel label="Quản lý">
            <TouchableRow
              icon={require("../../../res/icon/icon-profile-offer.png")}
              label="Ưu đãi"
              onPress={this.navigateToOffer}
              resizeMode="stretch"
              iconStyle={styles.offer}
            />
            <TouchableRow
              onPress={this.navigateToGift}
              icon={require("../../../res/icon/icon-profile-gift.png")}
              label="Phần thưởng"
              iconStyle={styles.gift}
            />
            <TouchableRow
              icon={require("../../../res/icon/share-earn.png")}
              onPress={this.navigateToShareToEarn}
              label="Chia sẻ nhận thưởng"
            />
            <TouchableRow
              icon={require("../../../res/icon/share-earn.png")}
              onPress={this.navigateToYollaBizShareToEarn}
              label="Yolla Biz - Chia sẻ nhận thưởng"
            />
            <TouchableRow
              icon={require("../../../res/icon/profile-bell.png")}
              onPress={this.navigateToNotification}
              label="Thông báo"
            />
            <TouchableRow
              icon={require("../../../res/icon/follow.png")}
              onPress={this.navigateToFollow}
              label="Theo dõi"
            />
            <TouchableRow
              resizeMode="contain"
              icon={IC_HEART_GRAY}
              onPress={() => {
                appConfig.activeApp = appMap.all;
                this.navigate(FAVORITE_CATEGORY_SCREEN)();
              }}
              label="Chủ đề yêu thích"
            />
            <TouchableRow
              icon={require("../../../res/icon/follow.png")}
              onPress={this.navigateToUpdateLocation}
              label="Thay đổi vị trí"
            />
          </Panel>
          <Panel label="Lịch sử">
            <TouchableRow
              icon={require("../../../res/icon/icon-profile-use-offer.png")}
              label="Sử dụng ưu đãi"
              onPress={this.navigateToSavingHistory}
              iconStyle={styles.icon}
            />
            <TouchableRow
              icon={require("../../../res/icon/cart_history.png")}
              label="Lịch sử mua hàng"
              onPress={this.navigateToHistoryPrepaid}
              iconStyle={styles.icon}
            />
            <TouchableRow
              icon={require("../../../res/icon/icon-profile-receive.png")}
              label="Nhận/đổi thưởng"
              onPress={this.navigateToRedemptionHistory}
              iconStyle={styles.icon}
            />
          </Panel>
          <TouchableRow
            style={styles.logout}
            hideArrow
            rightIcon={
              <Image
                style={styles.iconLogout}
                source={require("../../../res/icon/icon-profile-logout.png")}
              />
            }
            labelStyle={styles.highlight}
            onPress={this.logout}
            loading={loggingOut}
            label="Đăng xuất"
          />
        </ScrollView>
        <NotificationView ref={ref => (this.notificationView = ref)} />
      </View>
    );
  }

  showOptionPicker = () => {
    const values = ["Chọn ảnh từ thư viện", "Chụp ảnh", "Huỷ"];
    const cancelButtonIndex = values.length - 1;
    this.props.showActionSheetWithOptions(
      {
        options: values,
        cancelButtonIndex
      },
      buttonIndex => {
        if (buttonIndex === 2) return;
        this.uploadAvatar(buttonIndex);
      }
    );
  };

  uploadAvatar = async optionIndex => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    try {
      const image =
        optionIndex === 0 ? await pickPictureFromGallery() : await capturePicture();
      this.setState({ uploading: true });
      await Api.uploadAvatar(image);
      await this.props.requestedProfile();
      this.setState({ uploading: false });
    } catch (err) {
      this.setState({ uploading: false });
    }
  };

  navigateToUpdateLocation = () => {
    this.props.navigateToPage("UpdateLocation");
  };

  navigateToShareToEarn = () => {
    this.props.navigateToPage("ShareToEarn");
  };

  navigateToYollaBizShareToEarn = () => {
    this.props.navigateToPage(YOLLA_BIZ_SHARE_TO_EARN_SCREEN);
  };

  navigateToAccount = () => {
    this.props.navigateToPage("Account");
  };

  navigateToNotification = () => {
    this.props.navigateToPage("Notifications");
  };

  navigateToContact = () => {
    this.props.navigateToPage("Contact");
  };

  navigateToAccountBank = () => {
    this.props.navigateToPage("AccountBank");
  };

  navigateToReferral = () => {
    this.props.navigateToPage("Referral");
  };

  navigateToFollow = () => {
    this.props.navigateToPage("Follow");
  };

  navigate = (screenName, params) => () => {
    this.props.navigateToPage(screenName, params);
  };

  logout = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    try {
      this.setState({ loggingOut: true });
      GoogleSignin.configure({
        offlineAccess: false
      });
      await GoogleSignin.signOut();
      await LoginManager.logOut();
      const fcmToken = await firebase.messaging().getToken();
      await Api.unregisterTokenId(fcmToken);
      await removeClientCas();
      await removeUserInfoForSentry();
      // TODO: Guessing below clear cookie make will lead to the bug
      // YA15-121 [Yolla Network 1.2 build 36 - 123][iOS] The session is time out when login by log in by fb, Google, phone number
      // Temporary comment it out to let user login successfully
      // await Cookie.clear(appConfig.rootUrl);
      // Fix YOLLA-497
      // [Yolla-App-95-84][Notification] The user still receives the notification while logged out of the application
      Api.registerTokenIdAnonymous(fcmToken);

      this.props.resetProfile();
      if (Platform.OS === "ios") {
        PushNotificationIOS.setApplicationIconBadgeNumber(0);
      } else {
        BadgeAndroid.setBadge(0);
      }

      this.props.countedNotifications({ unreadcount: 0 });
      this.props.resetPage("Login");

      this.setState({ loggingOut: false });
    } catch (err) {
      this.setState({ loggingOut: false });
    }
  };
}

export default connect(
  state => ({
    profile: state.profile
  }),
  {
    resetPage,
    navigateToPage,
    loadedProfile,
    resetProfile,
    requestedProfile,
    countedNotifications
  }
)(ProfileScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F9F9F9"
  },
  logout: {
    marginTop: moderateScale(20)
  },
  header: {
    height: moderateScale(120),
    paddingHorizontal: sizeWidth(9),
    width: sizeWidth(320),
    backgroundColor: appColor.primary,
    flexDirection: "row",
    alignItems: "center"
  },
  avatar: {
    width: moderateScale(70),
    height: moderateScale(70),
    borderRadius: moderateScale(35)
  },
  uploading: {
    width: moderateScale(70),
    height: moderateScale(70),
    borderRadius: moderateScale(35),
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center"
  },
  name: {
    color: "#FFFFFF",
    fontSize: sizeFont(16),
    fontFamily: font.medium
  },
  offer: {
    width: moderateScale(15),
    height: moderateScale(10)
  },
  gift: {
    width: moderateScale(13),
    height: moderateScale(13)
  },
  icon: {
    width: moderateScale(14),
    height: moderateScale(14)
  },
  highlight: {
    color: "#FF3B30"
  },
  iconLogout: {
    width: moderateScale(20),
    height: moderateScale(20),
    marginLeft: sizeWidth(12)
  },
  body: {
    flex: 1,
    marginHorizontal: sizeWidth(12)
  },
  phone: {
    fontSize: sizeFont(13),
    color: "white",
    marginTop: moderateScale(6)
  },
  qr: {
    width: moderateScale(20),
    height: moderateScale(20)
  },
  touch: {
    position: "absolute",
    bottom: 0,
    right: 0
  },
  camera: {
    width: moderateScale(20),
    height: moderateScale(20)
  }
});
