import PropTypes from "prop-types";
import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Image } from "react-native";
import { sizeFont, sizeHeight, sizeWidth } from "../../helpers/size.helper";
import Text from "../../components/common/text";
import { font, locations } from "../../constants/app.constant";
import Dropdown from "../../components/common/dropdown";
import Button from "../../components/common/button";
import lodash from "lodash";
import ActionSheet from "react-native-actionsheet";

export default class PickLocationDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedLocation: null
    };
  }

  render() {
    const { selectedLocation } = this.state;
    const { onSaveLocation } = this.props;

    let values = locations.map(item => lodash.get(item, "name"));
    values = [...values, "Huỷ"];
    const cancelButtonIndex = locations.length;
    return (
      <View style={styles.container}>
        <Image
          style={styles.pin}
          source={require("../../../res/icon/pin-location.png")}
        />
        <Text style={styles.label}>Chọn vị trí</Text>
        <Text style={styles.desc}>
          Chọn vị trí để nhận được những ưu đãi tốt nhất theo vị trí đã chọn
        </Text>
        <Dropdown
          onPress={() => this.actionSheet.show()}
          style={styles.dropdown}
          label="Vị trí của bạn"
          value={lodash.get(selectedLocation, "name") || "Toàn quốc"}
        />
        <Button
          onPress={() => onSaveLocation(selectedLocation)}
          style={styles.button}
          text="OK"
        />
        <ActionSheet
          ref={o => (this.actionSheet = o)}
          options={values}
          cancelButtonIndex={cancelButtonIndex}
          onPress={index => {
            if (index !== locations.length)
              this.setState({ selectedLocation: locations[index] });
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: sizeWidth(261),
    borderRadius: sizeWidth(4),
    alignItems: "center",
    backgroundColor: "white",
    paddingVertical: sizeWidth(22),
    paddingHorizontal: sizeWidth(12)
  },
  label: {
    fontSize: sizeFont(18),
    fontFamily: font.medium
  },
  desc: {
    fontSize: sizeFont(13),
    paddingHorizontal: sizeWidth(22),
    textAlign: "center",
    marginVertical: sizeWidth(12)
  },
  button: {
    marginTop: sizeWidth(22),
    width: sizeWidth(238)
  },
  dropdown: {
    width: sizeWidth(238)
  },
  pin: {
    width: sizeWidth(172),
    height: sizeWidth(116),
    marginBottom: sizeWidth(5)
  }
});
