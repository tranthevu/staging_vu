import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image } from "react-native";
import {
  resetPage,
  navigateBack,
  navigateToPage
} from "../../actions/nav.action";
import { connect } from "react-redux";
import { sizeWidth, sizeHeight } from "../../helpers/size.helper";
class UserScreen extends Component {
  render(): ReactNode {
    return <View style={styles.container} />;
  }

  componentDidMount = async () => {};
}

export default connect(
  null,
  { resetPage, navigateBack, navigateToPage }
)(UserScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
