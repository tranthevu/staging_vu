import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ActivityIndicator,
  TouchableOpacity
} from "react-native";
import { connect } from "react-redux";
import { GoogleSignin, statusCodes } from "react-native-google-signin";
import { LoginManager, AccessToken } from "react-native-fbsdk";
import firebase from "react-native-firebase";
import lodash from "lodash";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";
import Text from "../../components/common/text";
import {
  sizeWidth,
  sizeHeight,
  sizeFont,
  moderateScale
} from "../../helpers/size.helper";
import Button from "../../components/common/button";
import { text, loginType, environmentMap } from "../../constants/app.constant";
import Api from "../../api/api";
import { saveClientCas } from "../../helpers/storage.helper";
import { requestedBanners } from "../../actions/banners.action";
import { requestedTopCategories } from "../../actions/top-categories.action";
import { requestCountNotifications } from "../../actions/notifications.action";
import { requestedProfile } from "../../actions/profile.action";
import { requestedTopMerchants } from "../../actions/top-merchants.action";
import { requestedFilters } from "../../actions/filters.action";
import { requestedMasterData } from "../../actions/master-data.action";
import { appConfig } from "../../config/app.config";
import { checkApiStatus, logDebug } from "../../helpers/app.helper";
import NotificationView from "../../components/common/notification-view";
import { showError } from "../../helpers/error.helper";

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      lastScreen: "Main",
      action: "",
      url: ""
    };
  }

  render(): ReactNode {
    const { loading } = this.state;
    return (
      <View style={styles.container}>
        {loading ? (
          this.renderLoading()
        ) : (
          <>
            <View>
              <Image
                style={styles.logo}
                resizeMode="stretch"
                source={require("../../../res/icon/app-logo.png")}
              />
              <Button
                leftIcon={
                  <Image
                    source={require("../../../res/icon/smartphone.png")}
                    style={styles.icon}
                  />
                }
                onPress={this.loginByPhone}
                text="Đăng nhập bằng số điện thoại"
              />
              <Button
                style={styles.facebook}
                leftIcon={
                  <Image
                    source={require("../../../res/icon/facebook.png")}
                    style={styles.icon}
                  />
                }
                onPress={this.loginFacebook}
                text="Đăng nhập bằng Facebook"
              />
              <Button
                leftIcon={
                  <Image
                    source={require("../../../res/icon/google.png")}
                    style={styles.icon}
                  />
                }
                style={styles.google}
                onPress={this.loginGoogle}
                textStyle={styles.text}
                text="Đăng nhập bằng Google"
              />
            </View>
            <TouchableOpacity
              onPress={this.navigateToMain}
              style={styles.later}
              hitSlop={{
                top: 20,
                bottom: 20,
                left: 20,
                right: 20
              }}
            >
              <Text>Để sau</Text>
            </TouchableOpacity>
          </>
        )}
        <NotificationView />
      </View>
    );
  }

  renderLoading = () => {
    return (
      <View style={styles.loading}>
        <ActivityIndicator size="large" />
      </View>
    );
  };

  componentDidMount = async () => {
    GoogleSignin.configure({
      offlineAccess: false
    });
    const isForce = lodash.get(this.props.navigation.state.params, "isForce");
    if (isForce) showError("Phiên làm việc của bạn đã hết hạn");
    const lastScreen = lodash.get(this.props.navigation.state.params, "lastScreen");
    const action = lodash.get(this.props.navigation.state.params, "action");
    const url = lodash.get(this.props.navigation.state.params, "url");
    this.setState({
      ...this.state,
      lastScreen: lastScreen,
      action: action,
      url: url
    });
  };

  navigateToMain = () => {
    const canBack = lodash.get(this.props.navigation.state.params, "canBack");
    if (canBack) {
      this.props.navigateBack();
    } else {
      this.props.resetPage("Main");
    }
  };

  postLogin = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const fcmToken = await firebase.messaging().getToken();
    await Api.registerTokenId(fcmToken);
    // TODO: Temporary hide banner as requested
    // this.props.requestedBanners();
    this.props.requestedTopCategories();
    this.props.requestCountNotifications();
    this.props.requestedProfile();
    this.props.requestedTopMerchants();
    this.props.requestedFilters();
    this.props.requestedMasterData();

    const lastScreen = lodash.get(this.props.navigation.state.params, "lastScreen");
    const action = lodash.get(this.props.navigation.state.params, "action");
    const url = lodash.get(this.props.navigation.state.params, "url");
    if (lastScreen !== undefined)
      this.props.navigateToPage(lastScreen, {
        isComeback: "true",
        action: action,
        url: url
      });
    else this.props.resetPage("Main");
  };

  loginGoogle = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    try {
      this.setState({ loading: true });
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      logDebug("login.screen.js loginGoogle userInfo:", `${JSON.stringify(userInfo)}`);
      const { accessToken } = userInfo;
      const response = await Api.loginForApp({
        service: appConfig.ssoService,
        provider: loginType.google,
        type: loginType.google,
        social_access_token: accessToken
      });
      if (response.status === "DISABLE") {
        await this.verifyPhone(loginType.otp, accessToken, loginType.google);
      } else if (response.status === "SUCCESS") {
        await saveClientCas(response.client_cas);
        await this.postLogin();
      } else {
        showError("Đăng nhập thất bại. Vui lòng thử lại!");
      }
      this.setState({ loading: false });
    } catch (error) {
      logDebug("login.screen.js", "loginGoogle catch error", error);
      this.setState({ loading: false });
      if (error.code === statusCodes.SIGN_IN_CANCELLED) return;
      showError("Đăng nhập thất bại. Vui lòng thử lại!");
    }
  };

  loginByPhone = async () => {
    this.props.navigateToPage("LoginByPhone", {
      lastScreen: this.state.lastScreen,
      action: this.state.action,
      url: this.state.url
    });
  };

  verifyPhone = async (provider, socialAccessToken, type) => {
    this.props.navigateToPage("LoginByPhone", {
      provider,
      socialAccessToken,
      type,
      lastScreen: this.state.lastScreen,
      action: this.state.action,
      url: this.state.url
    });
  };

  loginFacebook = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    try {
      this.setState({ loading: true });
      const result = await LoginManager.logInWithPermissions(["public_profile"]);
      AccessToken.getCurrentAccessToken().then(async accessToken => {
        const response = await Api.loginForApp({
          service: appConfig.ssoService,
          provider: loginType.facebook,
          type: loginType.facebook,
          social_access_token: accessToken
        });
        if (response.status === "DISABLE") {
          await this.verifyPhone(loginType.otp, accessToken, loginType.facebook);
        } else if (response.status === "SUCCESS") {
          await saveClientCas(response.client_cas);
          await this.postLogin();
        } else {
          showError("Đăng nhập thất bại. Vui lòng thử lại!");
        }
        this.setState({ loading: false });
      });
    } catch (err) {
      if (appConfig.environment !== environmentMap.production) {
        showError(`Đăng nhập thất bại. Vui lòng thử lại! ${err.toString()}`);
      } else {
        showError("Đăng nhập thất bại. Vui lòng thử lại!");
      }
      this.setState({ loading: false });
    }
  };
}

export default connect(null, {
  navigateBack,
  resetPage,
  navigateToPage,
  requestedBanners,
  requestCountNotifications,
  requestedTopCategories,
  requestedProfile,
  requestedTopMerchants,
  requestedFilters,
  requestedMasterData
})(LoginScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: sizeHeight(12),
    backgroundColor: "#FFFFFF",
    justifyContent: "center",
    alignItems: "center"
  },
  logo: {
    marginBottom: sizeWidth(47),
    alignSelf: "center",
    width: sizeWidth(150),
    height: sizeWidth(32)
  },
  icon: {
    width: moderateScale(24),
    height: moderateScale(24),
    position: "absolute",
    left: moderateScale(12),
    alignSelf: "center"
  },
  facebook: {
    marginVertical: sizeWidth(15),
    backgroundColor: "#4E71A8"
  },
  google: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#DDDDDD"
  },
  text: {
    color: "#444444"
  },
  later: {
    position: "absolute",
    bottom: sizeWidth(23),
    fontSize: sizeFont(12)
  },
  loading: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center"
  }
});
