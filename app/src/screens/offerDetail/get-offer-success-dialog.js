import React, { Component } from "react";
import { StyleSheet, View, Image } from "react-native";
import { sizeWidth, sizeFont } from "../../helpers/size.helper";
import Button from "../../components/common/button";
import Text from "../../components/common/text";
import { font } from "../../constants/app.constant";
import TouchableIcon from "../../components/common/touchable-icon";

export default class GetOfferSuccessDialog extends Component {
  render() {
    const { onOkPress, title } = this.props;
    return (
      <View style={styles.container}>
        <Image
          resizeMode="stretch"
          source={require("../../../res/icon/check.png")}
          style={styles.image}
        />
        <Text style={styles.title}>{title}</Text>
        <Button onPress={onOkPress} style={styles.ok} text="OK" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: sizeWidth(23),
    alignItems: "center",
    backgroundColor: "#FFFFFF"
  },
  ok: {
    width: sizeWidth(238),
    alignSelf: "center",
    marginTop: sizeWidth(20)
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.medium,
    alignSelf: "center"
  },
  image: {
    width: sizeWidth(77),
    height: sizeWidth(77),
    alignSelf: "center",
    marginBottom: sizeWidth(20)
  }
});
