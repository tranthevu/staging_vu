import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  FlatList,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import TouchableIcon from "../../components/common/touchable-icon";
import lodash from "lodash";

class ImageViewer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0
    };
  }

  render(): ReactNode {
    const { selectedIndex } = this.state;
    const { images } = this.props;
    const cover = images[selectedIndex];
    return (
      <View style={styles.container}>
        <Image
          style={styles.cover}
          source={{ uri: lodash.get(cover, "original.full") }}
        />
        <View>
          <FlatList
            horizontal={true}
            contentContainerStyle={styles.list}
            extraData={selectedIndex}
            showsHorizontalScrollIndicator={false}
            renderItem={this.renderImage}
            keyExtractor={(item, index) => index.toString()}
            data={images}
          />
        </View>
      </View>
    );
  }

  selectImage = selectedIndex => {
    this.setState({ selectedIndex });
  };

  renderImage = ({ item, index }) => {
    const { selectedIndex } = this.state;
    return (
      <TouchableOpacity
        key={index.toString()}
        onPress={() => this.selectImage(index)}
        style={selectedIndex === index ? styles.selected : styles.item}
      >
        <Image
          source={{ uri: lodash.get(item, "original.full") }}
          style={styles.image}
        />
      </TouchableOpacity>
    );
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(ImageViewer);

const styles = StyleSheet.create({
  container: {
    paddingBottom: sizeWidth(15)
  },
  item: {
    width: sizeWidth(91),
    height: sizeWidth(51),
    borderRadius: sizeWidth(6),
    overflow: "hidden",
    marginHorizontal: sizeWidth(3)
  },
  selected: {
    width: sizeWidth(91),
    height: sizeWidth(51),
    borderRadius: sizeWidth(6),
    borderWidth: sizeWidth(3),
    borderColor: appColor.primary,
    overflow: "hidden",
    marginHorizontal: sizeWidth(3)
  },
  image: {
    width: sizeWidth(91),
    height: sizeWidth(51)
  },
  cover: {
    width: sizeWidth(302),
    height: sizeWidth(169),
    borderRadius: sizeWidth(6),
    alignSelf: "center",
    marginBottom: sizeWidth(15)
  },
  list: {
    paddingHorizontal: sizeWidth(6)
  }
});
