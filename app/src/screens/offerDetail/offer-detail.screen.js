import React, { Component, ReactNode } from "react";
import {
  View,
  Image,
  ScrollView,
  Share,
  ImageBackground,
  FlatList,
  TouchableOpacity,
  Linking,
  Platform,
  AsyncStorage
} from "react-native";
import Dialog, { FadeAnimation } from "react-native-popup-dialog";
import _ from "lodash";
import { connect } from "react-redux";
import lodash from "lodash";
import HTML from "react-native-render-html";
import moment from "moment";
// import Matomo from "react-native-matomo";
import LinearGradient from "react-native-linear-gradient";
import firebase from "react-native-firebase";
import { moderateScale, sizeFont, sizeWidth } from "../../helpers/size.helper";
import { resetPage, navigateToPage, replace } from "../../actions/nav.action";
import Text from "../../components/common/text";
import {
  font,
  appColor,
  productClass,
  event,
  analyticsEvent,
  usage,
  giftStatus,
  VOUCHER_TYPE
} from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import ImageViewer from "./image-viewer";
import Button from "../../components/common/button";

import GetOfferSuccessDialog from "./get-offer-success-dialog";
import StoreItem from "./store-item";
import PromotionItem from "../dashboard/promotion-item";
import Api from "../../api/api";
import LoadingIndicator from "../../components/common/loading-indicator";

import EventRegister from "../../helpers/event-register.helper";
import AnalyticsHelper from "../../helpers/analytics.helper";
import NotificationView from "../../components/common/notification-view";
import AuthHelper from "../../helpers/auth.helper";
import {
  getProductType,
  isEmptyPrice,
  formatPrice,
  makePhoneCall,
  openLinkUrl
} from "../../helpers/common.helper";
import { styles } from "./offer-detail.style";
import { showError } from "../../helpers/error.helper";
import StickyMessage from "../../components/common/sticky-message";
import ShareToEarnView from "./share-to-earn-view";
import { appConfig } from "../../config/app.config";
import CacheImage from "../../components/common/cache-image";
import { shareUrl } from "../../ultis/index";
import { IC_HOTLINE_RED, IC_FANPAGE_RED } from "../../commons/images";
import { PAGE_NOT_FOUND_SCREEN } from "../../navigators/screensName";
import BackToMainIcon from "../../components/common/back-to-main-icon";
import { checkApiStatus } from "../../helpers/app.helper";

const tabs = {
  overview: 0,
  details: 1
};

class OfferDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popupVisible: false,
      selectedTab: tabs.overview,
      product: {},
      loading: false,
      saving: false,
      removing: false,
      recommendedProducts: [],
      isShowGuideContent: false,
      isAuthenticated: false,
      buying: false,
      isComeback: false
    };
  }

  share = async url => {
    const isAuthenticated = await AuthHelper.isAuthenticated();
    if (isAuthenticated) {
      const username = lodash.get(this.props, "profile.profile.username");
      if (url && url.length > 0) {
        const lastChar = url.charAt(url.length - 1);
        if (lastChar === "/") {
          url = `${url}${username}`;
        } else {
          url = `${url}/${username}`;
        }
      }

      await shareUrl(url);
    } else {
      const lastScreen = this.props.navigation.state.routeName;
      this.props.navigateToPage("Login", {
        canBack: true,
        lastScreen: lastScreen,
        action: "share",
        url: url
      });
    }
  };

  getRelatedProducts = async slug => {
    const { productSlug } = this.props.navigation.state.params;
    let recommendedProducts = [];
    if (slug) {
      const isMaintenance = await checkApiStatus();
      if (isMaintenance) {
        this.props.resetPage("Maintenance");
      }
      const relateRes = await Api.searchProducts({
        page: 1,
        page_size: 6,
        category: slug
      });
      recommendedProducts = relateRes.results.filter(item => item.slug !== productSlug);
    }
    return recommendedProducts;
  };

  onRemoveCode = async slug => {
    const { productSlug } = this.props.navigation.state.params;
    if (productSlug !== slug) return;
    try {
      this.setState({ loading: true });
      const { product, recommendedProducts } = await this.getProduct(productSlug);
      this.setState({ product, recommendedProducts, loading: false });
    } catch (err) {
      this.setState({ loading: false });
    }
  };

  getProduct = async productSlug => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    let data = {};
    try {
      data = await Api.getProductDetail(productSlug);
    } catch (error) {
      if (!lodash.get(error, "response.data.data")) {
        this.props.replace(PAGE_NOT_FOUND_SCREEN);
      } else throw error;
    }

    const [dealset, breadcrumb, redeemLocations] = await Promise.all([
      Api.dealsetDetail(productSlug),
      Api.productBreadcrumb(productSlug),
      Api.redeemLocations(productSlug)
    ]);
    const slug = lodash.get(breadcrumb, "data[0].slug");
    const recommendedProducts = await this.getRelatedProducts(slug);
    const isAuthenticated = await AuthHelper.isAuthenticated();
    const product = {
      ...data.data,
      deal_reward: lodash.get(dealset, "data.deal_reward"),
      status_receive: isAuthenticated && lodash.get(dealset, "data.status_receive"),
      dealset: lodash.get(dealset, "data.dealset"),
      use_now: isAuthenticated && lodash.get(dealset, "data.use_now"),
      redemption_location: redeemLocations.data
    };
    return { product, recommendedProducts };
  };

  componentDidMount = async () => {
    await this.init();
  };

  init = async () => {
    const productSlug = lodash.get(this.props.navigation.state.params, "productSlug");
    const action = lodash.get(this.props.navigation.state.params, "action");
    const isComeback = lodash.get(this.props.navigation.state.params, "isComeback");
    const url = lodash.get(this.props.navigation.state.params, "url");

    if (action !== undefined) {
      this.setState({
        ...this.state,
        isComeback: true
      });
    }

    if (isComeback === undefined) {
      await AsyncStorage.removeItem("beforeLogin:slug").then(res => {
        AsyncStorage.setItem("beforeLogin:slug", productSlug);
      });
    }
    await AsyncStorage.getItem("beforeLogin:slug").then(async res => {
      this.removedCodeEvent = EventRegister.on(event.removedCode, this.onRemoveCode);
      try {
        this.setState({ loading: true });
        const productSlug = res;
        const isAuthenticated = await AuthHelper.isAuthenticated();
        const { product, recommendedProducts } = await this.getProduct(productSlug);
        // Matomo.trackEvent("view", "view_detail", "productSlug", product.id);
        this.setState({
          product,
          isAuthenticated,
          recommendedProducts,
          loading: false
        });
        AnalyticsHelper.logEvent(analyticsEvent.viewProduct, {
          id: product.id,
          title: product.title
        });

        this.resolveAction(action, url);
      } catch (err) {
        this.setState({ loading: false });
      }
    });
  };

  resolveAction = (action, url) => {
    switch (action) {
      case "share":
        this.share(url);
        break;
      case "getOffer":
        this.getOffer();
        break;
      case "paid":
        this.btnBuy();
        break;
      case "paid_now":
        this.buyNow();
    }
  };

  renderTabs = () => {
    const { selectedTab } = this.state;
    return (
      <View style={styles.tabs}>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.overview })}
          style={styles.tab}
        >
          <Text style={selectedTab === tabs.overview ? styles.activeText : styles.text}>
            Tổng quan
          </Text>
          {selectedTab === tabs.overview && <View style={styles.line} />}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.details })}
          style={styles.tab}
        >
          <Text style={selectedTab === tabs.details ? styles.activeText : styles.text}>
            Chi tiết
          </Text>
          {selectedTab === tabs.details && <View style={styles.line} />}
        </TouchableOpacity>
      </View>
    );
  };

  navigateToMerchantDetail = code => {
    this.props.navigateToPage("MerchantDetail", { merchantCode: code });
  };

  renderMerchant = (logo, merchant, code) => {
    const totalProduct = lodash.get(
      this.state,
      "product.redemption_location.partner.total_product"
    );
    return (
      <View style={styles.merchantContainer}>
        <TouchableOpacity
          onPress={() => this.navigateToMerchantDetail(code)}
          style={styles.body}
        >
          <View style={styles.content}>
            <View style={styles.logoWrap}>
              <Image
                style={styles.logo}
                resizeMode="stretch"
                source={
                  logo ? { uri: logo } : require("../../../res/img/default-image.png")
                }
              />
            </View>
            <View>
              <Text style={styles.name}>{merchant}</Text>
              {totalProduct && (
                <Text style={styles.totalProduct}>{`${totalProduct} ưu đãi`}</Text>
              )}
            </View>
          </View>
          <Image
            style={styles.shop}
            source={require("../../../res/icon/merchant-arrow-right.png")}
          />
        </TouchableOpacity>
        {this.renderMerchantInfo()}
      </View>
    );
  };

  renderMerchantInfo = () => {
    const { product } = this.state;
    const hotline = lodash.get(product, "redemption_location.partner.hot_line");
    const fanPage = lodash.get(product, "redemption_location.partner.fan_page");
    const usernameFanPage = lodash.get(
      product,
      "redemption_location.partner.username_fan_page"
    );
    if (!hotline && !fanPage) {
      return <View />;
    }
    return (
      <View style={styles.detailBottom}>
        {!!hotline && (
          <View style={styles.merchantRow}>
            <View style={{ flexDirection: "row" }}>
              <Image
                source={IC_HOTLINE_RED}
                resizeMode="contain"
                style={styles.bottomIcon}
              />
              <Text style={styles.merchantLabel}>Hotline</Text>
            </View>
            <TouchableOpacity onPress={() => makePhoneCall(hotline)}>
              <Text style={styles.merchantValue}>{hotline}</Text>
            </TouchableOpacity>
          </View>
        )}
        {!!hotline && !!fanPage && <View style={styles.merchantLine} />}
        {!!fanPage && (
          <View style={styles.merchantRow}>
            <View style={{ flexDirection: "row" }}>
              <Image
                source={IC_FANPAGE_RED}
                resizeMode="contain"
                style={styles.bottomIcon}
              />
              <Text style={styles.merchantLabel}>Fanpage</Text>
            </View>
            <TouchableOpacity onPress={() => openLinkUrl(fanPage)}>
              <Text style={styles.merchantValue}>{usernameFanPage}</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  };

  renderOverview = () => {
    const { product } = this.state;
    const claimed_count = lodash.get(product, "dealset.claimed_count");
    const redeemed_count = lodash.get(product, "dealset.redeemed_count");
    const images = lodash.get(product, "images", []);
    const orderedImages = lodash.orderBy(images, "display_order", "asc");
    const hasImages = orderedImages.length > 0;
    const locations = lodash.get(product, "redemption_location.store", []);
    const type = product.product_class;
    const merchant = lodash.get(product, "redemption_location.partner.name");
    const code = lodash.get(product, "redemption_location.partner.code");
    const logo = lodash.get(product, "redemption_location.partner.logo_image.thumbnail");
    const timenow = moment(lodash.get(product, "timenow"));
    const validEnd = moment(lodash.get(product, "attributes.date_valid_end"));
    const isExpired = validEnd.isBefore(timenow);
    const soldOut = this.isSoldOut(product);
    let shareUrl = lodash.get(product, "attributes.campaign_page_url");
    const username = lodash.get(this.props, "profile.profile.username");
    if (shareUrl && shareUrl.length > 0) {
      const lastChar = shareUrl.charAt(shareUrl.length - 1);
      if (lastChar === "/") {
        shareUrl = `${shareUrl}${username}`;
      } else {
        shareUrl = `${shareUrl}/${username}`;
      }
    }
    return (
      <ScrollView
        contentContainerStyle={
          (type === productClass.voucher && !isExpired && !soldOut) ||
          (type === productClass.event && !isExpired) ||
          (type === productClass.affiliateOffer && !isExpired) ||
          type === productClass.shareToEarn
            ? styles.list
            : styles.scrollview
        }
      >
        {hasImages && <ImageViewer images={orderedImages} />}
        {this.renderInfo()}
        {shareUrl && (
          <ShareToEarnView
            urlShare={shareUrl}
            shareUrl={shareUrl}
            redeemed_count={redeemed_count}
            claimed_count={claimed_count}
          />
        )}
        {shareUrl && this.renderGift()}

        {this.renderDetail()}
        {this.renderGuide()}
        {!!merchant && this.renderMerchant(logo, merchant, code)}
        {this.renderLocations(locations)}
        {this.renderRelatedOffers()}
      </ScrollView>
    );
  };

  dialCall = number => {
    let phoneNumber = "";
    if (Platform.OS === "android") {
      phoneNumber = `tel:${number}`;
    } else {
      phoneNumber = `telprompt:${number}`;
    }
    Linking.openURL(phoneNumber);
  };

  learnMore = url => {
    Linking.openURL(url);
  };

  renderGuide = () => {
    const { product } = this.state;
    const guide_content = lodash.get(product, "guide.guide_content");
    const guide_phone = lodash.get(product, "guide.guide_phone");
    const guide_url = lodash.get(product, "guide.guide_url");
    let hasGuideContent = false;
    let hasGuidePhone = false;
    let hasGuideUrl = false;
    let guide = false;
    if (typeof guide_url === "string") {
      hasGuideUrl = true;
    }
    if (typeof guide_phone === "string") {
      hasGuidePhone = true;
    }
    if (typeof guide_content === "string") {
      hasGuideContent = true;
    }
    if (hasGuideContent || hasGuidePhone || hasGuideUrl) {
      guide = true;
    }
    let styleHTML = { height: moderateScale(105), overflow: "hidden" };
    if (this.state.isShowGuideContent) {
      styleHTML = { overflow: "hidden" };
    }
    const showReadMore = guide_content && guide_content.length > 300;
    return (
      guide && (
        <View style={styles.guide}>
          <View style={!hasGuideContent ? styles.info : styles.infoGuide}>
            <View>
              <View style={styles.wrapper}>
                <View style={styles.guideText}>
                  <Text style={styles.labelGuide}>Hướng dẫn mua hàng</Text>
                </View>
                {hasGuidePhone && (
                  <TouchableOpacity
                    onPress={() => {
                      this.dialCall(guide_phone);
                    }}
                  >
                    <Image
                      style={styles.phoneImage}
                      source={require("../../../res/icon/phone-image.png")}
                    />
                  </TouchableOpacity>
                )}
              </View>
              {hasGuideUrl && (
                <TouchableOpacity
                  style={styles.linkWrap}
                  onPress={() => this.learnMore(guide_url)}
                >
                  <Text style={styles.link}>{guide_url}</Text>
                </TouchableOpacity>
              )}
            </View>
            {hasGuideContent && (
              <View>
                <HTML
                  containerStyle={styleHTML}
                  baseFontStyle={{
                    fontFamily: font.regular,
                    color: appColor.text,
                    fontSize: sizeFont(13)
                  }}
                  ignoredStyles={[
                    "font-family",
                    "font-weight",
                    "letter-spacing",
                    "line-height",
                    "display",
                    "font-size"
                  ]}
                  html={guide_content}
                  imagesMaxWidth={sizeWidth(320)}
                />
                {showReadMore && (
                  <View>
                    <LinearGradient
                      colors={["rgba(255, 255, 255, 0.5)", "#FFFFFF"]}
                      style={styles.overlay}
                    />
                    <TouchableOpacity
                      style={styles.more}
                      onPress={() =>
                        this.props.navigateToPage("Document", {
                          title: "Hướng dẫn mua hàng",
                          content: guide_content
                        })
                      }
                    >
                      <Text style={styles.textMore}>Xem thêm</Text>
                      <Image
                        source={require("../../../res/icon/more.png")}
                        style={styles.arrow}
                      />
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            )}
          </View>
        </View>
      )
    );
  };

  renderGift = () => {
    const { product, isAuthenticated } = this.state;
    const name = lodash.get(product, "deal_reward.name");
    const reward_status = lodash.get(product, "deal_reward.reward_status");
    const short_description = lodash.get(product, "deal_reward.short_description");
    const images = lodash.get(product, "deal_reward.images", []);
    const image = images.find(e => e.caption === "16:9" && e.display_order === 0);
    const url = lodash.get(image, "original.thumbnail");
    return (
      <View style={styles.gift}>
        <View style={styles.info}>
          <Text style={styles.label}>Phần thưởng</Text>
          <CacheImage
            style={styles.cover}
            uri={url}
            placeholder={require("../../../res/img/default-offer.jpg")}
          />
          <Text numberOfLines={2} style={styles.name}>
            {name}
          </Text>
          <Text numberOfLines={3} style={styles.description}>
            {short_description}
          </Text>
          {!isAuthenticated || reward_status === 0 ? (
            <Text style={styles.message}>Chưa đạt điều kiện nhận thưởng</Text>
          ) : (
            <Text style={styles.message}>
              Đã nhận thưởng.{" "}
              <Text onPress={this.navigateToGift} style={styles.see}>
                Xem phần thưởng
              </Text>
            </Text>
          )}
        </View>
      </View>
    );
  };

  navigateToGift = () => {
    this.props.resetPage("Main");
    this.props.navigateToPage("Archive", {
      selectedTab: 1,
      status: giftStatus.all
    });
  };

  renderDetails = () => {
    const { product } = this.state;
    const { description } = product;
    const locations = lodash.get(product, "redemption_location.store", []);
    const type = product.product_class;
    return (
      <ScrollView
        contentContainerStyle={
          type === productClass.voucher ? styles.list : styles.scrollview
        }
      >
        <HTML
          baseFontStyle={{
            fontFamily: font.regular,
            color: appColor.text,
            fontSize: sizeFont(14)
          }}
          ignoredStyles={[
            "font-family",
            "font-weight",
            "letter-spacing",
            "line-height",
            "display",
            "font-size"
          ]}
          html={description}
          imagesMaxWidth={sizeWidth(320)}
        />
        {this.renderLocations(locations)}
      </ScrollView>
    );
  };

  componentWillUnmount = () => {
    EventRegister.off(this.removedCodeEvent);
  };

  removeOffer = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { product } = this.state;
    this.setState({ removing: true });
    try {
      await Api.removeOffer(product.slug);
      EventRegister.emit(event.removedWishlist);
      this.setState({
        removing: false,
        product: { ...product, status_receive: true }
      });
    } catch (err) {
      this.setState({ removing: false });
    }
  };

  buyNow = () => {
    const { product } = this.state;
    const is_reward_user = lodash.get(product, "is_reward_user");
    const referralUrl = lodash.get(product, "attributes.affiliate_referral_url");
    if (!is_reward_user) {
      if (referralUrl) Linking.openURL(referralUrl);
    } else {
      if (this.state.isAuthenticated) {
        if (referralUrl) {
          const yolla_id = lodash.get(this.props, "profile.profile.yolla_id");
          // YP-459 add below params to affiliate_referral_url
          // utm_content=<user_yolla_id>
          // utm_source=<user_yolla_id>
          // utm_campaign=<campaign_id>
          // utm_medium=<mobile>
          const campaign_id = lodash.get(product, "campaign");
          const affiliate_referralUrl = `${referralUrl}?utm_content=${yolla_id}&utm_source=${yolla_id}&utm_campaign=${campaign_id}&utm_medium=mobile`;
          Linking.openURL(affiliate_referralUrl);
        }
      } else {
        const lastScreen = this.props.navigation.state.routeName;
        this.props.navigateToPage("Login", {
          canBack: true,
          lastScreen: lastScreen,
          action: "paid_now"
        });
      }
    }
  };

  navigateToVoucher = () => {
    const { product } = this.state;
    this.props.navigateToPage("Voucher", {
      productId: product.id,
      productSlug: product.slug,
      type: product.product_class
    });
  };

  btnBuy = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    try {
      const isAuthenticated = await AuthHelper.isAuthenticated();
      if (isAuthenticated) {
        const { product } = this.state;
        this.setState({ buying: true });
        const data = await Api.addCart({
          product_slug: product.slug,
          quantity: 1
        });
        const basketID = lodash.get(data, "data.basket");
        this.setState({ buying: false });
        this.props.navigateToPage("PrepaidScreen", { product, basketID });
      } else {
        const lastScreen = this.props.navigation.state.routeName;
        this.props.navigateToPage("Login", {
          canBack: true,
          lastScreen: lastScreen,
          action: "paid"
        });
      }
    } catch (err) {
      this.setState({ buying: false });
    }
  };

  renderVoucherButtons = ({
    allowUseNow,
    canNotReceive,
    status_receive,
    isExpired,
    isAuthenticated
  }) => {
    const { saving, product, buying } = this.state;
    let shareUrl = lodash.get(product, "attributes.campaign_page_url");
    const type_offer = lodash.get(product, "type_offer");
    if (type_offer === VOUCHER_TYPE.prepaidVoucher) {
      return (
        <Button
          onPress={this.btnBuy}
          loading={saving || buying}
          style={styles.btnBuy}
          text="MUA NGAY"
          leftIcon={
            <Image
              source={require("../../../res/icon/prepaid_cart.png")}
              style={{
                width: sizeWidth(20),
                height: sizeWidth(20),
                marginRight: sizeWidth(5)
              }}
            />
          }
        />
      );
    }
    if (canNotReceive || isExpired) return null;

    let getOfferBtn = "NHẬN MÃ ƯU ĐÃI";
    // if  S2E has coupon promotion_voucher_code is true, show button "nhận mã ưu đãi
    // else button is "lưu ưu đãi
    const { promotion_voucher_code } = this.props.navigation.state.params;
    if (typeof promotion_voucher_code !== "undefined" && !promotion_voucher_code) {
      getOfferBtn = "LƯU ƯU ĐÃI";
    }

    if (!isAuthenticated && !shareUrl) {
      return (
        <Button
          onPress={this.getOffer}
          loading={saving}
          style={styles.get}
          text={getOfferBtn}
        />
      );
    }

    if (!isAuthenticated && shareUrl) {
      return (
        <View style={styles.affiliate}>
          <Button
            onPress={() => this.share(shareUrl)}
            style={styles.btnShare}
            text="CHIA SẺ"
          />
          <Button
            onPress={this.getOffer}
            loading={saving}
            style={styles.getAffiliate}
            text={getOfferBtn}
          />
        </View>
      );
    }

    return (
      <>
        {!shareUrl && !allowUseNow && status_receive && (
          <Button
            onPress={this.getOffer}
            loading={saving}
            style={styles.get}
            text={getOfferBtn}
          />
        )}
        {shareUrl && !status_receive && allowUseNow && (
          <View style={styles.affiliate}>
            <Button
              onPress={() => this.share(shareUrl)}
              style={styles.btnShare}
              text="CHIA SẺ"
            />
            <Button
              onPress={this.navigateToVoucher}
              loading={saving}
              style={styles.getAffiliate}
              text="SỬ DỤNG NGAY"
            />
          </View>
        )}
        {shareUrl && !status_receive && !allowUseNow && (
          <Button
            onPress={() => this.share(shareUrl)}
            style={styles.btnBigShare}
            text="CHIA SẺ"
          />
        )}
        {shareUrl && status_receive && (
          <View style={styles.affiliate}>
            <Button
              onPress={() => this.share(shareUrl)}
              style={styles.btnShare}
              text="CHIA SẺ"
            />
            <Button
              onPress={this.getOffer}
              loading={saving}
              style={styles.getAffiliate}
              text={getOfferBtn}
            />
          </View>
        )}
        {!shareUrl && allowUseNow && status_receive && (
          <View style={styles.affiliate}>
            <Button
              onPress={this.getOffer}
              loading={saving}
              style={styles.getAffiliate}
              text={getOfferBtn}
            />
            <Button
              onPress={this.navigateToVoucher}
              style={styles.getAffiliate}
              text="SỬ DỤNG NGAY"
            />
          </View>
        )}
        {!shareUrl && allowUseNow && !status_receive && (
          <Button
            onPress={this.navigateToVoucher}
            style={styles.get}
            text="SỬ DỤNG NGAY"
          />
        )}
      </>
    );
  };

  render(): ReactNode {
    const {
      popupVisible,
      removing,
      saving,
      loading,
      product,
      isAuthenticated
    } = this.state;
    const { status_receive } = product;
    const type = product.product_class;
    const timenow = moment(lodash.get(product, "timenow"));
    const validEnd = moment(lodash.get(product, "attributes.date_valid_end"));
    const isExpired = validEnd.isBefore(timenow);
    const timeIsReceived = lodash.get(product, "dealset.start_datetime");
    const canNotReceive = timeIsReceived && moment(timenow).isBefore(timeIsReceived);
    const totalCurrentUserCode = lodash.get(
      product,
      "dealset.total_code_received_user_login"
    );
    const use_now = lodash.get(product, "use_now");
    const allowUseNow = use_now && totalCurrentUserCode > 0;
    // Show save button when user not login
    // Or user have been logged-in, and API allow status_receive
    const showSaveButton = !isAuthenticated || status_receive;
    const { isComeback } = this.state;
    return (
      <View style={styles.container}>
        {isComeback ? (
          <Toolbar
            center={<Text style={styles.header}>Chi tiết</Text>}
            left={<BackToMainIcon />}
          />
        ) : (
          <Toolbar
            center={<Text style={styles.header}>Chi tiết</Text>}
            left={<BackIcon />}
          />
        )}
        {canNotReceive && <StickyMessage message="Chưa đến thời gian nhận mã" />}
        <View style={styles.wrap}>
          {loading ? (
            <LoadingIndicator />
          ) : (
            <View style={styles.center}>
              {this.renderOverview()}
              {(type === productClass.voucher ||
                type === productClass.shareToEarn ||
                type === productClass.prepaidVoucher) &&
                this.renderVoucherButtons({
                  allowUseNow,
                  canNotReceive,
                  isExpired,
                  status_receive,
                  showSaveButton,
                  isAuthenticated
                })}
              {type === productClass.event && !isExpired && (
                <Button
                  onPress={showSaveButton ? this.getOffer : this.removeOffer}
                  loading={saving || removing}
                  loadingColor={!status_receive && appColor.primary}
                  textStyle={!showSaveButton && styles.textGot}
                  style={showSaveButton ? styles.get : styles.got}
                  text={showSaveButton ? "LƯU ƯU ĐÃI" : "BỎ LƯU ƯU ĐÃI"}
                />
              )}
              {type === productClass.affiliateOffer && !isExpired && (
                <View style={styles.affiliate}>
                  <Button
                    onPress={showSaveButton ? this.getOffer : this.removeOffer}
                    loading={saving || removing}
                    loadingColor={!status_receive && appColor.primary}
                    textStyle={!showSaveButton && styles.textGot}
                    style={showSaveButton ? styles.getAffiliate : styles.gotAffiliate}
                    text={showSaveButton ? "LƯU ƯU ĐÃI" : "BỎ LƯU ƯU ĐÃI"}
                  />
                  <Button
                    leftIcon={
                      <Image
                        style={styles.buyNow}
                        source={require("../../../res/icon/buy-now.png")}
                      />
                    }
                    onPress={this.buyNow}
                    style={styles.buy}
                    text="MUA NGAY"
                  />
                </View>
              )}
            </View>
          )}
          <NotificationView />
        </View>
        <Dialog
          visible={popupVisible}
          width={sizeWidth(261)}
          onTouchOutside={() => {
            this.setState({ popupVisible: false });
          }}
          dialogAnimation={
            new FadeAnimation({
              toValue: 0,
              animationDuration: 150,
              useNativeDriver: true
            })
          }
        >
          <GetOfferSuccessDialog
            title={
              type === productClass.voucher || type === productClass.shareToEarn
                ? "Nhận mã ưu đãi thành công"
                : "Lưu ưu đãi thành công"
            }
            onOkPress={() => this.setState({ popupVisible: false })}
          />
        </Dialog>
      </View>
    );
  }

  getOffer = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    try {
      const isAuthenticated = await AuthHelper.isAuthenticated();
      if (!isAuthenticated) {
        const lastScreen = this.props.navigation.state.routeName;
        this.props.navigateToPage("Login", {
          canBack: true,
          lastScreen: lastScreen,
          action: "getOffer"
        });
      } else {
        this.setState({ saving: true });
        const { product } = this.state;
        const { user_ref } = this.props.navigation.state.params;
        let data = {};
        if (typeof user_ref !== "undefined" && user_ref !== null) {
          data = await Api.share2earnCampaignReceiveCode({
            campaign: product.campaign,
            ref: user_ref
          });
        } else {
          data = await Api.saveWishlist(product.slug);
        }
        if (product.product_class === productClass.voucher) {
          // Matomo.trackEvent("event", "get_code", "productId", product.id);
        } else {
          // Matomo.trackEvent("event", "claim_code", "productId", product.id);
        }
        if (data.message !== "Save OK" && data.message !== "success") {
          showError(data.message);
          this.setState({
            saving: false
          });
        } else {
          const productData = await Api.getProductDetail(product.slug);
          const dealsetData = await Api.dealsetDetail(product.slug);
          const updatedProduct = {
            ...product,
            ...productData.data,
            deal_reward: lodash.get(dealsetData, "data.deal_reward"),
            status_receive: lodash.get(dealsetData, "data.status_receive"),
            dealset: lodash.get(dealsetData, "data.dealset"),
            use_now: lodash.get(dealsetData, "data.use_now")
          };
          EventRegister.emit(event.savedWishlist);
          this.setState({
            popupVisible: true,
            saving: false,
            product: updatedProduct
          });

          AnalyticsHelper.logEvent(
            product.product_class === productClass.voucher
              ? analyticsEvent.getCode
              : analyticsEvent.saveEvent,
            {
              id: product.id,
              title: product.title
            }
          );
        }
      }
    } catch (err) {
      this.setState({ saving: false });
    }
  };

  renderLocations = locations => {
    if (locations.length === 0) return null;
    const first3Locations = locations.slice(0, 3);
    return (
      <View style={styles.panel}>
        <View style={styles.info}>
          <Text style={styles.label}>Địa điểm áp dụng</Text>
          {first3Locations.map((item, index) => (
            <StoreItem style={styles.store} key={index.toString()} item={item} />
          ))}
        </View>
        <View style={styles.separator} />
        {locations.length > 3 && (
          <TouchableOpacity onPress={this.navigateToLocations} style={styles.more}>
            <Text style={styles.textMore}>Xem thêm</Text>
            <Image source={require("../../../res/icon/more.png")} style={styles.arrow} />
          </TouchableOpacity>
        )}
      </View>
    );
  };

  navigateToLocations = () => {
    const { product } = this.state;
    const locations = lodash.get(product, "redemption_location.store", []);
    this.props.navigateToPage("Locations", { locations });
  };

  renderRelatedOffers = () => {
    const { recommendedProducts } = this.state;
    if (recommendedProducts.length === 0) return null;
    return (
      <View style={[styles.panel, styles.related]}>
        <View style={styles.info}>
          <Text style={styles.label}>Ưu đãi liên quan</Text>
          <View>
            <FlatList
              showsHorizontalScrollIndicator={false}
              data={recommendedProducts}
              horizontal
              keyExtractor={(item, index) => index.toString()}
              renderItem={this.renderPromotion}
            />
          </View>
        </View>
      </View>
    );
  };

  renderMaybeYouLikeOffers = () => {
    const { product } = this.state;
    const recommendedProducts = lodash.get(product, "recommended_products", []);
    if (recommendedProducts.length === 0) return null;
    return (
      <View style={styles.panel}>
        <View style={styles.info}>
          <Text style={styles.label}>Có thể bạn cũng thích</Text>
          <View>
            <FlatList
              showsHorizontalScrollIndicator={false}
              data={recommendedProducts}
              horizontal
              keyExtractor={(item, index) => index.toString()}
              renderItem={this.renderPromotion}
            />
          </View>
        </View>
      </View>
    );
  };

  getExpiringTime = (isExpired, diff) => {
    if (!isExpired) {
      if (diff === 0) return "Hết hạn hôm nay";
      return `Còn ${diff} ngày`;
    }
    return `Đã hết hạn`;
  };

  renderInfo = () => {
    const { product } = this.state;
    const currentPrice = lodash.get(product, "dealset.fixed_discount");
    const oldPrice = lodash.get(product, "attributes.price_old");
    const received = lodash.get(product, "dealset.total_code_received_all_user");
    const endDate = moment(lodash.get(product, "attributes.date_valid_end"));
    const now = moment();
    const diff = endDate.startOf("day").diff(now.startOf("day"), "days");
    const shortDescription = lodash.get(product, "attributes.short_description");
    const isExpired = endDate.isBefore(now);
    const discountPercent = lodash.get(product, "dealset.percent_discount", 0);
    const currentPricePrePaid = lodash.get(product, "attributes.price_current");
    const discountPercentPrePaid = lodash.get(product, "attributes.percent_discount", 0);
    const type = lodash.get(product, "product_class");
    return (
      <View style={styles.panel}>
        <View style={styles.top}>
          <View style={styles.row}>
            <Text style={styles.big}>{getProductType(product.product_class)}</Text>
          </View>
          {product.product_class === productClass.voucher && (
            <View style={styles.prices}>
              {!!discountPercent && (
                <ImageBackground
                  style={styles.tag}
                  resizeMode="stretch"
                  source={require("../../../res/icon/discount-tag.png")}
                >
                  <Text style={styles.discount}>-{discountPercent}%</Text>
                </ImageBackground>
              )}

              {!isEmptyPrice(currentPrice) && (
                <Text style={styles.current}>
                  {formatPrice(currentPrice)}
                  <Text style={styles.underline}>đ</Text>
                </Text>
              )}
              {!isEmptyPrice(oldPrice) && (
                <Text style={styles.old}>{formatPrice(oldPrice)}đ</Text>
              )}
            </View>
          )}
          {product.product_class === productClass.prepaidVoucher && (
            <View style={styles.prices}>
              {!!discountPercentPrePaid && (
                <ImageBackground
                  style={styles.tag}
                  resizeMode="stretch"
                  source={require("../../../res/icon/discount-tag.png")}
                >
                  <Text style={styles.discount}>-{discountPercentPrePaid}%</Text>
                </ImageBackground>
              )}
              {!isEmptyPrice(currentPricePrePaid) && (
                <Text style={styles.current}>
                  {formatPrice(currentPricePrePaid)}
                  <Text style={styles.underline}>đ</Text>
                </Text>
              )}
              {!isEmptyPrice(oldPrice) && (
                <Text style={styles.old}>{formatPrice(oldPrice)}đ</Text>
              )}
            </View>
          )}
          <Text numberOfLines={2} style={styles.eventTitle}>
            {product.title}
          </Text>
        </View>
        {product.product_class === productClass.voucher ||
        product.product_class === productClass.shareToEarn ? (
          <View style={styles.bottom}>
            <Text style={styles.description}>{shortDescription}</Text>
            <View style={styles.root}>
              <Image style={styles.icon} source={require("../../../res/icon/time.png")} />
              <Text>{this.getExpiringTime(isExpired, diff)}</Text>
              <Image
                style={[styles.icon, styles.cart]}
                source={require("../../../res/icon/archive.png")}
              />
              <Text>{received} đã nhận</Text>
            </View>
          </View>
        ) : (
          <View style={styles.bottom}>
            <Text style={styles.description}>{shortDescription}</Text>
            <View style={styles.root}>
              <Image style={styles.icon} source={require("../../../res/icon/time.png")} />
              <Text>{this.getExpiringTime(isExpired, diff)}</Text>
            </View>
          </View>
        )}
        {type !== productClass.shareToEarn ? (
          <TouchableIcon
            style={styles.share}
            onPress={this.shareOffer}
            iconStyle={styles.shareIcon}
            source={require("../../../res/icon/share.png")}
          />
        ) : (
          <View style={styles.topInfo}>
            <TouchableOpacity onPress={this.navigateToGuide} style={styles.guideWrap}>
              <Image
                style={styles.infoIcon}
                source={require("../../../res/icon/blue-i.png")}
              />
              <Text style={styles.infoText}>Hướng dẫn</Text>
            </TouchableOpacity>
            <TouchableIcon
              iconStyle={styles.infoIcon}
              source={require("../../../res/icon/bookmark.png")}
            />
          </View>
        )}
      </View>
    );
  };

  shareOffer = () => {
    const { product } = this.state;
    const slug = lodash.get(product, "slug");
    const url = `${appConfig.webUrl}product/${slug}`;
    shareUrl(url);
  };

  renderPromotion = ({ item }) => {
    return <PromotionItem style={styles.item} item={item} />;
  };

  showGuide() {
    const { product } = this.state;
    const shareUrl = lodash.get(product, "attributes.campaign_page_url");
    return (
      shareUrl && (
        <TouchableOpacity onPress={() => this.navigateToGuide()} style={styles.guideWrap}>
          <Image
            source={require("./../../../res/icon/guideS2E/guide.png")}
            style={styles.guideIcon}
          />
          <Text style={styles.textGuide}>Hướng dẫn</Text>
        </TouchableOpacity>
      )
    );
  }

  renderDetail = () => {
    const { product } = this.state;
    const { description } = product;
    const showReadMore = description && description.length > 300;
    return (
      <View style={styles.panel}>
        <View style={styles.info}>
          <Text style={styles.label}>Chi tiết ưu đãi</Text>
          <HTML
            containerStyle={styles.html}
            baseFontStyle={{
              fontFamily: font.regular,
              color: appColor.text,
              fontSize: sizeFont(14)
            }}
            ignoredStyles={[
              "font-family",
              "font-weight",
              "letter-spacing",
              "line-height",
              "display",
              "font-size",
              "flex"
            ]}
            html={description}
            imagesMaxWidth={sizeWidth(320)}
          />
          {showReadMore && (
            <LinearGradient
              colors={["rgba(255, 255, 255, 0.5)", "#FFFFFF"]}
              style={styles.overlay}
            />
          )}
        </View>
        {showReadMore && (
          <TouchableOpacity onPress={this.showMoreDetails} style={styles.more}>
            <Text style={styles.textMore}>Xem thêm</Text>
            <Image source={require("../../../res/icon/more.png")} style={styles.arrow} />
          </TouchableOpacity>
        )}
      </View>
    );
  };

  showMoreDetails = () => {
    const { product } = this.state;
    this.props.navigateToPage("OfferInfo", { product });
  };

  navigateToGuide = () => {
    this.props.navigateToPage("GuideS2N");
  };

  isSoldOut = product => {
    return !product.status_receive;
  };
}

export default connect(
  state => ({
    profile: state.profile
  }),
  { resetPage, navigateToPage, replace }
)(OfferDetailScreen);
