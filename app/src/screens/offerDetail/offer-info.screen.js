import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  Share,
  ImageBackground,
  FlatList,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import {
  font,
  appColor,
  productClass,
  event
} from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import Api from "../../api/api";
import lodash from "lodash";
import HTML from "react-native-render-html";

class OfferInfoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderDetails = () => {
    const description = lodash.get(
      this.props.navigation.state,
      "params.product.description"
    );
    return (
      <ScrollView contentContainerStyle={styles.scrollview}>
        <HTML
          baseFontStyle={{
            fontFamily: font.regular,
            color: appColor.text,
            fontSize: sizeFont(14)
          }}
          ignoredStyles={[
            "font-family",
            "font-weight",
            "letter-spacing",
            "line-height",
            "display",
            "font-size"
          ]}
          html={description}
          imagesMaxWidth={sizeWidth(320)}
        />
      </ScrollView>
    );
  };

  render(): ReactNode {
    return (
      <View style={styles.container}>
        <Toolbar
          center={<Text style={styles.header}>Chi tiết ưu đãi</Text>}
          left={<BackIcon />}
        />
        {this.renderDetails()}
      </View>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(OfferInfoScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  scrollview: {
    padding: sizeWidth(12)
  }
});
