import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Share, Image, TouchableOpacity, Linking } from "react-native";
import { connect } from "react-redux";
import QRCode from "react-native-qrcode";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import { moderateScale, sizeWidth, sizeFont } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import AuthHelper from "../../helpers/auth.helper";
import { shareUrl } from "../../ultis/index";

class ShareToEarnView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: false
    };
  }

  componentDidMount = async () => {
    const isAuthenticated = await AuthHelper.isAuthenticated();
    this.setState({ isAuthenticated });
  };

  learnMore = () => {
    const { shareUrl } = this.props;
    Linking.openURL(shareUrl);
  };

  share = () => {
    const url = this.props.shareUrl;
    shareUrl(url);
  };

  render(): ReactNode {
    const { isAuthenticated } = this.state;
    return (
      <View style={styles.container}>
        {isAuthenticated && (
          <>
            <Text style={styles.textFriends}>Số bạn bè đã</Text>
            <View style={styles.countE2N}>
              <View style={styles.redeemed_count}>
                <Text style={styles.count}>{this.props.claimed_count}</Text>
                <Text style={styles.label}>Nhận mã ưu đãi</Text>
              </View>
              <View style={styles.separator} />
              <View style={styles.redeemed_count}>
                <Text style={styles.count}>{this.props.redeemed_count}</Text>
                <Text style={styles.label}>Sử dụng ưu đãi</Text>
              </View>
            </View>
            <Image style={styles.line} source={require("../../../res/icon/line.png")} />
            <View style={styles.qrcode}>
              <QRCode
                value={this.props.shareUrl}
                size={sizeWidth(160)}
                bgColor="black"
                fgColor="white"
              />
              <Text style={styles.shareText}>
                Chia sẽ QR code trực tiếp với bạn bè để nhận thưởng
              </Text>
            </View>
            <Image style={styles.line} source={require("../../../res/icon/line.png")} />
          </>
        )}

        {/* <View style={styles.body}>
          <Text style={styles.share}>Chia sẻ liên kết đến</Text>
          <View style={styles.socials}>
            <TouchableIcon
              onPress={this.share}
              iconStyle={styles.social}
              source={require("../../../res/icon/instagram.png")}
            />
            <TouchableIcon
              onPress={this.share}
              iconStyle={styles.social}
              source={require("../../../res/icon/facebook-blue.png")}
            />
            <TouchableIcon
              onPress={this.share}
              iconStyle={styles.social}
              source={require("../../../res/icon/messager.png")}
            />
            <TouchableIcon
              iconStyle={styles.social}
              onPress={this.share}
              source={require("../../../res/icon/telegram.png")}
            />
            <TouchableIcon
              iconStyle={styles.social}
              onPress={this.share}
              source={require("../../../res/icon/share-link.png")}
            />
          </View>
        </View> */}

        <Text style={styles.note}>
          Lưu ý: Bạn cần đăng nhập và chia sẻ link từ trang chiến dịch hoặc từ website
          Yolla Network để có thể đạt điều kiện nhận thưởng.
        </Text>
        <Image style={styles.line} source={require("../../../res/icon/line.png")} />
        <TouchableOpacity onPress={this.learnMore} style={styles.bottom}>
          <Image
            style={styles.internal}
            source={require("../../../res/icon/internal.png")}
          />
          <Text style={styles.more}>Website chương trình</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(ShareToEarnView);

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    marginBottom: moderateScale(10)
  },
  qrcode: {
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: moderateScale(20),
    paddingTop: moderateScale(20)
  },
  label: {
    fontSize: sizeFont(12)
  },
  count: {
    fontSize: sizeFont(28),
    marginBottom: moderateScale(15),
    fontFamily: font.bold
  },
  countE2N: {
    flexDirection: "row",
    paddingBottom: 20,
    paddingTop: 20
  },
  redeemed_count: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  separator: {
    width: 1,
    backgroundColor: "#DDDDDD",
    height: moderateScale(59),
    alignSelf: "center"
  },
  internal: {
    width: moderateScale(24),
    tintColor: appColor.blur,
    height: moderateScale(24),
    marginRight: sizeWidth(10)
  },
  more: {
    fontSize: sizeFont(13),
    color: appColor.blur
  },
  bottom: {
    height: moderateScale(54),
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  line: {
    width: "100%",
    height: 1
  },
  shareText: {
    color: "black",
    paddingHorizontal: moderateScale(63),
    textAlign: "center",
    marginTop: moderateScale(10)
  },
  note: {
    paddingHorizontal: sizeWidth(9),
    paddingVertical: sizeWidth(13),
    fontSize: sizeFont(13),
    color: "#828FA2",
    fontFamily: font.lightItalic
  },
  body: {
    paddingHorizontal: sizeWidth(13),
    paddingVertical: moderateScale(15)
  },
  share: {
    fontFamily: font.medium,
    alignSelf: "center",
    fontSize: sizeFont(13),
    marginBottom: moderateScale(15)
  },
  social: {
    width: sizeWidth(40),
    height: sizeWidth(40)
  },
  socials: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  text: {
    fontSize: sizeFont(15),
    fontFamily: font.medium,
    color: "#F14D3F"
  },
  info: {
    marginHorizontal: moderateScale(10),
    flex: 1
  },
  top: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: moderateScale(15),
    paddingHorizontal: moderateScale(10)
  },
  earn: {
    width: moderateScale(32),
    height: moderateScale(32)
  },
  gift: {
    marginTop: moderateScale(4),
    fontSize: sizeFont(13)
  },
  question: {
    width: moderateScale(20),
    height: moderateScale(20)
  },
  textFriends: {
    fontSize: sizeFont(16),
    color: "#444444",
    fontWeight: "500",
    lineHeight: sizeWidth(19),
    fontStyle: "normal",
    paddingTop: moderateScale(12),
    paddingLeft: moderateScale(9)
  }
});
