import { StyleSheet } from "react-native";
import { appColor, font } from "../../constants/app.constant";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  wrap: {
    flex: 1
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  panel: {
    backgroundColor: "white",
    width: sizeWidth(320),
    marginBottom: moderateScale(10)
  },
  related: {
    backgroundColor: appColor.bg
  },
  info: {
    paddingVertical: moderateScale(15),
    paddingHorizontal: sizeWidth(11)
  },
  html: {
    height: moderateScale(105),
    overflow: "hidden"
  },
  label: {
    fontSize: sizeFont(16),
    fontFamily: font.medium,
    marginBottom: moderateScale(10)
  },
  cover: {
    width: "100%",
    aspectRatio: 1.77,
    borderRadius: moderateScale(3),
    marginBottom: moderateScale(10)
  },
  lableShoppingGuide: {
    flex: 5,
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    marginBottom: moderateScale(6)
  },
  desc: {},
  more: {
    height: moderateScale(44),
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  textMore: {
    color: appColor.blur,
    fontSize: sizeFont(13),
    marginRight: sizeWidth(7)
  },
  arrow: {
    width: moderateScale(7),
    height: moderateScale(11)
  },
  separator: {
    height: 1,
    backgroundColor: "#DDDDDD",
    width: "100%"
  },
  get: {
    position: "absolute",
    bottom: moderateScale(12),
    alignSelf: "center"
  },
  btnBigShare: {
    position: "absolute",
    bottom: moderateScale(12),
    alignSelf: "center",
    backgroundColor: "#00ACFF"
  },
  getAffiliate: {
    width: sizeWidth(146),
    paddingHorizontal: sizeWidth(8),
    marginHorizontal: sizeWidth(6)
  },
  btnShare: {
    width: sizeWidth(146),
    paddingHorizontal: sizeWidth(8),
    marginHorizontal: sizeWidth(6),
    backgroundColor: "#00ACFF"
  },
  btnBuy: {
    position: "absolute",
    bottom: moderateScale(12),
    alignSelf: "center",
    backgroundColor: "#FF792E"
  },
  gotAffiliate: {
    width: sizeWidth(146),
    backgroundColor: "white",
    borderWidth: 1,
    paddingHorizontal: sizeWidth(8),
    borderColor: appColor.primary,
    marginHorizontal: sizeWidth(6)
  },
  got: {
    position: "absolute",
    bottom: moderateScale(12),
    alignSelf: "center",
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: appColor.primary
  },
  textGot: {
    color: appColor.primary
  },
  list: {
    paddingTop: moderateScale(10),
    paddingBottom: moderateScale(48)
  },
  top: {
    paddingTop: moderateScale(12),
    paddingHorizontal: sizeWidth(12),
    justifyContent: "center"
  },
  scrollview: {
    paddingVertical: moderateScale(10)
  },
  item: {
    marginVertical: sizeWidth(0),
    marginHorizontal: sizeWidth(8),
    width: moderateScale(260)
  },
  shop: {
    width: moderateScale(7),
    height: moderateScale(12)
  },
  body: {
    flexDirection: "row",
    backgroundColor: "white",
    alignItems: "center",
    paddingHorizontal: moderateScale(12),
    paddingVertical: moderateScale(15)
  },
  logo: {
    width: moderateScale(50),
    height: moderateScale(50),
    borderRadius: moderateScale(25)
  },
  logoWrap: {
    width: moderateScale(50),
    borderRadius: moderateScale(25),
    height: moderateScale(50),
    overflow: "hidden",
    marginRight: sizeWidth(8),
    borderWidth: 1,
    borderColor: "#DADADA"
  },
  name: {
    fontFamily: font.medium
  },
  content: {
    flexDirection: "row",
    flex: 1,
    alignItems: "center",
    marginRight: sizeWidth(8)
  },
  tabs: {
    flexDirection: "row",
    height: moderateScale(44),
    backgroundColor: "white"
  },
  tab: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    color: appColor.blur,
    fontFamily: font.medium
  },
  activeText: {
    fontFamily: font.bold
  },
  line: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: sizeWidth(3),
    backgroundColor: appColor.primary
  },
  detail: {
    height: moderateScale(970),
    width: sizeWidth(320),
    marginBottom: moderateScale(20)
  },
  center: {
    flex: 1
    // paddingBottom: moderateScale(32)
  },
  webview: {
    paddingTop: moderateScale(24),
    backgroundColor: "white"
  },
  big: {
    fontSize: sizeFont(13),
    fontFamily: font.medium,
    flex: 1
  },
  textGuide: {
    fontSize: sizeFont(13),
    marginLeft: moderateScale(7),
    color: "#37AEE2"
  },
  prices: {
    flexDirection: "row",
    marginTop: moderateScale(6),
    alignItems: "flex-end"
  },
  current: {
    color: "#FF6961",
    fontSize: sizeFont(24),
    fontFamily: font.bold,
    marginRight: sizeWidth(12)
  },
  old: {
    color: appColor.blur,
    fontSize: sizeFont(12),
    marginBottom: sizeWidth(3),
    textDecorationLine: "line-through"
  },
  underline: {
    textDecorationLine: "underline",
    color: "#FF6961",
    fontSize: sizeFont(24),
    fontFamily: font.bold
  },
  bottom: {
    backgroundColor: "white",
    paddingHorizontal: sizeWidth(12),
    paddingVertical: moderateScale(12)
  },
  root: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: moderateScale(8)
  },
  icon: {
    width: moderateScale(13),
    height: moderateScale(13),
    marginRight: sizeWidth(8)
  },
  gift: {
    backgroundColor: "white",
    marginBottom: moderateScale(10)
  },
  cart: {
    marginLeft: sizeWidth(24),
    width: moderateScale(15),
    height: moderateScale(15)
  },
  see: {
    fontSize: sizeFont(13),
    fontFamily: font.regular,
    fontStyle: "normal",
    color: "#4080FF"
  },
  message: {
    textAlign: "center",
    color: appColor.blur,
    fontSize: sizeFont(13),
    fontStyle: "italic",
    fontFamily: font.lightItalic
  },
  description: {
    fontSize: sizeFont(12),
    marginVertical: moderateScale(12)
  },
  productTitle: {
    color: "#000000",
    fontFamily: font.medium,
    fontSize: sizeFont(16)
  },
  eventTitle: {
    color: "#000000",
    fontFamily: font.medium,
    fontSize: sizeFont(16),
    marginTop: moderateScale(12)
  },
  share: {
    position: "absolute",
    top: moderateScale(10),
    right: moderateScale(14)
  },
  shareIcon: {
    width: moderateScale(20),
    height: moderateScale(20)
  },
  topInfo: {
    position: "absolute",
    top: moderateScale(10),
    right: moderateScale(10),
    flexDirection: "row",
    alignItems: "center"
  },
  infoIcon: {
    width: moderateScale(15),
    height: moderateScale(15),
    marginRight: moderateScale(4)
  },
  infoText: {
    color: "#4080FF",
    fontSize: sizeFont(12),
    marginRight: moderateScale(6)
  },
  discount: {
    fontSize: sizeFont(11),
    color: "white",
    fontFamily: font.bold,
    marginLeft: sizeWidth(3)
  },
  tag: {
    width: sizeWidth(41),
    height: moderateScale(20),
    marginRight: sizeWidth(7),
    alignSelf: "center",
    justifyContent: "center"
  },
  overlay: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: moderateScale(60)
  },
  store: {
    borderWidth: 1
  },
  affiliate: {
    position: "absolute",
    bottom: moderateScale(12),
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  buy: {
    width: sizeWidth(146),
    backgroundColor: "#FF792E",
    marginHorizontal: sizeWidth(6)
  },
  buyNow: {
    width: moderateScale(14),
    height: moderateScale(14),
    marginRight: sizeWidth(10)
  },
  guide: {
    backgroundColor: "#FFF",
    marginBottom: moderateScale(10)
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  guideWrap: {
    flexDirection: "row",
    alignItems: "center"
  },
  guideIcon: {
    width: moderateScale(16),
    height: moderateScale(16)
  },
  phoneImage: { width: moderateScale(34), height: moderateScale(34) },
  guideText: { flex: 1 },
  wrapper: { alignItems: "center", flexDirection: "row" },
  labelGuide: {
    fontSize: sizeFont(16),
    fontFamily: font.medium
  },
  link: {
    color: "#4080FF",
    fontSize: sizeFont(13)
  },
  linkWrap: {
    marginTop: moderateScale(12)
  },
  infoGuide: {
    paddingTop: moderateScale(15),
    paddingHorizontal: sizeWidth(11)
  },
  bottomIcon: {
    width: moderateScale(18),
    height: moderateScale(18),
    marginRight: sizeWidth(8)
  },
  detailBottom: {
    width: "100%",
    paddingHorizontal: sizeWidth(16),
    backgroundColor: "#FFF"
  },
  bottomSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: moderateScale(15),
    borderColor: "#DDDDDD",
    borderWidth: 1
  },
  hotline: {
    borderRightWidth: 0.5,
    borderLeftWidth: 0
  },
  fanpage: {
    borderLeftWidth: 0.5,
    borderRightWidth: 0
  },
  textBottom: {
    fontSize: sizeFont(13)
  },
  merchantLabel: {
    marginLeft: sizeWidth(8),
    fontSize: sizeFont(12)
  },
  merchantValue: {
    fontSize: sizeFont(12),
    color: "#4080FF"
  },
  merchantLine: {
    width: "100%",
    alignSelf: "center",
    height: 1,
    backgroundColor: "#F1F1F1",
    marginVertical: sizeWidth(6)
  },
  merchantRow: {
    width: "100%",
    flexDirection: "row",
    paddingVertical: sizeWidth(6),
    justifyContent: "space-between"
  },
  merchantContainer: {
    marginBottom: moderateScale(10)
  },
  totalProduct: {
    fontSize: sizeFont(12)
  }
});
