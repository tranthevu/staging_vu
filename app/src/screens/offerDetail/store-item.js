import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import lodash from "lodash";
import LinearGradient from "react-native-linear-gradient";
import TouchableIcon from "../../components/common/touchable-icon";
import phoneCall from "react-native-phone-call";

class StoreItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  navigateToStoreDetail = () => {
    const { item } = this.props;
    this.props.navigateToPage("StoreDetail", { store: item });
  };

  call = () => {
    const { item } = this.props;
    const phone = lodash.get(item, "partnerstore.phone_number");
    phoneCall({
      number: phone,
      prompt: true
    });
  };

  navigateToStoreMap = () => {
    const { item } = this.props;
    const latitude = lodash.get(item, "partnerstore.latitude");
    const longitude = lodash.get(item, "partnerstore.longitude");
    if (!latitude || !longitude) return;
    this.props.navigateToPage("StoreMap", { store: item });
  };

  render(): ReactNode {
    const { style, item, hideArrow, onPress } = this.props;
    const name = lodash.get(item, "partnerstore.name");
    const address = lodash.get(item, "partnerstore.address");
    const phone = lodash.get(item, "partnerstore.phone_number");
    return (
      <View style={[styles.container, style]}>
        <View style={styles.line} />
        <View style={styles.body}>
          <Text numberOfLines={1} style={styles.name}>
            {name}
          </Text>
          <Text numberOfLines={2} style={styles.address}>
            {address}
          </Text>
          <Text numberOfLines={1} style={styles.phone}>
            Điện thoại:{" "}
            <Text onPress={this.call} style={styles.text}>
              {phone}
            </Text>
          </Text>
        </View>
        {!hideArrow && (
          <TouchableIcon
            onPress={this.navigateToStoreMap}
            iconStyle={styles.icon}
            source={require("../../../res/icon/store-arrow-right.png")}
          />
        )}
      </View>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(StoreItem);

const styles = StyleSheet.create({
  line: {
    height: moderateScale(50),
    width: sizeWidth(3),
    backgroundColor: appColor.primary,
    borderTopRightRadius: sizeWidth(6),
    borderBottomRightRadius: sizeWidth(6),
    alignSelf: "center",
    position: "absolute",
    left: 0
  },
  container: {
    borderRadius: sizeWidth(6),
    borderWidth: 0,
    borderColor: "#DDDDDD",
    alignSelf: "center",
    alignItems: "center",
    flexDirection: "row",
    marginVertical: moderateScale(5),
    width: sizeWidth(302),
    height: moderateScale(90),
    paddingHorizontal: sizeWidth(18),
    paddingVertical: sizeWidth(11),
    backgroundColor: "white",
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 1
  },
  cover: {
    width: sizeWidth(302),
    height: moderateScale(90),
    position: "absolute"
  },
  name: {
    fontFamily: font.bold,
    fontSize: sizeFont(14)
  },
  address: {
    fontSize: sizeFont(11),
    marginVertical: sizeWidth(6)
  },
  phone: {
    fontSize: sizeFont(11)
  },
  body: {
    flex: 1,
    marginRight: sizeWidth(11)
  },
  icon: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  text: {
    fontSize: sizeFont(11),
    color: "rgb(82, 130, 247)"
  }
});
