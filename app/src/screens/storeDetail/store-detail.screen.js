import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import ProductItem from "../dashboard/product-item";
import BackIcon from "../../components/common/back-icon";
import SearchInput from "../../components/common/search-input";
import Rate from "../../components/common/rate";
import StoreItem from "../offerDetail/store-item";
import LinearGradient from "react-native-linear-gradient";
import lodash from "lodash";
import NotificationView from "../../components/common/notification-view";

class StoreDetailScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render(): ReactNode {
    const { store } = this.props.navigation.state.params;
    const address = lodash.get(store, "partnerstore.address");
    const email = lodash.get(store, "partnerstore.email");
    const faceAddress = lodash.get(store, "partnerstore.face_address");
    const phone = lodash.get(store, "partnerstore.phone_number");
    const name = lodash.get(store, "partnerstore.name");
    const cover_img = lodash.get(store, "partnerstore.image.original.full");
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image
            style={styles.cover}
            source={
              cover_img
                ? { uri: cover_img }
                : require("../../../res/img/store-cover.jpg")
            }
          />
          <BackIcon
            source={require("../../../res/icon/back-store.png")}
            iconStyle={styles.backIcon}
            style={styles.back}
          />
          <View style={styles.info}>
            <LinearGradient
              colors={["rgba(0, 0, 0, 0)", "rgba(0, 0, 0, 0.5)"]}
              style={styles.overlay}
            />
            <Text style={styles.shop}>{name}</Text>
          </View>
        </View>
        <View style={styles.content}>
          <Text style={styles.name}>{name}</Text>
          {!!address && (
            <View style={styles.row}>
              <Image
                resizeMode="stretch"
                style={styles.marker}
                source={require("../../../res/icon/marker.png")}
              />
              <Text style={styles.text}>{address}</Text>
            </View>
          )}
          {!!phone && (
            <View style={styles.row}>
              <Image
                resizeMode="stretch"
                style={styles.icon}
                source={require("../../../res/icon/phone.png")}
              />
              <Text style={[styles.text, styles.blue]}>{phone}</Text>
            </View>
          )}
          {!!email && (
            <View style={styles.row}>
              <Image
                resizeMode="stretch"
                style={styles.icon}
                source={require("../../../res/icon/envelope.png")}
              />
              <Text style={styles.text}>{email}</Text>
            </View>
          )}
          {!!faceAddress && (
            <View style={styles.row}>
              <Image
                resizeMode="stretch"
                style={styles.icon}
                source={require("../../../res/icon/facebook-light.png")}
              />
              <Text style={[styles.text, styles.blue]}>{faceAddress}</Text>
            </View>
          )}
        </View>
        <View style={styles.map}>
          <Text style={styles.label}>Bản đồ</Text>
          <StoreItem item={store} disabled={true} />
        </View>
        <NotificationView />
      </View>
    );
  }

  navigateToStoreMap = () => {
    const { store } = this.props.navigation.state.params;
    const latitude = lodash.get(store, "partnerstore.latitude");
    const longitude = lodash.get(store, "partnerstore.longitude");
    if (!latitude || !longitude) return;
    this.props.navigateToPage("StoreMap", { store });
  };
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(StoreDetailScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  cover: {
    width: sizeWidth(320),
    height: moderateScale(180),
    position: "absolute"
  },
  header: {
    justifyContent: "flex-end",
    width: sizeWidth(320),
    height: moderateScale(180)
  },
  back: {
    position: "absolute",
    top: moderateScale(32),
    left: moderateScale(11),
    width: moderateScale(30),
    height: moderateScale(30),
    borderRadius: moderateScale(15),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    padding: 0
  },
  backIcon: {
    width: moderateScale(10),
    height: moderateScale(17),
    marginRight: moderateScale(3)
  },
  label: {
    color: "#000000",
    fontSize: sizeFont(18),
    fontFamily: font.bold
  },
  map: {
    backgroundColor: "white",
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(15)
  },
  content: {
    paddingVertical: moderateScale(10),
    paddingHorizontal: moderateScale(15),
    flex: 1,
    backgroundColor: "white",
    marginBottom: sizeWidth(10)
  },
  name: {
    fontFamily: font.bold,
    marginBottom: sizeWidth(4)
  },
  marker: {
    width: moderateScale(13),
    height: moderateScale(16)
  },
  row: {
    flexDirection: "row",
    marginVertical: sizeWidth(4),
    paddingHorizontal: sizeWidth(3),
    alignItems: "center"
  },
  icon: {
    width: moderateScale(16),
    height: moderateScale(16)
  },
  text: {
    fontSize: sizeFont(13),
    marginLeft: sizeWidth(12)
  },
  blue: {
    color: "rgb(79, 166, 206)"
  },
  green: {
    fontSize: sizeFont(13),
    color: "rgb(118, 213, 114)"
  },
  info: {
    height: moderateScale(90),
    paddingHorizontal: moderateScale(15),
    justifyContent: "flex-end"
  },
  shop: {
    color: "white",
    marginBottom: moderateScale(14),
    fontSize: sizeFont(18),
    fontFamily: font.bold
  },
  overlay: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  }
});
