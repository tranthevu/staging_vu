import React, { Component, ReactNode } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import {
  font,
  codeProductType,
  codeEvent,
  codeVoucher,
  codeOnlinePromotion
} from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import Button from "../../components/common/button";
import Dropdown from "../../components/common/dropdown";
import { resetPage, navigateToPage, navigateBack } from "../../actions/nav.action";

const getOptionsStatusFromProductType = type => {
  if (type === "voucher") {
    return codeVoucher;
  }
  if (type === "affiliate-offer") {
    return codeOnlinePromotion;
  }
  return codeEvent;
};

class MySavedFilterScreen extends Component {
  constructor(props) {
    super(props);
    const appliedFilters = lodash.get(
      this.props.navigation.state,
      "params.appliedFilters",
      {}
    );
    const type = lodash.get(appliedFilters, "selectedProductType.type");
    this.state = {
      selectedProductType: appliedFilters.selectedProductType,
      optionsStatus: getOptionsStatusFromProductType(type),
      selectedStatus: appliedFilters.selectedStatus
    };
  }

  componentDidMount() {
    if (
      this.state.selectedProductType &&
      this.state.selectedProductType.type === "event"
    ) {
      this.setState({
        optionsStatus: codeEvent
      });
    }
  }

  render(): ReactNode {
    const { selectedStatus, selectedProductType } = this.state;
    const selectedType = lodash.get(selectedProductType, "type");
    return (
      <View style={styles.container}>
        <Toolbar
          right={
            <Text onPress={this.props.navigateBack} style={styles.cancel}>
              Hủy
            </Text>
          }
          center={<Text style={styles.title}>Bộ lọc</Text>}
        />
        <ScrollView bounces={false} contentContainerStyle={styles.list}>
          <Dropdown
            options={codeProductType}
            path="name"
            onValueChange={value => this.onChangeProductType(value)}
            value={lodash.get(selectedProductType, "name", "Chọn loại ưu đãi")}
            label="Loại ưu đãi"
          />
          {selectedType && selectedType !== "all" && (
            <Dropdown
              options={this.state.optionsStatus}
              path="name"
              onValueChange={value => this.setState({ selectedStatus: value })}
              value={lodash.get(selectedStatus, "name", "Chọn trạng thái")}
              label="Trạng thái"
            />
          )}
        </ScrollView>
        <View style={styles.actions}>
          {selectedType && (
            <Button
              style={[styles.halfSize, styles.btnReset]}
              textStyle={styles.btnResetText}
              onPress={this.onResetFilter}
              text="ĐẶT LẠI"
            />
          )}
          <Button
            style={selectedType ? styles.halfSize : {}}
            onPress={this.applyFilter}
            text="ÁP DỤNG"
          />
        </View>
      </View>
    );
  }

  onChangeProductType = value => {
    const { selectedProductType, selectedStatus } = this.state;
    this.setState({
      selectedProductType: value,
      optionsStatus: getOptionsStatusFromProductType(value.type),
      selectedStatus:
        lodash.get(selectedProductType, "type") === value.type ? selectedStatus : null
    });
  };

  applyFilter = () => {
    const onApplyFilters = lodash.get(
      this.props.navigation.state,
      "params.onApplyFilters"
    );
    const { selectedStatus, selectedProductType } = this.state;
    if (onApplyFilters)
      onApplyFilters({
        selectedStatus,
        selectedProductType
      });
    this.props.navigateBack();
  };

  onResetFilter = () => {
    const onApplyFilters = lodash.get(
      this.props.navigation.state,
      "params.onApplyFilters"
    );
    if (onApplyFilters) onApplyFilters({});
    this.props.navigateBack();
  };
}

export default connect(
  null,
  { resetPage, navigateToPage, navigateBack }
)(MySavedFilterScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  cancel: {
    fontSize: sizeFont(13),
    color: "white"
  },
  list: {
    paddingBottom: sizeWidth(40)
  },
  actions: {
    position: "absolute",
    bottom: sizeWidth(12),
    width: "100%",
    flexDirection: "row",
    justifyContent: "center"
  },
  btnReset: {
    backgroundColor: "#FFF",
    borderWidth: 1,
    borderColor: "#DDD"
  },
  btnResetText: {
    color: "#444"
  },
  halfSize: {
    width: sizeWidth(146),
    paddingHorizontal: sizeWidth(8),
    marginHorizontal: sizeWidth(6)
  }
});
