import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  FlatList,
  TouchableOpacity
} from "react-native";
import {
  resetPage,
  navigateToPage,
  navigateBack
} from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import {
  font,
  appColor,
  text,
  keywordType
} from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import BackIcon from "../../components/common/back-icon";
import Button from "../../components/common/button";
import Dropdown from "../../components/common/dropdown";
import SearchInput from "../../components/common/search-input";
import { requestedPopularKeywords } from "../../actions/popular-keywords.action";
import { requestedAutocomplete } from "../../actions/autocomplete.action";
import LoadingIndicator from "../../components/common/loading-indicator";
import SearchToolbar from "../../components/common/search-toolbar";
import {
  saveSearchHistory,
  getSearchHistories,
  removeSearchHistories
} from "../../helpers/storage.helper";
import lodash from "lodash";
import Api from "../../api/api";
import NotificationView from "../../components/common/notification-view";

class SearchScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      suggestionsShown: false,
      keyword: text.emptyString,
      histories: [],
      loading: false,
      category: ""
    };
  }

  onKeywordChange = keyword => {
    this.setState({
      keyword
    });
    const { category } = this.state;
    this.props.requestedAutocomplete(keyword, category);
  };

  render(): ReactNode {
    const { histories, keyword } = this.state;
    const hasHistories = histories.length > 0;
    return (
      <View style={styles.container}>
        <SearchToolbar
          ref={ref => (this.searchInput = ref)}
          value={keyword}
          searchable={true}
          onIconPress={() => {
            if (!keyword) return;
            this.navigateToSearchResults({
              value: keyword,
              type: keywordType.keyword
            });
          }}
          onChangeText={this.onKeywordChange}
          placeholder="Tìm kiếm"
        />
        <View style={styles.body}>
          {!keyword && hasHistories && this.renderHistory()}
          {!!keyword && this.renderSearchSuggestions()}
          <NotificationView />
        </View>
      </View>
    );
  }

  clearSearchHistories = async () => {
    await removeSearchHistories();
    this.setState({
      histories: []
    });
  };

  renderHistory = () => {
    const { histories, loading } = this.state;
    return (
      <View style={styles.group}>
        <Text style={styles.title}>Lịch sử tìm kiếm</Text>
        <FlatList
          extraData={loading}
          keyExtractor={(item, index) => index.toString()}
          renderItem={this.renderHistoryItem}
          ListFooterComponent={
            <>
              {this.renderSeparator()}
              <Text onPress={this.clearSearchHistories} style={styles.clear}>
                Xóa lịch sử tìm kiếm
              </Text>
            </>
          }
          ItemSeparatorComponent={this.renderSeparator}
          data={histories}
        />
      </View>
    );
  };

  renderHistoryItem = ({ item }) => {
    const { loading } = this.state;
    return (
      <TouchableOpacity
        disabled={loading}
        onPress={() => this.navigateToSearchResults(item)}
        style={styles.history}
      >
        {item.type === keywordType.keyword ? (
          <View style={styles.content}>
            <Text style={styles.name}>{item.value}</Text>
          </View>
        ) : (
          <View style={styles.content}>
            <Text style={styles.name}>{item.value}</Text>
            <Text numberOfLines={1} style={styles.in}>
              trong <Text style={styles.highlight}>{item.name}</Text>
            </Text>
          </View>
        )}
        <Image
          style={styles.image}
          source={require("../../../res/icon/expand.png")}
        />
      </TouchableOpacity>
    );
  };

  renderPopularSearch = popular => {
    if (popular.length === 0) return null;
    const data = popular.map(item => ({
      type: keywordType.keyword,
      value: item.search_keyword
    }));
    return (
      <View>
        {this.renderSeparator()}
        <Text style={styles.title}>Tìm kiếm phổ biến</Text>
        <View style={styles.keywords}>
          {data.map((item, index) => this.renderKeyword(item, index))}
        </View>
      </View>
    );
  };

  renderSearchSuggestions = () => {
    const {
      autocomplete,
      loading: autocompleteLoading
    } = this.props.autocomplete;
    const { loading } = this.state;
    const {
      category,
      popular,
      area,
      tag_in_area,
      tag_in_category,
      keyword
    } = autocomplete;
    if (autocompleteLoading) return <LoadingIndicator />;
    const keywords = keyword
      .slice(0, 4)
      .map(item => ({ type: keywordType.keyword, value: item }));
    const categories = category.slice(0, 3).map(item => ({
      type: keywordType.category,
      value: this.state.keyword,
      slug: item.slug,
      name: item.name
    }));
    // As requested from LocDT on July 02, 2019. Temporary hide area on app
    // const areas = area.slice(0, 3).map(item => ({
    //   type: keywordType.area,
    //   value: this.state.keyword,
    //   slug: item.slug,
    //   name: item.name
    // }));
    // const items = [...categories, ...areas].slice(0, 3);
    const items = [...categories].slice(0, 3);
    const data = [...items, ...keywords];

    return (
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        data={data}
        extraData={loading}
        ListFooterComponent={this.renderPopularSearch(popular)}
        ItemSeparatorComponent={this.renderSeparator}
        renderItem={this.renderSuggestion}
      />
    );
  };

  renderSeparator = () => {
    return <View style={styles.separator} />;
  };

  renderSuggestion = ({ item, index }) => {
    const { keyword, loading } = this.state;
    return (
      <TouchableOpacity
        disabled={loading}
        onPress={() => this.navigateToSearchResults(item)}
        style={styles.suggestion}
      >
        {item.type === keywordType.keyword ? (
          <View style={styles.content}>
            <Text style={styles.name}>{item.value}</Text>
          </View>
        ) : (
          <View style={styles.content}>
            <Text style={styles.name}>{keyword}</Text>
            <Text numberOfLines={1} style={styles.in}>
              trong <Text style={styles.highlight}>{item.name}</Text>
            </Text>
          </View>
        )}

        <Image
          style={styles.expand}
          resizeMode="stretch"
          source={require("../../../res/icon/expand.png")}
        />
      </TouchableOpacity>
    );
  };

  navigateToSearchResults = async keyword => {
    try {
      const { category } = this.state;
      this.setState({ loading: true });
      this.props.navigateToPage("SearchResults", { keyword, category });
      await saveSearchHistory(keyword);
      const histories = await getSearchHistories();
      this.setState({ histories, loading: false });
    } catch (err) {
      this.setState({ loading: false });
    }
  };

  renderKeyword = (item, index) => {
    const { loading } = this.state;
    return (
      <TouchableOpacity
        disabled={loading}
        key={index}
        onPress={() => this.navigateToSearchResults(item)}
        style={styles.keyword}
      >
        <Text numberOfLines={1} style={styles.text}>
          {item.value}
        </Text>
      </TouchableOpacity>
    );
  };

  componentDidMount = async () => {
    const category =
      lodash.get(this.props.navigation.state, "params.category") || "";
    const histories = await getSearchHistories();
    this.setState({ histories, category });
    this.searchInput.getWrappedInstance().focus();
  };
}

export default connect(
  state => ({
    popularKeywords: state.popularKeywords,
    autocomplete: state.autocomplete
  }),
  {
    resetPage,
    navigateToPage,
    navigateBack,
    requestedPopularKeywords,
    requestedAutocomplete
  }
)(SearchScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  body: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(13),
    fontFamily: font.bold,
    paddingHorizontal: sizeWidth(10),
    paddingVertical: moderateScale(10)
  },
  search: {
    alignSelf: "center",
    width: sizeWidth(302)
  },
  keywords: {
    flexDirection: "row",
    flexWrap: "wrap",
    paddingHorizontal: sizeWidth(6)
  },
  label: {
    color: appColor.blur,
    marginVertical: sizeWidth(15),
    paddingHorizontal: sizeWidth(9),
    fontFamily: font.medium
  },
  keyword: {
    backgroundColor: "#FFFFFF",
    height: moderateScale(24),
    borderRadius: moderateScale(12),
    paddingHorizontal: sizeWidth(10),
    justifyContent: "center",
    margin: sizeWidth(5),
    borderWidth: 1,
    borderColor: "#DDDDDD"
  },
  text: {
    fontSize: sizeFont(12)
  },
  suggestion: {
    flexDirection: "row",
    backgroundColor: "white",
    alignItems: "center",
    paddingHorizontal: sizeWidth(10),
    paddingVertical: moderateScale(11)
  },
  content: {
    flex: 1
  },
  name: {},
  expand: {
    marginLeft: sizeWidth(10),
    width: sizeWidth(7),
    height: sizeWidth(12)
  },
  separator: {
    backgroundColor: "#DDDDDD",
    width: "100%",
    height: 1
  },
  image: {
    width: moderateScale(7),
    height: moderateScale(11),
    marginLeft: sizeWidth(12)
  },
  history: {
    paddingHorizontal: sizeWidth(10),
    paddingVertical: moderateScale(12),
    backgroundColor: "white",
    flexDirection: "row",
    alignItems: "center"
  },
  clear: {
    color: appColor.blur,
    fontSize: sizeFont(13),
    fontStyle: "italic",
    paddingHorizontal: sizeWidth(10),
    paddingVertical: moderateScale(12)
  },
  in: {
    color: appColor.blur,
    fontSize: sizeFont(13),
    marginTop: sizeWidth(4)
  },
  highlight: {
    color: "#1ca8d2",
    fontSize: sizeFont(13)
  },
  group: {
    flex: 1
  }
});
