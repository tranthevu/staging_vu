import React, { Component, ReactNode } from "react";
import { View, StyleSheet, TouchableOpacity, Image } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import { resetPage, navigateBack, navigateToPage } from "../../actions/nav.action";
import {
  sizeWidth,
  sizeHeight,
  sizeFont,
  moderateScale
} from "../../helpers/size.helper";
import Toolbar from "../../components/common/toolbar";
import Text from "../../components/common/text";
import BackIcon from "../../components/common/back-icon";
import { appColor, font } from "../../constants/app.constant";
import NotificationItem from "./notification-item";
import {
  fetchMoreUnreadNotifications,
  refreshUnreadNotifications,
  requestUnreadNotifications
} from "../../actions/unread-notifications.action";
import PaginationList from "../../components/common/pagination-list";
import {
  fetchMoreNotifications,
  refreshNotifications,
  requestNotifications,
  readNotification,
  requestCountNotifications
} from "../../actions/notifications.action";
import Button from "../../components/common/button";
import AuthHelper from "../../helpers/auth.helper";
import LoadingIndicator from "../../components/common/loading-indicator";
import NotificationView from "../../components/common/notification-view";
import EmptyView from "../../components/common/empty-view";

const tabs = {
  all: 0,
  unread: 1
};

class NotificationsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedTab: tabs.all,
      isAuthenticated: false,
      loading: true
    };
  }

  renderTabs = () => {
    const { selectedTab } = this.state;
    return (
      <View style={styles.tabs}>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.all })}
          style={styles.tab}
        >
          <Text style={selectedTab === tabs.all ? styles.activeText : styles.text}>
            Tất cả
          </Text>
          {selectedTab === tabs.all && <View style={styles.line} />}
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.setState({ selectedTab: tabs.unread })}
          style={styles.tab}
        >
          <Text style={selectedTab === tabs.unread ? styles.activeText : styles.text}>
            Chưa đọc
          </Text>
          {selectedTab === tabs.unread && <View style={styles.line} />}
        </TouchableOpacity>
      </View>
    );
  };

  renderAll = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      notifications
    } = this.props.notifications;
    const { refreshNotifications, fetchMoreNotifications } = this.props;
    return (
      <PaginationList
        EmptyComponent={this.renderEmpty()}
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderNotification}
        loading={loading}
        renderSeparator={this.renderSeparator}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        firstLoading={firstLoading}
        onEndReached={fetchMoreNotifications}
        onRefresh={refreshNotifications}
        data={notifications}
      />
    );
  };

  renderEmpty = () => {
    return (
      <View style={styles.noResult}>
        <Image
          style={styles.no}
          source={require("../../../res/icon/empty-notification.png")}
        />
        <Text style={styles.empty}>Không có thông báo</Text>
      </View>
    );
  };

  renderSeparator = () => {
    return <View style={styles.separator} />;
  };

  renderUnread = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      unreadNotifications
    } = this.props.unreadNotifications;
    return (
      <PaginationList
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderNotification}
        loading={loading}
        reachedEnd={reachedEnd}
        refreshing={refreshing}
        renderSeparator={this.renderSeparator}
        firstLoading={firstLoading}
        onEndReached={fetchMoreUnreadNotifications}
        onRefresh={refreshUnreadNotifications}
        data={unreadNotifications}
        EmptyComponent={<EmptyView />}
      />
    );
  };

  renderNotification = ({ item, index }) => {
    const { readNotification, requestCountNotifications } = this.props;
    return (
      <NotificationItem
        key={index}
        readNotification={readNotification}
        unread={lodash.get(item, "notify_status") === "unread"}
        item={item}
        requestCountNotifications={requestCountNotifications}
      />
    );
  };

  renderNotLoggedIn = () => {
    return (
      <View style={styles.body}>
        <Image
          source={require("../../../res/icon/bell-yellow.png")}
          style={styles.image}
        />
        <Text style={styles.login}>Đăng nhập để nhận thông báo</Text>
        <Text style={styles.desc}>
          Đăng nhập để cập nhật nhanh nhất các thông tin về ưu đãi và có nhiều trải nghiệm
          tốt hơn
        </Text>
        <Button
          onPress={this.navigateToLogin}
          style={styles.button}
          text="ĐĂNG NHẬP NGAY"
        />
      </View>
    );
  };

  render(): ReactNode {
    const { selectedTab, isAuthenticated, loading } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          left={<BackIcon />}
          center={<Text style={styles.title}>Thông báo</Text>}
        />
        <View style={styles.content}>
          {loading ? (
            <LoadingIndicator />
          ) : isAuthenticated ? (
            this.renderAll()
          ) : (
            this.renderNotLoggedIn()
          )}
          <NotificationView />
        </View>
      </View>
    );
  }

  navigateToLogin = () => {
    this.props.navigateToPage("Login", { canBack: true });
  };

  componentDidMount = async () => {
    const isAuthenticated = await AuthHelper.isAuthenticated();
    this.setState({ isAuthenticated, loading: false });
    if (isAuthenticated) this.props.requestNotifications();
  };
}

export default connect(
  state => ({
    unreadNotifications: state.unreadNotifications,
    notifications: state.notifications
  }),
  {
    resetPage,
    navigateBack,
    navigateToPage,
    fetchMoreUnreadNotifications,
    refreshUnreadNotifications,
    requestUnreadNotifications,
    fetchMoreNotifications,
    refreshNotifications,
    requestNotifications,
    readNotification,
    requestCountNotifications
  }
)(NotificationsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#F9F9F9"
  },
  content: {
    flex: 1
  },
  title: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  tabs: {
    flexDirection: "row",
    height: sizeWidth(44),
    backgroundColor: "white"
  },
  tab: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  text: {
    color: appColor.blur,
    fontFamily: font.medium
  },
  activeText: {
    fontFamily: font.bold
  },
  line: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: sizeWidth(3),
    backgroundColor: appColor.primary
  },
  old: {
    color: appColor.blur,
    marginHorizontal: sizeWidth(9),
    marginTop: sizeWidth(10)
  },
  noResult: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center"
  },
  no: {
    width: sizeWidth(120),
    height: sizeWidth(120)
  },
  empty: {
    color: appColor.blur,
    fontSize: sizeFont(13),
    fontStyle: "italic",
    textAlign: "center",
    alignSelf: "center",
    marginTop: sizeWidth(22)
  },
  separator: {
    height: 1,
    backgroundColor: "#DDDDDD",
    width: sizeWidth(320)
  },
  image: {
    width: moderateScale(88),
    marginTop: moderateScale(28),
    marginBottom: moderateScale(23),
    height: moderateScale(102)
  },
  login: {
    fontSize: sizeFont(16),
    fontFamily: font.bold,
    marginVertical: moderateScale(10)
  },
  desc: {
    textAlign: "center",
    fontSize: sizeFont(13),
    paddingHorizontal: sizeWidth(20)
  },
  body: {
    alignItems: "center",
    flex: 1
  },
  button: {
    marginTop: moderateScale(15)
  }
});
