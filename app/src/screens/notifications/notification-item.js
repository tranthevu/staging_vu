import React, { Component, ReactNode } from "react";
import { View, StyleSheet, Image, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import lodash from "lodash";
import moment from "moment";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { appColor, font } from "../../constants/app.constant";

import { navigateNotification } from "../../helpers/notification-navigate.helper";
import Api from "../../api/api";
import "moment/min/locales";
import { checkApiStatus } from "../../helpers/app.helper";

class NotificationItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  navigate = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const { item, dispatch, requestCountNotifications, readNotification } = this.props;
    navigateNotification(dispatch, item);
    try {
      await Api.updateNotification({ id: item.id, status: "read" });
      readNotification(item.id);
      requestCountNotifications();
    } catch (err) {}
  };

  render(): ReactNode {
    const { style, item = {}, unread } = this.props;
    const timestamp = moment(item.date_create);
    const time = timestamp.locale("vi").fromNow();
    const title = lodash.get(item, "title");
    const body = lodash.get(item, "body");
    return (
      <TouchableOpacity onPress={this.navigate} style={[styles.container, style]}>
        <Image
          style={styles.icon}
          source={require("../../../res/icon/yo-notification.jpg")}
        />
        <View style={styles.content}>
          <Text numberOfLines={2} style={styles.title}>
            {title}
          </Text>
          <Text style={styles.body}>{body}</Text>
          <Text style={styles.time}>{time}</Text>
        </View>
        {unread && this.renderDot()}
      </TouchableOpacity>
    );
  }

  renderDot = () => {
    return <View style={styles.dot} />;
  };
}
export default connect(null, null)(NotificationItem);

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    width: sizeWidth(320),
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: sizeWidth(12),
    paddingVertical: sizeWidth(6)
  },
  content: {
    flex: 1,
    marginRight: moderateScale(8)
  },
  title: {
    fontSize: sizeFont(13),
    fontFamily: font.medium
  },
  body: {
    fontSize: sizeFont(13),
    marginTop: sizeWidth(4)
  },
  time: {
    fontSize: sizeFont(12),
    marginTop: sizeWidth(4),
    color: appColor.blur
  },
  dot: {
    backgroundColor: "#EF4136",
    width: moderateScale(8),
    alignSelf: "center",
    height: moderateScale(8),
    borderRadius: moderateScale(8)
  },
  icon: {
    width: moderateScale(40),
    alignSelf: "flex-start",
    height: moderateScale(40),
    borderRadius: moderateScale(6),
    marginRight: sizeWidth(12)
  }
});
