import React, { Component } from "react";
import {
  View,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Image
} from "react-native";
import { connect } from "react-redux";
import { isIphoneX } from "react-native-iphone-x-helper";
import lodash from "lodash";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import Text from "../../components/common/text";
import PaginationList from "../../components/common/pagination-list";
import { sizeFont, sizeWidth, moderateScale } from "../../helpers/size.helper";
import { font, appColor, text } from "../../constants/app.constant";
import Toolbar from "../../components/common/toolbar";
import TouchableIcon from "../../components/common/touchable-icon";
import PromotionItem from "../dashboard/promotion-item";
import BackIcon from "../../components/common/back-icon";
import Brand from "./brand";
import LoadingIndicator from "../../components/common/loading-indicator";
import Api from "../../api/api";
import {
  fetchMoreCategoryProducts,
  refreshCategoryProducts,
  requestCategoryProducts,
  resetCategoryProducts
} from "../../actions/category-products.action";
import PromotionItemList from "../dashboard/promotion-item-list";
import ChatbotIcon from "../../components/common/chatbot-icon";
import NotificationView from "../../components/common/notification-view";
import FavoriteItem from "../../components/Item/FavoriteItem";
import {
  updateFavoriteCategory,
  checkFavoriteCategoriesSubmit
} from "../../actions/profile.action";
import AuthHelper from "../../helpers/auth.helper";
import { appConfig } from "../../config/app.config";
import { checkApiStatus } from "../../helpers/app.helper";
const { width, height } = Dimensions.get("window");
const viewType = {
  grid: 0,
  list: 1
};

class CategoryDashboardScreen extends Component {
  constructor(props) {
    super(props);
    this.params = props.navigation.state.params || {};

    this.state = {
      category: {},
      childs: [],
      selectedChild: null,
      loading: true,
      appliedFilters: {},
      selectedViewType: viewType.list,
      isAuthenticated: false,
      isFavorite: false,
      isExclusive: false
    };
  }

  onCheckFavoriteCategoryResponse = (err, response) => {
    if (response) {
      const { data = {} } = response || {};
      const { is_favorite } = data || {};
      this.setState({ isFavorite: is_favorite });
    }
  };

  checkFavoriteCategory = slug => {
    this.props.checkFavoriteCategoriesSubmit({
      slug,
      callback: this.onCheckFavoriteCategoryResponse
    });
  };

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    const slug = lodash.get(this.props.navigation.state.params, "category.category.slug");
    const isExclusive = lodash.get(this.props.navigation.state.params, "isExclusive");
    const selectedChild = lodash.get(this.props.navigation.state.params, "selectedChild");
    AuthHelper.isAuthenticated().then(result => {
      if (result) {
        this.setState({ isAuthenticated: true });
        this.checkFavoriteCategory(slug);
      }
    });
    if (!slug) return;
    try {
      const { activeApp } = appConfig;
      const data = await Api.getCategory(slug, activeApp.key);
      const category = data.data.categories.self.find(item => item.slug === slug);
      this.setState({
        category,
        selectedChild:
          selectedChild ||
          (data.data.categories.childs
            ? { ...category, name: "Tất cả" }
            : { ...category }),
        childs: data.data.categories.childs
          ? [{ ...category, name: "Tất cả" }, ...data.data.categories.childs]
          : [],
        loading: false,
        appliedFilters: {
          selectedCategory: selectedChild || { ...category, name: "Tất cả" }
        },
        isExclusive
      });
      this.props.requestCategoryProducts(
        selectedChild ? selectedChild.slug : category.slug,
        undefined,
        undefined,
        activeApp.key,
        isExclusive
      );
    } catch (err) {
      this.setState({ loading: false });
    }
  };

  onPressToggleFavorite = async (item, isCheck) => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      this.props.resetPage("Maintenance");
    }
    if (item && item.slug)
      this.props.updateFavoriteCategory({
        slug: item.slug,
        isFavorite: !isCheck,
        callback: () => {
          this.checkFavoriteCategory(item.slug);
        }
      });
  };

  renderHeader = () => {
    const {
      selectedChild,
      selectedViewType,
      isAuthenticated,
      isFavorite,
      appliedFilters
    } = this.state;
    const { category = {} } = lodash.get(this.params, "category", {});
    const selectedCatSlug = lodash.get(selectedChild, "slug");
    const defaultCatSlug = lodash.get(category, "slug");
    const activeCategory =
      selectedCatSlug === defaultCatSlug || lodash.isEmpty(selectedChild)
        ? category
        : selectedChild;

    const selectedCategory = lodash.get(appliedFilters, "selectedCategory");
    const selectedMerchants = lodash.get(appliedFilters, "selectedMerchants");
    const isSelectedCategory =
      !lodash.isEmpty(selectedCategory) &&
      lodash.get(selectedCategory, "name") !== "Tất cả";
    const isSelectedMerchants = !lodash.isEmpty(selectedMerchants);
    return (
      <View style={styles.top}>
        {isAuthenticated && (
          <FavoriteItem
            isChecked={isFavorite}
            item={activeCategory}
            style={styles.favorite}
            onPressToggle={this.onPressToggleFavorite}
          />
        )}
        <View style={styles.body}>
          <Text numberOfLines={1} style={styles.title}>
            Ưu đãi mới
          </Text>

          <TouchableIcon
            style={styles.view}
            iconStyle={styles.viewIcon}
            onPress={this.toggleViewType}
            source={
              selectedViewType === viewType.list
                ? require("../../../res/icon/list.png")
                : require("../../../res/icon/grid.png")
            }
          />
        </View>
        {(isSelectedCategory || isSelectedMerchants) && (
          <View style={styles.selectedFilters}>
            <ScrollView
              horizontal={true}
              contentContainerStyle={styles.selectedFiltersScroll}
              bounces={false}
              showsHorizontalScrollIndicator={false}
            >
              {isSelectedCategory && this.renderSelectedFilter(selectedCategory, false)}
              {isSelectedMerchants &&
                selectedMerchants.map(item => this.renderSelectedFilter(item, true))}
            </ScrollView>
          </View>
        )}
      </View>
    );
  };

  renderSelectedFilter = (selected, isMerchant) => {
    return (
      <View style={styles.selectedFilter}>
        <Text style={styles.selectedFilterText}>
          {lodash.get(selected, "name", " - ")}
        </Text>
        <TouchableOpacity
          style={styles.btnCloseFilter}
          onPress={() => {
            if (isMerchant) {
              this.resetMerchant(selected);
              return;
            }
            this.resetCategory();
          }}
        >
          <Image
            style={styles.filterCloseIco}
            source={require("../../../res/icon/x-tag.png")}
          />
        </TouchableOpacity>
      </View>
    );
  };

  resetCategory = () => {
    const { appliedFilters, category } = this.state;
    const newAppliedFilters = {
      ...appliedFilters,
      selectedCategory: { ...category, name: "Tất cả" }
    };
    this.onApplyFilters(newAppliedFilters);
  };

  resetMerchant = selectedMerchant => {
    const { appliedFilters } = this.state;
    const selectedMerchants = lodash.get(appliedFilters, "selectedMerchants");
    const newAppliedFilters = {
      ...appliedFilters,
      selectedMerchants: selectedMerchants.filter(
        item => item.code !== selectedMerchant.code
      )
    };
    this.onApplyFilters(newAppliedFilters);
  };

  toggleViewType = () => {
    const { selectedViewType } = this.state;
    this.setState({
      selectedViewType: selectedViewType === viewType.grid ? viewType.list : viewType.grid
    });
  };

  renderMerchants = () => {
    const { topMerchants = [] } = this.props.topMerchants;
    if (topMerchants.length === 0) return null;
    return (
      <View>
        <Text style={styles.label}>Thương hiệu</Text>
        <View style={styles.list}>
          <FlatList
            data={topMerchants}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderBrand}
          />
        </View>
      </View>
    );
  };

  renderBrand = ({ item }) => {
    return <Brand item={item} />;
  };

  navigateToSearch = () => {
    this.props.navigateToPage("Search");
  };

  render() {
    const { loading } = this.state;
    const name = lodash.get(this.props.navigation.state.params, "category.category.name");
    return (
      <View style={styles.container}>
        <Toolbar
          leftStyle={[styles.part, styles.left]}
          rightStyle={styles.part}
          center={
            <Text numberOfLines={1} style={styles.header}>
              {name}
            </Text>
          }
          left={<BackIcon />}
          right={
            <View style={styles.right}>
              <TouchableIcon
                disabled={loading}
                style={styles.boundary}
                onPress={this.navigateToFilter}
                iconStyle={styles.icon}
                source={require("../../../res/icon/sort.png")}
              />
              <TouchableIcon
                style={styles.boundary}
                onPress={this.navigateToSearch}
                iconStyle={styles.icon}
                source={require("../../../res/icon/home-search.png")}
              />
            </View>
          }
        />
        {/* <SearchToolbar /> */}
        <View style={styles.wrap}>
          {loading ? <LoadingIndicator /> : this.renderBody()}

          <NotificationView />
        </View>
        <ChatbotIcon
          maxY={height - moderateScale(isIphoneX() ? 60 : 15)}
          y={height - moderateScale(isIphoneX() ? 140 : 95)}
        />
      </View>
    );
  }

  selectCategory = category => {
    if (!category) return;

    const { appliedFilters, isAuthenticated, isExclusive } = this.state;
    const merchant = "";
    const ordering = lodash.get(appliedFilters, "selectedOrder.type", text.emptyString);
    this.setState({
      selectedChild: category,
      appliedFilters: {
        ...appliedFilters,
        selectedMerchants: [],
        selectedCategory: category
      }
    });

    if (isAuthenticated) {
      this.checkFavoriteCategory(category.slug);
    }
    const { activeApp } = appConfig;
    this.props.requestCategoryProducts(
      category.slug,
      merchant,
      ordering,
      activeApp.key,
      isExclusive
    );
  };

  renderChildCategory = ({ item }) => {
    const { selectedChild } = this.state;
    const selected = lodash.get(item, "id") === lodash.get(selectedChild, "id");
    return (
      <TouchableOpacity onPress={() => this.selectCategory(item)} style={styles.tab}>
        {/* YA15-222
          commented out the total_product since API not supported yet
        */}
        <Text style={selected ? styles.activeText : styles.text}>
          {lodash.get(item, "name")}
          {/* ({lodash.get(item, "total_product", 0)}) */}
        </Text>
        {selected && <View style={styles.line} />}
      </TouchableOpacity>
    );
  };

  resetFilter = () => {
    const { category, isExclusive } = this.state;
    this.setState({
      selectedChild: { ...category, name: "Tất cả" },
      appliedFilters: {
        selectedCategory: { ...category, name: "Tất cả" }
      }
    });
    const { activeApp } = appConfig;
    const slug = lodash.get(category, "slug");
    this.props.requestCategoryProducts(
      slug,
      undefined,
      undefined,
      activeApp.key,
      isExclusive
    );
  };

  renderEmpty = () => {
    const { appliedFilters } = this.state;
    const merchant = lodash
      .get(appliedFilters, "selectedMerchants", [])
      .map(item => item.code)
      .join(",");
    const ordering = lodash.get(appliedFilters, "selectedOrder.type", text.emptyString);

    return merchant || ordering ? (
      <View style={styles.empty}>
        <Text style={styles.textEmpty}>
          Không có ưu đãi nào phù hợp với tiêu chí bộ lọc
        </Text>
        <Text onPress={this.resetFilter} style={styles.reset}>
          Đặt lại mặc định
        </Text>
      </View>
    ) : (
      <View style={styles.empty}>
        <Text style={styles.noResults}>Bạn chưa có ưu đãi nào</Text>
      </View>
    );
  };

  renderBody = () => {
    const { childs, selectedChild } = this.state;
    return (
      <View style={styles.root}>
        <View>
          <FlatList
            extraData={selectedChild}
            data={childs}
            bounces={false}
            contentContainerStyle={styles.content}
            showsHorizontalScrollIndicator={false}
            horizontal
            keyExtractor={(item, index) => index.toString()}
            renderItem={this.renderChildCategory}
          />
        </View>
        {this.renderOffers()}
      </View>
    );
  };

  renderOffers = () => {
    const {
      loading,
      reachedEnd,
      refreshing,
      firstLoading,
      categoryProducts
    } = this.props.categoryProducts;
    const { selectedChild, appliedFilters, isExclusive } = this.state;
    const category = lodash.get(selectedChild, "slug");
    const merchant = lodash
      .get(appliedFilters, "selectedMerchants", [])
      .map(item => item.code)
      .join(",");
    const ordering = lodash.get(appliedFilters, "selectedOrder.type", text.emptyString);
    const { fetchMoreCategoryProducts, refreshCategoryProducts } = this.props;
    const { activeApp } = appConfig;
    return (
      <PaginationList
        keyExtractor={(item, index) => index.toString()}
        renderItem={this.renderPromotion}
        extraData={this.state}
        loading={loading}
        reachedEnd={reachedEnd}
        EmptyComponent={this.renderEmpty()}
        refreshing={refreshing}
        firstLoading={firstLoading}
        ListHeaderComponent={this.renderHeader()}
        onEndReached={() =>
          fetchMoreCategoryProducts(
            category,
            merchant,
            ordering,
            activeApp.key,
            isExclusive
          )
        }
        onRefresh={() =>
          refreshCategoryProducts(
            category,
            merchant,
            ordering,
            activeApp.key,
            isExclusive
          )
        }
        data={categoryProducts}
      />
    );
  };

  navigateToFilter = () => {
    const { appliedFilters, selectedChild, childs, isExclusive } = this.state;
    const category = lodash.get(selectedChild, "slug");
    this.props.navigateToPage("Filter", {
      onApplyFilters: this.onApplyFilters,
      appliedFilters,
      category,
      categories: childs,
      isExclusive
    });
  };

  onApplyFilters = appliedFilters => {
    this.setState({ appliedFilters });
    const { isExclusive } = this.state;
    const { selectedCategory } = appliedFilters;
    const category = lodash.get(selectedCategory, "slug");
    this.setState({ selectedChild: selectedCategory });
    const merchant = lodash
      .get(appliedFilters, "selectedMerchants", [])
      .map(item => item.code)
      .join(",");
    const ordering = lodash.get(appliedFilters, "selectedOrder.type", text.emptyString);
    const { activeApp } = appConfig;
    this.props.requestCategoryProducts(
      category,
      merchant,
      ordering,
      activeApp.key,
      isExclusive
    );
  };

  renderPromotion = ({ item }) => {
    const { selectedViewType } = this.state;
    return selectedViewType === viewType.grid ? (
      <PromotionItem item={item} />
    ) : (
      <PromotionItemList item={item} />
    );
  };

  componentWillUnmount = () => {
    this.props.resetCategoryProducts();
  };
}

export default connect(
  state => ({
    topMerchants: state.topMerchants,
    categoryProducts: state.categoryProducts
  }),
  {
    resetPage,
    navigateToPage,
    fetchMoreCategoryProducts,
    refreshCategoryProducts,
    requestCategoryProducts,
    resetCategoryProducts,
    updateFavoriteCategory,
    checkFavoriteCategoriesSubmit
  }
)(CategoryDashboardScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: appColor.bg
  },
  wrap: {
    flex: 1
  },
  root: {
    flex: 1
  },
  label: {
    fontSize: sizeFont(24),
    fontFamily: font.bold,
    paddingHorizontal: sizeWidth(9)
  },
  title: {
    fontSize: sizeFont(16),
    fontFamily: font.medium,
    flex: 1
  },
  menu: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  body: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: moderateScale(12),
    marginBottom: moderateScale(5),
    paddingHorizontal: sizeWidth(9)
  },
  list: {
    marginTop: moderateScale(6),
    marginBottom: moderateScale(15)
  },
  header: {
    fontSize: sizeFont(18),
    fontFamily: font.bold,
    color: "white"
  },
  count: {
    fontSize: sizeFont(14),
    fontFamily: font.bold
  },
  filterIcon: {
    width: moderateScale(20),
    height: moderateScale(20)
  },
  filter: {
    alignSelf: "center",
    marginLeft: sizeWidth(12)
  },
  viewIcon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: appColor.blur
  },
  view: {
    alignSelf: "center",
    marginLeft: sizeWidth(12)
  },
  tab: {
    height: moderateScale(44),
    paddingHorizontal: moderateScale(10),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  },
  text: {
    color: appColor.blur,
    fontFamily: font.medium
  },
  activeText: {
    fontFamily: font.bold
  },
  line: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    height: sizeWidth(3),
    backgroundColor: "#FF6961"
  },
  content: {
    backgroundColor: "white"
  },
  empty: {
    alignItems: "center",
    padding: sizeWidth(12)
  },
  noResults: {
    color: "#979797",
    fontSize: sizeFont(13),
    fontStyle: "italic"
  },
  textEmpty: {
    fontSize: sizeFont(13)
  },
  reset: {
    color: "#55ACEE",
    fontSize: sizeFont(13),
    marginTop: sizeWidth(12)
  },
  icon: {
    width: moderateScale(24),
    height: moderateScale(24),
    tintColor: "white"
  },
  boundary: {
    marginLeft: moderateScale(6)
  },
  right: {
    flexDirection: "row",
    alignItems: "flex-end",
    paddingRight: moderateScale(5)
  },
  part: {
    width: moderateScale(90)
  },
  left: {
    alignItems: "flex-start",
    paddingLeft: moderateScale(10)
  },
  top: {
    paddingTop: moderateScale(10)
  },
  favorite: {
    paddingHorizontal: sizeWidth(9),
    marginBottom: moderateScale(5)
  },
  selectedFilters: {
    flexDirection: "row",
    marginBottom: sizeWidth(5)
  },
  selectedFiltersScroll: {
    paddingHorizontal: sizeWidth(10)
  },
  selectedFilter: {
    paddingLeft: sizeWidth(13),
    paddingRight: sizeWidth(9),
    height: sizeWidth(22),
    marginRight: sizeWidth(5),
    backgroundColor: "#FFF",
    borderWidth: 1,
    borderColor: "#DDD",
    borderRadius: sizeWidth(15),
    flexDirection: "row",
    alignItems: "center"
  },
  selectedFilterText: {
    fontSize: sizeFont(13)
  },
  btnCloseFilter: {
    width: moderateScale(15),
    height: moderateScale(15),
    marginLeft: moderateScale(7),
    justifyContent: "center",
    alignItems: "center",
    marginBottom: sizeWidth(1)
  },
  filterCloseIco: {
    width: moderateScale(8),
    height: moderateScale(8)
  }
});
