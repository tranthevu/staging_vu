import React, { Component, ReactNode } from "react";
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TextInput,
  SafeAreaView,
  TouchableOpacity
} from "react-native";
import { resetPage, navigateToPage } from "../../actions/nav.action";
import { connect } from "react-redux";
import Text from "../../components/common/text";
import { sizeFont, sizeWidth } from "../../helpers/size.helper";
import { font } from "../../constants/app.constant";
import TouchableIcon from "../../components/common/touchable-icon";
import lodash from "lodash";

class Brand extends Component {
  constructor(props) {
    super(props);
  }

  navigateToMerchant = () => {
    this.props.navigateToPage("MerchantDetail");
  };

  componentDidMount = () => {};

  render(): ReactNode {
    const { item } = this.props;
    const name = lodash.get(item, "merchant.name");
    const image = lodash.get(item, "merchant.image");
    const total = lodash.get(item, "merchant.total_product");
    return (
      <View onPress={this.navigateToMerchant} style={styles.container}>
        <Image
          resizeMode="stretch"
          source={
            image
              ? { uri: image }
              : require("../../../res/img/default-image.png")
          }
          style={styles.image}
        />
        <View style={styles.body}>
          <Text numberOfLines={2} style={styles.name}>
            {name}
          </Text>
          <Text style={styles.count}>{total} ưu đãi mới</Text>
        </View>
      </View>
    );
  }
}

export default connect(
  null,
  { resetPage, navigateToPage }
)(Brand);

const styles = StyleSheet.create({
  container: {
    width: sizeWidth(152),
    marginHorizontal: sizeWidth(5),
    borderRadius: sizeWidth(6),
    backgroundColor: "white"
  },
  image: {
    width: sizeWidth(152),
    height: sizeWidth(114)
  },
  name: {
    fontFamily: font.bold,
    fontSize: sizeFont(12),
    marginBottom: sizeWidth(4)
  },
  count: {
    fontSize: sizeFont(12),
    fontStyle: "italic"
  },
  body: {
    paddingHorizontal: sizeWidth(14),
    paddingVertical: sizeWidth(9)
  }
});
