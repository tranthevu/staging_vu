import { Platform, Share } from 'react-native';

export function shareUrl(url) {
  // Fix https://yollateam.atlassian.net/browse/YA15-402
  // Fix https://yollateam.atlassian.net/browse/YA15-380
  // https://facebook.github.io/react-native/docs/share
  if (Platform.OS === "ios") {
    Share.share({ url });
  } else {
    Share.share({ message: url });
  }
}
