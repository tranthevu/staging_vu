import { Alert } from "react-native";

export function showAlertConfirm(
  config = { title: "", message: "" },
  callback = () => {},
  options = { cancelable: false }
) {
  const { message = "", title = "Thông báo" } = config;
  Alert.alert(
    title,
    message,
    [
      {
        text: "Không",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel"
      },
      {
        text: "Có",
        onPress: () => {
          callback();
        }
      }
    ],
    options
  );
}

export function showAlertNotification(
  config = { title: "", message: "" },
  callback = () => {},
  options = { cancelable: false }
) {
  const { message = "", title = "Thông báo" } = config;
  Alert.alert(
    title,
    message,
    [
      {
        text: "Ok",
        onPress: () => {
          if (typeof callback === "function") callback();
        }
      }
    ],
    options
  );
}
