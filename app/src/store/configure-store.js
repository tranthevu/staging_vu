import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import createSagaMiddleware from "redux-saga";
import rootSaga from "../sagas/root.saga";
import { bannersReducer } from "../reducers/banners.reducer";
import { topCategoriesReducer } from "../reducers/top-categories.reducer";
import { popularKeywordsReducer } from "../reducers/popular-keywords.reducer";
import { autocompleteReducer } from "../reducers/autocomplete.reducer";
import notificationsReducer from "../reducers/notifications.reducer";
import areasReducer from "../reducers/areas.reducer";
import hotProductsReducer from "../reducers/hot-products.reducer";
import recentProductsReducer from "../reducers/recent-products.reducer";
import expiringProductsReducer from "../reducers/expiring-products.reducer";
import unusedVouchersReducer from "../reducers/unused-vouchers.reducer";
import usedVouchersReducer from "../reducers/used-vouchers.reducer";
import unreadNotificationsReducer from "../reducers/unread-notifications.reducer";
import { profileReducer } from "../reducers/profile.reducer";
import { topMerchantsReducer } from "../reducers/top-merchants.reducer";
import { filtersReducer } from "../reducers/filters.reducer";
import categoryProductsReducer from "../reducers/category-products.reducer";
import searchProductsReducer from "../reducers/search-products.reducer";
import { masterDataReducer } from "../reducers/master-data.reducer";
import { homeAreasReducer } from "../reducers/home-areas.reducer";
import locationProductsReducer from "../reducers/location-products.reducer";
import savedOffersReducer from "../reducers/saved-offers.reducer";
import allProductsReducer from "../reducers/all-products.reducer";
import merchantProductsReducer from "../reducers/merchant-products.reducer";
import onlineProductsReducer from "../reducers/online-products.reducer";
import savedGiftsReducer from "../reducers/saved-gifts.reducer";
import exclusiveProductsReducer from "../reducers/exclusive-products.reducer";
import loadingReducer from "../reducers/loading.reducer";
import promoProductsReducer from "../reducers/promo-products.reducer";
import couponProductsReducer from "../reducers/coupon-products.reducer";
import share2EarnProductsReducer from "../reducers/share2earn-products.reducer";
import joinedCampaignsReducer from "../reducers/joined-campaigns.reducer";
import followMerchantsReducer from "../reducers/follow-merchants.reducer";
import partnerProductsReducer from "../reducers/partner-products.reducer";
import share2EarnPromoReducer from "../reducers/share2earn-promo.reducer";
import savingHistoryReducer from "../reducers/saving-history.reducer";
import redemptionHistoryReducer from "../reducers/redemption-history.reducer";
import { merchantReducer } from "../reducers/merchant.reducer";

const sagaMiddleware = createSagaMiddleware();

// eslint-disable-next-line no-undef
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(applyMiddleware(sagaMiddleware));

const configureStore = (navReducer, preloadedState = {}) => {
  const appReducers = combineReducers({
    nav: navReducer,
    banners: bannersReducer,
    topCategories: topCategoriesReducer,
    popularKeywords: popularKeywordsReducer,
    autocomplete: autocompleteReducer,
    notifications: notificationsReducer,
    areas: areasReducer,
    hotProducts: hotProductsReducer,
    recentProducts: recentProductsReducer,
    expiringProducts: expiringProductsReducer,
    unusedVouchers: unusedVouchersReducer,
    usedVouchers: usedVouchersReducer,
    unreadNotifications: unreadNotificationsReducer,
    profile: profileReducer,
    topMerchants: topMerchantsReducer,
    filters: filtersReducer,
    categoryProducts: categoryProductsReducer,
    searchProducts: searchProductsReducer,
    masterData: masterDataReducer,
    homeAreas: homeAreasReducer,
    locationProducts: locationProductsReducer,
    savedOffers: savedOffersReducer,
    savedGifts: savedGiftsReducer,
    allProducts: allProductsReducer,
    merchantProducts: merchantProductsReducer,
    onlineProducts: onlineProductsReducer,
    promoProducts: promoProductsReducer,
    couponProducts: couponProductsReducer,
    exclusiveProducts: exclusiveProductsReducer,
    share2EarnProducts: share2EarnProductsReducer,
    loadingReducer,
    joinedCampaigns: joinedCampaignsReducer,
    followMerchants: followMerchantsReducer,
    partnerProducts: partnerProductsReducer,
    share2EarnPromo: share2EarnPromoReducer,
    savingHistory: savingHistoryReducer,
    redemptionHistory: redemptionHistoryReducer,
    merchantReducer
  });
  const store = createStore(appReducers, preloadedState, enhancer);

  sagaMiddleware.run(rootSaga);

  return { store };
};

export default configureStore;
