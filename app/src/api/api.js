/* eslint-disable no-undef */
import qs from "qs";
import axios from "axios";
import RequestHelper from "../helpers/request.helper";
import { appConfig } from "../config/app.config";
import { getLocation } from "../helpers/storage.helper";
import { URL_API } from "../constants/app.constant";
import VoucherHelper from "../helpers/voucher.helper";

export default class Api {
  static loginForApp({
    service,
    provider,
    type,
    social_access_token,
    phone,
    password,
    full_name
  }) {
    let data = `group=yolla_network&service=${service}&provider=${provider}&type=${type}&social_access_token=${social_access_token}`;
    if (phone) data += `&phone=${phone}`;
    if (password) data += `&password=${password}`;
    if (full_name) data += `&full_name=${full_name}`;
    return RequestHelper.post(`${appConfig.hostUrl}login_for_app/`, data, {
      "Content-Type": "application/x-www-form-urlencoded"
    });
  }

  static getProfile() {
    return RequestHelper.get(`${appConfig.hostUrl}users/userprofile/`);
  }

  static updateInfo(data) {
    return RequestHelper.post(`${appConfig.hostUrl}users/info/`, data);
  }

  static register(data) {
    return RequestHelper.post(`${appConfig.apiUrl}register`, data);
  }

  static async getTopCategories() {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.apiUrl}categories/top/?locations=${location}`);
  }

  static async getTopMerchants() {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.apiUrl}merchants/top/?locations=${location}`);
  }

  static async getOffersByCategory(
    category,
    { page, page_size, merchant, ordering, type, field }
  ) {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.hostUrl}products/?category=${category}`, {
      page,
      page_size,
      merchant,
      ordering,
      type,
      locations: location,
      field
    });
  }

  static async getOffersByLocation(
    area,
    { page, page_size, category, merchant, ordering }
  ) {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.hostUrl}products/?area=${area}`, {
      page,
      page_size,
      category,
      merchant,
      ordering,
      locations: location
    });
  }

  static async searchProducts(params) {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.hostUrl}products/`, {
      locations: location,
      ...params
    });
  }

  static getSlideBanner() {
    return RequestHelper.get(
      `${appConfig.hostUrl}banner/banner_promotion_slider_mobile/`
    );
  }

  static getSlideBannerPromo() {
    return RequestHelper.get(`${appConfig.hostUrl}banner/banner_promo_mobile/`);
  }

  static getSlideBannerShareToEarnPromo() {
    return RequestHelper.get(`${appConfig.hostUrl}banner/banner_share_to_earn_mobile/`);
  }

  static getSlideBannerEcoupon() {
    return RequestHelper.get(`${appConfig.hostUrl}banner/banner_ecoupon_mobile/`);
  }

  static getPopularKeywords() {
    return RequestHelper.get(`${appConfig.apiUrl}search/popular`);
  }

  static getAutocomplete(keyword, category) {
    return RequestHelper.get(
      `${appConfig.apiUrl}search/autocomplete/?q=${keyword}&category=${category}`
    );
  }

  static getNotifications({ page, page_size }) {
    return RequestHelper.get(`${appConfig.apiUrl}notification/`, {
      page,
      page_size,
      type: "all"
    });
  }

  static async getAreas({ page, page_size }) {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.apiUrl}all-area/`, {
      page,
      page_size,
      locations: location
    });
  }

  static async getHomeAreas() {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.apiUrl}all-area/`, {
      page: 1,
      page_size: 6,
      locations: location,
      home: 1
    });
  }

  static getUnreadNotifications({ page, page_size }) {
    return RequestHelper.get(`${appConfig.apiUrl}notification/`, {
      page,
      page_size,
      type: "unread"
    });
  }

  static countNotifications() {
    return RequestHelper.get(`${appConfig.apiUrl}notification/count/`);
  }

  static async getRecentProducts({ page, page_size, category, merchant, ordering }) {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.hostUrl}products/`, {
      page,
      page_size,
      locations: location,
      field: "newest",
      category,
      merchant,
      ordering: ordering || "recent"
    });
  }

  static async getHotProducts({ page, page_size, category, merchant, ordering }) {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.hostUrl}products/`, {
      page,
      page_size,
      locations: location,
      field: "hot",
      category,
      merchant,
      ordering: "-priority,-date_valid_start"
    });
  }

  static async getAllProducts({ page, page_size, category, merchant, ordering }) {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.hostUrl}products/`, {
      page,
      page_size,
      field: "newest",
      locations: location,
      category,
      merchant
    });
  }

  static async getExpiringProducts({ page, page_size, category, merchant, ordering }) {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.hostUrl}products/`, {
      page,
      page_size,
      locations: location,
      field: "expiring",
      category,
      merchant,
      ordering: ordering || "expiring"
    });
  }

  static getProductDetail(productSlug) {
    return RequestHelper.get(`${appConfig.hostUrl}products/${productSlug}/`);
  }

  static dealsetDetail(productSlug) {
    return RequestHelper.get(`${appConfig.hostUrl}dealset/product/${productSlug}/`);
  }

  static saveWishlist(productSlug) {
    return RequestHelper.post(
      `${appConfig.hostUrl}dealset/product/${productSlug}/saved/`,
      null,
      null,
      "saveWishlist"
    );
  }

  static getUnusedVouchers(params) {
    return RequestHelper.get(`${appConfig.apiUrl}products/saved/`, params);
  }

  static userRef(username) {
    return RequestHelper.get(`${appConfig.hostUrl}users/user_ref/info/${username}/`);
  }

  static getSavedOffers(params) {
    return RequestHelper.get(`${appConfig.hostUrl}dealset/offer/`, params);
  }

  static getSavedGifts(params) {
    return RequestHelper.get(`${appConfig.hostUrl}users/reward_list/pending/`, params);
  }

  static getUsedVouchers(params) {
    return RequestHelper.get(`${appConfig.apiUrl}products/saved/`, params);
  }

  static getSavedProduct(productSlug, type) {
    return RequestHelper.get(
      `${appConfig.hostUrl}dealset/product/${productSlug}/codes/`,
      { type }
    );
  }

  static removeCode(productSlug, codeId) {
    return RequestHelper.delete(
      `${appConfig.hostUrl}dealset/product/${productSlug}/codes/${codeId}/`
    );
  }

  static removeOffer(productSlug) {
    return RequestHelper.delete(
      `${appConfig.hostUrl}dealset/product/${productSlug}/event/remove/`,
      null,
      null,
      "removeOffer"
    );
  }

  static async getCategory(slug, type) {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.apiUrl}home/sidebar-menu/`, {
      category: slug,
      locations: location,
      type,
      is_check_favorite: "True"
    });
  }

  static async getFilters(category = "", isExclusive = false) {
    const location = (await getLocation()) || "toan-quoc";
    if (isExclusive) {
      return RequestHelper.get(
        `${appConfig.hostUrl}products/page_detail/section/sidebar-menu/?category=${category}&merchant=&locations=${location}&section_slug=uu-dai-doc-quyen`
      );
    }
    return RequestHelper.get(
      `${appConfig.apiUrl}home/sidebar-menu/?locations=${location}&category=${category}&type=&area=&merchant=&product_types=`
    );
  }

  static getStaticIntroduction() {
    return RequestHelper.get(`${appConfig.hostUrl}post/gioi-thieu/`);
  }

  static getStaticPrivacy() {
    return RequestHelper.get(`${appConfig.hostUrl}post/chinh-sach-quyen-rieng-tu/`);
  }

  static getStaticPolicy() {
    return RequestHelper.get(`${appConfig.hostUrl}post/chinh-sach-va-dieu-khoan/`);
  }

  static getActivityPolicy() {
    return RequestHelper.get(`${appConfig.hostUrl}post/chinh-sach-hoat-dong/`);
  }

  static getStaticContact() {
    return RequestHelper.get(`${appConfig.hostUrl}post/lien-he/`);
  }

  static getStaticFAQs() {
    return RequestHelper.get(`${appConfig.hostUrl}post/faqs/`);
  }

  static getMasterData() {
    return RequestHelper.get(`${appConfig.apiUrl}merchant-register/data/`);
  }

  static updateProfile(data) {
    return RequestHelper.post(`${appConfig.hostUrl}users/userprofile/`, data);
  }

  static getAccountBank() {
    return RequestHelper.get(`${appConfig.hostUrl}users/receive_reward_info/`);
  }

  static updateAccountBank(data) {
    return RequestHelper.put(`${appConfig.hostUrl}users/receive_reward_info/`, data);
  }

  static uploadAvatar(image) {
    const formData = new FormData();
    formData.append("avatar", image);
    return RequestHelper.post(`${appConfig.hostUrl}users/userprofile/`, formData, {
      "Content-Type": "multipart/form-data"
    });
  }

  static confirmMultiUseOffer(data) {
    return RequestHelper.post(
      `${appConfig.apiUrl}products/merchant_used_offer/`,
      data,
      null,
      "confirmMultiUseOffer"
    );
  }

  static getMerchantDetail(code) {
    return RequestHelper.get(`${appConfig.hostUrl}merchants/${code}/`);
  }

  static getMerchantPartners(code) {
    return RequestHelper.get(`${appConfig.apiUrl}merchant/partner/${code}/`);
  }

  static registerTokenId(token) {
    return RequestHelper.post(`${appConfig.apiUrl}notification/register_token_id/`, {
      reg_id: token
    });
  }

  static registerTokenIdAnonymous(token) {
    return RequestHelper.postAnonymous(
      `${appConfig.apiUrl}notification/register_token_id/`,
      { reg_id: token }
    );
  }

  static unregisterTokenId(token) {
    return RequestHelper.post(`${appConfig.apiUrl}notification/register_token_id/`, {
      prev_reg_id: token
    });
  }

  static getNotificationList(params) {
    return RequestHelper.get(`${appConfig.apiUrl}notification/list/`, params);
  }

  static useMultiCode(data) {
    return RequestHelper.post(`${appConfig.apiUrl}products/usedoffer/multi`, data);
  }

  static getNotificationPopup() {
    return RequestHelper.get(`${appConfig.apiUrl}notification/popup/`);
  }

  static updateNotification(data) {
    return RequestHelper.post(`${appConfig.apiUrl}notification/update/`, data);
  }

  static addCart(data) {
    return RequestHelper.post(`${appConfig.hostUrl}carts/`, data);
  }

  static getCartPayment(data) {
    return RequestHelper.get(`${appConfig.hostUrl}carts/payment/${data}/`);
  }

  static proceedPayment(basketId, data) {
    return RequestHelper.post(`${appConfig.hostUrl}carts/payment/${basketId}/`, data);
  }

  static getTransaction(transactionId) {
    return RequestHelper.get(
      `${appConfig.hostUrl}carts/payment/transactions/${transactionId}/`
    );
  }

  static getPaymentTransactions(params = {}) {
    return RequestHelper.get(`${appConfig.hostUrl}carts/payment/transactions/`, params);
  }

  static getPaymentMethods() {
    return RequestHelper.get(`${appConfig.hostUrl}carts/payment-method/`);
  }

  static getSavedCode(productSlug, codeId) {
    return RequestHelper.get(
      `${appConfig.hostUrl}dealset/product/${productSlug}/codes/${codeId}/`
    );
  }

  static async getOnlineOffers() {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.hostUrl}products/`, {
      page_size: 7,
      ordering: "-expiring",
      page: 1,
      type: "affiliate-offer",
      field: "running",
      locations: location
    });
  }

  static async getExclusiveOffers() {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.hostUrl}products/exclusive/`, {
      page_size: 7,
      ordering: "recent",
      page: 1,
      locations: location
    });
  }

  static async getProductsByNear(data) {
    const { page, page_size, ...request } = data;
    return RequestHelper.post(
      `${appConfig.hostUrl}products/search/location/?page=${page}&&page_size=${page_size}`,
      request
    );
  }

  static async getProductsByNearDetail(data) {
    const { page, ...request } = data;
    return RequestHelper.post(
      `${appConfig.hostUrl}products/search/location/products/`,
      request
    );
  }

  static async nearbyOffers({ page, page_size, point, radius, category }) {
    return RequestHelper.post(
      `${appConfig.hostUrl}products/search/location/?page=${page}&page_size=${page_size}`,
      {
        point,
        radius,
        category
      }
    );
  }

  static statusShare2Earn() {
    return RequestHelper.get(`${appConfig.hostUrl}share2earn/status/`);
  }

  static share2EarnProducts(params) {
    return RequestHelper.get(`${appConfig.hostUrl}share2earn/`, params);
  }

  static share2EarnProductsYollaBiz(params) {
    return RequestHelper.get(`${appConfig.hostUrl}share2earn/yolla-biz/`, params);
  }

  static share2EarnProduct(productSlug) {
    return RequestHelper.get(`${appConfig.hostUrl}share2earn/${productSlug}/product/`);
  }

  static share2earnCampaignReceiveCode(params) {
    return RequestHelper.get(`${appConfig.hostUrl}share2earn-campaign/claim/`, params);
  }

  static async share2EarnPromo(params) {
    const location = (await getLocation()) || "toan-quoc";
    return RequestHelper.get(`${appConfig.hostUrl}products/`, {
      ...params,
      location,
      type: "share-to-earn"
    });
  }

  static productBreadcrumb(productSlug) {
    return RequestHelper.get(`${appConfig.hostUrl}products/${productSlug}/breadcrumb/`);
  }

  static redeemLocations(productSlug) {
    return RequestHelper.get(
      `${appConfig.hostUrl}products/${productSlug}/redeem-locations/`
    );
  }

  static getFollowMerchants(params) {
    return RequestHelper.get(`${appConfig.hostUrl}merchants/favorite/`, params);
  }

  static partnerProducts(params) {
    return RequestHelper.get(`${appConfig.hostUrl}products/partners-favorite/`, params);
  }

  static toggleFavorite({ merchantCode, isFavorite }) {
    return RequestHelper.post(`${appConfig.hostUrl}merchants/favorite/${merchantCode}/`, {
      is_favorite: isFavorite
    });
  }

  static updateFavoriteCategory({ slug, isFavorite }) {
    return RequestHelper.post(
      `${appConfig.hostUrl}categories/favorite_category/${slug}/`,
      {
        is_favorite: isFavorite
      }
    );
  }

  static getFavoriteCategories(params) {
    return RequestHelper.get(`${appConfig.hostUrl}categories/favorite_category/`, params);
  }

  static checkFavoriteCategory({ slug }) {
    return RequestHelper.get(`${appConfig.hostUrl}categories/favorite_category/${slug}/`);
  }

  static getMyPlaceByLocation = async ({ lat, lng }) => {
    const response = await axios.get(`${URL_API.GOOGLE}geocode/json`, {
      params: {
        latlng: `${lat},${lng}`,
        key: appConfig.mapApiKey,
        language: "vi"
      }
    });
    const place = response.data.results[0];
    return place;
  };

  static redemptionDetail(payCode) {
    return RequestHelper.get(`${appConfig.hostUrl}users/reward_list/history/${payCode}/`);
  }

  static savingHistory(params) {
    return RequestHelper.get(`${appConfig.hostUrl}users/saving_list/history/`, params);
  }

  static redemptionHistory(params) {
    return RequestHelper.get(`${appConfig.hostUrl}users/reward_list/history/`, params);
  }

  static seatchListMerchantByAddress = ({ category, type, point, page_size, page }) => {
    return RequestHelper.post(
      `${appConfig.hostUrl}products/search/location/address/${point}/?page=${page}&page_size=${page_size}`,
      {
        category,
        type
      }
    );
  };

  static getListPromoByAddress = ({ category, type, point, page_size, page }) => {
    return RequestHelper.post(
      `${appConfig.hostUrl}products/search/location/address/${point}/products/?page=${page}&page_size=${page_size}`,
      {
        category,
        type
      }
    );
  };

  static checkOtp(code) {
    return RequestHelper.get(`${appConfig.hostUrl}dealset/code/${code}/otp/`);
  }

  static checkIsPassword(phone) {
    return RequestHelper.get(`${appConfig.ssoUrl}check_is_password/${phone}/`);
  }

  static verifyOTP({ phone, otp }) {
    return RequestHelper.post(`${appConfig.ssoUrl}verify_otp/`, { phone, otp });
  }

  static sendOTP(phone) {
    return RequestHelper.post(`${appConfig.ssoUrl}otp-sms/`, { phone });
  }

  static newPassword(phone, password) {
    return RequestHelper.post(`${appConfig.ssoUrl}new_password/`, { phone, password });
  }

  static redeemptEcoupon(code, code_bill, value_bill) {
    return RequestHelper.post(`${appConfig.hostUrl}dealset/redeempt/code/`, {
      code,
      code_bill,
      value_bill
    });
  }
}
