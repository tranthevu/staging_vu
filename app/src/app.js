/* eslint-disable no-undef */
/* eslint-disable no-global-assign */
import * as React from "react";
import { Provider } from "react-redux";
import { ActionSheetProvider } from "@expo/react-native-action-sheet";
import firebase from "react-native-firebase";
import * as Sentry from "@sentry/react-native";
// import Matomo from "react-native-matomo";
import Main from "./main";
import { navReducer, initialState } from "./reducers/nav.reducer";
import configureStore from "./store/configure-store";
import { appConfig } from "./config/app.config";
import { environmentMap } from "./constants/app.constant";

if (appConfig.environment !== environmentMap.development) {
  Sentry.init({
    dsn: "https://8ac5780311ce48f298e0c90c5025a9f6@sentry.io/1552197",
    environment: appConfig.environment
  });
  Sentry.setExtra("environment", appConfig.environment);
}

const Analytics = firebase.analytics();
const noop = () => {};
export default class App extends React.Component {
  constructor(props) {
    super(props);
    Analytics.setAnalyticsCollectionEnabled(true);
    if (!__DEV__) {
      console.disableYellowBox = true;
      console.ignoredYellowBox = ["Warning: Each"];
      console.log = noop;
      console.warn = noop;
      console.error = noop;
    }
  }

  componentDidMount = () => {
    // Temporary disable Matomo due to this bug: YA15-254 [Yolla Network App][1.2 build 60 -140] Crash app on iphone 5
    // Later will update follow: YA15-207 [Yolla Network App] Add tracking view detail
    // Matomo.initTracker("https://matomo.yolla.dev/piwik.php", 1);
  };

  render(): React.Node {
    return (
      <Provider store={storeConfig.store}>
        <ActionSheetProvider>
          <Main />
        </ActionSheetProvider>
      </Provider>
    );
  }
}

export const storeConfig = configureStore(navReducer, { nav: initialState });
