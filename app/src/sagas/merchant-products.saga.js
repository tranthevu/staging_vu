import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillMerchantProducts,
  rejectMerchantProducts,
  appendMerchantProducts,
  reachEndMerchantProducts,
  merchantProductsAction
} from "../actions/merchant-products.action";

function getPage(state: any): any {
  return state.merchantProducts.page;
}

function* requestMerchantProducts(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.searchProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndMerchantProducts());
    }
    yield put(fulfillMerchantProducts(data.results));
  } catch (e) {
    yield put(rejectMerchantProducts());
  }
}

function* refreshMerchantProducts(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.searchProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndMerchantProducts());
    }
    yield put(fulfillMerchantProducts(data.results));
  } catch (e) {
    yield put(rejectMerchantProducts());
  }
}

function* loadNextMerchantProducts(action): void {
  const page = yield select(getPage);
  try {
    const { params } = action.payload;

    const data = yield call(Api.searchProducts, {
      page: page + 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndMerchantProducts());
    }
    yield put(appendMerchantProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectMerchantProducts());
  }
}

export function* watchMerchantProductsSaga(): void {
  yield all([
    takeLatest(
      merchantProductsAction.MERCHANT_PRODUCTS_REQUESTED,
      requestMerchantProducts
    ),
    takeLatest(
      merchantProductsAction.MERCHANT_PRODUCTS_FETCHED_MORE,
      loadNextMerchantProducts
    ),
    takeLatest(
      merchantProductsAction.MERCHANT_PRODUCTS_REFRESHED,
      refreshMerchantProducts
    )
  ]);
}
