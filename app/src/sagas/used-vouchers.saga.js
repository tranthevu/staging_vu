import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillUsedVouchers,
  rejectUsedVouchers,
  appendUsedVouchers,
  reachEndUsedVouchers,
  usedVouchersAction,
  fetchMoreUsedVouchers
} from "../actions/used-vouchers.action";

function getPage(state: any): any {
  return state.usedVouchers.page;
}

function* requestUsedVouchers(): void {
  try {
    const data = yield call(Api.getUsedVouchers, {
      page: 1,
      page_size: pagination.pageSize,
      is_used: 2
    });
    if (!data.next) {
      yield put(reachEndUsedVouchers());
    }
    yield put(fulfillUsedVouchers(data.results));
  } catch (e) {
    yield put(rejectUsedVouchers());
  }
}

function* refreshUsedVouchers(): void {
  try {
    const data = yield call(Api.getUsedVouchers, {
      page: 1,
      page_size: pagination.pageSize,
      is_used: 2
    });
    if (!data.next) {
      yield put(reachEndUsedVouchers());
    }
    yield put(fulfillUsedVouchers(data.results));
  } catch (e) {
    yield put(rejectUsedVouchers());
  }
}

function* loadNextUsedVouchers(): void {
  const page = yield select(getPage);
  try {
    const data = yield call(Api.getUsedVouchers, {
      page: page + 1,
      page_size: pagination.pageSize,
      is_used: 2
    });
    if (!data.next) {
      yield put(reachEndUsedVouchers());
    }
    yield put(appendUsedVouchers(data.results, page + 1));
  } catch (e) {
    yield put(rejectUsedVouchers());
  }
}

export function* watchUsedVouchersSaga(): void {
  yield all([
    takeLatest(usedVouchersAction.USED_VOUCHERS_REQUESTED, requestUsedVouchers),
    takeLatest(
      usedVouchersAction.USED_VOUCHERS_FETCHED_MORE,
      loadNextUsedVouchers
    ),
    takeLatest(usedVouchersAction.USED_VOUCHERS_REFRESHED, refreshUsedVouchers)
  ]);
}
