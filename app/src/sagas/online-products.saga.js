import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillOnlineProducts,
  rejectOnlineProducts,
  appendOnlineProducts,
  reachEndOnlineProducts,
  onlineProductsAction
} from "../actions/online-products.action";

function getPage(state: any): any {
  return state.onlineProducts.page;
}

function* requestOnlineProducts(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.searchProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ordering: "-expiring",
      type: "affiliate-offer",
      field: "running",
      ...params
    });
    if (!data.next) {
      yield put(reachEndOnlineProducts());
    }
    yield put(fulfillOnlineProducts(data.results));
  } catch (e) {
    yield put(rejectOnlineProducts());
  }
}

function* refreshOnlineProducts(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.searchProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ordering: "-expiring",
      type: "affiliate-offer",
      field: "running",
      ...params
    });
    if (!data.next) {
      yield put(reachEndOnlineProducts());
    }
    yield put(fulfillOnlineProducts(data.results));
  } catch (e) {
    yield put(rejectOnlineProducts());
  }
}

function* loadNextOnlineProducts(action): void {
  const page = yield select(getPage);
  try {
    const { params } = action.payload;

    const data = yield call(Api.searchProducts, {
      page: page + 1,
      page_size: pagination.pageSize,
      ordering: "-expiring",
      field: "running",
      type: "affiliate-offer",
      ...params
    });
    if (!data.next) {
      yield put(reachEndOnlineProducts());
    }
    yield put(appendOnlineProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectOnlineProducts());
  }
}

export function* watchOnlineProductsSaga(): void {
  yield all([
    takeLatest(onlineProductsAction.ONLINE_PRODUCTS_REQUESTED, requestOnlineProducts),
    takeLatest(onlineProductsAction.ONLINE_PRODUCTS_FETCHED_MORE, loadNextOnlineProducts),
    takeLatest(onlineProductsAction.ONLINE_PRODUCTS_REFRESHED, refreshOnlineProducts)
  ]);
}
