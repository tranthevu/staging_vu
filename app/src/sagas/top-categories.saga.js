import { put, takeLatest, call, all } from "redux-saga/effects";
import Api from "../api/api";
import {
  loadedTopCategories,
  requestTopCategoriesFailed,
  topCategoriesAction
} from "../actions/top-categories.action";

function* fetchTopCategories() {
  try {
    const response = yield call(Api.getTopCategories);
    yield put(loadedTopCategories(response.results));
  } catch (err) {
    yield put(requestTopCategoriesFailed());
  }
}

export function* watchTopCategoriesSaga() {
  yield all([
    takeLatest(topCategoriesAction.TOP_CATEGORIES_REQUESTED, fetchTopCategories)
  ]);
}
