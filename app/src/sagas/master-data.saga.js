import { put, takeLatest, call, all } from "redux-saga/effects";
import Api from "../api/api";
import {
  loadedMasterData,
  requestMasterDataFailed,
  masterDataAction
} from "../actions/master-data.action";

function* fetchMasterData() {
  try {
    const response = yield call(Api.getMasterData);
    yield put(loadedMasterData(response.data));
  } catch (err) {
    yield put(requestMasterDataFailed());
  }
}

export function* watchMasterDataSaga() {
  yield all([
    takeLatest(masterDataAction.MASTER_DATA_REQUESTED, fetchMasterData)
  ]);
}
