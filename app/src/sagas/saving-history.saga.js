import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillSavingHistory,
  rejectSavingHistory,
  appendSavingHistory,
  reachEndSavingHistory,
  savingHistoryAction
} from "../actions/saving-history.action";

function getPage(state: any): any {
  return state.savingHistory.page;
}

function* requestSavingHistory(): void {
  try {
    const data = yield call(Api.savingHistory, {
      page: 1,
      page_size: pagination.pageSize
    });
    if (!data.next) {
      yield put(reachEndSavingHistory());
    }
    yield put(fulfillSavingHistory(data.results));
  } catch (e) {
    yield put(rejectSavingHistory());
  }
}

function* refreshSavingHistory(): void {
  try {
    const data = yield call(Api.savingHistory, {
      page: 1,
      page_size: pagination.pageSize
    });
    if (!data.next) {
      yield put(reachEndSavingHistory());
    }
    yield put(fulfillSavingHistory(data.results));
  } catch (e) {
    yield put(rejectSavingHistory());
  }
}

function* loadNextSavingHistory(): void {
  const page = yield select(getPage);
  try {
    const data = yield call(Api.savingHistory, {
      page: page + 1,
      page_size: pagination.pageSize
    });
    if (!data.next) {
      yield put(reachEndSavingHistory());
    }
    yield put(appendSavingHistory(data.results, page + 1));
  } catch (e) {
    yield put(rejectSavingHistory());
  }
}

export function* watchSavingHistorySaga(): void {
  yield all([
    takeLatest(savingHistoryAction.SAVING_HISTORY_REQUESTED, requestSavingHistory),
    takeLatest(savingHistoryAction.SAVING_HISTORY_FETCHED_MORE, loadNextSavingHistory),
    takeLatest(savingHistoryAction.SAVING_HISTORY_REFRESHED, refreshSavingHistory)
  ]);
}
