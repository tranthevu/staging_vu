import { put, takeLatest, call, all } from "redux-saga/effects";
import Api from "../api/api";
import {
  loadedHomeAreas,
  requestHomeAreasFailed,
  homeAreasAction
} from "../actions/home-areas.action";

function* fetchHomeAreas() {
  try {
    const response = yield call(Api.getHomeAreas);
    yield put(loadedHomeAreas(response.results, response.count));
  } catch (err) {
    yield put(requestHomeAreasFailed());
  }
}

export function* watchHomeAreasSaga() {
  yield all([takeLatest(homeAreasAction.HOME_AREAS_REQUESTED, fetchHomeAreas)]);
}
