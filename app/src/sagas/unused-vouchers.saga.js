import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillUnusedVouchers,
  rejectUnusedVouchers,
  appendUnusedVouchers,
  reachEndUnusedVouchers,
  unusedVouchersAction,
  fetchMoreUnusedVouchers
} from "../actions/unused-vouchers.action";

function getPage(state: any): any {
  return state.unusedVouchers.page;
}

function* requestUnusedVouchers(): void {
  try {
    const data = yield call(Api.getUnusedVouchers, {
      page: 1,
      page_size: pagination.pageSize,
      is_used: 1
    });
    if (!data.next) {
      yield put(reachEndUnusedVouchers());
    }
    yield put(fulfillUnusedVouchers(data.results));
  } catch (e) {
    yield put(rejectUnusedVouchers());
  }
}

function* refreshUnusedVouchers(): void {
  try {
    const data = yield call(Api.getUnusedVouchers, {
      page: 1,
      page_size: pagination.pageSize,
      is_used: 1
    });
    if (!data.next) {
      yield put(reachEndUnusedVouchers());
    }
    yield put(fulfillUnusedVouchers(data.results));
  } catch (e) {
    yield put(rejectUnusedVouchers());
  }
}

function* loadNextUnusedVouchers(): void {
  const page = yield select(getPage);
  try {
    const data = yield call(Api.getUnusedVouchers, {
      page: page + 1,
      page_size: pagination.pageSize,
      is_used: 1
    });
    if (!data.next) {
      yield put(reachEndUnusedVouchers());
    }
    yield put(appendUnusedVouchers(data.results, page + 1));
  } catch (e) {
    yield put(rejectUnusedVouchers());
  }
}

export function* watchUnusedVouchersSaga(): void {
  yield all([
    takeLatest(
      unusedVouchersAction.UNUSED_VOUCHERS_REQUESTED,
      requestUnusedVouchers
    ),
    takeLatest(
      unusedVouchersAction.UNUSED_VOUCHERS_FETCHED_MORE,
      loadNextUnusedVouchers
    ),
    takeLatest(
      unusedVouchersAction.UNUSED_VOUCHERS_REFRESHED,
      refreshUnusedVouchers
    )
  ]);
}
