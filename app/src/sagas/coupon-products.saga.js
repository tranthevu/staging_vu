import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillCouponProducts,
  rejectCouponProducts,
  appendCouponProducts,
  reachEndCouponProducts,
  couponProductsAction
} from "../actions/coupon-products.action";

function getPage(state: any): any {
  return state.couponProducts.page;
}

function* requestCouponProducts(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.searchProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ordering: "-expiring",
      type: "voucher, voucher_prepaid",
      field: "running",
      ...params
    });
    if (!data.next) {
      yield put(reachEndCouponProducts());
    }
    yield put(fulfillCouponProducts(data.results));
  } catch (e) {
    yield put(rejectCouponProducts());
  }
}

function* refreshCouponProducts(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.searchProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ordering: "-expiring",
      type: "voucher, voucher_prepaid",
      field: "running",
      ...params
    });
    if (!data.next) {
      yield put(reachEndCouponProducts());
    }
    yield put(fulfillCouponProducts(data.results));
  } catch (e) {
    yield put(rejectCouponProducts());
  }
}

function* loadNextCouponProducts(action): void {
  const page = yield select(getPage);
  try {
    const { params } = action.payload;

    const data = yield call(Api.searchProducts, {
      page: page + 1,
      page_size: pagination.pageSize,
      ordering: "-expiring",
      field: "running",
      type: "voucher, voucher_prepaid",
      ...params
    });
    if (!data.next) {
      yield put(reachEndCouponProducts());
    }
    yield put(appendCouponProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectCouponProducts());
  }
}

export function* watchCouponProductsSaga(): void {
  yield all([
    takeLatest(couponProductsAction.COUPON_PRODUCTS_REQUESTED, requestCouponProducts),
    takeLatest(couponProductsAction.COUPON_PRODUCTS_FETCHED_MORE, loadNextCouponProducts),
    takeLatest(couponProductsAction.COUPON_PRODUCTS_REFRESHED, refreshCouponProducts)
  ]);
}
