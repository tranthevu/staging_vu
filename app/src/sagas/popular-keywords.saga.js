import { put, takeLatest, call, all } from "redux-saga/effects";
import Api from "../api/api";
import {
  loadedPopularKeywords,
  requestPopularKeywordsFailed,
  popularKeywordsAction
} from "../actions/popular-keywords.action";

function* fetchPopularKeywords() {
  try {
    const response = yield call(Api.getPopularKeywords);
    yield put(loadedPopularKeywords(response.data));
  } catch (err) {
    yield put(requestPopularKeywordsFailed());
  }
}

export function* watchPopularKeywordsSaga() {
  yield all([
    takeLatest(
      popularKeywordsAction.POPULAR_KEYWORDS_REQUESTED,
      fetchPopularKeywords
    )
  ]);
}
