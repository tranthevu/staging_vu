import { put, call, takeLatest, select, all } from "redux-saga/effects";
import lodash from "lodash";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillSavedGifts,
  rejectSavedGifts,
  appendSavedGifts,
  reachEndSavedGifts,
  savedGiftsAction
} from "../actions/saved-gifts.action";

function getPage(state: any): any {
  return state.savedGifts.page;
}

function* requestSavedGifts(action): void {
  try {
    const status = lodash.get(action, "payload.status");
    const data = yield call(Api.getSavedGifts, {
      page: 1,
      page_size: pagination.pageSize,
      status
    });
    if (!data.next) {
      yield put(reachEndSavedGifts());
    }
    yield put(fulfillSavedGifts(data.results));
  } catch (e) {
    yield put(rejectSavedGifts());
  }
}

function* refreshSavedGifts(action): void {
  try {
    const status = lodash.get(action, "payload.status");
    const data = yield call(Api.getSavedGifts, {
      page: 1,
      page_size: pagination.pageSize,
      status
    });
    if (!data.next) {
      yield put(reachEndSavedGifts());
    }
    yield put(fulfillSavedGifts(data.results));
  } catch (e) {
    yield put(rejectSavedGifts());
  }
}

function* loadNextSavedGifts(action): void {
  const page = yield select(getPage);
  try {
    const status = lodash.get(action, "payload.status");
    const data = yield call(Api.getSavedGifts, {
      page: page + 1,
      page_size: pagination.pageSize,
      status
    });
    if (!data.next) {
      yield put(reachEndSavedGifts());
    }
    yield put(appendSavedGifts(data.results, page + 1));
  } catch (e) {
    yield put(rejectSavedGifts());
  }
}

export function* watchSavedGiftsSaga(): void {
  yield all([
    takeLatest(savedGiftsAction.SAVED_GIFTS_REQUESTED, requestSavedGifts),
    takeLatest(savedGiftsAction.SAVED_GIFTS_FETCHED_MORE, loadNextSavedGifts),
    takeLatest(savedGiftsAction.SAVED_GIFTS_REFRESHED, refreshSavedGifts)
  ]);
}
