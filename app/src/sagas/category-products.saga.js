import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillCategoryProducts,
  rejectCategoryProducts,
  appendCategoryProducts,
  reachEndCategoryProducts,
  categoryProductsAction,
  fetchMoreCategoryProducts
} from "../actions/category-products.action";

function getPage(state: any): any {
  return state.categoryProducts.page;
}

function* requestCategoryProducts(action): void {
  try {
    const { category, merchant, ordering, type, field } = action.payload;
    const data = yield call(Api.getOffersByCategory, category, {
      page: 1,
      page_size: pagination.pageSize,
      merchant,
      ordering,
      type,
      field
    });
    if (!data.next) {
      yield put(reachEndCategoryProducts());
    }
    yield put(fulfillCategoryProducts(data.results));
  } catch (e) {
    yield put(rejectCategoryProducts());
  }
}

function* refreshCategoryProducts(action): void {
  try {
    const { category, merchant, ordering, type, field } = action.payload;
    const data = yield call(Api.getOffersByCategory, category, {
      page: 1,
      page_size: pagination.pageSize,
      merchant,
      ordering,
      type,
      field
    });
    if (!data.next) {
      yield put(reachEndCategoryProducts());
    }
    yield put(fulfillCategoryProducts(data.results));
  } catch (e) {
    yield put(rejectCategoryProducts());
  }
}

function* loadNextCategoryProducts(action): void {
  const page = yield select(getPage);
  try {
    const { category, merchant, ordering, type, field } = action.payload;
    const data = yield call(Api.getOffersByCategory, category, {
      page: page + 1,
      page_size: pagination.pageSize,
      merchant,
      ordering,
      type,
      field
    });
    if (!data.next) {
      yield put(reachEndCategoryProducts());
    }
    yield put(appendCategoryProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectCategoryProducts());
  }
}

export function* watchCategoryProductsSaga(): void {
  yield all([
    takeLatest(
      categoryProductsAction.CATEGORY_PRODUCTS_REQUESTED,
      requestCategoryProducts
    ),
    takeLatest(
      categoryProductsAction.CATEGORY_PRODUCTS_FETCHED_MORE,
      loadNextCategoryProducts
    ),
    takeLatest(
      categoryProductsAction.CATEGORY_PRODUCTS_REFRESHED,
      refreshCategoryProducts
    )
  ]);
}
