import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillLocationProducts,
  rejectLocationProducts,
  appendLocationProducts,
  reachEndLocationProducts,
  locationProductsAction,
  fetchMoreLocationProducts
} from "../actions/location-products.action";

function getPage(state: any): any {
  return state.locationProducts.page;
}

function* requestLocationProducts(action): void {
  try {
    const { location, category, merchant, ordering } = action.payload;
    const data = yield call(Api.getOffersByLocation, location, {
      page: 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndLocationProducts());
    }
    yield put(fulfillLocationProducts(data.results));
  } catch (e) {
    yield put(rejectLocationProducts());
  }
}

function* refreshLocationProducts(action): void {
  try {
    const { location, category, merchant, ordering } = action.payload;
    const data = yield call(Api.getOffersByLocation, location, {
      page: 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndLocationProducts());
    }
    yield put(fulfillLocationProducts(data.results));
  } catch (e) {
    yield put(rejectLocationProducts());
  }
}

function* loadNextLocationProducts(action): void {
  const page = yield select(getPage);
  try {
    const { location, category, merchant, ordering } = action.payload;
    const data = yield call(Api.getOffersByLocation, location, {
      page: page + 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndLocationProducts());
    }
    yield put(appendLocationProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectLocationProducts());
  }
}

export function* watchLocationProductsSaga(): void {
  yield all([
    takeLatest(
      locationProductsAction.LOCATION_PRODUCTS_REQUESTED,
      requestLocationProducts
    ),
    takeLatest(
      locationProductsAction.LOCATION_PRODUCTS_FETCHED_MORE,
      loadNextLocationProducts
    ),
    takeLatest(
      locationProductsAction.LOCATION_PRODUCTS_REFRESHED,
      refreshLocationProducts
    )
  ]);
}
