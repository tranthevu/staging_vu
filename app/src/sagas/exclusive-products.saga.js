import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillExclusiveProducts,
  rejectExclusiveProducts,
  appendExclusiveProducts,
  reachEndExclusiveProducts,
  exclusiveProductsAction,
  fetchMoreExclusiveProducts
} from "../actions/exclusive-products.action";

function getPage(state: any): any {
  return state.exclusiveProducts.page;
}

function* requestExclusiveProducts(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.searchProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ordering: "recent",
      field: "exclusive",
      ...params
    });
    if (!data.next) {
      yield put(reachEndExclusiveProducts());
    }
    yield put(fulfillExclusiveProducts(data.results));
  } catch (e) {
    yield put(rejectExclusiveProducts());
  }
}

function* refreshExclusiveProducts(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.searchProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ordering: "recent",
      field: "exclusive",
      ...params
    });
    if (!data.next) {
      yield put(reachEndExclusiveProducts());
    }
    yield put(fulfillExclusiveProducts(data.results));
  } catch (e) {
    yield put(rejectExclusiveProducts());
  }
}

function* loadNextExclusiveProducts(action): void {
  const page = yield select(getPage);
  try {
    const { params } = action.payload;

    const data = yield call(Api.searchProducts, {
      page: page + 1,
      page_size: pagination.pageSize,
      ordering: "recent",
      field: "exclusive",
      ...params
    });
    if (!data.next) {
      yield put(reachEndExclusiveProducts());
    }
    yield put(appendExclusiveProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectExclusiveProducts());
  }
}

export function* watchExclusiveProductsSaga(): void {
  yield all([
    takeLatest(
      exclusiveProductsAction.EXCLUSIVE_PRODUCTS_REQUESTED,
      requestExclusiveProducts
    ),
    takeLatest(
      exclusiveProductsAction.EXCLUSIVE_PRODUCTS_FETCHED_MORE,
      loadNextExclusiveProducts
    ),
    takeLatest(
      exclusiveProductsAction.EXCLUSIVE_PRODUCTS_REFRESHED,
      refreshExclusiveProducts
    )
  ]);
}
