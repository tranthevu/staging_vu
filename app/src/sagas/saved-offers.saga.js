import { put, call, takeLatest, select, all } from "redux-saga/effects";
import lodash from "lodash";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillSavedOffers,
  rejectSavedOffers,
  appendSavedOffers,
  reachEndSavedOffers,
  savedOffersAction
} from "../actions/saved-offers.action";

function getPage(state: any): any {
  return state.savedOffers.page;
}

function* requestSavedOffers(action): void {
  try {
    const status = lodash.get(action, "payload.status");
    const productType = lodash.get(action, "payload.productType");
    const data = yield call(Api.getSavedOffers, {
      page: 1,
      page_size: pagination.pageSize,
      time_out: status,
      product_type: productType !== "all" ? productType : "",
      status_code: !Number.isInteger(status) && productType !== "all" && "avaiable"
    });
    if (!data.next) {
      yield put(reachEndSavedOffers());
    }
    yield put(fulfillSavedOffers(data.results));
  } catch (e) {
    yield put(rejectSavedOffers());
  }
}

function* refreshSavedOffers(action): void {
  try {
    const status = lodash.get(action, "payload.status");
    const productType = lodash.get(action, "payload.productType");
    const data = yield call(Api.getSavedOffers, {
      page: 1,
      page_size: pagination.pageSize,
      time_out: status,
      product_type: productType !== "all" ? productType : "",
      status_code: !Number.isInteger(status) && productType !== "all" && "avaiable"
    });
    if (!data.next) {
      yield put(reachEndSavedOffers());
    }
    yield put(fulfillSavedOffers(data.results));
  } catch (e) {
    yield put(rejectSavedOffers());
  }
}

function* loadNextSavedOffers(action): void {
  const page = yield select(getPage);
  try {
    const status = lodash.get(action, "payload.status");
    const productType = lodash.get(action, "payload.productType");
    const data = yield call(Api.getSavedOffers, {
      page: page + 1,
      page_size: pagination.pageSize,
      time_out: status,
      product_type: productType !== "all" ? productType : "",
      status_code: !Number.isInteger(status) && productType !== "all" && "avaiable"
    });
    if (!data.next) {
      yield put(reachEndSavedOffers());
    }
    yield put(appendSavedOffers(data.results, page + 1));
  } catch (e) {
    yield put(rejectSavedOffers());
  }
}

export function* watchSavedOffersSaga(): void {
  yield all([
    takeLatest(savedOffersAction.SAVED_OFFERS_REQUESTED, requestSavedOffers),
    takeLatest(savedOffersAction.SAVED_OFFERS_FETCHED_MORE, loadNextSavedOffers),
    takeLatest(savedOffersAction.SAVED_OFFERS_REFRESHED, refreshSavedOffers)
  ]);
}
