import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillRecentProducts,
  rejectRecentProducts,
  appendRecentProducts,
  reachEndRecentProducts,
  recentProductsAction,
  fetchMoreRecentProducts
} from "../actions/recent-products.action";

function getPage(state: any): any {
  return state.recentProducts.page;
}

function* requestRecentProducts(action): void {
  const { category, merchant, ordering } = action.payload;
  try {
    const data = yield call(Api.getRecentProducts, {
      page: 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndRecentProducts());
    }
    yield put(fulfillRecentProducts(data.results));
  } catch (e) {
    yield put(rejectRecentProducts());
  }
}

function* refreshRecentProducts(action): void {
  const { category, merchant, ordering } = action.payload;
  try {
    const data = yield call(Api.getRecentProducts, {
      page: 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndRecentProducts());
    }
    yield put(fulfillRecentProducts(data.results));
  } catch (e) {
    yield put(rejectRecentProducts());
  }
}

function* loadNextRecentProducts(action): void {
  const { category, merchant, ordering } = action.payload;
  const page = yield select(getPage);
  try {
    const data = yield call(Api.getRecentProducts, {
      page: page + 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndRecentProducts());
    }
    yield put(appendRecentProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectRecentProducts());
  }
}

export function* watchRecentProductsSaga(): void {
  yield all([
    takeLatest(
      recentProductsAction.RECENT_PRODUCTS_REQUESTED,
      requestRecentProducts
    ),
    takeLatest(
      recentProductsAction.RECENT_PRODUCTS_FETCHED_MORE,
      loadNextRecentProducts
    ),
    takeLatest(
      recentProductsAction.RECENT_PRODUCTS_REFRESHED,
      refreshRecentProducts
    )
  ]);
}
