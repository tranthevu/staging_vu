import { fork, all } from "redux-saga/effects";
import { watchBannersSaga } from "./banners.saga";
import { watchTopCategoriesSaga } from "./top-categories.saga";
import { watchPopularKeywordsSaga } from "./popular-keywords.saga";
import { watchAutocompleteSaga } from "./autocomplete.saga";
import { watchNotificationsSaga } from "./notifications.saga";
import { watchAreasSaga } from "./areas.saga";
import { watchHotProductsSaga } from "./hot-products.saga";
import { watchExpiringProductsSaga } from "./expiring-products.saga";
import { watchRecentProductsSaga } from "./recent-products.saga";
import { watchUnusedVouchersSaga } from "./unused-vouchers.saga";
import { watchUsedVouchersSaga } from "./used-vouchers.saga";
import { watchUnreadNotificationsSaga } from "./unread-notifications.saga";
import { watchProfileSaga } from "./profile.saga";
import { watchTopMerchantsSaga } from "./top-merchants.saga";
import { watchFiltersSaga } from "./filters.saga";
import { watchCategoryProductsSaga } from "./category-products.saga";
import { watchSearchProductsSaga } from "./search-products.saga";
import { watchMasterDataSaga } from "./master-data.saga";
import { watchHomeAreasSaga } from "./home-areas.saga";
import { watchLocationProductsSaga } from "./location-products.saga";
import { watchSavedOffersSaga } from "./saved-offers.saga";
import { watchAllProductsSaga } from "./all-products.saga";
import { watchMerchantProductsSaga } from "./merchant-products.saga";
import { watchOnlineProductsSaga } from "./online-products.saga";
import { watchSavedGiftsSaga } from "./saved-gifts.saga";
import { watchExclusiveProductsSaga } from "./exclusive-products.saga";
import { watchPromoProductsSaga } from "./promo-products.saga";
import { watchCouponProductsSaga } from "./coupon-products.saga";
import { watchShare2EarnProductsSaga } from "./share2earn-products.saga";
import { watchJoinedCampaignsSaga } from "./joined-campaigns.saga";
import { watchFollowMerchantsSaga } from "./follow-merchants.saga";
import { watchPartnerProductsSaga } from "./partner-products.saga";
import { watchShare2EarnPromoSaga } from "./share2earn-promo.saga";
import { watchSavingHistorySaga } from "./saving-history.saga";
import { watchRedemptionHistorySaga } from "./redemption-history.saga";
import { merChantSaga } from "./merchant.saga";

export default function* rootSaga() {
  yield all([
    fork(watchBannersSaga),
    fork(watchTopCategoriesSaga),
    fork(watchPopularKeywordsSaga),
    fork(watchAutocompleteSaga),
    fork(watchNotificationsSaga),
    fork(watchAreasSaga),
    fork(watchHotProductsSaga),
    fork(watchExpiringProductsSaga),
    fork(watchRecentProductsSaga),
    fork(watchUnusedVouchersSaga),
    fork(watchUsedVouchersSaga),
    fork(watchUnreadNotificationsSaga),
    fork(watchProfileSaga),
    fork(watchTopMerchantsSaga),
    fork(watchFiltersSaga),
    fork(watchCategoryProductsSaga),
    fork(watchSearchProductsSaga),
    fork(watchMasterDataSaga),
    fork(watchHomeAreasSaga),
    fork(watchLocationProductsSaga),
    fork(watchSavedOffersSaga),
    fork(watchAllProductsSaga),
    fork(watchMerchantProductsSaga),
    fork(watchOnlineProductsSaga),
    fork(watchSavedGiftsSaga),
    fork(watchExclusiveProductsSaga),
    fork(watchPromoProductsSaga),
    fork(watchCouponProductsSaga),
    fork(watchShare2EarnProductsSaga),
    fork(watchJoinedCampaignsSaga),
    fork(watchFollowMerchantsSaga),
    fork(watchPartnerProductsSaga),
    fork(watchShare2EarnPromoSaga),
    fork(watchSavingHistorySaga),
    fork(watchRedemptionHistorySaga),
    fork(merChantSaga)
  ]);
}
