import { put, takeLatest, call, all } from "redux-saga/effects";
import Api from "../api/api";
import {
  loadedTopMerchants,
  requestTopMerchantsFailed,
  topMerchantsAction
} from "../actions/top-merchants.action";

function* fetchTopMerchants() {
  try {
    const response = yield call(Api.getTopMerchants);
    yield put(loadedTopMerchants(response.results));
  } catch (err) {
    yield put(requestTopMerchantsFailed());
  }
}

export function* watchTopMerchantsSaga() {
  yield all([
    takeLatest(topMerchantsAction.TOP_MERCHANTS_REQUESTED, fetchTopMerchants)
  ]);
}
