import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillPartnerProducts,
  rejectPartnerProducts,
  appendPartnerProducts,
  reachEndPartnerProducts,
  partnerProductsAction
} from "../actions/partner-products.action";

function getPage(state: any): any {
  return state.partnerProducts.page;
}

function* requestPartnerProducts(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.partnerProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndPartnerProducts());
    }
    yield put(fulfillPartnerProducts(data.results));
  } catch (e) {
    yield put(rejectPartnerProducts());
  }
}

function* refreshPartnerProducts(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.partnerProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndPartnerProducts());
    }
    yield put(fulfillPartnerProducts(data.results));
  } catch (e) {
    yield put(rejectPartnerProducts());
  }
}

function* loadNextPartnerProducts(action): void {
  const page = yield select(getPage);
  try {
    const { params } = action.payload;

    const data = yield call(Api.partnerProducts, {
      page: page + 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndPartnerProducts());
    }
    yield put(appendPartnerProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectPartnerProducts());
  }
}

export function* watchPartnerProductsSaga(): void {
  yield all([
    takeLatest(partnerProductsAction.PARTNER_PRODUCTS_REQUESTED, requestPartnerProducts),
    takeLatest(
      partnerProductsAction.PARTNER_PRODUCTS_FETCHED_MORE,
      loadNextPartnerProducts
    ),
    takeLatest(partnerProductsAction.PARTNER_PRODUCTS_REFRESHED, refreshPartnerProducts)
  ]);
}
