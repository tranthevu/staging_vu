import { put, takeLatest, call, all } from "redux-saga/effects";
import Api from "../api/api";
import {
  loadedBanners,
  requestBannersFailed,
  bannersAction,
  loadedPromoBanners,
  loadedEcouponBanners,
  getShareToEarnBannersSuccess
} from "../actions/banners.action";

function* fetchBanners() {
  try {
    const response = yield call(Api.getSlideBanner);
    yield put(loadedBanners(response.data.slides));
  } catch (err) {
    yield put(requestBannersFailed());
  }
}

function* fetchBannerPromo() {
  try {
    const response = yield call(Api.getSlideBannerPromo);
    yield put(loadedPromoBanners(response.data.slides));
  } catch (err) {
    yield put(requestBannersFailed());
  }
}
function* fetchBannerEcoupon() {
  try {
    const response = yield call(Api.getSlideBannerEcoupon);
    yield put(loadedEcouponBanners(response.data.slides));
  } catch (err) {
    yield put(requestBannersFailed());
  }
}

function* fetchBannerShareToEarnPromo() {
  try {
    const response = yield call(Api.getSlideBannerShareToEarnPromo);
    yield put(getShareToEarnBannersSuccess(response.data.slides));
  } catch (err) {
    yield put(requestBannersFailed());
  }
}

export function* watchBannersSaga() {
  yield all([
    takeLatest(bannersAction.BANNERS_REQUESTED, fetchBanners),
    takeLatest(
      bannersAction.BANNERS_SHARE_TO_EARN_PROMO.REQUEST,
      fetchBannerShareToEarnPromo
    )
  ]);
  yield all([takeLatest(bannersAction.BANNERS_REQUESTED, fetchBannerPromo)]);
  yield all([takeLatest(bannersAction.BANNERS_REQUESTED, fetchBannerEcoupon)]);
}
