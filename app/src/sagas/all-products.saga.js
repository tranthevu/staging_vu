import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillAllProducts,
  rejectAllProducts,
  appendAllProducts,
  reachEndAllProducts,
  allProductsAction,
  fetchMoreAllProducts
} from "../actions/all-products.action";

function getPage(state: any): any {
  return state.allProducts.page;
}

function* requestAllProducts(action): void {
  const { category, merchant, ordering } = action.payload;
  try {
    const data = yield call(Api.getAllProducts, {
      page: 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndAllProducts());
    }
    yield put(fulfillAllProducts(data.results));
  } catch (e) {
    yield put(rejectAllProducts());
  }
}

function* refreshAllProducts(action): void {
  const { category, merchant, ordering } = action.payload;
  try {
    const data = yield call(Api.getAllProducts, {
      page: 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndAllProducts());
    }
    yield put(fulfillAllProducts(data.results));
  } catch (e) {
    yield put(rejectAllProducts());
  }
}

function* loadNextAllProducts(action): void {
  const { category, merchant, ordering } = action.payload;
  const page = yield select(getPage);
  try {
    const data = yield call(Api.getAllProducts, {
      page: page + 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndAllProducts());
    }
    yield put(appendAllProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectAllProducts());
  }
}

export function* watchAllProductsSaga(): void {
  yield all([
    takeLatest(allProductsAction.ALL_PRODUCTS_REQUESTED, requestAllProducts),
    takeLatest(
      allProductsAction.ALL_PRODUCTS_FETCHED_MORE,
      loadNextAllProducts
    ),
    takeLatest(allProductsAction.ALL_PRODUCTS_REFRESHED, refreshAllProducts)
  ]);
}
