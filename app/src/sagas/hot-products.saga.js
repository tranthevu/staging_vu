import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillHotProducts,
  rejectHotProducts,
  appendHotProducts,
  reachEndHotProducts,
  hotProductsAction,
  fetchMoreHotProducts
} from "../actions/hot-products.action";

function getPage(state: any): any {
  return state.hotProducts.page;
}

function* requestHotProducts(action): void {
  const { category, merchant, ordering } = action.payload;
  try {
    const data = yield call(Api.getHotProducts, {
      page: 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndHotProducts());
    }
    yield put(fulfillHotProducts(data.results));
  } catch (e) {
    yield put(rejectHotProducts());
  }
}

function* refreshHotProducts(action): void {
  const { category, merchant, ordering } = action.payload;
  try {
    const data = yield call(Api.getHotProducts, {
      page: 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndHotProducts());
    }
    yield put(fulfillHotProducts(data.results));
  } catch (e) {
    yield put(rejectHotProducts());
  }
}

function* loadNextHotProducts(action): void {
  const { category, merchant, ordering } = action.payload;
  const page = yield select(getPage);
  try {
    const data = yield call(Api.getHotProducts, {
      page: page + 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndHotProducts());
    }
    yield put(appendHotProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectHotProducts());
  }
}

export function* watchHotProductsSaga(): void {
  yield all([
    takeLatest(hotProductsAction.HOT_PRODUCTS_REQUESTED, requestHotProducts),
    takeLatest(
      hotProductsAction.HOT_PRODUCTS_FETCHED_MORE,
      loadNextHotProducts
    ),
    takeLatest(hotProductsAction.HOT_PRODUCTS_REFRESHED, refreshHotProducts)
  ]);
}
