import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillFollowMerchants,
  rejectFollowMerchants,
  appendFollowMerchants,
  reachEndFollowMerchants,
  followMerchantsAction
} from "../actions/follow-merchants.action";

function getPage(state: any): any {
  return state.followMerchants.page;
}

function* requestFollowMerchants(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.getFollowMerchants, {
      page: 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndFollowMerchants());
    }
    yield put(fulfillFollowMerchants(data.results));
  } catch (e) {
    yield put(rejectFollowMerchants());
  }
}

function* refreshFollowMerchants(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.getFollowMerchants, {
      page: 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndFollowMerchants());
    }
    yield put(fulfillFollowMerchants(data.results));
  } catch (e) {
    yield put(rejectFollowMerchants());
  }
}

function* loadNextFollowMerchants(action): void {
  const page = yield select(getPage);
  try {
    const { params } = action.payload;

    const data = yield call(Api.getFollowMerchants, {
      page: page + 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndFollowMerchants());
    }
    yield put(appendFollowMerchants(data.results, page + 1));
  } catch (e) {
    yield put(rejectFollowMerchants());
  }
}

export function* watchFollowMerchantsSaga(): void {
  yield all([
    takeLatest(followMerchantsAction.FOLLOW_MERCHANTS_REQUESTED, requestFollowMerchants),
    takeLatest(
      followMerchantsAction.FOLLOW_MERCHANTS_FETCHED_MORE,
      loadNextFollowMerchants
    ),
    takeLatest(followMerchantsAction.FOLLOW_MERCHANTS_REFRESHED, refreshFollowMerchants)
  ]);
}
