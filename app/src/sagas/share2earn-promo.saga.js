import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillShare2EarnPromo,
  rejectShare2EarnPromo,
  appendShare2EarnPromo,
  reachEndShare2EarnPromo,
  share2EarnPromoAction
} from "../actions/share2earn-promo.action";

function getPage(state: any): any {
  return state.share2EarnPromo.page;
}

function* requestShare2EarnPromo(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.share2EarnPromo, {
      page: 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndShare2EarnPromo());
    }
    yield put(fulfillShare2EarnPromo(data.results));
  } catch (e) {
    yield put(rejectShare2EarnPromo());
  }
}

function* refreshShare2EarnPromo(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.share2EarnPromo, {
      page: 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndShare2EarnPromo());
    }
    yield put(fulfillShare2EarnPromo(data.results));
  } catch (e) {
    yield put(rejectShare2EarnPromo());
  }
}

function* loadNextShare2EarnPromo(action): void {
  const page = yield select(getPage);
  try {
    const { params } = action.payload;

    const data = yield call(Api.share2EarnPromo, {
      page: page + 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndShare2EarnPromo());
    }
    yield put(appendShare2EarnPromo(data.results, page + 1));
  } catch (e) {
    yield put(rejectShare2EarnPromo());
  }
}

export function* watchShare2EarnPromoSaga(): void {
  yield all([
    takeLatest(share2EarnPromoAction.SHARE2EARN_PROMO_REQUESTED, requestShare2EarnPromo),
    takeLatest(
      share2EarnPromoAction.SHARE2EARN_PROMO_FETCHED_MORE,
      loadNextShare2EarnPromo
    ),
    takeLatest(share2EarnPromoAction.SHARE2EARN_PROMO_REFRESHED, refreshShare2EarnPromo)
  ]);
}
