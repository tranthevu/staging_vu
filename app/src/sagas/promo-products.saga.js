import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination, PAGE_LIMIT } from "../constants/app.constant";
import {
  fulfillPromoProducts,
  rejectPromoProducts,
  appendPromoProducts,
  reachEndPromoProducts,
  promoProductsAction
} from "../actions/promo-products.action";
import { GET_LIST_PROMO_BY_ADDRESS_ASYNC } from "../actions/actionTypes";
import { invoke } from "../helpers/sagas.helper";
import { callbackSuccess } from "../helpers/callback";

function getPage(state: any): any {
  return state.promoProducts.page;
}

function* requestPromoProducts(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.searchProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ordering: "-expiring",
      type: "event",
      field: "running",
      ...params
    });
    if (!data.next) {
      yield put(reachEndPromoProducts());
    }
    yield put(fulfillPromoProducts(data.results));
  } catch (e) {
    yield put(rejectPromoProducts());
  }
}

function* refreshPromoProducts(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.searchProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ordering: "-expiring",
      type: "event",
      field: "running",
      ...params
    });
    if (!data.next) {
      yield put(reachEndPromoProducts());
    }
    yield put(fulfillPromoProducts(data.results));
  } catch (e) {
    yield put(rejectPromoProducts());
  }
}

function* loadNextPromoProducts(action): void {
  const page = yield select(getPage);
  try {
    const { params } = action.payload;

    const data = yield call(Api.searchProducts, {
      page: page + 1,
      page_size: pagination.pageSize,
      ordering: "-expiring",
      field: "running",
      type: "event",
      ...params
    });
    if (!data.next) {
      yield put(reachEndPromoProducts());
    }
    yield put(appendPromoProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectPromoProducts());
  }
}

function* getListPromoByAddress({ payload }) {
  const { showLoading = false, category, type, page, point } = payload || {};
  yield invoke(
    function* execution() {
      const response = yield call(Api.getListPromoByAddress, {
        category,
        type,
        point,
        page_size: PAGE_LIMIT,
        page
      });
      const { results } = response;
      yield callbackSuccess(payload, results);
    },
    function* handleError() {
      yield callbackSuccess(null, []);
    },
    showLoading
  );
}

export function* watchPromoProductsSaga() {
  yield all([
    takeLatest(promoProductsAction.PROMO_PRODUCTS_REQUESTED, requestPromoProducts),
    takeLatest(promoProductsAction.PROMO_PRODUCTS_FETCHED_MORE, loadNextPromoProducts),
    takeLatest(promoProductsAction.PROMO_PRODUCTS_REFRESHED, refreshPromoProducts),
    takeLatest(GET_LIST_PROMO_BY_ADDRESS_ASYNC.REQUEST, getListPromoByAddress)
  ]);
}
