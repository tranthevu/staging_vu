import { put, call, takeLatest, select, all } from "redux-saga/effects";
import lodash from "lodash";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillShare2EarnProducts,
  rejectShare2EarnProducts,
  appendShare2EarnProducts,
  reachEndShare2EarnProducts,
  share2EarnProductsAction
} from "../actions/share2earn-products.action";

function getPage(state: any): any {
  return state.share2EarnProducts.page;
}

function transformToEarnProduct(item) {
  return {
    promotion_voucher_code:
      lodash.get(item, "dealset.deal_set_type") === "promotion_voucher_code",
    product: {
      ...item,
      campaign_has_product: false
    },
    info: {
      url: lodash.get(item, "attributes.campaign_url") || "",
      image: lodash.get(item, "images[0].original") || {}
    }
  };
}

function* requestShare2EarnProducts(action): void {
  try {
    const { params, isYollaBiz } = action.payload;
    let data = {};
    const requestParams = {
      page: 1,
      page_size: pagination.pageSize,
      ...params
    };
    if (!isYollaBiz) {
      data = yield call(Api.share2EarnProducts, requestParams);
    } else {
      data = yield call(Api.share2EarnProductsYollaBiz, requestParams);
      if (data.results) {
        data.results = data.results.map(item => transformToEarnProduct(item));
      }
    }
    if (!data.next) {
      yield put(reachEndShare2EarnProducts());
    }
    yield put(fulfillShare2EarnProducts(data.results));
  } catch (e) {
    yield put(rejectShare2EarnProducts());
  }
}

function* refreshShare2EarnProducts(action): void {
  try {
    const { params, isYollaBiz } = action.payload;
    let data = {};
    const requestParams = {
      page: 1,
      page_size: pagination.pageSize,
      ...params
    };
    if (!isYollaBiz) {
      data = yield call(Api.share2EarnProducts, requestParams);
    } else {
      data = yield call(Api.share2EarnProductsYollaBiz, requestParams);
      if (data.results) {
        data.results = data.results.map(item => transformToEarnProduct(item));
      }
    }
    if (!data.next) {
      yield put(reachEndShare2EarnProducts());
    }
    yield put(fulfillShare2EarnProducts(data.results));
  } catch (e) {
    yield put(rejectShare2EarnProducts());
  }
}

function* loadNextShare2EarnProducts(action): void {
  const page = yield select(getPage);
  try {
    const { params, isYollaBiz } = action.payload;
    let data = {};
    const requestParams = {
      page: page + 1,
      page_size: pagination.pageSize,
      ...params
    };
    if (!isYollaBiz) {
      data = yield call(Api.share2EarnProducts, requestParams);
    } else {
      data = yield call(Api.share2EarnProductsYollaBiz, requestParams);
      if (data.results) {
        data.results = data.results.map(item => transformToEarnProduct(item));
      }
    }
    if (!data.next) {
      yield put(reachEndShare2EarnProducts());
    }
    yield put(appendShare2EarnProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectShare2EarnProducts());
  }
}

export function* watchShare2EarnProductsSaga(): void {
  yield all([
    takeLatest(
      share2EarnProductsAction.SHARE2EARN_PRODUCTS_REQUESTED,
      requestShare2EarnProducts
    ),
    takeLatest(
      share2EarnProductsAction.SHARE2EARN_PRODUCTS_FETCHED_MORE,
      loadNextShare2EarnProducts
    ),
    takeLatest(
      share2EarnProductsAction.SHARE2EARN_PRODUCTS_REFRESHED,
      refreshShare2EarnProducts
    )
  ]);
}
