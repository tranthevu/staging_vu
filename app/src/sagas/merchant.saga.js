import { put, takeLatest, call } from "redux-saga/effects";
import Api from "../api/api";
import { invoke } from "../helpers/sagas.helper";
import {
  searchListMerchantByAddressSuccess,
  getTopMerchantByAddressSuccess
} from "../actions/merchant.actions";
import { callbackSuccess } from "../helpers/callback";
import {
  SEARCH_LIST_MERCHANT_BY_ADDRESS_ASYNC,
  GET_TOP_MERCHANT_BY_ADDRESS_ASYNC
} from "../actions/actionTypes";
import { PAGE_LIMIT } from "../constants/app.constant";

function* searchListMerchantByAddress({ payload }) {
  const { showLoading = false, category, type, page, point } = payload || {};
  yield invoke(
    function* execution() {
      const response = yield call(Api.seatchListMerchantByAddress, {
        category,
        type,
        point,
        page_size: PAGE_LIMIT,
        page
      });
      yield put(searchListMerchantByAddressSuccess(response));
      yield callbackSuccess(payload, response);
    },
    function* handleError() {
      yield callbackSuccess(null, { results: [] });
    },
    showLoading
  );
}

function* getTopMerchantByAddress({ payload }) {
  const { showLoading = false, category, type, point } = payload || {};
  yield invoke(
    function* execution() {
      const response = yield call(Api.seatchListMerchantByAddress, {
        category,
        type,
        point,
        page_size: 5,
        page: 1
      });
      yield put(getTopMerchantByAddressSuccess(response.results));
      yield callbackSuccess(payload, response);
    },
    null,
    showLoading
  );
}

export function* merChantSaga() {
  yield takeLatest(
    SEARCH_LIST_MERCHANT_BY_ADDRESS_ASYNC.REQUEST,
    searchListMerchantByAddress
  );
  yield takeLatest(GET_TOP_MERCHANT_BY_ADDRESS_ASYNC.REQUEST, getTopMerchantByAddress);
}
