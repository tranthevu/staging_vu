import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillExpiringProducts,
  rejectExpiringProducts,
  appendExpiringProducts,
  reachEndExpiringProducts,
  expiringProductsAction,
  fetchMoreExpiringProducts
} from "../actions/expiring-products.action";

function getPage(state: any): any {
  return state.expiringProducts.page;
}

function* requestExpiringProducts(action): void {
  const { category, merchant, ordering } = action.payload;
  try {
    const data = yield call(Api.getExpiringProducts, {
      page: 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndExpiringProducts());
    }
    yield put(fulfillExpiringProducts(data.results));
  } catch (e) {
    yield put(rejectExpiringProducts());
  }
}

function* refreshExpiringProducts(action): void {
  const { category, merchant, ordering } = action.payload;
  try {
    const data = yield call(Api.getExpiringProducts, {
      page: 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndExpiringProducts());
    }
    yield put(fulfillExpiringProducts(data.results));
  } catch (e) {
    yield put(rejectExpiringProducts());
  }
}

function* loadNextExpiringProducts(action): void {
  const { category, merchant, ordering } = action.payload;
  const page = yield select(getPage);
  try {
    const data = yield call(Api.getExpiringProducts, {
      page: page + 1,
      page_size: pagination.pageSize,
      category,
      merchant,
      ordering
    });
    if (!data.next) {
      yield put(reachEndExpiringProducts());
    }
    yield put(appendExpiringProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectExpiringProducts());
  }
}

export function* watchExpiringProductsSaga(): void {
  yield all([
    takeLatest(
      expiringProductsAction.EXPIRING_PRODUCTS_REQUESTED,
      requestExpiringProducts
    ),
    takeLatest(
      expiringProductsAction.EXPIRING_PRODUCTS_FETCHED_MORE,
      loadNextExpiringProducts
    ),
    takeLatest(
      expiringProductsAction.EXPIRING_PRODUCTS_REFRESHED,
      refreshExpiringProducts
    )
  ]);
}
