import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillUnreadNotifications,
  rejectUnreadNotifications,
  appendUnreadNotifications,
  reachEndUnreadNotifications,
  unreadNotificationsAction,
  fetchMoreUnreadNotifications
} from "../actions/unread-notifications.action";

function getPage(state: any): any {
  return state.unreadNotifications.page;
}

function* requestUnreadNotifications(): void {
  try {
    const data = yield call(Api.getUnreadNotifications, {
      page: 1,
      page_size: pagination.pageSize
    });
    if (!data.next) {
      yield put(reachEndUnreadNotifications());
    }
    yield put(fulfillUnreadNotifications(data.results));
  } catch (e) {
    yield put(rejectUnreadNotifications());
  }
}

function* refreshUnreadNotifications(): void {
  try {
    const data = yield call(Api.getUnreadNotifications, {
      page: 1,
      page_size: pagination.pageSize
    });
    if (!data.next) {
      yield put(reachEndUnreadNotifications());
    }
    yield put(fulfillUnreadNotifications(data.results));
  } catch (e) {
    yield put(rejectUnreadNotifications());
  }
}

function* loadNextUnreadNotifications(): void {
  const page = yield select(getPage);
  try {
    const data = yield call(Api.getUnreadNotifications, {
      page: page + 1,
      page_size: pagination.pageSize
    });
    if (!data.next) {
      yield put(reachEndUnreadNotifications());
    }
    yield put(appendUnreadNotifications(data.results, page + 1));
  } catch (e) {
    yield put(rejectUnreadNotifications());
  }
}

export function* watchUnreadNotificationsSaga(): void {
  yield all([
    takeLatest(
      unreadNotificationsAction.UNREAD_NOTIFICATIONS_REQUESTED,
      requestUnreadNotifications
    ),
    takeLatest(
      unreadNotificationsAction.UNREAD_NOTIFICATIONS_FETCHED_MORE,
      loadNextUnreadNotifications
    ),
    takeLatest(
      unreadNotificationsAction.UNREAD_NOTIFICATIONS_REFRESHED,
      refreshUnreadNotifications
    )
  ]);
}
