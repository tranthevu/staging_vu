import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillRedemptionHistory,
  rejectRedemptionHistory,
  appendRedemptionHistory,
  reachEndRedemptionHistory,
  redemptionHistoryAction
} from "../actions/redemption-history.action";

function getPage(state: any): any {
  return state.redemptionHistory.page;
}

function* requestRedemptionHistory(): void {
  try {
    const res = yield call(Api.redemptionHistory, {
      page: 1,
      page_size: pagination.pageSize
    });
    const { data } = res;
    if (!data.next) {
      yield put(reachEndRedemptionHistory());
    }
    yield put(fulfillRedemptionHistory(data.results));
  } catch (e) {
    yield put(rejectRedemptionHistory());
  }
}

function* refreshRedemptionHistory(): void {
  try {
    const res = yield call(Api.redemptionHistory, {
      page: 1,
      page_size: pagination.pageSize
    });
    const { data } = res;
    if (!data.next) {
      yield put(reachEndRedemptionHistory());
    }
    yield put(fulfillRedemptionHistory(data.results));
  } catch (e) {
    yield put(rejectRedemptionHistory());
  }
}

function* loadNextRedemptionHistory(): void {
  const page = yield select(getPage);
  try {
    const res = yield call(Api.redemptionHistory, {
      page: page + 1,
      page_size: pagination.pageSize
    });
    const { data } = res;
    if (!data.next) {
      yield put(reachEndRedemptionHistory());
    }
    yield put(appendRedemptionHistory(data.results, page + 1));
  } catch (e) {
    yield put(rejectRedemptionHistory());
  }
}

export function* watchRedemptionHistorySaga(): void {
  yield all([
    takeLatest(
      redemptionHistoryAction.REDEMPTION_HISTORY_REQUESTED,
      requestRedemptionHistory
    ),
    takeLatest(
      redemptionHistoryAction.REDEMPTION_HISTORY_FETCHED_MORE,
      loadNextRedemptionHistory
    ),
    takeLatest(
      redemptionHistoryAction.REDEMPTION_HISTORY_REFRESHED,
      refreshRedemptionHistory
    )
  ]);
}
