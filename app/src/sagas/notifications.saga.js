import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillNotifications,
  rejectNotifications,
  appendNotifications,
  reachEndNotifications,
  notificationsAction,
  fetchMoreNotifications,
  countedNotifications
} from "../actions/notifications.action";
import { PushNotificationIOS, Platform } from "react-native";
import BadgeAndroid from "react-native-android-badge";
import lodash from "lodash";

function getPage(state: any): any {
  return state.notifications.page;
}

function* requestNotifications(): void {
  try {
    const data = yield call(Api.getNotificationList, {
      page: 1,
      page_size: pagination.pageSize
    });
    if (!data.next) {
      yield put(reachEndNotifications());
    }
    yield put(fulfillNotifications(data.results));
  } catch (e) {
    yield put(rejectNotifications());
  }
}

function* refreshNotifications(): void {
  try {
    const data = yield call(Api.getNotificationList, {
      page: 1,
      page_size: pagination.pageSize
    });
    if (!data.next) {
      yield put(reachEndNotifications());
    }
    yield put(fulfillNotifications(data.results));
  } catch (e) {
    yield put(rejectNotifications());
  }
}

function* countNotifications(): void {
  try {
    const data = yield call(Api.countNotifications);
    const unreadcount = parseInt(lodash.get(data, "data.unreadcount"), 10);
    if (Platform.OS === "ios") {
      PushNotificationIOS.setApplicationIconBadgeNumber(unreadcount);
    } else {
      BadgeAndroid.setBadge(unreadcount);
    }
    yield put(countedNotifications(data.data));
  } catch (e) {}
}

function* loadNextNotifications(): void {
  const page = yield select(getPage);
  try {
    const data = yield call(Api.getNotificationList, {
      page: page + 1,
      page_size: pagination.pageSize
    });
    if (!data.next) {
      yield put(reachEndNotifications());
    }
    yield put(appendNotifications(data.results, page + 1));
  } catch (e) {
    yield put(rejectNotifications());
  }
}

export function* watchNotificationsSaga(): void {
  yield all([
    takeLatest(
      notificationsAction.NOTIFICATIONS_REQUESTED,
      requestNotifications
    ),
    takeLatest(
      notificationsAction.NOTIFICATIONS_FETCHED_MORE,
      loadNextNotifications
    ),
    takeLatest(
      notificationsAction.NOTIFICATIONS_REFRESHED,
      refreshNotifications
    ),
    takeLatest(
      notificationsAction.NOTIFICATIONS_REQUESTED_COUNT,
      countNotifications
    )
  ]);
}
