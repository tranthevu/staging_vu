import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillAreas,
  rejectAreas,
  appendAreas,
  reachEndAreas,
  areasAction,
  fetchMoreAreas
} from "../actions/areas.action";

function getPage(state: any): any {
  return state.areas.page;
}

function* requestAreas(): void {
  try {
    const data = yield call(Api.getAreas, {
      page: 1,
      page_size: pagination.pageSize
    });
    if (!data.next) {
      yield put(reachEndAreas());
    }
    yield put(fulfillAreas(data.results));
  } catch (e) {
    yield put(rejectAreas());
  }
}

function* refreshAreas(): void {
  try {
    const data = yield call(Api.getAreas, {
      page: 1,
      page_size: pagination.pageSize
    });
    if (!data.next) {
      yield put(reachEndAreas());
    }
    yield put(fulfillAreas(data.results));
  } catch (e) {
    yield put(rejectAreas());
  }
}

function* loadNextAreas(): void {
  const page = yield select(getPage);
  try {
    const data = yield call(Api.getAreas, {
      page: page + 1,
      page_size: pagination.pageSize
    });
    if (!data.next) {
      yield put(reachEndAreas());
    }
    yield put(appendAreas(data.results, page + 1));
  } catch (e) {
    yield put(rejectAreas());
  }
}

export function* watchAreasSaga(): void {
  yield all([
    takeLatest(areasAction.AREAS_REQUESTED, requestAreas),
    takeLatest(areasAction.AREAS_FETCHED_MORE, loadNextAreas),
    takeLatest(areasAction.AREAS_REFRESHED, refreshAreas)
  ]);
}
