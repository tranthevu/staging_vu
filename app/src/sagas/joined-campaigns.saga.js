import { put, call, takeLatest, select, all } from "redux-saga/effects";
import Api from "../api/api";
import { pagination } from "../constants/app.constant";
import {
  fulfillJoinedCampaigns,
  rejectJoinedCampaigns,
  appendJoinedCampaigns,
  reachEndJoinedCampaigns,
  joinedCampaignsAction
} from "../actions/joined-campaigns.action";

function getPage(state: any): any {
  return state.joinedCampaigns.page;
}

function* requestJoinedCampaigns(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.share2EarnProducts, {
      page: 1,
      page_size: pagination.pageSize,
      status: "joined",
      ...params
    });
    if (!data.next) {
      yield put(reachEndJoinedCampaigns());
    }
    yield put(fulfillJoinedCampaigns(data.results));
  } catch (e) {
    yield put(rejectJoinedCampaigns());
  }
}

function* refreshJoinedCampaigns(action): void {
  try {
    const { params } = action.payload;
    const data = yield call(Api.share2EarnProducts, {
      page: 1,
      page_size: pagination.pageSize,
      status: "joined",
      ...params
    });
    if (!data.next) {
      yield put(reachEndJoinedCampaigns());
    }
    yield put(fulfillJoinedCampaigns(data.results));
  } catch (e) {
    yield put(rejectJoinedCampaigns());
  }
}

function* loadNextJoinedCampaigns(action): void {
  const page = yield select(getPage);
  try {
    const { params } = action.payload;

    const data = yield call(Api.share2EarnProducts, {
      page: page + 1,
      page_size: pagination.pageSize,
      status: "joined",
      ...params
    });
    if (!data.next) {
      yield put(reachEndJoinedCampaigns());
    }
    yield put(appendJoinedCampaigns(data.results, page + 1));
  } catch (e) {
    yield put(rejectJoinedCampaigns());
  }
}

export function* watchJoinedCampaignsSaga(): void {
  yield all([
    takeLatest(joinedCampaignsAction.JOINED_CAMPAIGNS_REQUESTED, requestJoinedCampaigns),
    takeLatest(
      joinedCampaignsAction.JOINED_CAMPAIGNS_FETCHED_MORE,
      loadNextJoinedCampaigns
    ),
    takeLatest(joinedCampaignsAction.JOINED_CAMPAIGNS_REFRESHED, refreshJoinedCampaigns)
  ]);
}
