import { put, takeLatest, call, all } from "redux-saga/effects";
import Api from "../api/api";
import {
  loadedAutocomplete,
  requestAutocompleteFailed,
  autocompleteAction
} from "../actions/autocomplete.action";

function* fetchAutocomplete(action) {
  try {
    const { keyword, category } = action.payload;
    const response = yield call(Api.getAutocomplete, keyword, category);
    yield put(loadedAutocomplete(response.data));
  } catch (err) {
    yield put(requestAutocompleteFailed());
  }
}

export function* watchAutocompleteSaga() {
  yield all([
    takeLatest(autocompleteAction.AUTOCOMPLETE_REQUESTED, fetchAutocomplete)
  ]);
}
