import { put, takeLatest, call, all } from "redux-saga/effects";
import Api from "../api/api";
import {
  loadedFilters,
  requestFiltersFailed,
  filtersAction
} from "../actions/filters.action";

function* fetchFilters() {
  try {
    const response = yield call(Api.getFilters);
    yield put(loadedFilters(response.data));
  } catch (err) {
    yield put(requestFiltersFailed());
  }
}

export function* watchFiltersSaga() {
  yield all([takeLatest(filtersAction.FILTERS_REQUESTED, fetchFilters)]);
}
