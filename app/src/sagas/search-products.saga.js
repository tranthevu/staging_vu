import { put, call, takeLatest, select, all } from "redux-saga/effects";
import _ from "lodash";
import Api from "../api/api";
import { pagination, PAGE_LIMIT } from "../constants/app.constant";
import {
  fulfillSearchProducts,
  rejectSearchProducts,
  appendSearchProducts,
  reachEndSearchProducts,
  searchProductsAction,
  getProductNearBySuccess
} from "../actions/search-products.action";
import { GET_PRODUCTS_NEAR_BY } from "../actions/actionTypes";
import { invoke } from "../helpers/sagas.helper";
import { callbackSuccess, callbackError } from "../helpers/callback";

function getPage(state) {
  return state.searchProducts.page;
}

function* requestSearchProducts(action) {
  try {
    const { params } = action.payload;
    const data = yield call(Api.searchProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndSearchProducts());
    }
    yield put(fulfillSearchProducts(data.results, data.count));
  } catch (e) {
    yield put(rejectSearchProducts());
  }
}

function* refreshSearchProducts(action) {
  try {
    const { params } = action.payload;
    const data = yield call(Api.searchProducts, {
      page: 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndSearchProducts());
    }
    yield put(fulfillSearchProducts(data.results, data.count));
  } catch (e) {
    yield put(rejectSearchProducts());
  }
}

function* loadNextSearchProducts(action) {
  const page = yield select(getPage);
  try {
    const { params } = action.payload;

    const data = yield call(Api.searchProducts, {
      page: page + 1,
      page_size: pagination.pageSize,
      ...params
    });
    if (!data.next) {
      yield put(reachEndSearchProducts());
    }
    yield put(appendSearchProducts(data.results, page + 1));
  } catch (e) {
    yield put(rejectSearchProducts());
  }
}

function* getProductsNearBy({ payload }) {
  const { showLoading = true, callback } = payload;
  yield invoke(
    function* execution() {
      const { point, radius, category, type, page = 1 } = payload || {};
      const data = yield call(Api.getProductsByNear, {
        point,
        radius,
        category,
        type,
        page,
        page_size: PAGE_LIMIT
      });
      const result = yield call(Api.getProductsByNearDetail, {
        point,
        radius,
        category,
        type,
        list_ids: _.get(data, "results", []).map(e => e.id)
      });
      const dataPaging = { next: data.next, ...result };
      yield put(getProductNearBySuccess(dataPaging));
      callbackSuccess(payload, dataPaging);
    },
    callback
      ? function* handleError(err) {
          yield callbackError(payload, err);
        }
      : null,
    showLoading
  );
}

export function* watchSearchProductsSaga() {
  yield all([
    takeLatest(searchProductsAction.SEARCH_PRODUCTS_REQUESTED, requestSearchProducts),
    takeLatest(searchProductsAction.SEARCH_PRODUCTS_FETCHED_MORE, loadNextSearchProducts),
    takeLatest(searchProductsAction.SEARCH_PRODUCTS_REFRESHED, refreshSearchProducts),
    takeLatest(GET_PRODUCTS_NEAR_BY.REQUEST, getProductsNearBy)
  ]);
}
