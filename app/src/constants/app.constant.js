import { appConfig } from "../config/app.config";

export const environmentMap = {
  development: "development",
  staging: "staging",
  production: "production"
};

export const appMap = {
  all: {
    key: ""
  },
  promo: {
    key: "event"
  },
  eCoupon: {
    key: "voucher, voucher_prepaid"
  },
  onlinePromo: {
    key: "affiliate-offer"
  },
  shareToEarn: {
    key: "share-to-earn"
  },
  loyalty: {
    key: "to be define later"
  }
};

export const apiMapping = {
  status_url: `${appConfig.apiUrl}status/`
};

export const dateFormat = {};

export const text = {
  emptyString: ""
};

export const message = {
  unknownError: "Seems something went wrong",
  networkError: "Please check network connection and try again!"
};

export const font = {
  medium: "Roboto-Medium",
  bold: "Roboto-Bold",
  regular: "Roboto-Regular",
  light: "Roboto-Light",
  lightItalic: "Roboto-LightItalic"
};

export const appColor = {
  primary: "#EF4136",
  icon: "#979797",
  text: "#444444",
  blur: "#979797",
  bg: "#F9F9F9",
  blue: "#4C87F8",
  gray: "#DDDDDD",
  backgroundGrayLight: "#E5E5E5",
  green: "#2DB766",
  placeholderTextColor: "#979797",
  white: "white"
};

export const pagination = {
  pageSize: 10
};

export const productClass = {
  voucher: "Voucher",
  event: "Event",
  affiliateOffer: "Affiliate Offer",
  shareToEarn: "Share To Earn",
  yollaBizShareToEarn: "YollaBiz Share To Earn",
  prepaidVoucher: "Voucher Prepaid",
  rewardCode: "Reward Code"
};

export const productCodeType = {
  voucher: "promotion_voucher_code",
  prepaidVoucher: "promotion_e_voucher_prepaid_code",
  prepaidnVoucherDiscount: "promotion_e_voucher_prepaid_discount_code",
  shareToEarn: "share_to_earn_code",
  yollaBizShareToEarn: "yolla_biz_app_share_to_earn_code_reward",
  rewardCode: "reward_code"
};

export const pageLink = {
  yolla: "https://yolla.vn/",
  help: "https://yolla.vn/#/contact"
};

export const documentType = {
  introduction: "introduction",
  policy: "policy",
  privacy: "privacy",
  activity: "activity",
  contact: "contact",
  faqs: "faqs"
};

export const gender = {
  male: 0,
  female: 1,
  other: 2
};

export const event = {
  savedWishlist: "save_wishlist",
  removedWishlist: "removed_wishlist",
  removedCode: "removed_code",
  jumpPage: "jumpPage",
  shownNotification: "shownNotification",
  codeUsed: "codeUsed",
  forceLogout: "force_logout",
  showPopupNotification: "showPopupNotification",
  networkError: "network_error"
};

export const analyticsEvent = {
  viewProduct: "view_product",
  getCode: "get_code",
  saveEvent: "save_event"
};

export const codeStatus = [
  { type: 1, name: "Mã ưu đãi chưa dùng" },
  { type: 2, name: "Mã ưu đãi đã dùng" },
  { name: "Tất cả" }
];

export const codeProductType = [
  { type: "voucher", name: "Coupon đã lưu" },
  { type: "event", name: "Chương trình khuyến mãi" },
  { type: "affiliate-offer", name: "Ưu đãi online" },
  { type: "all", name: "Tất cả" }
];

export const codeVoucher = [
  { type: 2, name: "Chưa dùng" },
  { type: 4, name: "Đã dùng" },
  { type: 3, name: "Hết hạn dùng" },
  { type: 1, name: "Tin hết hạn " },
  { type: 0, name: "Tất cả" }
];

export const giftStatus = {
  all: 0,
  received: 1,
  used: 3
};

export const codeEvent = [
  { type: 1, name: "Còn hạn" },
  { type: 2, name: "Hết hạn" },
  { type: 0, name: "Tất cả" }
];

export const codeOnlinePromotion = [
  { type: 1, name: "Còn hạn" },
  { type: 2, name: "Hết hạn" },
  { type: 0, name: "Tất cả" }
];

export const codeType = {
  single: 0,
  multi: 1
};

export const keywordType = {
  keyword: 0,
  category: 1,
  area: 2
};

export const locations = [
  { name: "Toàn quốc", key: "toan-quoc" },
  { name: "Hồ Chí Minh", key: "ho-chi-minh" },
  { name: "Hà Nội", key: "ha-noi" }
];

export const apiError = {
  confirmMultiUseOffer: {
    1: "Mã ưu đãi không hợp lệ, yêu cầu nhiều mã sử dụng",
    2: "Mã ưu đãi không có sẵn",
    3: "Mã ưu đãi đã hết hạn",
    4: "Bạn chưa nhận được mã ưu đãi này",
    5: "Xác nhận mật khẩu không chính xác"
  },
  saveWishlist: {
    1: "Ưu đãi đã hết hạn",
    2: "Mã ưu đãi không có sẵn",
    3: "Mã ưu đãi đã hết"
  },
  removeOffer: {
    1: "Bạn chưa nhận được mã ưu đãi này",
    2: "Mã ưu đãi đã được sử dụng",
    3: "Mã ưu được trả về trước đó",
    4: "Mã ưu đãi đã hết hạn"
  }
};

export const loginType = {
  otp: "otp",
  google: "google",
  facebook: "facebook"
};

export const notificationType = {
  notify: "Notify",
  popup: "Popup"
};

export const notificationAction = {
  productList: "product_list",
  productDetail: "product_detail",
  myCodeList: "my_code_list",
  myCodeDetail: "my_code_detail",
  myRewardDetail: "my_reward_detail",
  openbrowser: "openbrowser",
  popup: "popup"
};

export const voucherStatus = {
  used: 4,
  isExpired: 3,
  isOfferExpired: 1
};

export const VOUCHER_TYPE = {
  SHARE_TO_EARN: "share_to_earn_code",
  prepaidVoucher: "voucher_prepaid",
  shareToEarn: "share_to_earn",
  eCoupon: "voucher",
  yollaBizShareToEarn: "yolla_biz_share_to_earn"
};

export const cookie = {
  clientCas: "client_cas"
};

export const usage = {
  single: "Single use",
  multi: "Multi use"
};

export const LATITUDE_DELTA = 0.05;

export const LONGITUDE_DELTA = 0.05;

export const ERROR_CODE = {
  ECONNABORTED: "ECONNABORTED",
  NOT_FOUND: 404,
  NOT_INTERNET: "NOT_INTERNET"
};

export const RADIUS_INIT = 5;

export const URL_API = {
  GOOGLE: "https://maps.googleapis.com/maps/api/"
};

export const PAGE_LIMIT = 15;

export const TYPE_ACTION = {
  PRESS: 0,
  DELETE: 1
};

export const GIFT_STATUS_CODE = {
  RECEIVED: 1,
  EXPIRED: 2,
  USED: 3
};

export const GIFT_STATUS = [
  { status: 0, name: "Tất cả" },
  { status: GIFT_STATUS_CODE.RECEIVED, name: "Phần thưởng còn hạn" },
  { status: GIFT_STATUS_CODE.EXPIRED, name: "Phần thưởng đã hết hạn" },
  { status: GIFT_STATUS_CODE.USED, name: "Phần thưởng Đã dùng" }
];

export const GIFT_REWARD_STATUS = {
  RECEIVED: 1,
  EXPIRED: 0,
  USED: 2
};

export const dateTimeFormat = {
  ddmmyyyyhhmm: "DD/MM/YYYY HH:mm",
  ddmmyyyy: "DD/MM/YYYY"
};

export const eVoucherStatus = {
  initial: "Đang tạo",
  pending: "Đang chờ",
  settled: "Thất bại",
  failed: "Thất bại",
  cancelled: "Đã hủy",
  success: "Thành công",
  waiting: "Đang xử lý"
};
