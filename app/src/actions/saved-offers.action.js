export const savedOffersAction = {
  SAVED_OFFERS_FULFILLED: "SAVED_OFFERS_FULFILLED",
  SAVED_OFFERS_REQUESTED: "SAVED_OFFERS_REQUESTED",
  SAVED_OFFERS_REJECTED: "SAVED_OFFERS_REJECTED",
  SAVED_OFFERS_REFRESHED: "SAVED_OFFERS_REFRESHED",
  SAVED_OFFERS_FETCHED_MORE: "SAVED_OFFERS_FETCHED_MORE",
  SAVED_OFFERS_APPENDED: "SAVED_OFFERS_APPENDED",
  SAVED_OFFERS_REACHED_END: "SAVED_OFFERS_REACHED_END"
};

export const requestSavedOffers = (status, productType) => {
  return {
    type: savedOffersAction.SAVED_OFFERS_REQUESTED,
    payload: {
      status,
      productType
    }
  };
};

export const fulfillSavedOffers = savedOffers => {
  return {
    payload: {
      savedOffers
    },
    type: savedOffersAction.SAVED_OFFERS_FULFILLED
  };
};

export const rejectSavedOffers = () => {
  return {
    type: savedOffersAction.SAVED_OFFERS_REJECTED
  };
};

export const refreshSavedOffers = (status, productType) => {
  return {
    type: savedOffersAction.SAVED_OFFERS_REFRESHED,
    payload: {
      status,
      productType
    }
  };
};

export const fetchMoreSavedOffers = (status, productType) => {
  return {
    type: savedOffersAction.SAVED_OFFERS_FETCHED_MORE,
    payload: {
      status,
      productType
    }
  };
};

export const appendSavedOffers = (savedOffers, page) => {
  return {
    type: savedOffersAction.SAVED_OFFERS_APPENDED,
    payload: {
      savedOffers,
      page
    }
  };
};

export const reachEndSavedOffers = () => {
  return {
    type: savedOffersAction.SAVED_OFFERS_REACHED_END
  };
};
