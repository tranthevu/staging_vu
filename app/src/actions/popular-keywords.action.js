export const popularKeywordsAction = {
  POPULAR_KEYWORDS_LOADED: "POPULAR_KEYWORDS_LOADED",
  POPULAR_KEYWORDS_REQUESTED: "POPULAR_KEYWORDS_REQUESTED",
  POPULAR_KEYWORDS_REQUEST_FAILED: "POPULAR_KEYWORDS_REQUEST_FAILED"
};

export const loadedPopularKeywords = popularKeywords => {
  return {
    type: popularKeywordsAction.POPULAR_KEYWORDS_LOADED,
    payload: {
      popularKeywords
    }
  };
};

export const requestedPopularKeywords = () => {
  return {
    type: popularKeywordsAction.POPULAR_KEYWORDS_REQUESTED
  };
};

export const requestPopularKeywordsFailed = () => {
  return {
    type: popularKeywordsAction.POPULAR_KEYWORDS_REQUEST_FAILED
  };
};
