export const unreadNotificationsAction = {
  UNREAD_NOTIFICATIONS_FULFILLED: "UNREAD_NOTIFICATIONS_FULFILLED",
  UNREAD_NOTIFICATIONS_REQUESTED: "UNREAD_NOTIFICATIONS_REQUESTED",
  UNREAD_NOTIFICATIONS_REJECTED: "UNREAD_NOTIFICATIONS_REJECTED",
  UNREAD_NOTIFICATIONS_REFRESHED: "UNREAD_NOTIFICATIONS_REFRESHED",
  UNREAD_NOTIFICATIONS_FETCHED_MORE: "UNREAD_NOTIFICATIONS_FETCHED_MORE",
  UNREAD_NOTIFICATIONS_APPENDED: "UNREAD_NOTIFICATIONS_APPENDED",
  UNREAD_NOTIFICATIONS_REACHED_END: "UNREAD_NOTIFICATIONS_REACHED_END"
};

export const requestUnreadNotifications = () => {
  return {
    type: unreadNotificationsAction.UNREAD_NOTIFICATIONS_REQUESTED
  };
};

export const fulfillUnreadNotifications = unreadNotifications => {
  return {
    payload: {
      unreadNotifications
    },
    type: unreadNotificationsAction.UNREAD_NOTIFICATIONS_FULFILLED
  };
};

export const rejectUnreadNotifications = () => {
  return {
    type: unreadNotificationsAction.UNREAD_NOTIFICATIONS_REJECTED
  };
};

export const refreshUnreadNotifications = () => {
  return {
    type: unreadNotificationsAction.UNREAD_NOTIFICATIONS_REFRESHED
  };
};

export const fetchMoreUnreadNotifications = () => {
  return {
    type: unreadNotificationsAction.UNREAD_NOTIFICATIONS_FETCHED_MORE
  };
};

export const appendUnreadNotifications = (unreadNotifications, page) => {
  return {
    type: unreadNotificationsAction.UNREAD_NOTIFICATIONS_APPENDED,
    payload: {
      unreadNotifications,
      page
    }
  };
};

export const reachEndUnreadNotifications = () => {
  return {
    type: unreadNotificationsAction.UNREAD_NOTIFICATIONS_REACHED_END
  };
};
