import {
  UPDATE_FAVORITE_CATEGORY_ASYNC,
  GET_FAVORITE_CATEGORIES_ASYNC,
  CHECK_FAVORITE_CATEGORIES_ASYNC,
  GET_USER_LOCATION_ASYNC
} from "./actionTypes";

export const profileAction = {
  PROFILE_LOADED: "PROFILE_LOADED",
  PROFILE_RESETED: "PROFILE_RESETED",
  PROFILE_REQUESTED: "PROFILE_REQUESTED",
  PROFILE_REQUEST_FAILED: "PROFILE_REQUEST_FAILED"
};

export const loadedProfile = profile => {
  return {
    type: profileAction.PROFILE_LOADED,
    payload: {
      profile
    }
  };
};

export const resetProfile = () => {
  return {
    type: profileAction.PROFILE_RESETED
  };
};

export const requestedProfile = () => {
  return {
    type: profileAction.PROFILE_REQUESTED
  };
};

export const requestProfileFailed = () => {
  return {
    type: profileAction.PROFILE_REQUEST_FAILED
  };
};

export const getPlaceUserByLocationSubmit = payload => {
  return {
    type: GET_USER_LOCATION_ASYNC.REQUEST,
    payload
  };
};

export const updateFavoriteCategory = payload => {
  return {
    type: UPDATE_FAVORITE_CATEGORY_ASYNC.REQUEST,
    payload
  };
};

export const getPlaceUserByLocationSuccess = payload => {
  return {
    type: GET_USER_LOCATION_ASYNC.SUCCESS,
    payload
  };
};

export const getFavoriteCategories = payload => {
  return {
    type: GET_FAVORITE_CATEGORIES_ASYNC.REQUEST,
    payload
  };
};

export const checkFavoriteCategoriesSubmit = payload => {
  return {
    type: CHECK_FAVORITE_CATEGORIES_ASYNC.REQUEST,
    payload
  };
};
