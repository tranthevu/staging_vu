export const filtersAction = {
  FILTERS_LOADED: "FILTERS_LOADED",
  FILTERS_REQUESTED: "FILTERS_REQUESTED",
  FILTERS_REQUEST_FAILED: "FILTERS_REQUEST_FAILED"
};

export const loadedFilters = filters => {
  return {
    type: filtersAction.FILTERS_LOADED,
    payload: {
      filters
    }
  };
};

export const requestedFilters = () => {
  return {
    type: filtersAction.FILTERS_REQUESTED
  };
};

export const requestFiltersFailed = () => {
  return {
    type: filtersAction.FILTERS_REQUEST_FAILED
  };
};
