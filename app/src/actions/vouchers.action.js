export const voucherAction = {
  VOUCHER_REMOVED: "VOUCHER_REMOVED"
};

export const removeVoucher = voucher => {
  return {
    type: voucherAction.VOUCHER_REMOVED,
    payload: {
      voucher
    }
  };
};
