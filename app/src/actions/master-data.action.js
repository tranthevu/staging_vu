export const masterDataAction = {
  MASTER_DATA_LOADED: "MASTER_DATA_LOADED",
  MASTER_DATA_REQUESTED: "MASTER_DATA_REQUESTED",
  MASTER_DATA_REQUEST_FAILED: "MASTER_DATA_REQUEST_FAILED"
};

export const loadedMasterData = masterData => {
  return {
    type: masterDataAction.MASTER_DATA_LOADED,
    payload: {
      masterData
    }
  };
};

export const requestedMasterData = () => {
  return {
    type: masterDataAction.MASTER_DATA_REQUESTED
  };
};

export const requestMasterDataFailed = () => {
  return {
    type: masterDataAction.MASTER_DATA_REQUEST_FAILED
  };
};
