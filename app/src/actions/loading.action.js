import { DIALOG_WAITING, STATE_API } from "./actionTypes";

export const showLoading = () => ({
  type: DIALOG_WAITING.SHOW
});

export const hideLoading = () => ({
  type: DIALOG_WAITING.HIDE
});

export const isLoading = () => ({
  type: STATE_API.IS_LOADING
});

export const nonLoading = () => ({
  type: STATE_API.NON_LOADING
});
