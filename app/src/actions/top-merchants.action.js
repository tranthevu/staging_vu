export const topMerchantsAction = {
  TOP_MERCHANTS_LOADED: "TOP_MERCHANTS_LOADED",
  TOP_MERCHANTS_REQUESTED: "TOP_MERCHANTS_REQUESTED",
  TOP_MERCHANTS_REQUEST_FAILED: "TOP_MERCHANTS_REQUEST_FAILED"
};

export const loadedTopMerchants = topMerchants => {
  return {
    type: topMerchantsAction.TOP_MERCHANTS_LOADED,
    payload: {
      topMerchants
    }
  };
};

export const requestedTopMerchants = () => {
  return {
    type: topMerchantsAction.TOP_MERCHANTS_REQUESTED
  };
};

export const requestTopMerchantsFailed = () => {
  return {
    type: topMerchantsAction.TOP_MERCHANTS_REQUEST_FAILED
  };
};
