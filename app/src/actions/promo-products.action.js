import { GET_LIST_PROMO_BY_ADDRESS_ASYNC } from "./actionTypes";

export const promoProductsAction = {
  PROMO_PRODUCTS_FULFILLED: "PROMO_PRODUCTS_FULFILLED",
  PROMO_PRODUCTS_REQUESTED: "PROMO_PRODUCTS_REQUESTED",
  PROMO_PRODUCTS_REJECTED: "PROMO_PRODUCTS_REJECTED",
  PROMO_PRODUCTS_REFRESHED: "PROMO_PRODUCTS_REFRESHED",
  PROMO_PRODUCTS_FETCHED_MORE: "PROMO_PRODUCTS_FETCHED_MORE",
  PROMO_PRODUCTS_APPENDED: "PROMO_PRODUCTS_APPENDED",
  PROMO_PRODUCTS_REACHED_END: "PROMO_PRODUCTS_REACHED_END"
};

export const requestPromoProducts = params => {
  return {
    type: promoProductsAction.PROMO_PRODUCTS_REQUESTED,
    payload: {
      params
    }
  };
};

export const fulfillPromoProducts = promoProducts => {
  return {
    payload: {
      promoProducts
    },
    type: promoProductsAction.PROMO_PRODUCTS_FULFILLED
  };
};

export const rejectPromoProducts = () => {
  return {
    type: promoProductsAction.PROMO_PRODUCTS_REJECTED
  };
};

export const refreshPromoProducts = params => {
  return {
    type: promoProductsAction.PROMO_PRODUCTS_REFRESHED,
    payload: {
      params
    }
  };
};

export const fetchMorePromoProducts = params => {
  return {
    type: promoProductsAction.PROMO_PRODUCTS_FETCHED_MORE,
    payload: {
      params
    }
  };
};

export const appendPromoProducts = (promoProducts, page) => {
  return {
    type: promoProductsAction.PROMO_PRODUCTS_APPENDED,
    payload: {
      promoProducts,
      page
    }
  };
};

export const reachEndPromoProducts = () => {
  return {
    type: promoProductsAction.PROMO_PRODUCTS_REACHED_END
  };
};

export const getListPromoByAddressSubmit = payload => ({
  type: GET_LIST_PROMO_BY_ADDRESS_ASYNC.REQUEST,
  payload
});
