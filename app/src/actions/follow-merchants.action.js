export const followMerchantsAction = {
  FOLLOW_MERCHANTS_FULFILLED: "FOLLOW_MERCHANTS_FULFILLED",
  FOLLOW_MERCHANTS_REQUESTED: "FOLLOW_MERCHANTS_REQUESTED",
  FOLLOW_MERCHANTS_REJECTED: "FOLLOW_MERCHANTS_REJECTED",
  FOLLOW_MERCHANTS_REFRESHED: "FOLLOW_MERCHANTS_REFRESHED",
  FOLLOW_MERCHANTS_FETCHED_MORE: "FOLLOW_MERCHANTS_FETCHED_MORE",
  FOLLOW_MERCHANTS_APPENDED: "FOLLOW_MERCHANTS_APPENDED",
  FOLLOW_MERCHANTS_REACHED_END: "FOLLOW_MERCHANTS_REACHED_END"
};

export const requestFollowMerchants = params => {
  return {
    type: followMerchantsAction.FOLLOW_MERCHANTS_REQUESTED,
    payload: {
      params
    }
  };
};

export const fulfillFollowMerchants = followMerchants => {
  return {
    payload: {
      followMerchants
    },
    type: followMerchantsAction.FOLLOW_MERCHANTS_FULFILLED
  };
};

export const rejectFollowMerchants = () => {
  return {
    type: followMerchantsAction.FOLLOW_MERCHANTS_REJECTED
  };
};

export const refreshFollowMerchants = params => {
  return {
    type: followMerchantsAction.FOLLOW_MERCHANTS_REFRESHED,
    payload: {
      params
    }
  };
};

export const fetchMoreFollowMerchants = params => {
  return {
    type: followMerchantsAction.FOLLOW_MERCHANTS_FETCHED_MORE,
    payload: {
      params
    }
  };
};

export const appendFollowMerchants = (followMerchants, page) => {
  return {
    type: followMerchantsAction.FOLLOW_MERCHANTS_APPENDED,
    payload: {
      followMerchants,
      page
    }
  };
};

export const reachEndFollowMerchants = () => {
  return {
    type: followMerchantsAction.FOLLOW_MERCHANTS_REACHED_END
  };
};
