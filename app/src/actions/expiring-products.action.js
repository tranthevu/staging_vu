export const expiringProductsAction = {
  EXPIRING_PRODUCTS_FULFILLED: "EXPIRING_PRODUCTS_FULFILLED",
  EXPIRING_PRODUCTS_REQUESTED: "EXPIRING_PRODUCTS_REQUESTED",
  EXPIRING_PRODUCTS_REJECTED: "EXPIRING_PRODUCTS_REJECTED",
  EXPIRING_PRODUCTS_REFRESHED: "EXPIRING_PRODUCTS_REFRESHED",
  EXPIRING_PRODUCTS_FETCHED_MORE: "EXPIRING_PRODUCTS_FETCHED_MORE",
  EXPIRING_PRODUCTS_APPENDED: "EXPIRING_PRODUCTS_APPENDED",
  EXPIRING_PRODUCTS_REACHED_END: "EXPIRING_PRODUCTS_REACHED_END"
};

export const requestExpiringProducts = (category, merchant, ordering) => {
  return {
    type: expiringProductsAction.EXPIRING_PRODUCTS_REQUESTED,
    payload: {
      category,
      merchant,
      ordering
    }
  };
};

export const fulfillExpiringProducts = expiringProducts => {
  return {
    payload: {
      expiringProducts
    },
    type: expiringProductsAction.EXPIRING_PRODUCTS_FULFILLED
  };
};

export const rejectExpiringProducts = () => {
  return {
    type: expiringProductsAction.EXPIRING_PRODUCTS_REJECTED
  };
};

export const refreshExpiringProducts = (category, merchant, ordering) => {
  return {
    type: expiringProductsAction.EXPIRING_PRODUCTS_REFRESHED,
    payload: {
      category,
      merchant,
      ordering
    }
  };
};

export const fetchMoreExpiringProducts = (category, merchant, ordering) => {
  return {
    type: expiringProductsAction.EXPIRING_PRODUCTS_FETCHED_MORE,
    payload: {
      category,
      merchant,
      ordering
    }
  };
};

export const appendExpiringProducts = (expiringProducts, page) => {
  return {
    type: expiringProductsAction.EXPIRING_PRODUCTS_APPENDED,
    payload: {
      expiringProducts,
      page
    }
  };
};

export const reachEndExpiringProducts = () => {
  return {
    type: expiringProductsAction.EXPIRING_PRODUCTS_REACHED_END
  };
};
