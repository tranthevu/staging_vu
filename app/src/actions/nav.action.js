import { NavigationActions, StackActions } from "react-navigation";
import { storeConfig } from "../app";

export const navigateToPage = (pageName, data) => {
  return NavigationActions.navigate({
    routeName: pageName,
    params: data
  });
};

export const replace = (routeName, params) => {
  storeConfig.store.dispatch({ type: "Navigation/REPLACE", routeName, params });
};

export const toggleDrawer = () => {
  return NavigationActions.navigate({ routeName: "DrawerToggle" });
};

export const navigateBack = () => {
  return NavigationActions.back({});
};

export const resetPage = (page, data) => {
  return NavigationActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: page, params: data })]
  });
};
