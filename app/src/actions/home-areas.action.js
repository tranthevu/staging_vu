export const homeAreasAction = {
  HOME_AREAS_LOADED: "HOME_AREAS_LOADED",
  HOME_AREAS_REQUESTED: "HOME_AREAS_REQUESTED",
  HOME_AREAS_REQUEST_FAILED: "HOME_AREAS_REQUEST_FAILED"
};

export const loadedHomeAreas = (homeAreas, count) => {
  return {
    type: homeAreasAction.HOME_AREAS_LOADED,
    payload: {
      homeAreas,
      count
    }
  };
};

export const requestedHomeAreas = () => {
  return {
    type: homeAreasAction.HOME_AREAS_REQUESTED
  };
};

export const requestHomeAreasFailed = () => {
  return {
    type: homeAreasAction.HOME_AREAS_REQUEST_FAILED
  };
};
