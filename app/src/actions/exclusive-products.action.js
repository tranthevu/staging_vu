export const exclusiveProductsAction = {
  EXCLUSIVE_PRODUCTS_FULFILLED: "EXCLUSIVE_PRODUCTS_FULFILLED",
  EXCLUSIVE_PRODUCTS_REQUESTED: "EXCLUSIVE_PRODUCTS_REQUESTED",
  EXCLUSIVE_PRODUCTS_REJECTED: "EXCLUSIVE_PRODUCTS_REJECTED",
  EXCLUSIVE_PRODUCTS_REFRESHED: "EXCLUSIVE_PRODUCTS_REFRESHED",
  EXCLUSIVE_PRODUCTS_FETCHED_MORE: "EXCLUSIVE_PRODUCTS_FETCHED_MORE",
  EXCLUSIVE_PRODUCTS_APPENDED: "EXCLUSIVE_PRODUCTS_APPENDED",
  EXCLUSIVE_PRODUCTS_REACHED_END: "EXCLUSIVE_PRODUCTS_REACHED_END"
};

export const requestExclusiveProducts = params => {
  return {
    type: exclusiveProductsAction.EXCLUSIVE_PRODUCTS_REQUESTED,
    payload: {
      params
    }
  };
};

export const fulfillExclusiveProducts = exclusiveProducts => {
  return {
    payload: {
      exclusiveProducts
    },
    type: exclusiveProductsAction.EXCLUSIVE_PRODUCTS_FULFILLED
  };
};

export const rejectExclusiveProducts = () => {
  return {
    type: exclusiveProductsAction.EXCLUSIVE_PRODUCTS_REJECTED
  };
};

export const refreshExclusiveProducts = params => {
  return {
    type: exclusiveProductsAction.EXCLUSIVE_PRODUCTS_REFRESHED,
    payload: {
      params
    }
  };
};

export const fetchMoreExclusiveProducts = params => {
  return {
    type: exclusiveProductsAction.EXCLUSIVE_PRODUCTS_FETCHED_MORE,
    payload: {
      params
    }
  };
};

export const appendExclusiveProducts = (exclusiveProducts, page) => {
  return {
    type: exclusiveProductsAction.EXCLUSIVE_PRODUCTS_APPENDED,
    payload: {
      exclusiveProducts,
      page
    }
  };
};

export const reachEndExclusiveProducts = () => {
  return {
    type: exclusiveProductsAction.EXCLUSIVE_PRODUCTS_REACHED_END
  };
};
