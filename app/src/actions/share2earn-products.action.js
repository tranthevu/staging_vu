export const share2EarnProductsAction = {
  SHARE2EARN_PRODUCTS_FULFILLED: "SHARE2EARN_PRODUCTS_FULFILLED",
  SHARE2EARN_PRODUCTS_REQUESTED: "SHARE2EARN_PRODUCTS_REQUESTED",
  SHARE2EARN_PRODUCTS_REJECTED: "SHARE2EARN_PRODUCTS_REJECTED",
  SHARE2EARN_PRODUCTS_REFRESHED: "SHARE2EARN_PRODUCTS_REFRESHED",
  SHARE2EARN_PRODUCTS_FETCHED_MORE: "SHARE2EARN_PRODUCTS_FETCHED_MORE",
  SHARE2EARN_PRODUCTS_APPENDED: "SHARE2EARN_PRODUCTS_APPENDED",
  SHARE2EARN_PRODUCTS_REACHED_END: "SHARE2EARN_PRODUCTS_REACHED_END"
};

export const requestShare2EarnProducts = (params, isYollaBiz) => {
  return {
    type: share2EarnProductsAction.SHARE2EARN_PRODUCTS_REQUESTED,
    payload: {
      params,
      isYollaBiz
    }
  };
};

export const fulfillShare2EarnProducts = share2EarnProducts => {
  return {
    payload: {
      share2EarnProducts
    },
    type: share2EarnProductsAction.SHARE2EARN_PRODUCTS_FULFILLED
  };
};

export const rejectShare2EarnProducts = () => {
  return {
    type: share2EarnProductsAction.SHARE2EARN_PRODUCTS_REJECTED
  };
};

export const refreshShare2EarnProducts = (params, isYollaBiz) => {
  return {
    type: share2EarnProductsAction.SHARE2EARN_PRODUCTS_REFRESHED,
    payload: {
      params,
      isYollaBiz
    }
  };
};

export const fetchMoreShare2EarnProducts = (params, isYollaBiz) => {
  return {
    type: share2EarnProductsAction.SHARE2EARN_PRODUCTS_FETCHED_MORE,
    payload: {
      params,
      isYollaBiz
    }
  };
};

export const appendShare2EarnProducts = (share2EarnProducts, page) => {
  return {
    type: share2EarnProductsAction.SHARE2EARN_PRODUCTS_APPENDED,
    payload: {
      share2EarnProducts,
      page
    }
  };
};

export const reachEndShare2EarnProducts = () => {
  return {
    type: share2EarnProductsAction.SHARE2EARN_PRODUCTS_REACHED_END
  };
};
