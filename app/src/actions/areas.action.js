export const areasAction = {
  AREAS_FULFILLED: "AREAS_FULFILLED",
  AREAS_REQUESTED: "AREAS_REQUESTED",
  AREAS_REJECTED: "AREAS_REJECTED",
  AREAS_REFRESHED: "AREAS_REFRESHED",
  AREAS_FETCHED_MORE: "AREAS_FETCHED_MORE",
  AREAS_APPENDED: "AREAS_APPENDED",
  AREAS_REACHED_END: "AREAS_REACHED_END"
};

export const requestAreas = () => {
  return {
    type: areasAction.AREAS_REQUESTED
  };
};

export const fulfillAreas = areas => {
  return {
    payload: {
      areas
    },
    type: areasAction.AREAS_FULFILLED
  };
};

export const rejectAreas = () => {
  return {
    type: areasAction.AREAS_REJECTED
  };
};

export const refreshAreas = () => {
  return {
    type: areasAction.AREAS_REFRESHED
  };
};

export const fetchMoreAreas = () => {
  return {
    type: areasAction.AREAS_FETCHED_MORE
  };
};

export const appendAreas = (areas, page) => {
  return {
    type: areasAction.AREAS_APPENDED,
    payload: {
      areas,
      page
    }
  };
};

export const reachEndAreas = () => {
  return {
    type: areasAction.AREAS_REACHED_END
  };
};
