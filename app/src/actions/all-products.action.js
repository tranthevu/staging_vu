export const allProductsAction = {
  ALL_PRODUCTS_FULFILLED: "ALL_PRODUCTS_FULFILLED",
  ALL_PRODUCTS_REQUESTED: "ALL_PRODUCTS_REQUESTED",
  ALL_PRODUCTS_REJECTED: "ALL_PRODUCTS_REJECTED",
  ALL_PRODUCTS_REFRESHED: "ALL_PRODUCTS_REFRESHED",
  ALL_PRODUCTS_FETCHED_MORE: "ALL_PRODUCTS_FETCHED_MORE",
  ALL_PRODUCTS_APPENDED: "ALL_PRODUCTS_APPENDED",
  ALL_PRODUCTS_REACHED_END: "ALL_PRODUCTS_REACHED_END"
};

export const requestAllProducts = (category, merchant, ordering) => {
  return {
    type: allProductsAction.ALL_PRODUCTS_REQUESTED,
    payload: {
      category,
      merchant,
      ordering
    }
  };
};

export const fulfillAllProducts = allProducts => {
  return {
    payload: {
      allProducts
    },
    type: allProductsAction.ALL_PRODUCTS_FULFILLED
  };
};

export const rejectAllProducts = () => {
  return {
    type: allProductsAction.ALL_PRODUCTS_REJECTED
  };
};

export const refreshAllProducts = (category, merchant, ordering) => {
  return {
    type: allProductsAction.ALL_PRODUCTS_REFRESHED,
    payload: {
      category,
      merchant,
      ordering
    }
  };
};

export const fetchMoreAllProducts = (category, merchant, ordering) => {
  return {
    type: allProductsAction.ALL_PRODUCTS_FETCHED_MORE,
    payload: {
      category,
      merchant,
      ordering
    }
  };
};

export const appendAllProducts = (allProducts, page) => {
  return {
    type: allProductsAction.ALL_PRODUCTS_APPENDED,
    payload: {
      allProducts,
      page
    }
  };
};

export const reachEndAllProducts = () => {
  return {
    type: allProductsAction.ALL_PRODUCTS_REACHED_END
  };
};
