export const share2EarnPromoAction = {
  SHARE2EARN_PROMO_FULFILLED: "SHARE2EARN_PROMO_FULFILLED",
  SHARE2EARN_PROMO_REQUESTED: "SHARE2EARN_PROMO_REQUESTED",
  SHARE2EARN_PROMO_REJECTED: "SHARE2EARN_PROMO_REJECTED",
  SHARE2EARN_PROMO_REFRESHED: "SHARE2EARN_PROMO_REFRESHED",
  SHARE2EARN_PROMO_FETCHED_MORE: "SHARE2EARN_PROMO_FETCHED_MORE",
  SHARE2EARN_PROMO_APPENDED: "SHARE2EARN_PROMO_APPENDED",
  SHARE2EARN_PROMO_REACHED_END: "SHARE2EARN_PROMO_REACHED_END"
};

export const requestShare2EarnPromo = params => {
  return {
    type: share2EarnPromoAction.SHARE2EARN_PROMO_REQUESTED,
    payload: {
      params
    }
  };
};

export const fulfillShare2EarnPromo = share2EarnPromo => {
  return {
    payload: {
      share2EarnPromo
    },
    type: share2EarnPromoAction.SHARE2EARN_PROMO_FULFILLED
  };
};

export const rejectShare2EarnPromo = () => {
  return {
    type: share2EarnPromoAction.SHARE2EARN_PROMO_REJECTED
  };
};

export const refreshShare2EarnPromo = params => {
  return {
    type: share2EarnPromoAction.SHARE2EARN_PROMO_REFRESHED,
    payload: {
      params
    }
  };
};

export const fetchMoreShare2EarnPromo = params => {
  return {
    type: share2EarnPromoAction.SHARE2EARN_PROMO_FETCHED_MORE,
    payload: {
      params
    }
  };
};

export const appendShare2EarnPromo = (share2EarnPromo, page) => {
  return {
    type: share2EarnPromoAction.SHARE2EARN_PROMO_APPENDED,
    payload: {
      share2EarnPromo,
      page
    }
  };
};

export const reachEndShare2EarnPromo = () => {
  return {
    type: share2EarnPromoAction.SHARE2EARN_PROMO_REACHED_END
  };
};
