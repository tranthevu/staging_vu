export const merchantProductsAction = {
  MERCHANT_PRODUCTS_FULFILLED: "MERCHANT_PRODUCTS_FULFILLED",
  MERCHANT_PRODUCTS_REQUESTED: "MERCHANT_PRODUCTS_REQUESTED",
  MERCHANT_PRODUCTS_REJECTED: "MERCHANT_PRODUCTS_REJECTED",
  MERCHANT_PRODUCTS_REFRESHED: "MERCHANT_PRODUCTS_REFRESHED",
  MERCHANT_PRODUCTS_FETCHED_MORE: "MERCHANT_PRODUCTS_FETCHED_MORE",
  MERCHANT_PRODUCTS_APPENDED: "MERCHANT_PRODUCTS_APPENDED",
  MERCHANT_PRODUCTS_REACHED_END: "MERCHANT_PRODUCTS_REACHED_END"
};

export const requestMerchantProducts = params => {
  return {
    type: merchantProductsAction.MERCHANT_PRODUCTS_REQUESTED,
    payload: {
      params
    }
  };
};

export const fulfillMerchantProducts = merchantProducts => {
  return {
    payload: {
      merchantProducts
    },
    type: merchantProductsAction.MERCHANT_PRODUCTS_FULFILLED
  };
};

export const rejectMerchantProducts = () => {
  return {
    type: merchantProductsAction.MERCHANT_PRODUCTS_REJECTED
  };
};

export const refreshMerchantProducts = params => {
  return {
    type: merchantProductsAction.MERCHANT_PRODUCTS_REFRESHED,
    payload: {
      params
    }
  };
};

export const fetchMoreMerchantProducts = params => {
  return {
    type: merchantProductsAction.MERCHANT_PRODUCTS_FETCHED_MORE,
    payload: {
      params
    }
  };
};

export const appendMerchantProducts = (merchantProducts, page) => {
  return {
    type: merchantProductsAction.MERCHANT_PRODUCTS_APPENDED,
    payload: {
      merchantProducts,
      page
    }
  };
};

export const reachEndMerchantProducts = () => {
  return {
    type: merchantProductsAction.MERCHANT_PRODUCTS_REACHED_END
  };
};
