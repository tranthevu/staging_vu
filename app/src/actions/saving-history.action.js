export const savingHistoryAction = {
  SAVING_HISTORY_FULFILLED: "SAVING_HISTORY_FULFILLED",
  SAVING_HISTORY_REQUESTED: "SAVING_HISTORY_REQUESTED",
  SAVING_HISTORY_REJECTED: "SAVING_HISTORY_REJECTED",
  SAVING_HISTORY_REFRESHED: "SAVING_HISTORY_REFRESHED",
  SAVING_HISTORY_FETCHED_MORE: "SAVING_HISTORY_FETCHED_MORE",
  SAVING_HISTORY_APPENDED: "SAVING_HISTORY_APPENDED",
  SAVING_HISTORY_REACHED_END: "SAVING_HISTORY_REACHED_END"
};

export const requestSavingHistory = () => {
  return {
    type: savingHistoryAction.SAVING_HISTORY_REQUESTED
  };
};

export const fulfillSavingHistory = savingHistory => {
  return {
    payload: {
      savingHistory
    },
    type: savingHistoryAction.SAVING_HISTORY_FULFILLED
  };
};

export const rejectSavingHistory = () => {
  return {
    type: savingHistoryAction.SAVING_HISTORY_REJECTED
  };
};

export const refreshSavingHistory = () => {
  return {
    type: savingHistoryAction.SAVING_HISTORY_REFRESHED
  };
};

export const fetchMoreSavingHistory = () => {
  return {
    type: savingHistoryAction.SAVING_HISTORY_FETCHED_MORE
  };
};

export const appendSavingHistory = (savingHistory, page) => {
  return {
    type: savingHistoryAction.SAVING_HISTORY_APPENDED,
    payload: {
      savingHistory,
      page
    }
  };
};

export const reachEndSavingHistory = () => {
  return {
    type: savingHistoryAction.SAVING_HISTORY_REACHED_END
  };
};
