export const usedVouchersAction = {
  USED_VOUCHERS_FULFILLED: "USED_VOUCHERS_FULFILLED",
  USED_VOUCHERS_REQUESTED: "USED_VOUCHERS_REQUESTED",
  USED_VOUCHERS_REJECTED: "USED_VOUCHERS_REJECTED",
  USED_VOUCHERS_REFRESHED: "USED_VOUCHERS_REFRESHED",
  USED_VOUCHERS_FETCHED_MORE: "USED_VOUCHERS_FETCHED_MORE",
  USED_VOUCHERS_APPENDED: "USED_VOUCHERS_APPENDED",
  USED_VOUCHERS_REACHED_END: "USED_VOUCHERS_REACHED_END"
};

export const requestUsedVouchers = () => {
  return {
    type: usedVouchersAction.USED_VOUCHERS_REQUESTED
  };
};

export const fulfillUsedVouchers = usedVouchers => {
  return {
    payload: {
      usedVouchers
    },
    type: usedVouchersAction.USED_VOUCHERS_FULFILLED
  };
};

export const rejectUsedVouchers = () => {
  return {
    type: usedVouchersAction.USED_VOUCHERS_REJECTED
  };
};

export const refreshUsedVouchers = () => {
  return {
    type: usedVouchersAction.USED_VOUCHERS_REFRESHED
  };
};

export const fetchMoreUsedVouchers = () => {
  return {
    type: usedVouchersAction.USED_VOUCHERS_FETCHED_MORE
  };
};

export const appendUsedVouchers = (usedVouchers, page) => {
  return {
    type: usedVouchersAction.USED_VOUCHERS_APPENDED,
    payload: {
      usedVouchers,
      page
    }
  };
};

export const reachEndUsedVouchers = () => {
  return {
    type: usedVouchersAction.USED_VOUCHERS_REACHED_END
  };
};
