export const onlineProductsAction = {
  ONLINE_PRODUCTS_FULFILLED: "ONLINE_PRODUCTS_FULFILLED",
  ONLINE_PRODUCTS_REQUESTED: "ONLINE_PRODUCTS_REQUESTED",
  ONLINE_PRODUCTS_REJECTED: "ONLINE_PRODUCTS_REJECTED",
  ONLINE_PRODUCTS_REFRESHED: "ONLINE_PRODUCTS_REFRESHED",
  ONLINE_PRODUCTS_FETCHED_MORE: "ONLINE_PRODUCTS_FETCHED_MORE",
  ONLINE_PRODUCTS_APPENDED: "ONLINE_PRODUCTS_APPENDED",
  ONLINE_PRODUCTS_REACHED_END: "ONLINE_PRODUCTS_REACHED_END"
};

export const requestOnlineProducts = params => {
  return {
    type: onlineProductsAction.ONLINE_PRODUCTS_REQUESTED,
    payload: {
      params
    }
  };
};

export const fulfillOnlineProducts = onlineProducts => {
  return {
    payload: {
      onlineProducts
    },
    type: onlineProductsAction.ONLINE_PRODUCTS_FULFILLED
  };
};

export const rejectOnlineProducts = () => {
  return {
    type: onlineProductsAction.ONLINE_PRODUCTS_REJECTED
  };
};

export const refreshOnlineProducts = params => {
  return {
    type: onlineProductsAction.ONLINE_PRODUCTS_REFRESHED,
    payload: {
      params
    }
  };
};

export const fetchMoreOnlineProducts = params => {
  return {
    type: onlineProductsAction.ONLINE_PRODUCTS_FETCHED_MORE,
    payload: {
      params
    }
  };
};

export const appendOnlineProducts = (onlineProducts, page) => {
  return {
    type: onlineProductsAction.ONLINE_PRODUCTS_APPENDED,
    payload: {
      onlineProducts,
      page
    }
  };
};

export const reachEndOnlineProducts = () => {
  return {
    type: onlineProductsAction.ONLINE_PRODUCTS_REACHED_END
  };
};
