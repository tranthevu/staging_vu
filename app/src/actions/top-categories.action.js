export const topCategoriesAction = {
  TOP_CATEGORIES_LOADED: "TOP_CATEGORIES_LOADED",
  TOP_CATEGORIES_REQUESTED: "TOP_CATEGORIES_REQUESTED",
  TOP_CATEGORIES_REQUEST_FAILED: "TOP_CATEGORIES_REQUEST_FAILED"
};

export const loadedTopCategories = topCategories => {
  return {
    type: topCategoriesAction.TOP_CATEGORIES_LOADED,
    payload: {
      topCategories
    }
  };
};

export const requestedTopCategories = () => {
  return {
    type: topCategoriesAction.TOP_CATEGORIES_REQUESTED
  };
};

export const requestTopCategoriesFailed = () => {
  return {
    type: topCategoriesAction.TOP_CATEGORIES_REQUEST_FAILED
  };
};
