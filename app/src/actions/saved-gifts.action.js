export const savedGiftsAction = {
  SAVED_GIFTS_FULFILLED: "SAVED_GIFTS_FULFILLED",
  SAVED_GIFTS_REQUESTED: "SAVED_GIFTS_REQUESTED",
  SAVED_GIFTS_REJECTED: "SAVED_GIFTS_REJECTED",
  SAVED_GIFTS_REFRESHED: "SAVED_GIFTS_REFRESHED",
  SAVED_GIFTS_FETCHED_MORE: "SAVED_GIFTS_FETCHED_MORE",
  SAVED_GIFTS_APPENDED: "SAVED_GIFTS_APPENDED",
  SAVED_GIFTS_REACHED_END: "SAVED_GIFTS_REACHED_END"
};

export const requestSavedGifts = status => {
  return {
    type: savedGiftsAction.SAVED_GIFTS_REQUESTED,
    payload: {
      status
    }
  };
};

export const fulfillSavedGifts = savedGifts => {
  return {
    payload: {
      savedGifts
    },
    type: savedGiftsAction.SAVED_GIFTS_FULFILLED
  };
};

export const rejectSavedGifts = () => {
  return {
    type: savedGiftsAction.SAVED_GIFTS_REJECTED
  };
};

export const refreshSavedGifts = status => {
  return {
    type: savedGiftsAction.SAVED_GIFTS_REFRESHED,
    payload: {
      status
    }
  };
};

export const fetchMoreSavedGifts = status => {
  return {
    type: savedGiftsAction.SAVED_GIFTS_FETCHED_MORE,
    payload: {
      status
    }
  };
};

export const appendSavedGifts = (savedGifts, page) => {
  return {
    type: savedGiftsAction.SAVED_GIFTS_APPENDED,
    payload: {
      savedGifts,
      page
    }
  };
};

export const reachEndSavedGifts = () => {
  return {
    type: savedGiftsAction.SAVED_GIFTS_REACHED_END
  };
};
