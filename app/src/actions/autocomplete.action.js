export const autocompleteAction = {
  AUTOCOMPLETE_LOADED: "AUTOCOMPLETE_LOADED",
  AUTOCOMPLETE_REQUESTED: "AUTOCOMPLETE_REQUESTED",
  AUTOCOMPLETE_REQUEST_FAILED: "AUTOCOMPLETE_REQUEST_FAILED"
};

export const loadedAutocomplete = autocomplete => {
  return {
    type: autocompleteAction.AUTOCOMPLETE_LOADED,
    payload: {
      autocomplete
    }
  };
};

export const requestedAutocomplete = (keyword, category) => {
  return {
    type: autocompleteAction.AUTOCOMPLETE_REQUESTED,
    payload: {
      keyword,
      category
    }
  };
};

export const requestAutocompleteFailed = () => {
  return {
    type: autocompleteAction.AUTOCOMPLETE_REQUEST_FAILED
  };
};
