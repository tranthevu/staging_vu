export const redemptionHistoryAction = {
  REDEMPTION_HISTORY_FULFILLED: "REDEMPTION_HISTORY_FULFILLED",
  REDEMPTION_HISTORY_REQUESTED: "REDEMPTION_HISTORY_REQUESTED",
  REDEMPTION_HISTORY_REJECTED: "REDEMPTION_HISTORY_REJECTED",
  REDEMPTION_HISTORY_REFRESHED: "REDEMPTION_HISTORY_REFRESHED",
  REDEMPTION_HISTORY_FETCHED_MORE: "REDEMPTION_HISTORY_FETCHED_MORE",
  REDEMPTION_HISTORY_APPENDED: "REDEMPTION_HISTORY_APPENDED",
  REDEMPTION_HISTORY_REACHED_END: "REDEMPTION_HISTORY_REACHED_END"
};

export const requestRedemptionHistory = () => {
  return {
    type: redemptionHistoryAction.REDEMPTION_HISTORY_REQUESTED
  };
};

export const fulfillRedemptionHistory = redemptionHistory => {
  return {
    payload: {
      redemptionHistory
    },
    type: redemptionHistoryAction.REDEMPTION_HISTORY_FULFILLED
  };
};

export const rejectRedemptionHistory = () => {
  return {
    type: redemptionHistoryAction.REDEMPTION_HISTORY_REJECTED
  };
};

export const refreshRedemptionHistory = () => {
  return {
    type: redemptionHistoryAction.REDEMPTION_HISTORY_REFRESHED
  };
};

export const fetchMoreRedemptionHistory = () => {
  return {
    type: redemptionHistoryAction.REDEMPTION_HISTORY_FETCHED_MORE
  };
};

export const appendRedemptionHistory = (redemptionHistory, page) => {
  return {
    type: redemptionHistoryAction.REDEMPTION_HISTORY_APPENDED,
    payload: {
      redemptionHistory,
      page
    }
  };
};

export const reachEndRedemptionHistory = () => {
  return {
    type: redemptionHistoryAction.REDEMPTION_HISTORY_REACHED_END
  };
};
