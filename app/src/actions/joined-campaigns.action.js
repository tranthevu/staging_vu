export const joinedCampaignsAction = {
  JOINED_CAMPAIGNS_FULFILLED: "JOINED_CAMPAIGNS_FULFILLED",
  JOINED_CAMPAIGNS_REQUESTED: "JOINED_CAMPAIGNS_REQUESTED",
  JOINED_CAMPAIGNS_REJECTED: "JOINED_CAMPAIGNS_REJECTED",
  JOINED_CAMPAIGNS_REFRESHED: "JOINED_CAMPAIGNS_REFRESHED",
  JOINED_CAMPAIGNS_FETCHED_MORE: "JOINED_CAMPAIGNS_FETCHED_MORE",
  JOINED_CAMPAIGNS_APPENDED: "JOINED_CAMPAIGNS_APPENDED",
  JOINED_CAMPAIGNS_REACHED_END: "JOINED_CAMPAIGNS_REACHED_END"
};

export const requestJoinedCampaigns = params => {
  return {
    type: joinedCampaignsAction.JOINED_CAMPAIGNS_REQUESTED,
    payload: {
      params
    }
  };
};

export const fulfillJoinedCampaigns = joinedCampaigns => {
  return {
    payload: {
      joinedCampaigns
    },
    type: joinedCampaignsAction.JOINED_CAMPAIGNS_FULFILLED
  };
};

export const rejectJoinedCampaigns = () => {
  return {
    type: joinedCampaignsAction.JOINED_CAMPAIGNS_REJECTED
  };
};

export const refreshJoinedCampaigns = params => {
  return {
    type: joinedCampaignsAction.JOINED_CAMPAIGNS_REFRESHED,
    payload: {
      params
    }
  };
};

export const fetchMoreJoinedCampaigns = params => {
  return {
    type: joinedCampaignsAction.JOINED_CAMPAIGNS_FETCHED_MORE,
    payload: {
      params
    }
  };
};

export const appendJoinedCampaigns = (joinedCampaigns, page) => {
  return {
    type: joinedCampaignsAction.JOINED_CAMPAIGNS_APPENDED,
    payload: {
      joinedCampaigns,
      page
    }
  };
};

export const reachEndJoinedCampaigns = () => {
  return {
    type: joinedCampaignsAction.JOINED_CAMPAIGNS_REACHED_END
  };
};
