import { GET_PRODUCTS_NEAR_BY } from "./actionTypes";

export const searchProductsAction = {
  SEARCH_PRODUCTS_FULFILLED: "SEARCH_PRODUCTS_FULFILLED",
  SEARCH_PRODUCTS_REQUESTED: "SEARCH_PRODUCTS_REQUESTED",
  SEARCH_PRODUCTS_REJECTED: "SEARCH_PRODUCTS_REJECTED",
  SEARCH_PRODUCTS_REFRESHED: "SEARCH_PRODUCTS_REFRESHED",
  SEARCH_PRODUCTS_FETCHED_MORE: "SEARCH_PRODUCTS_FETCHED_MORE",
  SEARCH_PRODUCTS_APPENDED: "SEARCH_PRODUCTS_APPENDED",
  SEARCH_PRODUCTS_REACHED_END: "SEARCH_PRODUCTS_REACHED_END"
};

export const requestSearchProducts = params => {
  return {
    type: searchProductsAction.SEARCH_PRODUCTS_REQUESTED,
    payload: {
      params
    }
  };
};

export const fulfillSearchProducts = (searchProducts, count) => {
  return {
    payload: {
      searchProducts,
      count
    },
    type: searchProductsAction.SEARCH_PRODUCTS_FULFILLED
  };
};

export const rejectSearchProducts = () => {
  return {
    type: searchProductsAction.SEARCH_PRODUCTS_REJECTED
  };
};

export const refreshSearchProducts = params => {
  return {
    type: searchProductsAction.SEARCH_PRODUCTS_REFRESHED,
    payload: {
      params
    }
  };
};

export const fetchMoreSearchProducts = params => {
  return {
    type: searchProductsAction.SEARCH_PRODUCTS_FETCHED_MORE,
    payload: {
      params
    }
  };
};

export const appendSearchProducts = (searchProducts, page) => {
  return {
    type: searchProductsAction.SEARCH_PRODUCTS_APPENDED,
    payload: {
      searchProducts,
      page
    }
  };
};

export const reachEndSearchProducts = () => {
  return {
    type: searchProductsAction.SEARCH_PRODUCTS_REACHED_END
  };
};

export const getProductNearBySubmit = data => {
  return {
    type: GET_PRODUCTS_NEAR_BY.REQUEST,
    payload: data
  };
};

export const getProductNearBySuccess = data => {
  return {
    type: GET_PRODUCTS_NEAR_BY.SUCCESS,
    payload: data
  };
};

export const clearProductsNearBy = () => {
  return {
    type: GET_PRODUCTS_NEAR_BY.CLEAR
  };
};
