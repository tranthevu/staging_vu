export const hotProductsAction = {
  HOT_PRODUCTS_FULFILLED: "HOT_PRODUCTS_FULFILLED",
  HOT_PRODUCTS_REQUESTED: "HOT_PRODUCTS_REQUESTED",
  HOT_PRODUCTS_REJECTED: "HOT_PRODUCTS_REJECTED",
  HOT_PRODUCTS_REFRESHED: "HOT_PRODUCTS_REFRESHED",
  HOT_PRODUCTS_FETCHED_MORE: "HOT_PRODUCTS_FETCHED_MORE",
  HOT_PRODUCTS_APPENDED: "HOT_PRODUCTS_APPENDED",
  HOT_PRODUCTS_REACHED_END: "HOT_PRODUCTS_REACHED_END"
};

export const requestHotProducts = (category, merchant, ordering) => {
  return {
    type: hotProductsAction.HOT_PRODUCTS_REQUESTED,
    payload: {
      category,
      merchant,
      ordering
    }
  };
};

export const fulfillHotProducts = hotProducts => {
  return {
    payload: {
      hotProducts
    },
    type: hotProductsAction.HOT_PRODUCTS_FULFILLED
  };
};

export const rejectHotProducts = () => {
  return {
    type: hotProductsAction.HOT_PRODUCTS_REJECTED
  };
};

export const refreshHotProducts = (category, merchant, ordering) => {
  return {
    type: hotProductsAction.HOT_PRODUCTS_REFRESHED,
    payload: {
      category,
      merchant,
      ordering
    }
  };
};

export const fetchMoreHotProducts = (category, merchant, ordering) => {
  return {
    type: hotProductsAction.HOT_PRODUCTS_FETCHED_MORE,
    payload: {
      category,
      merchant,
      ordering
    }
  };
};

export const appendHotProducts = (hotProducts, page) => {
  return {
    type: hotProductsAction.HOT_PRODUCTS_APPENDED,
    payload: {
      hotProducts,
      page
    }
  };
};

export const reachEndHotProducts = () => {
  return {
    type: hotProductsAction.HOT_PRODUCTS_REACHED_END
  };
};
