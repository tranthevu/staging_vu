export const recentProductsAction = {
  RECENT_PRODUCTS_FULFILLED: "RECENT_PRODUCTS_FULFILLED",
  RECENT_PRODUCTS_REQUESTED: "RECENT_PRODUCTS_REQUESTED",
  RECENT_PRODUCTS_REJECTED: "RECENT_PRODUCTS_REJECTED",
  RECENT_PRODUCTS_REFRESHED: "RECENT_PRODUCTS_REFRESHED",
  RECENT_PRODUCTS_FETCHED_MORE: "RECENT_PRODUCTS_FETCHED_MORE",
  RECENT_PRODUCTS_APPENDED: "RECENT_PRODUCTS_APPENDED",
  RECENT_PRODUCTS_REACHED_END: "RECENT_PRODUCTS_REACHED_END"
};

export const requestRecentProducts = (category, merchant, ordering) => {
  return {
    type: recentProductsAction.RECENT_PRODUCTS_REQUESTED,
    payload: {
      category,
      merchant,
      ordering
    }
  };
};

export const fulfillRecentProducts = recentProducts => {
  return {
    payload: {
      recentProducts
    },
    type: recentProductsAction.RECENT_PRODUCTS_FULFILLED
  };
};

export const rejectRecentProducts = () => {
  return {
    type: recentProductsAction.RECENT_PRODUCTS_REJECTED
  };
};

export const refreshRecentProducts = (category, merchant, ordering) => {
  return {
    type: recentProductsAction.RECENT_PRODUCTS_REFRESHED,
    payload: {
      category,
      merchant,
      ordering
    }
  };
};

export const fetchMoreRecentProducts = (category, merchant, ordering) => {
  return {
    type: recentProductsAction.RECENT_PRODUCTS_FETCHED_MORE,
    payload: {
      category,
      merchant,
      ordering
    }
  };
};

export const appendRecentProducts = (recentProducts, page) => {
  return {
    type: recentProductsAction.RECENT_PRODUCTS_APPENDED,
    payload: {
      recentProducts,
      page
    }
  };
};

export const reachEndRecentProducts = () => {
  return {
    type: recentProductsAction.RECENT_PRODUCTS_REACHED_END
  };
};
