export const unusedVouchersAction = {
  UNUSED_VOUCHERS_FULFILLED: "UNUSED_VOUCHERS_FULFILLED",
  UNUSED_VOUCHERS_REQUESTED: "UNUSED_VOUCHERS_REQUESTED",
  UNUSED_VOUCHERS_REJECTED: "UNUSED_VOUCHERS_REJECTED",
  UNUSED_VOUCHERS_REFRESHED: "UNUSED_VOUCHERS_REFRESHED",
  UNUSED_VOUCHERS_FETCHED_MORE: "UNUSED_VOUCHERS_FETCHED_MORE",
  UNUSED_VOUCHERS_APPENDED: "UNUSED_VOUCHERS_APPENDED",
  UNUSED_VOUCHERS_REACHED_END: "UNUSED_VOUCHERS_REACHED_END"
};

export const requestUnusedVouchers = () => {
  return {
    type: unusedVouchersAction.UNUSED_VOUCHERS_REQUESTED
  };
};

export const fulfillUnusedVouchers = unusedVouchers => {
  return {
    payload: {
      unusedVouchers
    },
    type: unusedVouchersAction.UNUSED_VOUCHERS_FULFILLED
  };
};

export const rejectUnusedVouchers = () => {
  return {
    type: unusedVouchersAction.UNUSED_VOUCHERS_REJECTED
  };
};

export const refreshUnusedVouchers = () => {
  return {
    type: unusedVouchersAction.UNUSED_VOUCHERS_REFRESHED
  };
};

export const fetchMoreUnusedVouchers = () => {
  return {
    type: unusedVouchersAction.UNUSED_VOUCHERS_FETCHED_MORE
  };
};

export const appendUnusedVouchers = (unusedVouchers, page) => {
  return {
    type: unusedVouchersAction.UNUSED_VOUCHERS_APPENDED,
    payload: {
      unusedVouchers,
      page
    }
  };
};

export const reachEndUnusedVouchers = () => {
  return {
    type: unusedVouchersAction.UNUSED_VOUCHERS_REACHED_END
  };
};
