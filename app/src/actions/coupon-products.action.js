export const couponProductsAction = {
  COUPON_PRODUCTS_FULFILLED: "COUPON_PRODUCTS_FULFILLED",
  COUPON_PRODUCTS_REQUESTED: "COUPON_PRODUCTS_REQUESTED",
  COUPON_PRODUCTS_REJECTED: "COUPON_PRODUCTS_REJECTED",
  COUPON_PRODUCTS_REFRESHED: "COUPON_PRODUCTS_REFRESHED",
  COUPON_PRODUCTS_FETCHED_MORE: "COUPON_PRODUCTS_FETCHED_MORE",
  COUPON_PRODUCTS_APPENDED: "COUPON_PRODUCTS_APPENDED",
  COUPON_PRODUCTS_REACHED_END: "COUPON_PRODUCTS_REACHED_END"
};

export const requestCouponProducts = params => {
  return {
    type: couponProductsAction.COUPON_PRODUCTS_REQUESTED,
    payload: {
      params
    }
  };
};

export const fulfillCouponProducts = couponProducts => {
  return {
    payload: {
      couponProducts
    },
    type: couponProductsAction.COUPON_PRODUCTS_FULFILLED
  };
};

export const rejectCouponProducts = () => {
  return {
    type: couponProductsAction.COUPON_PRODUCTS_REJECTED
  };
};

export const refreshCouponProducts = params => {
  return {
    type: couponProductsAction.COUPON_PRODUCTS_REFRESHED,
    payload: {
      params
    }
  };
};

export const fetchMoreCouponProducts = params => {
  return {
    type: couponProductsAction.COUPON_PRODUCTS_FETCHED_MORE,
    payload: {
      params
    }
  };
};

export const appendCouponProducts = (couponProducts, page) => {
  return {
    type: couponProductsAction.COUPON_PRODUCTS_APPENDED,
    payload: {
      couponProducts,
      page
    }
  };
};

export const reachEndCouponProducts = () => {
  return {
    type: couponProductsAction.COUPON_PRODUCTS_REACHED_END
  };
};
