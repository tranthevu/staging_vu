export const partnerProductsAction = {
  PARTNER_PRODUCTS_FULFILLED: "PARTNER_PRODUCTS_FULFILLED",
  PARTNER_PRODUCTS_REQUESTED: "PARTNER_PRODUCTS_REQUESTED",
  PARTNER_PRODUCTS_REJECTED: "PARTNER_PRODUCTS_REJECTED",
  PARTNER_PRODUCTS_REFRESHED: "PARTNER_PRODUCTS_REFRESHED",
  PARTNER_PRODUCTS_FETCHED_MORE: "PARTNER_PRODUCTS_FETCHED_MORE",
  PARTNER_PRODUCTS_APPENDED: "PARTNER_PRODUCTS_APPENDED",
  PARTNER_PRODUCTS_REACHED_END: "PARTNER_PRODUCTS_REACHED_END"
};

export const requestPartnerProducts = params => {
  return {
    type: partnerProductsAction.PARTNER_PRODUCTS_REQUESTED,
    payload: {
      params
    }
  };
};

export const fulfillPartnerProducts = partnerProducts => {
  return {
    payload: {
      partnerProducts
    },
    type: partnerProductsAction.PARTNER_PRODUCTS_FULFILLED
  };
};

export const rejectPartnerProducts = () => {
  return {
    type: partnerProductsAction.PARTNER_PRODUCTS_REJECTED
  };
};

export const refreshPartnerProducts = params => {
  return {
    type: partnerProductsAction.PARTNER_PRODUCTS_REFRESHED,
    payload: {
      params
    }
  };
};

export const fetchMorePartnerProducts = params => {
  return {
    type: partnerProductsAction.PARTNER_PRODUCTS_FETCHED_MORE,
    payload: {
      params
    }
  };
};

export const appendPartnerProducts = (partnerProducts, page) => {
  return {
    type: partnerProductsAction.PARTNER_PRODUCTS_APPENDED,
    payload: {
      partnerProducts,
      page
    }
  };
};

export const reachEndPartnerProducts = () => {
  return {
    type: partnerProductsAction.PARTNER_PRODUCTS_REACHED_END
  };
};
