export const locationProductsAction = {
  LOCATION_PRODUCTS_FULFILLED: "LOCATION_PRODUCTS_FULFILLED",
  LOCATION_PRODUCTS_REQUESTED: "LOCATION_PRODUCTS_REQUESTED",
  LOCATION_PRODUCTS_REJECTED: "LOCATION_PRODUCTS_REJECTED",
  LOCATION_PRODUCTS_REFRESHED: "LOCATION_PRODUCTS_REFRESHED",
  LOCATION_PRODUCTS_FETCHED_MORE: "LOCATION_PRODUCTS_FETCHED_MORE",
  LOCATION_PRODUCTS_APPENDED: "LOCATION_PRODUCTS_APPENDED",
  LOCATION_PRODUCTS_REACHED_END: "LOCATION_PRODUCTS_REACHED_END"
};

export const requestLocationProducts = (
  location,
  category,
  merchant,
  ordering
) => {
  return {
    type: locationProductsAction.LOCATION_PRODUCTS_REQUESTED,
    payload: {
      location,
      category,
      merchant,
      ordering
    }
  };
};

export const fulfillLocationProducts = locationProducts => {
  return {
    payload: {
      locationProducts
    },
    type: locationProductsAction.LOCATION_PRODUCTS_FULFILLED
  };
};

export const rejectLocationProducts = () => {
  return {
    type: locationProductsAction.LOCATION_PRODUCTS_REJECTED
  };
};

export const refreshLocationProducts = (
  location,
  category,
  merchant,
  ordering
) => {
  return {
    type: locationProductsAction.LOCATION_PRODUCTS_REFRESHED,
    payload: {
      location,
      category,
      merchant,
      ordering
    }
  };
};

export const fetchMoreLocationProducts = (
  location,
  category,
  merchant,
  ordering
) => {
  return {
    type: locationProductsAction.LOCATION_PRODUCTS_FETCHED_MORE,
    payload: {
      location,
      category,
      merchant,
      ordering
    }
  };
};

export const appendLocationProducts = (locationProducts, page) => {
  return {
    type: locationProductsAction.LOCATION_PRODUCTS_APPENDED,
    payload: {
      locationProducts,
      page
    }
  };
};

export const reachEndLocationProducts = () => {
  return {
    type: locationProductsAction.LOCATION_PRODUCTS_REACHED_END
  };
};
