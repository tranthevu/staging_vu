import { asyncTypes } from "../helpers/action.helper";

export const bannersAction = {
  BANNERS_LOADED: "BANNERS_LOADED",
  BANNERS_PROMO_LOADED: "BANNERS_PROMO_LOADED",
  BANNERS_ECOUPON_LOADED: "BANNERS_ECOUPON_LOADED",
  BANNERS_REQUESTED: "BANNERS_REQUESTED",
  BANNERS_SHARE_TO_EARN_PROMO: asyncTypes("BANNERS_SHARE_TO_EARN_PROMO"),
  BANNERS_REQUEST_FAILED: "BANNERS_REQUEST_FAILED"
};

export const loadedBanners = banners => {
  return {
    type: bannersAction.BANNERS_LOADED,
    payload: {
      banners
    }
  };
};
export const loadedPromoBanners = bannerPromos => {
  return {
    type: bannersAction.BANNERS_PROMO_LOADED,
    payload: {
      bannerPromos
    }
  };
};
export const loadedEcouponBanners = bannerEcoupon => {
  return {
    type: bannersAction.BANNERS_ECOUPON_LOADED,
    payload: {
      bannerEcoupon
    }
  };
};

export const requestedBanners = () => {
  return {
    type: bannersAction.BANNERS_REQUESTED
  };
};

export const requestBannersFailed = () => {
  return {
    type: bannersAction.BANNERS_REQUEST_FAILED
  };
};

export const getShareToEarnBannersSubmit = () => ({
  type: bannersAction.BANNERS_SHARE_TO_EARN_PROMO.REQUEST
});

export const getShareToEarnBannersSuccess = payload => ({
  type: bannersAction.BANNERS_SHARE_TO_EARN_PROMO.SUCCESS,
  payload
});
