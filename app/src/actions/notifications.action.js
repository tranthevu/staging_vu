export const notificationsAction = {
  NOTIFICATIONS_FULFILLED: "NOTIFICATIONS_FULFILLED",
  NOTIFICATIONS_REQUESTED: "NOTIFICATIONS_REQUESTED",
  NOTIFICATIONS_REJECTED: "NOTIFICATIONS_REJECTED",
  NOTIFICATIONS_REFRESHED: "NOTIFICATIONS_REFRESHED",
  NOTIFICATIONS_FETCHED_MORE: "NOTIFICATIONS_FETCHED_MORE",
  NOTIFICATIONS_APPENDED: "NOTIFICATIONS_APPENDED",
  NOTIFICATIONS_REACHED_END: "NOTIFICATIONS_REACHED_END",
  NOTIFICATIONS_COUNTED: "NOTIFICATIONS_COUNTED",
  NOTIFICATIONS_REQUESTED_COUNT: "NOTIFICATIONS_REQUESTED_COUNT",
  NOTIFICATIONS_READ: "NOTIFICATIONS_READ"
};

export const requestCountNotifications = () => {
  return {
    type: notificationsAction.NOTIFICATIONS_REQUESTED_COUNT
  };
};

export const countedNotifications = count => {
  return {
    type: notificationsAction.NOTIFICATIONS_COUNTED,
    payload: {
      count
    }
  };
};

export const requestNotifications = () => {
  return {
    type: notificationsAction.NOTIFICATIONS_REQUESTED
  };
};

export const fulfillNotifications = notifications => {
  return {
    payload: {
      notifications
    },
    type: notificationsAction.NOTIFICATIONS_FULFILLED
  };
};

export const rejectNotifications = () => {
  return {
    type: notificationsAction.NOTIFICATIONS_REJECTED
  };
};

export const refreshNotifications = () => {
  return {
    type: notificationsAction.NOTIFICATIONS_REFRESHED
  };
};

export const fetchMoreNotifications = () => {
  return {
    type: notificationsAction.NOTIFICATIONS_FETCHED_MORE
  };
};

export const appendNotifications = (notifications, page) => {
  return {
    type: notificationsAction.NOTIFICATIONS_APPENDED,
    payload: {
      notifications,
      page
    }
  };
};

export const reachEndNotifications = () => {
  return {
    type: notificationsAction.NOTIFICATIONS_REACHED_END
  };
};

export const readNotification = id => {
  return {
    type: notificationsAction.NOTIFICATIONS_READ,
    payload: {
      id
    }
  };
};
