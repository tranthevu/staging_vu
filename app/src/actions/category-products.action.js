export const categoryProductsAction = {
  CATEGORY_PRODUCTS_FULFILLED: "CATEGORY_PRODUCTS_FULFILLED",
  CATEGORY_PRODUCTS_REQUESTED: "CATEGORY_PRODUCTS_REQUESTED",
  CATEGORY_PRODUCTS_REJECTED: "CATEGORY_PRODUCTS_REJECTED",
  CATEGORY_PRODUCTS_RESETTED: "CATEGORY_PRODUCTS_RESETTED",
  CATEGORY_PRODUCTS_REFRESHED: "CATEGORY_PRODUCTS_REFRESHED",
  CATEGORY_PRODUCTS_FETCHED_MORE: "CATEGORY_PRODUCTS_FETCHED_MORE",
  CATEGORY_PRODUCTS_APPENDED: "CATEGORY_PRODUCTS_APPENDED",
  CATEGORY_PRODUCTS_REACHED_END: "CATEGORY_PRODUCTS_REACHED_END"
};

export const requestCategoryProducts = (
  category,
  merchant,
  ordering,
  type,
  isExclusive
) => {
  return {
    type: categoryProductsAction.CATEGORY_PRODUCTS_REQUESTED,
    payload: {
      category,
      merchant,
      ordering,
      type,
      field: isExclusive ? "exclusive" : undefined
    }
  };
};

export const fulfillCategoryProducts = categoryProducts => {
  return {
    payload: {
      categoryProducts
    },
    type: categoryProductsAction.CATEGORY_PRODUCTS_FULFILLED
  };
};

export const rejectCategoryProducts = () => {
  return {
    type: categoryProductsAction.CATEGORY_PRODUCTS_REJECTED
  };
};

export const resetCategoryProducts = () => {
  return {
    type: categoryProductsAction.CATEGORY_PRODUCTS_RESETTED
  };
};

export const refreshCategoryProducts = (
  category,
  merchant,
  ordering,
  type,
  isExclusive
) => {
  return {
    type: categoryProductsAction.CATEGORY_PRODUCTS_REFRESHED,
    payload: {
      category,
      merchant,
      ordering,
      type,
      field: isExclusive ? "exclusive" : undefined
    }
  };
};

export const fetchMoreCategoryProducts = (
  category,
  merchant,
  ordering,
  type,
  isExclusive
) => {
  return {
    type: categoryProductsAction.CATEGORY_PRODUCTS_FETCHED_MORE,
    payload: {
      category,
      merchant,
      ordering,
      type,
      field: isExclusive ? "exclusive" : undefined
    }
  };
};

export const appendCategoryProducts = (categoryProducts, page) => {
  return {
    type: categoryProductsAction.CATEGORY_PRODUCTS_APPENDED,
    payload: {
      categoryProducts,
      page
    }
  };
};

export const reachEndCategoryProducts = () => {
  return {
    type: categoryProductsAction.CATEGORY_PRODUCTS_REACHED_END
  };
};
