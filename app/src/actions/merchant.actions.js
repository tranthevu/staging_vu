import {
  SEARCH_LIST_MERCHANT_BY_ADDRESS_ASYNC,
  GET_TOP_MERCHANT_BY_ADDRESS_ASYNC
} from "./actionTypes";

export const searchListMerchantByAddressSubmit = payload => ({
  type: SEARCH_LIST_MERCHANT_BY_ADDRESS_ASYNC.REQUEST,
  payload
});

export const searchListMerchantByAddressSuccess = payload => ({
  type: SEARCH_LIST_MERCHANT_BY_ADDRESS_ASYNC.SUCCESS,
  payload
});

export const getTopMerchantByAddressSubmit = payload => ({
  type: GET_TOP_MERCHANT_BY_ADDRESS_ASYNC.REQUEST,
  payload
});

export const getTopMerchantByAddressSuccess = payload => ({
  type: GET_TOP_MERCHANT_BY_ADDRESS_ASYNC.SUCCESS,
  payload
});

export const clearTopMerchantByAddress = () => ({
  type: GET_TOP_MERCHANT_BY_ADDRESS_ASYNC.CLEAR
});
