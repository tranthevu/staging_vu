import React, { Component } from "react";
import {
  View,
  StyleSheet,
  StatusBar,
  Platform,
  BackHandler,
  SafeAreaView,
  PushNotificationIOS,
  Image,
  ProgressBarAndroid,
  ProgressViewIOS
} from "react-native";
import { addNavigationHelpers, NavigationActions } from "react-navigation";
import { connect } from "react-redux";
import firebase from "react-native-firebase";
import Dialog, { FadeAnimation } from "react-native-popup-dialog";
import lodash from "lodash";
import {
  createReduxBoundAddListener,
  createReactNavigationReduxMiddleware
} from "react-navigation-redux-helpers";
import uuid from "uuid";
import { GoogleSignin } from "react-native-google-signin";
import { LoginManager } from "react-native-fbsdk";
import BadgeAndroid from "react-native-android-badge";
import CodePush from "react-native-code-push";
import { AppNavigator } from "./navigators/app.navigator";
import {appColor, notificationAction, event, font} from "./constants/app.constant";
import Api from "./api/api";
import NotificationPopup from "./components/common/notification-popup";
import {moderateScale, sizeWidth} from "./helpers/size.helper";
import {resetPage} from "./actions/nav.action";

import {
  navigateNotification,
  updateDidNavigate,
  navigateToDetail
} from "./helpers/notification-navigate.helper";
import {
  readNotification,
  requestCountNotifications,
  countedNotifications
} from "./actions/notifications.action";
import { checkApiStatus, logDebug } from "./helpers/app.helper";
import EventRegister from "./helpers/event-register.helper";
import AuthHelper from "./helpers/auth.helper";
import {
  removeClientCas,
  saveNotShowAgainNoti,
  removeUserInfoForSentry
} from "./helpers/storage.helper";
import { loadedProfile, resetProfile, requestedProfile } from "./actions/profile.action";
import { appConfig } from "./config/app.config";
import { getIsLoading } from "./selector/loadingSelector";
import OverlayLoading from "./components/OverlayLoading";
import NetworkErrorPopup from "./components/common/network-error-popup";
import Text from "./components/common/text";

const middleware = createReactNavigationReduxMiddleware("root", state => state.nav);
const addListener = createReduxBoundAddListener("root");

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notificationVisible: false,
      notification: null,
      networkErrorVisible: false,
      updateAvailable: false,
      progressStatus: 0,
      receivedMb: "0",
      totalMb:"0",
      version: "",
    };
  }

  popupdisplayTimeout = {};

  popupexpiredTimeout = {};

  shouldCloseApp = () => {
    return this.props.nav.index === 0;
  };

  handleBackPress = () => {
    if (this.shouldCloseApp()) {
      return false;
    }
    this.props.dispatch(NavigationActions.back({}));
    return true;
  };

  showNotification = notification => {
    const notif = new firebase.notifications.Notification()
      .setNotificationId(uuid.v4())
      .setTitle(notification.title)
      .setBody(notification.body)
      .setData(notification.data);
    notif.android
      .setChannelId("yolla-network")
      .android.setAutoCancel(true)
      .android.setSmallIcon("ic_stat_yolla");
    firebase.notifications().displayNotification(notif);
  };

  createAndroidChannel = () => {
    if (Platform.OS === "android") {
      const channel = new firebase.notifications.Android.Channel(
        "yolla-network",
        "Yolla Network",
        firebase.notifications.Android.Importance.Max
      ).setDescription("Yolla Network channel");

      firebase.notifications().android.createChannel(channel);
    }
  };

  showPopup = notification => {
    this.setState({
      notificationVisible: true,
      notification: {
        body: lodash.get(notification, "data.content"),
        title: lodash.get(notification, "title"),
        data: lodash.get(notification, "data")
      }
    });
    const popupdisplay = parseInt(lodash.get(notification, "data.popupdisplay", 0), 10);
    const popupid = lodash.get(notification, "data.popupid");
    if (popupdisplay > 0) {
      clearTimeout(this.popupdisplayTimeout[popupid]);
      this.popupdisplayTimeout[popupid] = setTimeout(
        this.hideNotificationPopup,
        popupdisplay * 1000
      );
    }
  };

  isHomeShowing = () => {
    const { nav } = this.props;
    const main = nav.routes[nav.index];
    return main.routeName === "Main" && main.index === 0;
  };

  handlePopupType = notification => {
    const popuptype = lodash.get(notification, "data.popuptype");
    if (popuptype == "1") return;
    if (popuptype == "2") {
      this.showPopup(notification);
    }
    const homeShowing = this.isHomeShowing();
    if (popuptype == "3" && homeShowing) {
      this.showPopup(notification);
    }
  };

  listenFirebaseMessage = async () => {
    this.notificationListener = firebase
      .notifications()
      .onNotification(async notification => {
        /**
         * Schema of a notification https://rnfirebase.io/docs/v5.x.x/notifications/reference/Notification
         * {
         *    data: Object,
         *    title: string,
         *    body: string
         * }
         */
        logDebug("onNotification", notification);
        const type = lodash.get(notification, "data.type");
        if (type === "Notify") {
          const action = lodash.get(notification, "data.action");
          if (action === notificationAction.myCodeDetail) {
            const data = lodash.get(notification, "data");
            this.sendCodeUsedEvent(data);
          }
          this.showNotification(notification);
        } else {
          this.handlePopupType(notification);
        }
        const isAuthenticated = await AuthHelper.isAuthenticated();
        if (isAuthenticated) this.props.requestCountNotifications();
      });
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const { action } = notificationOpen;
        const { notification } = notificationOpen;
        navigateNotification(this.props.dispatch, notification);
      });
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
      const { notification } = notificationOpen;
      const action = lodash.get(notification, "data.action");
      if (
        action !== notificationAction.openbrowser &&
        action !== notificationAction.popup
      )
        updateDidNavigate(true);
      navigateNotification(this.props.dispatch, notification, {
        isInitial: true
      });
    }
  };

  sendCodeUsedEvent = data => {
    logDebug("main.js prepare to emit event: ", "event.codeUsed", data);
    EventRegister.emit(event.codeUsed, data);
    this.props.requestedProfile();
  };

  initFirebase = async () => {
    const fcmToken = await firebase.messaging().getToken();
    const isAuthenticated = await AuthHelper.isAuthenticated();
    if (isAuthenticated) {
      await Api.registerTokenId(fcmToken);
    } else {
      await Api.registerTokenIdAnonymous(fcmToken);
    }
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.listenFirebaseMessage();
    } else {
      await firebase.messaging().requestPermission();
      this.listenFirebaseMessage();
    }
  };

  showPopupNetworkError = () => {
    this.setState({
      networkErrorVisible: true
    });
  };

  componentDidMount = async () => {
    const isMaintenance = await checkApiStatus();
    if (isMaintenance) {
      return;
    }
    this.initFirebase();
    this.createAndroidChannel();
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
    this.forceLogoutEvent = EventRegister.on(event.forceLogout, this.forceLogout);
    this.networkErrorEvent = EventRegister.on(
      event.networkError,
      this.showPopupNetworkError
    );
    this.syncImmediate();
    this.showPopupNotificationEvent = EventRegister.on(
      event.showPopupNotification,
      this.showPopup
    );
    firebase
      .links()
      .getInitialLink()
      .then(url => {
        if (!url) return;
        navigateToDetail(url, this.props.dispatch);
      });

    firebase.links().onLink(url => {
      if (!url) return;
      navigateToDetail(url, this.props.dispatch);
    });
    // test showing popup
    // setTimeout(() => {
    //   this.handlePopupType({
    //     data: {
    //       action: "popup",
    //       content: "<a href=''><img src='https://yolla-stg.s3.amazonaws.com/static/images/notify/2019-11-19/yx5zmmcxa2.jpg'/></a>",
    //       msgid: "370",
    //       popupdisplay: "0",
    //       popupexpired: "0",
    //       popupid: "71",
    //       popuptype: "3",
    //       type: "Popup"
    //     },
    //     title: "Su test Nov 19 - 07 - type 3"
    //   });
    // }, 15000);
  };

  forceLogout = async () => {
    GoogleSignin.configure({
      offlineAccess: false
    });
    await GoogleSignin.signOut();
    await LoginManager.logOut();
    const fcmToken = await firebase.messaging().getToken();
    await Api.unregisterTokenId(fcmToken);
    await removeClientCas();
    await removeUserInfoForSentry();
    // TODO: Guessing below clear cookie make will lead to the bug
    // YA15-121 [Yolla Network 1.2 build 36 - 123][iOS] The session is time out when login by log in by fb, Google, phone number
    // Temporary comment it out to let user login successfully
    // await Cookie.clear(appConfig.rootUrl);
    Api.registerTokenIdAnonymous(fcmToken);

    this.props.resetProfile();
    if (Platform.OS === "ios") {
      PushNotificationIOS.setApplicationIconBadgeNumber(0);
    } else {
      BadgeAndroid.setBadge(0);
    }
    this.props.countedNotifications({ unreadcount: 0 });
    this.props.dispatch(resetPage("Login", { isForce: true }));
  };

  codePushStatusDidChange(syncStatus) {
    if (syncStatus === CodePush.SyncStatus.DOWNLOADING_PACKAGE) {
      this.setState({updateAvailable:true});
    }
  };

  codePushDownloadDidProgress(progress) {
    const progressStatus_percent = progress.receivedBytes/progress.totalBytes;
    this.setState({
      progressStatus: progressStatus_percent,
      receivedMb: parseFloat(progress.receivedBytes/1048576).toFixed(1),
      totalMb: parseFloat(progress.totalBytes/1048576).toFixed(1)
    })
  };

  syncImmediate() {
    CodePush.sync({
        installMode: CodePush.InstallMode.IMMEDIATE,
        updateDialog: false,
      },
      this.codePushStatusDidChange.bind(this),
      this.codePushDownloadDidProgress.bind(this)
    );
  };

  componentWillUnmount = () => {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
    if (this.notificationListener) this.notificationListener();
    if (this.notificationOpenedListener) this.notificationOpenedListener();
    EventRegister.off(this.forceLogoutEvent);
    EventRegister.off(this.networkErrorEvent);
    EventRegister.off(this.showPopupNotificationEvent);
    CodePush.getUpdateMetadata().then((metadata) =>{
      this.setState({
        // label: metadata.label,
        version: metadata.appVersion,
        // description: metadata.description
      });
    });
  };

  onConfirmNotification = async () => {
    const { notification } = this.state;
    this.setState({ notificationVisible: false, notification: null });
    navigateNotification(this.props.dispatch, notification);
    const id = lodash.get(notification, "data.msgid");
    const isAuthenticated = await AuthHelper.isAuthenticated();
    if (!id || !isAuthenticated) return;
    try {
      await Api.updateNotification({ id, status: "read" });
      this.props.readNotification(id);
      this.props.requestCountNotifications();
    } catch (err) {}
  };

  dontShowAgainPopup = async notification => {
    const popupid = lodash.get(notification, "data.popupid");
    await saveNotShowAgainNoti(popupid);
    this.setState({ notificationVisible: false });
  };

  hideNotificationPopup = notification => {
    const popupexpired = parseInt(lodash.get(notification, "data.popupexpired"), 10);
    const popupid = lodash.get(notification, "data.popupid");
    if (popupexpired > 0) {
      clearTimeout(this.popupexpiredTimeout[popupid]);
      this.popupexpiredTimeout[popupid] = setTimeout(
        () => this.showPopup(notification),
        popupexpired * 1000
      );
    }
    this.setState({ notificationVisible: false });
  };

  render() {
    const { dispatch, nav, loading } = this.props;
    const { notificationVisible, networkErrorVisible, notification } = this.state;
    return (
      (this.state.updateAvailable)?(this.render_update())
        :(
          <View style={styles.container}>
          <StatusBar
            backgroundColor={appColor.primary}
            hidden={false}
            barStyle="light-content"
          />
          <View style={styles.statusBar} />
          <SafeAreaView style={styles.body}>
            <View style={styles.content}>
              <AppNavigator
                navigation={addNavigationHelpers({
                  dispatch,
                  state: nav,
                  addListener
                })}
              />
            </View>
          </SafeAreaView>
          <Dialog
            visible={notificationVisible}
            width={sizeWidth(300)}
            containerStyle={{ backgroundColor: "transparent" }}
            dialogStyle={{ backgroundColor: "transparent" }}
            onTouchOutside={() => this.hideNotificationPopup(notification)}
            dialogAnimation={
              new FadeAnimation({
                toValue: 0,
                animationDuration: 150,
                useNativeDriver: true
              })
            }
          >
            <NotificationPopup
              onClose={() => this.dontShowAgainPopup(notification)}
              notification={notification}
              onConfirmNotification={this.onConfirmNotification}
            />
          </Dialog>
          {loading && <OverlayLoading isVisible />}
          <Dialog
            visible={networkErrorVisible}
            width={sizeWidth(302)}
            dialogAnimation={
              new FadeAnimation({
                toValue: 0,
                animationDuration: 150,
                useNativeDriver: true
              })
            }
          >
            <NetworkErrorPopup onClose={() => this.closeApp()} />
          </Dialog>
          </View>
      )
    );
  }

  render_update() {
    const appVersion = this.state.version;
    return (
      <View style={styles_update.container}>
        <View style={styles_update.icon_p}>
          <Image
              style={styles_update.icon}
              source={require("../res/icon/icon-app-yolla-network.png")}
          />
        </View>
        <View style={{ alignItems: 'center', marginBottom: moderateScale(51) }}>
          <Text style={styles_update.text}>Ứng dụng đang được cập nhật</Text>
        </View>
        <View style={styles_update.precess_text}>
          <View style={{flexDirection: "row"}}>
            <View style={{ flex: 1 }}>
              <Text style={styles_update.textProcess}>{this.state.receivedMb}/{this.state.totalMb} MB</Text>
            </View>
            <Text style={styles_update.textProcess}> {parseFloat(this.state.progressStatus * 100).toFixed(1)}%</Text>
          </View>
        </View>
        <View style={styles_update.updateProcess}>
          {
            ( Platform.OS === 'android' )
              ? ( <ProgressBarAndroid styleAttr = "Horizontal" color={appColor.primary} progress = { this.state.progressStatus } indeterminate = { false } /> )
              : ( <ProgressViewIOS progressTintColor={appColor.primary} progress = { this.state.progressStatus } /> )
          }
        </View>
          <View style={styles_update.precess_text}>
            <View style={{flexDirection: "row"}}>
              <View style={{ flex: 1 }}>
                {
                  (appVersion !== "")?
                      (<Text style={styles_update.textProcess}>Phiên bản cập nhật: {appVersion}</Text>)
                      :(<Text style={styles_update.textProcess}/>)
                }
              </View>
            </View>
          </View>
      </View>
    );
  };

  closeApp = () => {
    this.setState({
      networkErrorVisible: false
    });
  };
}

const statusBarHeight = Platform.OS === "ios" ? 20 : 0;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgb(31, 39, 43)"
  },
  statusBar: {
    height: statusBarHeight,
    backgroundColor: appColor.primary
  },
  body: {
    flex: 1,
    backgroundColor: appColor.primary
  },
  content: {
    overflow: "hidden",
    flex: 1
  }
});

const styles_update = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: "#FFFFFF"
  },
  icon_p: {
    alignItems: 'center',
    backgroundColor: "#FFFFFF",
    marginTop: moderateScale(93),
    marginBottom: moderateScale(20.25),
  },
  icon: {
    width: sizeWidth(60),
    height: sizeWidth(60),
    borderRadius: sizeWidth(30),
    overflow: "hidden",
  },
  updateProcess:{
    borderRadius: sizeWidth(6),
    backgroundColor: "white",
    justifyContent: 'space-evenly',
    paddingHorizontal:  moderateScale(30),
    paddingTop: moderateScale(5),
    paddingBottom: moderateScale(10),
  },
  text:{
    color: '#444444',
    fontFamily: font.bold,
    fontSize: sizeWidth(16),
    marginBottom: sizeWidth(50),
  },
  precess_text: {
    backgroundColor: "white",
    alignItems: 'center',
    paddingHorizontal:  moderateScale(30)
  },
  textProcess: {
    color: '#444444',
    fontFamily: font.medium,
    fontSize: sizeWidth(12)
  }
});

export default connect(
  state => ({
    nav: state.nav,
    loading: getIsLoading(state)
  }),
  dispatch => ({
    dispatch,
    readNotification: id => dispatch(readNotification(id)),
    requestCountNotifications: () => dispatch(requestCountNotifications()),
    resetProfile: () => dispatch(resetProfile()),
    requestedProfile: () => dispatch(requestedProfile()),
    loadedProfile: data => dispatch(loadedProfile(data)),
    countedNotifications: data => dispatch(countedNotifications(data))
  })
)(Main);
