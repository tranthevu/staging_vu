import React from "react";
import { StyleSheet } from "react-native";
import { TabNavigator } from "react-navigation";
import BottomTabIcon from "../components/common/bottom-tab-icon";
import DashboardScreen from "../screens/dashboard/dashboard.screen";
import QrCodeScreen from "../screens/qrCode/qr-code.screen";
import UserScreen from "../screens/user/user.screen";
import { sizeWidth, moderateScale } from "../helpers/size.helper";
import PromotionScreen from "../screens/promotion/promotion.screen";
import MyArchivementScreen from "../screens/myArchivement/my-archivement.screen";
import SettingsScreen from "../screens/settings/settings.screen";
import ProfileScreen from "../screens/profile/profile.screen";
import TabBar from "../components/common/tab-bar";
import MemberCodeScreen from "../screens/memberCode/member-code.screen";

export const TabsNavigator = TabNavigator(
  {
    Dashboard: {
      screen: DashboardScreen,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <BottomTabIcon
            style={styles.dashboard}
            focused={focused}
            source={require("../../res/icon/home-tab.png")}
          />
        )
      }
    },
    Archive: {
      screen: MyArchivementScreen,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <BottomTabIcon
            style={styles.archive}
            focused={focused}
            source={require("../../res/icon/archive.png")}
          />
        )
      }
    },
    MemberCode: {
      screen: MemberCodeScreen,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <BottomTabIcon
            style={styles.code}
            focused={focused}
            source={require("../../res/icon/member-code.png")}
          />
        )
      }
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <BottomTabIcon
            focused={focused}
            style={styles.user}
            source={require("../../res/icon/profile.png")}
          />
        )
      }
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: {
        tabBarIcon: ({ focused }) => (
          <BottomTabIcon
            focused={focused}
            style={styles.settings}
            source={require("../../res/icon/settings.png")}
          />
        )
      }
    }
  },
  {
    // YA15-249
    swipeEnabled: false,
    tabBarPosition: "bottom",
    initialRouteName: "Dashboard",
    backBehavior: "none",
    lazy: true,
    tabBarComponent: TabBar,
    tabBarOptions: {
      indicatorStyle: {
        backgroundColor: "transparent"
      },
      showIcon: true,
      showLabel: false,
      upperCaseLabel: false,
      tabStyle: {
        alignItems: "center",
        height: moderateScale(44),
        justifyContent: "center"
      },
      style: {
        backgroundColor: "white",
        justifyContent: "flex-end",
        height: moderateScale(44)
      },
      iconStyle: {
        justifyContent: "center",
        alignItems: "center"
      },
    }
  }
);

const styles = StyleSheet.create({
  dashboard: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  customer: {
    width: moderateScale(20),
    height: moderateScale(15)
  },
  archive: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  settings: {
    width: moderateScale(24),
    height: moderateScale(24)
  },
  user: {
    width: moderateScale(18.5),
    height: moderateScale(17)
  },
  code: {
    width: moderateScale(25),
    height: moderateScale(25)
  }
});
