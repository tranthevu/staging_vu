import { StyleSheet } from "react-native";
import { DrawerNavigator } from "react-navigation";
import { sizeWidth } from "../helpers/size.helper";
import Menu from "../screens/dashboard/menu";
import { TabsNavigator } from "./tabs.navigator";

export const initMainRouter = () => {
  const mainScreens = [
    {
      key: "Tab",
      screen: TabsNavigator
    }
  ];

  const routes = {};
  mainScreens.forEach(screen => {
    routes[screen.key] = {
      screen: screen.screen
    };
  });
  return DrawerNavigator(routes, {
    drawerWidth: sizeWidth(269),
    contentComponent: Menu,
    contentOptions: {
      activeTintColor: "#FFC107",
      inactiveTintColor: "white",
      iconContainerStyle: {}
    }
  });
};

const styles = StyleSheet.create({});
