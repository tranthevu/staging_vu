/* eslint-disable import/no-cycle */
import { StackNavigator } from "react-navigation";
import SplashScreen from "../screens/splash/splash.screen";
import MaintenanceScreen from "../screens/maintenance/maintenance.screen";
import LoginScreen from "../screens/login/login.screen";
import RegisterScreen from "../screens/register/register.screen";
import ForgotPasswordScreen from "../screens/forgotPassword/forgot-password.screen";
import ProductsScreen from "../screens/products/products.screen";
import MerchantDetailScreen from "../screens/merchantDetail/merchant-detail.screen";
import StoreDetailScreen from "../screens/storeDetail/store-detail.screen";
import StoreMapScreen from "../screens/storeMap/store-map.screen";
import LoginByPhoneScreen from "../screens/loginByPhone/login-by-phone.screen";
import ConfirmPhoneScreen from "../screens/loginByPhone/confirm-phone.screen";
import ConfirmedPhoneScreen from "../screens/loginByPhone/confirmed-phone.screen";
import CompleteProfileScreen from "../screens/loginByPhone/complete-profile.screen";
import ResendCodeScreen from "../screens/loginByPhone/resend-code.screen";
import CategoryDashboardScreen from "../screens/categoryDashboard/category-dashboard.screen";
import FilterScreen from "../screens/filter/filter.screen";
import SearchScreen from "../screens/search/search.screen";
import SearchResultsScreen from "../screens/searchResults/search-results.screen";
import OfferDetailScreen from "../screens/offerDetail/offer-detail.screen";
import LocationsScreen from "../screens/locations/locations.screen";
import DocumentScreen from "../screens/document/document.screen";
import AccountScreen from "../screens/account/account.screen";
import ContactScreen from "../screens/contact/contact.screen";
import ReferralScreen from "../screens/referral/referral.screen";
import NotificationsScreen from "../screens/notifications/notifications.screen";
import VoucherScreen from "../screens/voucher/voucher.screen";
import LocationProductsScreen from "../screens/locationProducts/location-products.screen";
import UpdateLocationScreen from "../screens/updateLocation/update-location.screen";
import { TabsNavigator } from "./tabs.navigator";
import OfferInfoScreen from "../screens/offerDetail/offer-info.screen";
import MySavedFilterScreen from "../screens/mySavedFilter/my-saved-filter.screen";
import UseCodeScreen from "../screens/useCode/use-code.screen";
import ShareToEarnScreen from "../screens/shareToEarn/share-to-earn.screen";
import YollaBizShareToEarnScreen from "../screens/shareToEarn/share-to-earn-yolla-biz.screen";
import JoinedCampaignsScreen from "../screens/joinedCampaigns/joined-campaigns.screen";
import PromoScreen from "../screens/promo/promo.screen";
import CouponScreen from "../screens/coupon/coupon.screen";
import OnlinePromoScreen from "../screens/onlinePromo/online-promo.screen";
import FollowScreen from "../screens/follow/follow.screen";
import FollowMerchantScreen from "../screens/followMerchant/follow-merchant.screen";
import ExclusivePromoScreen from "../screens/exclusivePromo/exclusive-promo.screen";
import NearPromoScreen from "../screens/nearPromo/near-promo.screen";
import AccountBankScreen from "../screens/accountBank/account-bank.screen";
import GuideS2N from "../screens/shareToEarn/guide";
import PrepaidScreen from "../screens/prepaidVoucher/pre-paid.screen";
import NotificationPrepaid from "../screens/prepaidVoucher/notification-paid-voucher.screen";
import HistoryPrepaidScreen from "../screens/prepaidVoucher/history-paid.screen";
import DetailPrePaidScreen from "../screens/prepaidVoucher/detail-paid.screen";
import {
  NEAR_PROMO_SCREEN,
  FILTER_PRODUCT_NEAR_BY_SCREEN,
  FOLLOW_MERCHANT_SCREEN,
  FOLLOW_SCREEN,
  EXCLUSIVE_PROMO_SCREEN,
  ONLINE_PROMO_SCREEN,
  UPDATE_LOCATION_SCREEN,
  OFFER_INFO_SCREEN,
  MY_SAVED_FILTER_SCREEN,
  USE_CODE_SCREEN,
  SHARE_TO_EARN_SCREEN,
  YOLLA_BIZ_SHARE_TO_EARN_SCREEN,
  JOINED_CAMPAIGNS_SCREEN,
  PROMO_SCREEN,
  COUPON_SCREEN,
  FAVORITE_CATEGORY_SCREEN,
  SHARE_TO_EARN_PROMO_SCREEN,
  SHARE_TO_EARN_FILTER_SCREEN,
  GIFT_FILTER_SCREEN,
  GuideS2n,
  LIST_MERCHANT_BY_ADDRESS_SCREEN,
  LIST_PROMO_BY_ADDRESS_SCREEN,
  PREPAID_VOUCHER_SCREEN,
  NOTIFICATION_PREPAID_SCREEN,
  PREPAID_SCREEN,
  HISTORY_PREPARD_SCREEN,
  DETAIL_PREPAID_SCREEN,
  PAGE_NOT_FOUND_SCREEN,
  NOTICE_SCREEN
} from "./screensName";
import FilterProductNearByScreen from "../screens/filter/filterProductNearBy.screen";
import FavoriteCategoryScreen from "../screens/FavoriteCategory";
import ShareToEarnPromoScreen from "../screens/share2EarnPromo/share-to-earn-promo.screen";
import ShareToEarnFilterScreen from "../screens/shareToEarn/share-to-earn-filter.screen";
import GiftFilterScreen from "../screens/myArchivement/gift-filter.screen";
import SavingHistoryScreen from "../screens/savingHistory/saving-history.screen";
import RedemptionHistoryScreen from "../screens/redemptionHistory/redemption-history.screen";
import RedemptionDetailScreen from "../screens/redemptionHistory/redemption-detail.screen";
import PresenterScreen from "../screens/presenter/presenter.screen";
import ListMerchantByAddress from "../screens/nearPromo/ListMerchantByAddress";
import ListPromoByAddressScreen from "../screens/nearPromo/ListPromoByAddress";
import PageNotFoundScreen from "../screens/pages/PageNotFoundScreen";
import NoticeScreen from "../screens/notice/notice.screen";
import EnterPasswordScreen from "../screens/enterPassword/enter-password.screen";
import NewPasswordScreen from "../screens/newPassword/new-password.screen";
import SelectionListScreen from "../screens/selectionList/selection-list.screen";

const stackNavigatorOptions = {
  headerMode: "none",
  cardStyle: {
    backgroundColor: "white",
    shadowColor: "transparent"
  }
};
export const AppNavigator = StackNavigator(
  {
    Splash: { screen: SplashScreen },
    ForgotPassword: { screen: ForgotPasswordScreen },
    NewPassword: { screen: NewPasswordScreen },
    EnterPassword: { screen: EnterPasswordScreen },
    ResendCode: { screen: ResendCodeScreen },
    ConfirmPhone: { screen: ConfirmPhoneScreen },
    [NOTICE_SCREEN]: { screen: NoticeScreen },
    Maintenance: { screen: MaintenanceScreen },
    Main: { screen: TabsNavigator },
    Login: { screen: LoginScreen },
    Register: { screen: RegisterScreen },
    Products: { screen: ProductsScreen },
    MerchantDetail: { screen: MerchantDetailScreen },
    StoreDetail: { screen: StoreDetailScreen },
    StoreMap: { screen: StoreMapScreen },
    LoginByPhone: { screen: LoginByPhoneScreen },
    ConfirmedPhone: { screen: ConfirmedPhoneScreen },
    CompleteProfile: { screen: CompleteProfileScreen },
    // TODO: The RegionsScreen is not used anymore
    // Regions: { screen: RegionsScreen },
    CategoryDashboard: { screen: CategoryDashboardScreen },
    Filter: { screen: FilterScreen },
    Search: { screen: SearchScreen },
    OfferDetail: { screen: OfferDetailScreen },
    Locations: { screen: LocationsScreen },
    SearchResults: { screen: SearchResultsScreen },
    Document: { screen: DocumentScreen },
    Account: { screen: AccountScreen },
    Contact: { screen: ContactScreen },
    Referral: { screen: ReferralScreen },
    Notifications: { screen: NotificationsScreen },
    Voucher: { screen: VoucherScreen },
    LocationProducts: { screen: LocationProductsScreen },
    [UPDATE_LOCATION_SCREEN]: { screen: UpdateLocationScreen },
    [OFFER_INFO_SCREEN]: { screen: OfferInfoScreen },
    [MY_SAVED_FILTER_SCREEN]: { screen: MySavedFilterScreen },
    [USE_CODE_SCREEN]: { screen: UseCodeScreen },
    [SHARE_TO_EARN_SCREEN]: { screen: ShareToEarnScreen },
    [YOLLA_BIZ_SHARE_TO_EARN_SCREEN]: { screen: YollaBizShareToEarnScreen },
    [JOINED_CAMPAIGNS_SCREEN]: { screen: JoinedCampaignsScreen },
    [PROMO_SCREEN]: { screen: PromoScreen },
    [COUPON_SCREEN]: { screen: CouponScreen },
    [ONLINE_PROMO_SCREEN]: { screen: OnlinePromoScreen },
    [EXCLUSIVE_PROMO_SCREEN]: { screen: ExclusivePromoScreen },
    [FOLLOW_SCREEN]: { screen: FollowScreen },
    [FOLLOW_MERCHANT_SCREEN]: { screen: FollowMerchantScreen },
    [NEAR_PROMO_SCREEN]: { screen: NearPromoScreen },
    [FILTER_PRODUCT_NEAR_BY_SCREEN]: { screen: FilterProductNearByScreen },
    [FAVORITE_CATEGORY_SCREEN]: FavoriteCategoryScreen,
    [SHARE_TO_EARN_PROMO_SCREEN]: { screen: ShareToEarnPromoScreen },
    [SHARE_TO_EARN_FILTER_SCREEN]: { screen: ShareToEarnFilterScreen },
    [GIFT_FILTER_SCREEN]: { screen: GiftFilterScreen },
    [LIST_MERCHANT_BY_ADDRESS_SCREEN]: { screen: ListMerchantByAddress },
    [LIST_PROMO_BY_ADDRESS_SCREEN]: { screen: ListPromoByAddressScreen },
    [GuideS2n]: { screen: GuideS2N },
    [PREPAID_SCREEN]: { screen: PrepaidScreen },
    [NOTIFICATION_PREPAID_SCREEN]: { screen: NotificationPrepaid },
    [DETAIL_PREPAID_SCREEN]: { screen: DetailPrePaidScreen },
    AccountBank: { screen: AccountBankScreen },
    RedemptionDetail: { screen: RedemptionDetailScreen },
    RedemptionHistory: { screen: RedemptionHistoryScreen },
    SavingHistory: { screen: SavingHistoryScreen },
    Presenter: { screen: PresenterScreen },
    [HISTORY_PREPARD_SCREEN]: { screen: HistoryPrepaidScreen },
    [PAGE_NOT_FOUND_SCREEN]: { screen: PageNotFoundScreen },
    SelectionList: { screen: SelectionListScreen }
  },
  stackNavigatorOptions
);
