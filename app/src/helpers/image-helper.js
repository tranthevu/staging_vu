import ImagePicker from "react-native-image-crop-picker";
import {
  requestCameraPermission,
  requestStoragePermission
} from "./permission-helper";

export const capturePicture = async () => {
  await requestCameraPermission();
  return new Promise(async (resolve, reject) => {
    ImagePicker.openCamera({
      cropping: false,
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000
    })
      .then(image => {
        const { mime, path } = image;
        const uri = path;
        const uriTokens = uri.split("/");
        const name = uriTokens[uriTokens.length - 1];
        const result = { uri, name, type: mime };
        resolve(result);
      })
      .catch(err => {
        reject(err);
      });
  });
};

export const pickPictureFromGallery = async () => {
  await requestStoragePermission();
  return new Promise(async (resolve, reject) => {
    ImagePicker.openPicker({
      cropping: false,
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000
    })
      .then(image => {
        const { mime, path } = image;
        const uri = path;
        const uriTokens = uri.split("/");
        const name = uriTokens[uriTokens.length - 1];
        const result = { uri, name, type: mime };
        resolve(result);
      })
      .catch(err => {
        reject(err);
      });
  });
};

export const pickMultiplePictureFromGallery = async () => {
  await requestStoragePermission();
  return new Promise(async (resolve, reject) => {
    ImagePicker.openPicker({ multiple: true })
      .then(images => {
        let results = [];
        images.forEach(image => {
          const { mime, path } = image;
          const uri = path;
          const uriTokens = uri.split("/");
          const name = uriTokens[uriTokens.length - 1];
          results.push({ uri, name, type: mime });
        });
        resolve(results);
      })
      .catch(err => {
        reject(err);
      });
  });
};
