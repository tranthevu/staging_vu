import _ from "lodash";

export const callbackError = (ctx, error) => {
  if (ctx && ctx.onError && _.isFunction(ctx.onError)) {
    ctx.onError(error || null);
  }
  if (ctx && ctx.callback && _.isFunction(ctx.callback)) {
    ctx.callback(error, null);
  }
};

export const callbackSuccess = (ctx, resp) => {
  if (ctx && ctx.onSuccess && _.isFunction(ctx.onSuccess)) {
    ctx.onSuccess(resp || null);
  }
  if (ctx && ctx.callback && _.isFunction(ctx.callback)) {
    ctx.callback(null, resp);
  }
};
