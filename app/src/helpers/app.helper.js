import { appConfig } from "../config/app.config";
import RequestHelper from "./request.helper";
import { apiMapping } from "../constants/app.constant";

let logs = [];

export function logDebug(location, msg, obj = undefined) {
  if (appConfig.environment === "development") {
    if (logs.length < 100) {
      const message = `${new Date().toISOString()} [${location}] - ${msg}`;
      logs.push(message);
      console.log(message, obj);
    }
    while (logs.length > 100) {
      logs.splice(0, 1);
    }
  }
}

export function getLogs() {
  return logs;
}

export async function checkApiStatus() {
  let isMaintenance;
  try {
    // use fetch here, to skip axios error handler
    await fetch(`${appConfig.apiUrl}status/`, {
      headers: {
        Authorization: appConfig.authorizationHeader
      }
    }).then(function(res) {
      isMaintenance = res.status === 200 ? false : true;
    });
    return isMaintenance;
  } catch (error) {}
}
