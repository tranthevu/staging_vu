import { AsyncStorage } from "react-native";

const FCM_TOKEN = "fcm_token";
const KEY_TOKEN = "access_token";
const KEY_SEARCH_HISTORIES = "search_histories";
const KEY_LOCATION = "location";
const CLIENT_CAS = "client_cas";
const NOT_SHOW_AGAIN_NOTI = "not_show_again_noti";
const USER_INFO_FOR_SENTRY = "user_info_for_sentry";

export const saveFCMToken = fCMToken => AsyncStorage.setItem(FCM_TOKEN, fCMToken || "");

export const getFCMToken = () => {
  return AsyncStorage.getItem(FCM_TOKEN);
};

export const saveToken = token => AsyncStorage.setItem(KEY_TOKEN, token || "");

export const getToken = () => {
  return AsyncStorage.getItem(KEY_TOKEN);
};

export const removeToken = () => AsyncStorage.removeItem(KEY_TOKEN);

export const saveSearchHistory = async keyword => {
  let histories = await getSearchHistories();
  histories = histories.filter(item => JSON.stringify(item) !== JSON.stringify(keyword));
  histories = [keyword, ...histories];
  await AsyncStorage.setItem(KEY_SEARCH_HISTORIES, JSON.stringify(histories) || "");
};

export const getSearchHistories = async () => {
  const value = await AsyncStorage.getItem(KEY_SEARCH_HISTORIES);
  if (value) return JSON.parse(value);
  return [];
};

export const removeSearchHistories = () => AsyncStorage.removeItem(KEY_SEARCH_HISTORIES);
export const saveLocation = location =>
  AsyncStorage.setItem(KEY_LOCATION, location || "");

export const getLocation = () => {
  return AsyncStorage.getItem(KEY_LOCATION);
};

export const removeLocation = () => AsyncStorage.removeItem(KEY_LOCATION);

export const saveClientCas = clientCas =>
  AsyncStorage.setItem(CLIENT_CAS, clientCas || "");

export const getClientCas = () => {
  return AsyncStorage.getItem(CLIENT_CAS);
};

export const removeClientCas = () => AsyncStorage.removeItem(CLIENT_CAS);

export const saveNotShowAgainNoti = async popupid => {
  const notShowAgainNoti = await getNotShowAgainNoti();
  notShowAgainNoti.push(popupid);
  await AsyncStorage.setItem(NOT_SHOW_AGAIN_NOTI, notShowAgainNoti.join(",") || "");
};

export const getNotShowAgainNoti = async () => {
  const text = await AsyncStorage.getItem(NOT_SHOW_AGAIN_NOTI);
  if (!text) return [];
  return text.split(",");
};

export const removeNotShowAgainNoti = () => AsyncStorage.removeItem(NOT_SHOW_AGAIN_NOTI);

export const saveUserInfoForSentry = userInfo =>
  AsyncStorage.setItem(USER_INFO_FOR_SENTRY, JSON.stringify(userInfo || {}));

export const getUserInfoForSentry = async () => {
  const value = await AsyncStorage.getItem(USER_INFO_FOR_SENTRY);
  if (value) return JSON.parse(value);
  return {};
};

export const removeUserInfoForSentry = () =>
  AsyncStorage.removeItem(USER_INFO_FOR_SENTRY);
