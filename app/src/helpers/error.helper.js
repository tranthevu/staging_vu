import EventRegister from "./event-register.helper";
import { event } from "../constants/app.constant";

export const showError = error => {
  EventRegister.emit(event.shownNotification, error);
};

export const showMessage = message => {
  EventRegister.emit(event.shownNotification, message);
};
