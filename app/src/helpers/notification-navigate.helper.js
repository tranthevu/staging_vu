import lodash from "lodash";
import { Linking } from "react-native";
import { navigateToPage, resetPage } from "../actions/nav.action";
import { notificationAction } from "../constants/app.constant";
import Api from "../api/api";
import { checkApiStatus } from "./app.helper";

let didNavigate = false;

export const updateDidNavigate = value => {
  didNavigate = value;
};

export const getDidNavigate = () => {
  return didNavigate;
};

const navigateToProductList = (dispatch, notification, isInitial) => {
  const field = lodash.get(notification, "data.field");
  const category = lodash.get(notification, "data.cat");
  if (category) {
    if (isInitial) dispatch(resetPage("Main"));
    dispatch(
      navigateToPage("CategoryDashboard", {
        category: { category: { slug: category } }
      })
    );
  }
  if (field) {
    dispatch(resetPage("Main"));
    dispatch(navigateToPage("Dashboard", { field }));
  }
};

export const navigateNotification = (dispatch, notification, config) => {
  const action = lodash.get(notification, "data.action");
  const isInitial = lodash.get(config, "isInitial");
  const slug = lodash.get(notification, "data.slug");
  const url = lodash.get(notification, "data.url");
  switch (action) {
    case notificationAction.popup:
      break;
    case notificationAction.productList:
      navigateToProductList(dispatch, notification, isInitial);
      break;
    case notificationAction.productDetail:
      if (isInitial) dispatch(resetPage("Main"));

      if (slug) dispatch(navigateToPage("OfferDetail", { productSlug: slug }));
      break;
    case notificationAction.myCodeList:
      dispatch(resetPage("Main"));
      dispatch(navigateToPage("Archive"));
      break;
    case notificationAction.myRewardDetail:
    case notificationAction.myCodeDetail:
      if (isInitial) dispatch(resetPage("Main"));
      dispatch(
        navigateToPage("Voucher", {
          productId: lodash.get(notification, "data.id"),
          codeId: lodash.get(notification, "data.code_id"),
          productSlug: lodash.get(notification, "data.slug"),
          isFromNotification: true
        })
      );
      break;
    case notificationAction.openbrowser:
      if (url) Linking.openURL(url);
      break;
    default:
      dispatch(resetPage("Main"));
      break;
  }
};

export const navigateToDetail = async (url, dispatch) => {
  const isMaintenance = await checkApiStatus();
  if (isMaintenance) {
    this.props.resetPage("Maintenance");
  }
  try{
    let route = url.replace(/.*?:\/\//g, '');
    route = route.replace(/\?utm_source.*/g, '');//open by zalo ?utm_source=zalo?redirect%3D1
    const regex = route.match(/^.*?\/(.*?)(\/|\?slug=)(.*?)(\/*$|\?redirect=1)/);
    if (regex !== null) {
      const routeName = regex[1];
      const data = regex[3];
      updateDidNavigate(true);
      dispatch(resetPage(("Main")));
      switch (routeName) {
        case 'product':
          dispatch(navigateToPage("OfferDetail", {productSlug: data}));
          break;
        case 'merchant':
          dispatch(navigateToPage("MerchantDetail", {merchantCode: data}));
          break;
        case 'campaigns':
          const campaign  = data.split('/');
          const slug = campaign[0];
          let user_ref = null;
          if (campaign.length >= 2) {
            if (campaign[1] !==""){
              user_ref = campaign[1]
            }
          }
          const product = await Api.share2EarnProduct(slug);
          if (product.data > 0){
            dispatch(navigateToPage("OfferDetail", {productSlug: slug, user_ref:user_ref}));
          } else{
            await Linking.openURL(url);
          }
          break;
        case 'order-success':
          const basket_id  = data;
          dispatch(navigateToPage("DetailPrepaidScreen", { basket_id }));
          break;
        default:
          dispatch(resetPage("Main"));
      }
    } else {
      dispatch(resetPage("Main"));
    }
  }
  catch (e) {
    dispatch(resetPage("Main"));
  }
};
