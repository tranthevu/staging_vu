import firebase from "react-native-firebase";
let Analytics = firebase.analytics();

export default class AnalyticsHelper {
  static logEvent = (eventName, data) => {
    Analytics.logEvent(eventName, data);
  };
}
