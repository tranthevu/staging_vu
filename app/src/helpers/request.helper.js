/* eslint-disable consistent-return */
import axios from "axios";
import qs from "qs";
import Toast from "@remobile/react-native-toast";
import * as Sentry from "@sentry/react-native";
import lodash from "lodash";
import { message, apiError, event } from "../constants/app.constant";
import { appConfig } from "../config/app.config";
import EventRegister from "./event-register.helper";
import { getClientCas, getUserInfoForSentry } from "./storage.helper";
import { logDebug } from "./app.helper";
import { showError } from "./error.helper";

const instance = axios.create({
  timeout: 30000
});

const handleError = async (error, apiName) => {
  const clientCas = await getClientCas();
  const userInfo = await getUserInfoForSentry();
  Sentry.setExtra("clientCas", clientCas);
  Sentry.setUser(userInfo);
  Sentry.setTag("source", "api-error");
  Sentry.captureException(error);
  if (error.response) {
    Sentry.setExtra("url", error.request._url);
    Sentry.setExtra("response", JSON.stringify(error.response.data));
    logDebug(
      "request.helper.js error.response: ",
      `${error.request._url} ${JSON.stringify(error.response.data)}`
    );
    if (error.response.status === 401) {
      return EventRegister.emit(event.forceLogout);
    }
    const errorCode =
      lodash.get(error.response, "data.error") ||
      lodash.get(error.response, "data.error_code");
    const notification = lodash.get(apiError, `${apiName}.${errorCode}`);
    if (notification) return EventRegister.emit(event.shownNotification, notification);
    let content =
      lodash.get(error.response, "data.error.message") ||
      lodash.get(error.response, "data.message");
    if (appConfig.environment !== "production") {
      if (!content) {
        if (error.response.data && error.response.data.length > 0) {
          content = error.response.data.slice(0, 100);
        }
        Toast.show(`Error from API: ${error.request._url}:\n ${content}`);
      }
    }
    showError(content || message.unknownError);
  } else if (error.request) {
    EventRegister.emit(event.networkError);
    logDebug("request.helper.js", `${apiName} error.request`, error.request);
  } else {
    logDebug("request.helper.js", `${apiName} message.unknownError`);
    showError(message.unknownError);
  }
};

export default class RequestHelper {
  static async getHeader(config = {}): void {
    const clientCas = await getClientCas();
    return {
      "Content-Type": "application/json",
      Authorization: appConfig.authorizationHeader,
      Cookie: clientCas && `client_cas=${clientCas}`,
      ...config
    };
  }

  static async get(url, params, apiName) {
    const header = await this.getHeader();
    return instance
      .get(url, {
        headers: header,
        params,
        paramsSerializer: params => {
          return qs.stringify(params, { arrayFormat: "repeat" });
        }
      })
      .then(data => {
        logDebug(
          "request.helper.js",
          `get ${url}?${qs.stringify(params, {
            arrayFormat: "repeat"
          })} response: `,
          data.data
        );
        if (typeof data.data === "string") {
          logDebug("request.helper.js", "API did not return JSON format");
          throw "API did not return JSON format";
        }
        return data.data;
      })
      .catch(e => {
        handleError(e, apiName);
        throw e;
      });
  }

  static async post(url: string, data, config, apiName): void {
    const headers = await this.getHeader(config);
    logDebug(
      "request.helper.js",
      `prepare post to ${url}. Cookie: ${headers.Cookie} params: ${JSON.stringify(
        data
      )} header: ${JSON.stringify(headers)}`
    );
    return instance({
      method: "POST",
      url,
      headers,
      data
    })
      .then(res => {
        // eslint-disable-next-line no-unused-expressions
        // eslint-disable-next-line no-undef
        __DEV__ && console.log(">>>>>>Response>>>>", res);
        logDebug(
          "request.helper.js",
          `post to ${url}. Cookie: ${headers.Cookie} params: ${JSON.stringify(
            data
          )}. response: `,
          res.data
        );
        return res.data;
      })
      .catch(e => {
        handleError(e, apiName);
        throw e;
      });
  }

  // Fix YOLLA-497
  // [Yolla-App-95-84][Notification] The user still receives the notification while logged out of the application
  // Using axios here will cache cookie somehow
  static async postAnonymous(url, data): void {
    logDebug(
      "request.helper.js",
      `postAnonymous to ${url}. params: ${JSON.stringify(data)}.`
    );
    return fetch(url, {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    }).then(response => {
      response.json();
    });
  }

  static async put(apiUrl: string, data: {}, apiName): void {
    return instance({
      method: "PUT",
      url: apiUrl,
      headers: await this.getHeader(),
      data
    })
      .then(res => {
        logDebug(
          "request.helper.js",
          `post to ${apiUrl}. params: ${JSON.stringify(data)}. response: `,
          res.data
        );
        return res.data;
      })
      .catch(e => {
        handleError(e, apiName);
        throw e;
      });
  }

  static async delete(apiUrl: string, data: {}, apiName): void {
    return instance({
      method: "DELETE",
      url: apiUrl,
      headers: await this.getHeader(),
      data
    })
      .then(data => {
        console.log(data.data);
        return data.data;
      })
      .catch(e => {
        handleError(e, apiName);
        throw e;
      });
  }
}
