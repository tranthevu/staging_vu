import numeral from "numeral";
import { Platform, Linking, Alert } from "react-native";
import { productClass, productCodeType } from "../constants/app.constant";

export const getProductType = type => {
  if (type === productClass.event) return "Chương trình khuyến mãi";
  if (type === productClass.voucher || type === productCodeType.voucher) return "Coupon";
  if (type === productClass.affiliateOffer) return "Ưu đãi online";
  if (type === productClass.shareToEarn || type === productCodeType.shareToEarn)
    return "Chia sẻ nhận thưởng";
  if (
    type === productClass.yollaBizShareToEarn ||
    type === productCodeType.yollaBizShareToEarn
  )
    return "Yolla Biz - Chia sẻ nhận thưởng";
  if (type === productClass.prepaidVoucher || type === productCodeType.prepaidVoucher)
    return "eVoucher trả trước";
};

export const isEmptyPrice = price => {
  return price == null || price == undefined;
};

export const formatPrice = price => {
  return numeral(price).format("0,0");
};

export const makePhoneCall = async phone => {
  let phoneNumber = "";
  if (Platform.OS === "android") {
    phoneNumber = `tel:${phone}`;
  } else {
    phoneNumber = `telprompt:${phone}`;
  }
  const isSupported = await Linking.canOpenURL(phoneNumber);
  if (isSupported) {
    Linking.openURL(phoneNumber);
  } else {
    Alert.alert("Thông báo", "Số điện thoại không hợp lệ");
  }
};

export const openLinkUrl = async url => {
  const isSupported = await Linking.canOpenURL(url);
  if (isSupported) {
    Linking.openURL(url);
  } else {
    Alert.alert("Thông báo", "Địa chỉ trang web không hợp lệ");
  }
};
