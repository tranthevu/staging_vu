import { Dimensions, PixelRatio } from "react-native";

const { width, height } = Dimensions.get("window");

const vh = height / 100;
const vw = width / 100;
const ratio3Scale = 0.8;

export const sizeWidth = size => {
  let layoutSize = (size / 320) * 100 * vw;
  return PixelRatio.roundToNearestPixel(layoutSize);
};

export const sizeHeight = size => {
  let layoutSize = (size / 568) * 100 * vh;
  if (PixelRatio.get() == 3) {
    layoutSize = layoutSize * ratio3Scale;
  }
  return PixelRatio.roundToNearestPixel(layoutSize);
};

export const sizeFont = size => {
  let layoutSize = (size / 320) * 100 * vw;
  if (PixelRatio.get() == 3) {
    layoutSize = layoutSize * ratio3Scale;
  }
  return PixelRatio.roundToNearestPixel(layoutSize);
};

// inspired by moderateScale
// from https://github.com/nirsky/react-native-size-matters
export const moderateScale = size => {
  let layoutSize = (size / 320) * 100 * vw;
  if (PixelRatio.get() == 3) {
    layoutSize = layoutSize * ratio3Scale;
  }
  return PixelRatio.roundToNearestPixel(layoutSize);
};
