import { put } from "redux-saga/effects";
import { ERROR_CODE } from "../constants/app.constant";
import {
  isLoading,
  showLoading,
  nonLoading,
  hideLoading
} from "../actions/loading.action";
import { showError } from "./error.helper";

function convertDataError(error) {
  const { code = {}, response = {}, message } = error || {};
  const { status } = response || {};
  if (status) {
    switch (status) {
      case ERROR_CODE.NOT_FOUND:
        return "Không tìm thấy trang";
      default:
        return "Truy vấn xảy ra lỗi!";
    }
  } else {
    switch (code) {
      case ERROR_CODE.ECONNABORTED:
        return "Truy vấn quá lâu";
      case ERROR_CODE.NOT_INTERNET:
        return "Không có kết nối mạng";
      default: {
        if (message) {
          if (message.search("Cannot read property") >= 0) return "Truy vấn dữ liệu lỗi!";
          if (message.search("Network Error") >= 0)
            return "Không có kết nối internet, vui lòng kiểm tra lại!";
        }
        return "Truy vấn xảy ra lỗi!";
      }
    }
  }
}

export function* invoke(execution, handleError, showDialog) {
  try {
    yield put(isLoading());

    if (showDialog) {
      yield put(showLoading());
    }
    yield execution();
    yield put(nonLoading());
    if (showDialog) {
      yield put(hideLoading());
    }
  } catch (error) {
    console.log("Saga Invoke Error >>>>>", { error });
    yield put(nonLoading());
    if (showDialog) {
      yield put(hideLoading());
    }
    const strErr = convertDataError(error);
    if (handleError) {
      yield handleError(strErr);
    } else {
      yield showError(strErr);
    }
  }
}
