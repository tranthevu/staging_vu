import lodash from "lodash";
import {
  voucherStatus,
  text,
  GIFT_REWARD_STATUS,
  productClass,
  productCodeType
} from "../constants/app.constant";

export default class VoucherHelper {
  static getStatus = (item, isGift) => {
    if (isGift) {
      const used = lodash.get(item, "reward_status") === GIFT_REWARD_STATUS.USED;
      const isExpired = lodash.get(item, "reward_status") === GIFT_REWARD_STATUS.EXPIRED;
      if (used) return "Đã dùng";
      if (isExpired) return "Hết hạn dùng";
      return text.emptyString;
    }
    const used = lodash.get(item, "status") === voucherStatus.used;
    const isExpired = lodash.get(item, "status") === voucherStatus.isExpired;
    const isOfferExpired = lodash.get(item, "status") === voucherStatus.isOfferExpired;
    if (used) return "Đã dùng";
    if (isExpired) return "Hết hạn dùng";
    if (isOfferExpired) return "Tin hết hạn";
    return text.emptyString;
  };

  static isShowStatus = (item, isGift) => {
    if (isGift) {
      const used = lodash.get(item, "reward_status") === GIFT_REWARD_STATUS.USED;
      const isExpired = lodash.get(item, "reward_status") === GIFT_REWARD_STATUS.EXPIRED;
      return used || isExpired;
    }
    const used =
      (lodash.get(item, "product_class") === productClass.voucher ||
        lodash.get(item, "product_class") === productClass.shareToEarn) &&
      lodash.get(item, "status") === voucherStatus.used;
    const isExpired = lodash.get(item, "status") === voucherStatus.isExpired;
    const isOfferExpired = lodash.get(item, "status") === voucherStatus.isOfferExpired;
    return used || isExpired || isOfferExpired;
  };

  static mapProductClassToCodeType = pClass => {
    switch (pClass) {
      case productClass.voucher:
        return productCodeType.voucher;
      case productClass.prepaidVoucher:
        return productCodeType.prepaidVoucher;
      case productClass.shareToEarn:
        return productCodeType.shareToEarn;
      case productClass.yollaBizShareToEarn:
        return productCodeType.yollaBizShareToEarn;
      case productClass.rewardCode:
        return productCodeType.rewardCode;
      default:
        return "";
    }
  };
}
