import { getClientCas } from "./storage.helper";

export default class AuthHelper {
  static isAuthenticated = async () => {
    const clientCas = await getClientCas();
    return !!clientCas;
  };
}
