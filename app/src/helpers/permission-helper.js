import { Platform, PermissionsAndroid, Linking } from "react-native";
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import AndroidOpenSettings from "react-native-android-open-settings";
import { LATITUDE_DELTA, LONGITUDE_DELTA } from "../constants/app.constant";
import { showAlertConfirm } from "../ultis/arlert";

export const requestCameraPermission = () => {
  return new Promise(async (resolve, reject) => {
    if (Platform.OS === "ios") return resolve();
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "The Boaters App Camera Permission",
          message:
            "The Boaters App needs access to your camera " +
            "so you can take awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        resolve();
      } else {
        reject();
      }
    } catch (err) {
      reject();
    }
  });
};

export const requestStoragePermission = () => {
  return new Promise(async (resolve, reject) => {
    if (Platform.OS === "ios") return resolve();
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        {
          title: "Carebook App Storage Permission",
          message:
            "Carebook App needs access to your storage " +
            "so you can pick awesome pictures."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        resolve();
      } else {
        reject();
      }
    } catch (err) {
      reject();
    }
  });
};

export function checkGPSLocation(onSuccess, onError) {
  if (Platform.OS === "android") {
    LocationServicesDialogBox.checkLocationServicesIsEnabled({
      message:
        "<h2 style='color: #0af13e'>Truy cập vị tí</h2>Bạn chưa bật truy cập vị trí GPS và NETWORK",
      ok: "Vào cài đặt",
      cancel: "Quay lại",
      enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
      showDialog: true, // false => Opens the Location access page directly
      openLocationServices: true, // false => Directly catch method is called if location services are turned off
      preventOutSideTouch: false, // true => To prevent the location services window from closing when it is clicked outside
      preventBackClick: false, // true => To prevent the location services popup from closing when it is clicked back button
      providerListener: false // true ==> Trigger locationProviderStatusChange listener when the location state changes
    })
      .then(function(res) {
        const { enabled } = res;
        if (enabled)
          requestLocationPermission().then(bool => {
            if (bool) {
              onSuccess();
            } else {
              showAlertConfirm(
                {
                  title: "Quyền truy cập vị trí",
                  message: "Vào cài đặt để bật quyền vị trí."
                },
                () => {
                  AndroidOpenSettings.appDetailsSettings();
                }
              );
            }
          });
      })
      .catch(error => {
        onError(error);
      });
  } else {
    // geolocation.requestAuthorization();
    onSuccess();
  }
}

export async function requestLocationPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      // {
      //   title: "Example App",
      //   message: "Example App access to your location "
      // }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      // console.log("You can use the location");
      return true;
    }
    console.log("location permission denied");
    return false;
  } catch (err) {
    console.warn(err);
    return false;
  }
}

const requestAndroidLocationPermissions = (onSuccess, onError) => {
  requestLocationPermission()
    .then(data => {
      onSuccess(data);
    })
    .catch(err => {
      onError(err);
    });
};

function getLocation(onSuccess = () => {}, onError = () => {}) {
  // eslint-disable-next-line no-undef
  navigator.geolocation.getCurrentPosition(
    position => {
      const currentLatitude = position.coords.latitude;
      const currentLongitude = position.coords.longitude;
      const coords = {
        latitude: currentLatitude,
        longitude: currentLongitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      };
      onSuccess(coords);
    },
    error => {
      onError(error);
    },
    { timeout: 20000, maximumAge: 1000 }
  );
}

export function requestPermissionLocation(onSuccess = () => {}, onError = () => {}) {
  if (Platform.OS === "android") {
    requestAndroidLocationPermissions(
      granted => {
        if (granted) {
          getLocation(onSuccess);
        } else {
          onError(granted);
        }
      },
      error => onError(error)
    );
  } else {
    getLocation(onSuccess, onError);
  }
}

export function getCurrentPosition(onNext, onError) {
  requestPermissionLocation(onNext, onError);
}
