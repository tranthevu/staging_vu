export const getIsLoading = state => state.loadingReducer.isLoading;
export const getIsFetching = state => state.loadingReducer.isFetching;
