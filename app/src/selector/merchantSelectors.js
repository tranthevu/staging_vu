export const getListMerchantByAddressSelector = state =>
  state.merchantReducer.listMerchantByAddress;

export const getListTopMerchantByAddressSelector = state =>
  state.merchantReducer.listTopMerchantByAddress;
