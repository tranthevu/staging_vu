export const appConfig = {
  // environment: "development",
  environment: "staging",
  // environment: "production",

  // development API gateway
  // hostUrl: "https://apigateway.yolla.dev/network/v10-dev/",
  // apiUrl: "https://apigateway.yolla.dev/network/v10-dev/api/",
  // ssoUrl: "https://apigateway.yolla.dev/sso-net/v10-dev/api/",
  // ssoService: "https://apigateway.yolla.dev/networkwokey/v10-dev/accounts/login",
  // authorizationHeader: "5d24088161b64be1255a588979615a8f9f8b4740938101e755ca7185",
  // rootUrl: "https://apigateway.yolla.dev",
  // webUrl: "https://yollanetwork.yolla.dev/",
  // mapApiKey: "AIzaSyCch1pwwh4lGThvqdbEL_CUYCL_wT-pkuU",
  // dynamicDomain: "yolladev.page.link",
  // android_package_name: "dev.yolla.yollanetwork",

  // staging API gateway
  hostUrl: "https://apigateway.yolla.dev/network/v10-stg/",
  apiUrl: "https://apigateway.yolla.dev/network/v10-stg/api/",
  ssoUrl: "https://apigateway.yolla.dev/sso-net/v10-stg/api/",
  ssoService: "https://apigateway.yolla.dev/networkwokey/v10-stg/accounts/login",
  authorizationHeader: "5d24088161b64be1255a58890da87433141348d4a60d1261994d7ccf",
  rootUrl: "https://apigateway.yolla.dev",
  webUrl: "https://stgyn.yolla.dev/",
  mapApiKey: "AIzaSyBedes0KJwk2aGHw9CEzMF0YtjZRYaatPI",
  dynamicDomain: "yollastg.page.link",
  android_package_name: "stg.yolla.yollanetwork",

  // production API gateway
  // hostUrl: "https://api.yolla.vn/network/v10-prod/",
  // apiUrl: "https://api.yolla.vn/network/v10-prod/api/",
  // ssoUrl: "https://api.yolla.vn/sso-net/v10_prod/api/",
  // ssoService: "https://api.yolla.vn/networkwokey/v10-prod/accounts/login",
  // authorizationHeader:
  //   "5d78ec4356aabf68e774a894cf2ba67d736c477faaa9aa711c5e0d2e",
  // rootUrl: "https://api.yolla.vn",
  // webUrl: 'https://yolla.vn/',
  // mapApiKey: "AIzaSyAR0Wtz_sa3XHb7vN9mOxi7gL1O2hjM9Og",
  // dynamicDomain: "yolla.page.link",
  // android_package_name: "vn.yolla.yollanetwork"
  // activeApp use to detect Promo, eCoupon, Online Promo, Share to Earn, Loyalty
  activeApp: ""
};

// How to release production
// 1. edit environment config in app.config.js
// 2. edit hostUrl & apiUrl in app.config.js
// 3. replace FacebookAppID FacebookDisplayName
// 4. update GoogleService-Info.plist.prod
// 5. update strings.xml
// 6. update Info.plist
// 7. update android/app/build.gradle
// 8. update google-services.json
// 9. update AndroidManifest.xml
// Dev & Staging Info.plist
/*

// Production
/*
<key>CFBundleURLTypes</key>
<array>
  <dict>
  <key>CFBundleURLSchemes</key>
  <array>
    <string>fb2063673167044039</string>
  </array>
  </dict>
</array>
<key>FacebookAppID</key>
<string>2063673167044039</string>
<key>FacebookDisplayName</key>
<string>Yolla Consumer Network</string>
*/

// Production strings.xml
/*
<resources>
    <string name="app_name">Yolla Network</string>
    <string name="facebook_app_id">2063673167044039</string>
</resources>
*/

// New dev & staging
/*
<key>CFBundleURLTypes</key>
<array>
  <dict>
  <key>CFBundleURLSchemes</key>
  <array>
    <string>fb2761437013897188</string>
  </array>
  </dict>
</array>
<key>FacebookAppID</key>
<string>2761437013897188</string>
<key>FacebookDisplayName</key>
<string>Yolla Consumer Network - Dev</string>
*/
