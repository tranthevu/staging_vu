/**
 * Automatically generated file. DO NOT MODIFY
 */
package stg.yolla.yollanetwork;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "stg.yolla.yollanetwork";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 93;
  public static final String VERSION_NAME = "1.2";
}
